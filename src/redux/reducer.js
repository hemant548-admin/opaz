import AsyncStorage from "@react-native-community/async-storage"
import AppStrings from "../utils/AppStrings"

export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const CHECK_LOGIN = 'CHECK_LOGIN'
export const CHECK_SCREEN = 'CHECK_SCREEN'
export const CHECK_DATA = 'CHECK_DATA'
export const CHECK_ROLE='CHECK_ROLE'
export const CHECK_FORGOT_ROLE='CHECK_FORGOT_ROLE'

export const addItem = (item) => ({
  type: ADD_ITEM,
  payload: item,
  detail:description
})
export const CheckData = (requestData,checkIn,checkOut,extraBed) => ({
  type: CHECK_DATA,
  requestData: requestData,
  checkIn:checkIn,
  checkOut:checkOut,
  extraBed:extraBed
})
export const checkLogin = (Id,isLogin,screen) => ({
  type: CHECK_LOGIN,
  id: Id,
  isLogin:isLogin,
  screen:screen
})
export const checkScreen = (userScreen,token,isToken) => ({
    type: CHECK_SCREEN,
    userScreen:userScreen,
    token:token,
    isToken:isToken
  })
export const checkRole =async (role) => ({
    type: CHECK_ROLE,
    role:role
  })
 export const forgotCheckRole=(forgotRole)=>({
  type: CHECK_FORGOT_ROLE,
  forgotRole:forgotRole
 }) 
const initialState = {
  itemList: [],
  id:'',
  screen:'',
  isLogin:false,
  userScreen:'',
  requestData:{},
  checkIn:'',
  checkOut:'',
  extraBed:'',
  role:"",
  token:"",
  forgotRole:'',
  isToken:false
}
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_LOGIN:
        // We need to return a new state object
      return {
        ...state,
        id:action.id,
        screen:action.screen,
        isLogin:action.isLogin
      }
      break;
    case CHECK_SCREEN:
        // We need to return a new state object
      return {
        ...state,
        userScreen:action.userScreen,
        token:action.token,
        isToken:action.isToken
      }
      case CHECK_ROLE:
        // We need to return a new state object
      return {
        ...state,
        role:action.role,
      }
      case CHECK_DATA:
        // We need to return a new state object
      return {
        ...state,
        requestData:action.requestData,
        checkIn:action.checkIn,
        checkOut:action.checkOut,
        extraBed:action.extraBed,
      }
      case CHECK_FORGOT_ROLE:
        // We need to return a new state object
      return {
        ...state,
        forgotRole:action.forgotRole,
      }
    default:
      return state
  }
}

export default  reducer