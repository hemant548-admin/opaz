import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { SafeAreaView,ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AsyncStorage from '@react-native-community/async-storage';
import { checkLogin, checkScreen } from '../redux/reducer';
import { useDispatch, useSelector } from 'react-redux'
import MainScreen from '../screen/MainScreen';
import HotelRoutes from './HotelRoutes';
import UserRoutes from './UserRoutes';
import AppStrings from '../utils/AppStrings';
import AuthLoadingScreen from './AuthLoadingScreen';
import AppColor from '../utils/AppColor';

const AuthStack = createStackNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


export const MainNavigator = () => {
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName='Main'>
      <AuthStack.Screen name='Main' component={MainScreen} />
      <AuthStack.Screen name='HotelRoute' component={Hotel} />
      <AuthStack.Screen name='UserRoute' component={User} />
    </AuthStack.Navigator>
  )
}

const Hotel = () => {
  return (
    <AuthStack.Navigator headerMode="none" >
      <AuthStack.Screen name='Hotel' component={HotelRoutes} />
    </AuthStack.Navigator>
  )
}
const User = () => {
  return (
    <AuthStack.Navigator headerMode="none" >
      <AuthStack.Screen name='User' component={UserRoutes} />
    </AuthStack.Navigator>
  )
}


const MainRoutes = () => {
  const [role, setRole] = useState('')
  const [isLoading, setIsLoading] = useState(true);
  //const rol=useSelector(state=>state.role)

  
  useEffect(async () => {
    const rol = await AsyncStorage.getItem(AppStrings.ROLE)
    setRole(rol)
    setIsLoading(false);
    
    // setTimeout(()=>{

    // },1000)
    // switch(rol){
    //   case null:
    //     setComponet(MainNavigator);
    //     break;
    //     case 'user':
    //       setComponet(User);
    //       break;
    //       case 'hotel':
    //         setComponet(Hotel);
    //         break;


    // }
  }, []);
  if (isLoading) {
    return (
      <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color={AppColor.appColor} />
      </SafeAreaView>
    );
  }
  return (
    <NavigationContainer>

      {
        role === 'EndUser' ?
          <User /> :
          role!==null ?
          <Hotel />
          :
          <MainNavigator />
      }
    </NavigationContainer>
  );
}


export default MainRoutes;