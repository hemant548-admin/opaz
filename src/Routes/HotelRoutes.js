import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import { Image,SafeAreaView,ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import AppColor from '../utils/AppColor';
//import MainScreen from '../screen/MainScreen';
//import DrawerContent from '../screen/DrawerContent';
//import ProfileScreen from '../screen/ProfileScreen';
import HotelIntroScreen from '../screen/hotelscreen/hotel_introscreen/HotelIntroScreen';
import ProfileLoginScreen from '../screen/hotelscreen/profilescreen/ProfileLoginScreen';
import SignUpScreen from '../screen/hotelscreen/profilescreen/hotel/signup/SignUpScreen';
import OtpScreen from '../screen/hotelscreen/profilescreen/hotel/signup/OtpScreen';
import HotelRegister from '../screen/hotelscreen/profilescreen/hotel/signup/HotelRegister';
import CreatePassword from '../screen/hotelscreen/profilescreen/hotel/signup/CreatePassword';
import LoginScreen from '../screen/hotelscreen/profilescreen/hotel/login/LoginScreen';
import ChangePassword1 from '../screen/hotelscreen/profilescreen/hotel/login/ChangePassword1';
import ChangePassword2 from '../screen/hotelscreen/profilescreen/hotel/login/ChangePassword2';
import SuccessScreen from '../screen/hotelscreen/profilescreen/hotel/login/SuccessScreen';
import Hotelbg1 from '../screen/hotelscreen/profilescreen/hotel/login/Hotelbg1';
import Hotelbg2 from '../screen/hotelscreen/profilescreen/hotel/login/Hotelbg2';
import HomeScreen from '../screen/hotelscreen/profilescreen/hotel/home/HomeScreen';
import ProfileScreen from '../screen/hotelscreen/profilescreen/hotel/profile/ProfileScreen';
import DrawerContent from '../component/drawer/DrawerContent';
import PolicyScreen from '../screen/hotelscreen/profilescreen/hotel/policy/PolicyScreen';
import BranchManagerHome from '../screen/hotelscreen/profilescreen/BranchManager/BranchManagerHome';
import FrontdeskHome from '../screen/hotelscreen/profilescreen/frontdesk/FrontdeskHome';
import AppStrings from '../utils/AppStrings';
import AppImage from '../utils/AppImage';
import BranchList from '../screen/hotelscreen/profilescreen/hotel/branch/BranchList';
import HelpScreen from '../component/drawer/HelpScreen';
import AddBranch from '../screen/hotelscreen/profilescreen/hotel/branch/AddBranch';
import MapScreen from '../screen/hotelscreen/profilescreen/hotel/branch/MapScreen';
import RoomView from '../screen/hotelscreen/profilescreen/frontdesk/RoomView';
import UserView from '../screen/hotelscreen/profilescreen/frontdesk/UserView';
import ForgotPasswordOTp from '../screen/hotelscreen/profilescreen/hotel/login/ForgotPasswordOTP';
import SelectCountryCodeScreen from '../component/country/SelectCountryCodeScreen';
import UserScreen from '../screen/hotelscreen/profilescreen/BranchManager/UserScreen';
import TaskScreen from '../screen/hotelscreen/profilescreen/BranchManager/TaskScreen';
import EmployeeScreen from '../screen/hotelscreen/profilescreen/BranchManager/EmployeeScreen';
import ReportScreen from '../screen/hotelscreen/profilescreen/BranchManager/ReportScreen';
import EmployeeHome from '../screen/hotelscreen/profilescreen/employee/EmployeeHome';
import Task from '../screen/hotelscreen/profilescreen/employee/Task';
import EmployeeProfile from '../screen/hotelscreen/profilescreen/employee/EmployeeProfile';
import Department from '../screen/hotelscreen/profilescreen/employee/Department';
import Subscription from '../screen/hotelscreen/profilescreen/hotel/subscription/Subscription';
import SubscriptionDetail from '../screen/hotelscreen/profilescreen/hotel/subscription/SubscriptionDetail';
import PaymentScreen from '../screen/hotelscreen/profilescreen/hotel/subscription/PaymentScreen';
import BranchDetails from '../screen/hotelscreen/profilescreen/hotel/branch/BranchDetails';
import { MainNavigator } from './MainRoutes';
import EmpChangePassword1 from '../screen/hotelscreen/profilescreen/employee/EmpChangePassword1';
import EmpChangePassword2 from '../screen/hotelscreen/profilescreen/employee/EmpChangePassword2';
import { AuthContext } from './AuthContext';
import { useSelector } from 'react-redux';
const AuthStack = createStackNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();


const Navigator = () => {
  const screen = useSelector(state => state.userScreen)
  //console.log('sccc ', screen)
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName={screen}>
      <AuthStack.Screen name='Hotel' component={HotelIntroScreen} />
      <AuthStack.Screen name='SignUp' component={SignUpScreen} />
      <AuthStack.Screen name='AllProfile' component={ProfileLoginScreen} />
      {/* <AuthStack.Screen name='AllProfile' component={SignUpScreen1} /> */}
      <AuthStack.Screen name='Otp' component={OtpScreen} />
      <AuthStack.Screen name='Register' component={HotelRegister} />
      <AuthStack.Screen name='CreatePassword' component={CreatePassword} />
      <AuthStack.Screen name='Login' component={LoginScreen} />
      <AuthStack.Screen name='ChangedPassword1' component={ChangePassword1} />
      <AuthStack.Screen name='ChangedPassword2' component={ChangePassword2} />
      <AuthStack.Screen name='Success' component={SuccessScreen} />
      <AuthStack.Screen name='hotelbg1' component={Hotelbg1} />
      <AuthStack.Screen name='hotelbg2' component={Hotelbg2} />
      <AuthStack.Screen name='Main' component={MainNavigator} /> 
      {/* <Stack.Screen name='Policy' component={PolicyScreen} />
      <AuthStack.Screen name='home' component={DrawerScreen} />
      <AuthStack.Screen name='Manager' component={ManagerHome} />
      <AuthStack.Screen name='Front' component={FrontHome} />
      <AuthStack.Screen name='EmployeeHome' component={Employee} />
      <Stack.Screen name='AddBranch' component={AddBranch} />
      <Stack.Screen name='Subscription' component={Subscription} />
      {/* <AuthStack.Screen name='Login' component={SignUpScreen1} /> */}
      {/* <AuthStack.Screen name='Help' component={HelpScreen} />
      <AuthStack.Screen name='Map' component={MapScreen} />  */}
      <AuthStack.Screen name='CountrySelect' component={SelectCountryCodeScreen} />
      <AuthStack.Screen name='ForgotPasswordOTP' component={ForgotPasswordOTp} />
    </AuthStack.Navigator>
  )
}
const SignUpScreen1 = () => {
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName='PLogin'>
      <AuthStack.Screen name='AllProfile' component={ProfileLoginScreen} />
      <AuthStack.Screen name='Login' component={LoginScreen} />
      <AuthStack.Screen name='home' component={DrawerScreen} />
      <AuthStack.Screen name='Manager' component={ManagerHome} />
      <AuthStack.Screen name='Front' component={FrontHome} />
      <AuthStack.Screen name='EmployeeHome' component={Employee} />
    </AuthStack.Navigator>
  )
}
const HotelHome = () => {
  return (
    <Stack.Navigator headerMode="none" initialRouteName='HomeScreen'>
      <Stack.Screen name='HomeScreen' component={HomeScreen} />
      <Stack.Screen name='Policy' component={PolicyScreen} />
      <Stack.Screen name='BranchList' component={BranchList} />
      <Stack.Screen name='Profile' component={ProfileScreen} />
      <Stack.Screen name='AddBranch' component={AddBranch} />
      <Stack.Screen name='Subscription' component={Subscription} />
      <Stack.Screen name='SubscriptionDetail' component={SubscriptionDetail} />
      <Stack.Screen name='Payment' component={PaymentScreen} />
      <Stack.Screen name='BranchDetail' component={BranchDetails} />
      <Stack.Screen name='Main' component={MainNavigator} />
      <Stack.Screen name='Map' component={MapScreen} />
    </Stack.Navigator>
  )
}
const Employee = () => {
  return (
    <Stack.Navigator headerMode="none" initialRouteName='Employee'>
      <Stack.Screen name='Employee' component={EmployeeHome} />
      <Stack.Screen name='Task' component={Task} />
      <Stack.Screen name='EmployeeProfile' component={EmployeeProfile} />
      {/* <Stack.Screen name='Main' component={MainNavigator} /> */}
      <Stack.Screen name='Department' component={Department} />
      <Stack.Screen name='EmpChangePassword1' component={EmpChangePassword1} />
      <Stack.Screen name='EmpChangePassword2' component={EmpChangePassword2} />
    </Stack.Navigator>
  )
}
const ManagerHome = () => {
  return (
    <Drawer.Navigator
      drawerType="front"
      drawerContent={props => <DrawerContent {...props} />}
      drawerContentOptions={{
        activeTintColor: '#e91e63',
      }}>
      <Drawer.Screen name='BranchManagerHome' component={BranchManagerHome} />
      <Drawer.Screen name='UserScreen' component={UserScreen} />
      <Drawer.Screen name='TaskScreen' component={TaskScreen} />
      <Drawer.Screen name='EmployeeScreen' component={EmployeeScreen} />
      <Drawer.Screen name='ReportScreen' component={ReportScreen} />
      <Drawer.Screen name='Help' component={HelpScreen} />
      {/* <Stack.Screen name='Main' component={MainNavigator} /> */}
      {/* <Drawer.Screen name='AllProfile' component={SignUpScreen1} /> */}
    </Drawer.Navigator>
    // <Stack.Navigator headerMode="none" initialRouteName='BranchManagerHome'>
    //   <Stack.Screen name='BranchManagerHome' component={BranchManagerHome} />
    //   <Stack.Screen name='UserScreen' component={UserScreen} />
    //   <Stack.Screen name='TaskScreen' component={TaskScreen} />
    //   <Stack.Screen name='EmployeeScreen' component={EmployeeScreen} />
    //   <Stack.Screen name='ReportScreen' component={ReportScreen} />
    // </Stack.Navigator>
  )
}
const FrontHome = () => {
  return (
    <Drawer.Navigator
      drawerType="front"
      drawerContent={props => <DrawerContent {...props} />}
      drawerContentOptions={{
        activeTintColor: '#e91e63',
      }}>
      <Drawer.Screen name='FrontDesk' component={FrontdeskHome} />
      <Drawer.Screen name='RoomView' component={RoomView} />
      <Drawer.Screen name='UserView' component={UserView} />
      <Drawer.Screen name='Help' component={HelpScreen} />
      {/* <Stack.Screen name='Main' component={MainNavigator} /> */}
      {/* <Drawer.Screen name='AllProfile' component={SignUpScreen1} /> */}
    </Drawer.Navigator>
  )
}


const DrawerScreen = () => {
  return (
    <Drawer.Navigator
      drawerType="front"
      drawerContent={props => <DrawerContent {...props} />}
      drawerContentOptions={{
        activeTintColor: '#e91e63',
      }}>
      <Drawer.Screen name='Homedrawer' component={HotelHome} />
      {/* <Drawer.Screen name='AllProfile' component={SignUpScreen1} /> */}
      <Drawer.Screen name='Help' component={HelpScreen} />
      <Drawer.Screen name='Login' component={LoginScreen} />
    </Drawer.Navigator>
  )
}

const HotelRoutes = () => {
  // const [role, setRole] = useState('')
  // useEffect(async () => {
  //   const rol = await AsyncStorage.getItem(AppStrings.ROLE)
  //   setRole(rol)
  // }, []);
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
            user: null
          };
        case 'SIGN_SKIP':
          return {
            ...prevState,
            isSignout: false,
            userToken: null,
            user: 'Guest'
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      user: null
    }
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem(AppStrings.ROLE);
      } catch (e) {
        // Restoring token failed
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signInHotel: async data => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: data });
      },
      signOutHotel: () => dispatch({ type: 'SIGN_OUT' }),
      signUpHotel: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signSkipHotel: () => dispatch({ type: 'SIGN_SKIP' }),
    }),
    []
  );


  if (state.isLoading) {
    return (
      <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color={AppColor.appColor} />
      </SafeAreaView>
    );
  }
  const renderFunction=()=>{

  switch (state.userToken) {
    case 'BranchManager':
      return (

        <ManagerHome />
      );
      break;
    case 'FrontDesk':
      return (

        <FrontHome />
      );
      break;
    case 'Hotel':
      return (

        <DrawerScreen />
      );
      break;
    case 'Employee':
      return (

        <Employee />
      );
      break;
    default:
      return (
        <Navigator />
      );
      
  }
}
  return (
    <AuthContext.Provider value={authContext}>
      {renderFunction()}
    </AuthContext.Provider>

  );

}


export default HotelRoutes;