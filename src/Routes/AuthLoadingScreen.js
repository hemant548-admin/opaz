import React, { useEffect,useState} from 'react';
import {
  ActivityIndicator,
  StatusBar,
  View, Platform, AppState
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import { EventRegister } from 'react-native-event-listeners';
import AppStrings from '../utils/AppStrings';
import { useDispatch, useSelector } from 'react-redux'
import { checkRole, checkScreen } from '../redux/reducer';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";


const AuthLoadingScreen =()=> {
    const dispatch=useDispatch();
    const [loader,setLoader]=useState(true)

 useEffect(()=> {
    
    _bootstrapAsync();
    //initNotification()
  },[])

  // Fetch the token from storage then navigate to our appropriate place
  const _bootstrapAsync = async () => {
    try {
      const userToken = await AsyncStorage.getItem(AppStrings.ROLE);
      console.log('userToken: ', userToken);
     // dispatch(checkRole(userToken));
     setLoader(false)
      if (userToken === 'EndUser') {

        dispatch(checkScreen('drawer'));
        
  
      }
    } catch (e) {
        setLoader(false)
     // this.props.navigation.navigate('Auth');
    }
  };
//   const initNotification= async ()=>{
//   PushNotification.configure({
//     // (optional) Called when Token is generated (iOS and Android)
//     onRegister: async function (token) {
//       console.log("TOKEN:", token);
//       await AsyncStorage.setItem(AppStrings.DEVICE_TOKEN, token.token + "")
//     },

//     // (required) Called when a remote or local notification is opened or received
//     onNotification: function (notification) {
//       console.log("NOTIFICATION:", notification);

//       // process the notification here

//       // required on iOS only 
//       if (Platform.OS === 'ios') {
//         notification.finish(PushNotificationIOS.FetchResult.NoData);
//         //this.updateCount(notification);
//       }
//       handleNotification(notification);
//     },
//     // Android only
//     senderID: "303273891964",
//     // iOS only
//     permissions: {
//       alert: true,
//       badge: true,
//       sound: true
//     },
//     popInitialNotification: true,
//     requestPermissions: true,
//     contentAvailable: true
//   });

// }
 
    return (
      <View>
    {loader &&
        <ActivityIndicator size={'large'}
        animating={loader}
        />
          }
        {/* <StatusBar barStyle="light-content" /> */}
      </View>
    );
  }
export default AuthLoadingScreen;