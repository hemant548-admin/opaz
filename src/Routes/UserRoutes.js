import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Image, View, Text, SafeAreaView, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector, useDispatch } from 'react-redux'

//import MainScreen from '../screen/MainScreen';
//import DrawerContent from '../screen/DrawerContent';
//import ProfileScreen from '../screen/ProfileScreen';
import UserIntroScreen from '../screen/userscreen/user_introscreen/UserIntroScreen';
import SignUpScreen from '../screen/userscreen/signup/SignUpScreen';
import SignUpScreen1 from '../screen/userscreen/signup/SignUpScreen1';
import OTPScreen from '../screen/userscreen/signup/OTPScreen';
import PasswordScreen from '../screen/userscreen/signup/PasswordScreen';
import SuccessScreen from '../screen/userscreen/signup/SuccessScreen';
import LoginScreen from '../screen/userscreen/login/LoginScreen';
import Hotelbg from '../screen/userscreen/signup/Hotelbg';
import ForgotPassword from '../screen/userscreen/login/ForgotPassword';
import ForgotOTPScreen from '../screen/userscreen/login/ForgotOTPScreen';
import Success1 from '../screen/userscreen/login/Success1';
import ForgotCreatePassword from '../screen/userscreen/login/ForgotCreatePassword';
import HomeScreen from '../screen/userscreen/Home/HomeScreen';
import Profile from '../screen/userscreen/profile/Profile';
import Service from '../screen/userscreen/service/Service';
import Mybooking from '../screen/userscreen/mybooking/Mybooking';
import AppImage from '../utils/AppImage';
import DrawerComponent from '../screen/userscreen/Drawer/DrawerComponent';
import Help from '../screen/userscreen/Drawer/Help';
import Policy from '../screen/userscreen/Drawer/Policy';
import SearchScreen from '../screen/userscreen/search/SearchScreen';
import DetailScreen from '../screen/userscreen/search/DetailScreen';
import SelectCountryCodeScreen from '../component/country/SelectCountryCodeScreen';
import CalendarScren from '../screen/userscreen/search/CalendarScreen';
import ChooseRoom from '../screen/userscreen/search/ChooseRoom';
import TravellerProfile from '../screen/userscreen/search/TravellerProfile';
import UserScreen from '../screen/hotelscreen/profilescreen/BranchManager/UserScreen';
import BookingDetail from '../screen/userscreen/mybooking/BookingDetail';
import CancelPolicy from '../screen/userscreen/mybooking/CancelPolicy';
import Mybill from '../screen/userscreen/service/Mybill';
import ViewRequest from '../screen/userscreen/service/ViewRequest';
import Calender from '../screen/userscreen/Home/Calender';
import SelectLocation from '../screen/userscreen/search/SelectLocation';
import MainRoutes, { MainNavigator } from './MainRoutes';
import UserChangePassword1 from '../screen/userscreen/profile/UserChangePassword1';
import UserChangePassword2 from '../screen/userscreen/profile/UserChangePassword2';
import RoomDetails from '../screen/userscreen/search/RoomDetails';
import ImageView from '../screen/userscreen/search/ImageViews';
import ImageViews from '../screen/userscreen/search/ImageViews';
import MainScreen from '../screen/MainScreen';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../utils/AppStrings';
import AppColor from '../utils/AppColor';
import { AuthContext } from './AuthContext';



const AuthStack = createStackNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const Navigator = () => {
  const screen = useSelector(state => state.userScreen)
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName={screen}>
      <AuthStack.Screen name='drawer' component={DrawerScreen} />
      <AuthStack.Screen name='min' component={MainNavigator} />
      <AuthStack.Screen name='CountrySelect' component={SelectCountryCodeScreen} />
      <AuthStack.Screen name='ChooseRoom' component={ChooseRoom} />
    </AuthStack.Navigator>
  )
}

const AppLogin = () => {
  const screen = useSelector(state => state.userScreen)
  return (
    <AuthStack.Navigator headerMode="none" initialRouteName={screen}>
      <AuthStack.Screen name='User' component={UserIntroScreen} />
      <AuthStack.Screen name='Login' component={LoginScreen} />
      <AuthStack.Screen name='SignUp' component={SignUpScreen} />
      <AuthStack.Screen name='SignUp1' component={SignUpScreen1} />
      <AuthStack.Screen name='OTP' component={OTPScreen} />
      <AuthStack.Screen name='Password' component={PasswordScreen} />
      <AuthStack.Screen name='success' component={SuccessScreen} />
      <AuthStack.Screen name='min' component={MainNavigator} />
      <AuthStack.Screen name='Hotelbg' component={Hotelbg} />
      <AuthStack.Screen name='Forgot' component={ForgotPassword} />
      <AuthStack.Screen name='ForgotOTP' component={ForgotOTPScreen} />
      <AuthStack.Screen name='CreateForgot' component={ForgotCreatePassword} />
      <AuthStack.Screen name='Success1' component={Success1} />
      <AuthStack.Screen name='CountrySelect' component={SelectCountryCodeScreen} />
    </AuthStack.Navigator>
  )
}

const TabScreen = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, }) => {
          let iconName;
          let size; let title;
          switch (route.name) {
            case 'Home':
              iconName = focused
                ? AppImage.home_active
                : AppImage.home_inactive;
              size = focused ? styles.active : styles.inactive
              title = 'Home'
              break;
            case 'Profile':
              iconName = focused
                ? AppImage.profile_active
                : AppImage.branchName_icon;
              size = focused ? styles.active : styles.pf
              title = 'Profile'
              break;
            case 'Services':
              iconName = focused
                ? AppImage.service_active
                : AppImage.service_inactive;
              size = focused ? styles.active : styles.inactive
              title = 'Service'
              break;
            case 'My Booking':
              iconName = focused
                ? AppImage.favourite_active
                : AppImage.favourite_inactive;
              size = focused ? styles.active : styles.inactive
              title = 'My Booking'
              break;
          }


          // You can return any component that you like here!
          return <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Image source={iconName} style={size} />
            {!focused &&
              <Text style={{ textAlign: 'center', color: '#003727' }}>{title}</Text>
            }
          </View>;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        style: { height: 70 },
        showLabel: false
      }}
    >
      <Tab.Screen name="Home" component={HomeScreen}
        options={{ showlabel: false }}
      />
      <Tab.Screen name="Services" component={Service} />
      <Tab.Screen name="My Booking" component={Mybooking} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  )
}
const DrawerScreen = () => {
  const screen = useSelector(state => state.screen)
  return (
    <Drawer.Navigator
      drawerType="front"
      initialRouteName={screen}
      drawerContent={props => <DrawerComponent {...props} />}
      drawerContentOptions={{
        activeTintColor: '#e91e63',
      }}>
      <Drawer.Screen name='Tab' component={TabScreen} />
      <Drawer.Screen name='Help' component={Help} />
      <Drawer.Screen name='Search' component={SearchScreen} />
      <Drawer.Screen name='SelectLocation' component={SelectLocation} />
      <Drawer.Screen name='Detail' component={DetailScreen} />
      <Drawer.Screen name='Policy' component={Policy} />
      <Drawer.Screen name='Calendar' component={CalendarScren} />
      <Drawer.Screen name='Login' component={Navigator} />
      <Drawer.Screen name='ChooseRoom' component={ChooseRoom} />
      <Drawer.Screen name='Traveller' component={TravellerProfile} />
      <Drawer.Screen name='BookingDetail' component={BookingDetail} />
      <Drawer.Screen name='CancelPolicy' component={CancelPolicy} />
      <Drawer.Screen name='Mybill' component={Mybill} />
      <Drawer.Screen name='ViewRequest' component={ViewRequest} />
      <Drawer.Screen name='Calender' component={Calender} />
      <Drawer.Screen name='RoomDetail' component={RoomDetails} />
      <Drawer.Screen name='ImageView' component={ImageViews} />
      {/* <Drawer.Screen name='Main' component={MainNavigator} /> */}
      <Drawer.Screen name='UserChangePassword1' component={UserChangePassword1} />
      <Drawer.Screen name='UserChangePassword2' component={UserChangePassword2} />
    </Drawer.Navigator>
  )
}
// const DrawerScreen=()=>{
//     return(
//         <Drawer.Navigator
//         drawerType="front"
//         drawerContent={props => <DrawerContent {...props} />}
//         drawerContentOptions={{
//           activeTintColor: '#e91e63',
//         }}>
//             <Drawer.Screen name='Home' component={MainNavigator}/>
//             <Drawer.Screen name='Profile' component={ProfileScreen}/>
//         </Drawer.Navigator>
//     )
// }

const UserRoutes = () => {

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
            user: null
          };
        case 'SIGN_SKIP':
          return {
            ...prevState,
            isSignout: false,
            userToken: null,
            user: 'Guest'
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      user: null
    }
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem(AppStrings.ROLE);
      } catch (e) {
        // Restoring token failed
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (user, password) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `SecureStore`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signSkip: () => dispatch({ type: 'SIGN_SKIP' }),
    }),
    []
  );


  if (state.isLoading) {
    return (
      <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color={AppColor.appColor} />
      </SafeAreaView>
    );
  }

  return (
    <AuthContext.Provider value={authContext}>
      {state.userToken || state.user === 'Guest' ?
        <Navigator /> : <AppLogin />
      }
    </AuthContext.Provider>

  );
}
const styles = StyleSheet.create({
  active: {
    width: 55,
    height: 55,
    //marginBottom:5
  },
  inactive: {
    width: 24,
    height: 24,
  },
  pf: {
    width: 24,
    height: 24, resizeMode: 'contain'
  }
})

export default UserRoutes;