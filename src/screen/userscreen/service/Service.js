import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList, BackHandler,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground, Pressable, StyleSheet
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker'
import Header from '../../../component/header/Header';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import moment from 'moment';
import AppImage from '../../../utils/AppImage';
import Modal from 'react-native-modal';
import { Picker } from '@react-native-community/picker';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import AppStyles from '../../../utils/AppStyles';
import Loader from '../../../component/loader/Loader';
import AppStrings from '../../../utils/AppStrings';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');

const arr = [{ id: '1', department_name: 'Front Desk' }]

const Service = ({ navigation }) => {
    const [show, setShow] = useState(false);
    const [time, setTime] = useState(new Date());
    const [id, setId] = useState(-1);
    const [tick, setTick] = useState(null)
    const [name, setName] = useState('Front Desk')
    const [cab, setCab] = useState('')
    const [userId, setUserId] = useState('');
    const [password, setPassword] = useState('');
    const [comment, setComment] = useState('');
    const [error, setError] = useState('');
    const [invalid, setInvalid] = useState([false, false, false]);
    const [loader, setLoader] = useState(false);
    const [serviceUser, setServiceUser] = useState({});
    const [serviceList, setServicList] = useState([]);
    const [services, setServices] = useState([]);
    const [servicesId, setServicesID] = useState([]);
    const [departmentName, setDepartmentName] = useState(arr);
    const [isModalVisible, setModalVisible] = useState(false);
    const [hide, setHide]=useState(false)

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            checkAuth();
        });
        BackHandler.addEventListener(
            "hardwareBackPress",
            goBack
        );

        return () => {
            BackHandler.removeEventListener("hardwareBackPress", goBack);

            unsubscribe;
        }


    }, [navigation])

    const goBack = () => {
        setModalVisible(false)
        navigation.goBack()
        return true
    }
    const checkAuth = async () => {
        const id = await AsyncStorage.getItem(AppStrings.SERVICE_USERID);
        const branch_id = await AsyncStorage.getItem(AppStrings.BRANCH_ID, '')
        if (id !== null) {
            setModalVisible(false)
            departmentList(branch_id);
        }
        else {
            setModalVisible(true)
        }
    }

    const onSubmit = () => {
        if (userId.trim().length === 0) {
            setError('Enter your ID')
            setInvalid([true, false, false])
            return
        }
        if (password.trim().length === 0) {
            setError('Enter Password')
            setInvalid([false, true, false])
            return
        }
        checkApi();
    }
    const checkApi = () => {
        let requsetBody = JSON.stringify({
            mobile: userId,
            password: password,
        })
        console.log(requsetBody);
        console.log(APIStrings.EndUserServiceLoginView);
        simplePostCall(APIStrings.EndUserServiceLoginView, requsetBody)
            .then((data) => {

                setLoader(false)

                //console.log(data.token);
                if (data.status.code === 200) {
                    setModalVisible(false)
                    setServiceUser(data.output)
                    departmentList(data.output.branch.id)
                    parseData(data.output);
                    console.log("success", JSON.stringify(data));


                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)
                    setError('Invalid user ID or password')
                    setInvalid([false, false, true])

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const parseData = async (data) => {

        try {
            await AsyncStorage.setItem(AppStrings.SERVICE_USERID, data.user.id + '');
            await AsyncStorage.setItem(AppStrings.BRANCH_ID, data.branch.id + '');
            await AsyncStorage.setItem(AppStrings.ROOM_ID, data.room.id + '');
        }
        catch (e) {
            console.log(e)
            setLoader(false)
        }
    }
    const departmentList = (id) => {

        let requsetBody = JSON.stringify({
            branch_id: id
        })
        setLoader(true)
        console.log(requsetBody);
        console.log(APIStrings.EndUserServiceDepartmentList);
        simplePostCall(APIStrings.EndUserServiceDepartmentList, requsetBody)
            .then((data) => {

                setLoader(false)

                //console.log(data.token);
                if (data.status.code === 200) {
                    //setModalVisible(false)
                    setServicList(data.list_of_department)
                    servicesList(data.list_of_department[0].id)
                    setId(0)
                    //setDepartmentName([...departmentName,data.output.department_name])
                    console.log("success", JSON.stringify(data));
                    //console.log()
                    console.log([...departmentName, data.list_of_department]);

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)


                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const servicesList = (id) => {

        let requsetBody = JSON.stringify({
            department_id: id
        })
        setLoader(true)
        console.log(requsetBody);
        console.log(APIStrings.ListOfServicesForEndUser);
        simplePostCall(APIStrings.ListOfServicesForEndUser, requsetBody)
            .then((data) => {

                setLoader(false)

                //console.log(data.token);
                if (data.status.code === 200) {
                    //setModalVisible(false)
                    setServices(data.list_of_services)
                    //setDepartmentName([...departmentName,data.output.department_name])
                    console.log("success", JSON.stringify(data));
                    //console.log()
                    //console.log([...departmentName, data.list_of_department]);

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)


                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const SubmitResponse = async (id) => {
            if(servicesId.length===0){
                alert('Please select order')
                return;
            }
            if(time<=moment()){
                alert('Please select Valid time')
                return;
            }
        const branch_id = await AsyncStorage.getItem(AppStrings.BRANCH_ID, '')
        const room_id = await AsyncStorage.getItem(AppStrings.ROOM_ID, '')
        const user_id = await AsyncStorage.getItem(AppStrings.SERVICE_USERID, '')

        let requsetBody = JSON.stringify({
            branch_id: branch_id,
            user_id: user_id,
            room_id: room_id,
            service_time: time,
            comment: comment,
            service_id_list: servicesId
        })
        setLoader(true)
        console.log(requsetBody);
        console.log(APIStrings.GetServicesByEndUser);
        simplePostCall(APIStrings.GetServicesByEndUser, requsetBody)
            .then((data) => {

                setLoader(false)

                //console.log(data.token);
                if (data.status.code === 200) {
                    //setModalVisible(false)
                    //setServicList(data.list_of_department)
                    //setDepartmentName([...departmentName,data.output.department_name])
                    console.log("success", JSON.stringify(data));
                    Toast.show('Order Successfully placed !');
                    navigation.navigate('Mybill')
                    //console.log([...departmentName, data.list_of_department]);

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)


                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || time;
        //setShow(Platform.OS === 'ios');
        setShow(false)
        setTime(currentDate);

    };

    const showDatepicker = () => {
        setShow(true);
    };
    const setButton = (val, name, id) => {
        if (val < 0) {
            setId(val)
            setName(name)
        }
        else {
            setId(val)
            setName(name)
            servicesList(id);

            //setServices(serviceList[val].services)
        }


    }
    const onTick = (id) => {
        //setTick(!tick)
        let service = [...services];
        for (let data of service) {
            if (data.id == id) {
                data.selected = (data.selected == null) ? true : !data.selected;
                break;
            }
        }
        let temp;

        setServices(service)
        setTick(service)
        const res = service.filter(item => item.selected === true);
        //setServicesID()
        temp = res.map(item => {
            return item.id
        })
        console.log("res", res);
        console.log(temp)
        setServicesID(temp);
        // this.setState({renderData});
    }
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const renderModal = () => {
        return (
            <View >
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => { navigation.goBack(), setModalVisible(false) }}
                //deviceWidth={width}
                //deviceHeight={height*0.8}
                //style={{ marginLeft: 0,marginRight:0,marginTop:40,marginBottom:40 }}
                >

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View style={{
                            width: width, height: height * 0.7, backgroundColor: AppColor.appColor, alignSelf: 'center',
                            alignItems: 'center'
                        }}>
                            <Text style={{ fontSize: 22, color: 'white', marginTop: 40, fontFamily: AppFonts.medium }}>Enter Your ID and PASSWORD</Text>
                            <Text style={{
                                fontSize: 18, color: 'white', textAlign: 'center',
                                marginLeft: 30, marginRight: 30, marginTop: 20, fontFamily: AppFonts.light
                            }}>
                                If you don't have the credentials, Kindly connect with frontdesk to
                                get user ID and PASSWORD to avail services.
                        </Text>
                            <TextInput
                                style={{
                                    width: '92%', height: 50, backgroundColor: 'white', elevation: 5,
                                    borderRadius: 10, borderWidth: 1, paddingLeft: 20, fontSize: 18, marginTop: 60
                                }}
                                placeholder={'Enter your ID'}
                                value={userId}
                                onChangeText={(val) => { setUserId(val), setInvalid(!invalid[0]) }}
                            />
                            {invalid[0] && <Text style={{ color: 'darkred', alignSelf: 'flex-end', marginRight: 20, marginTop: 5 }}>{error}</Text>}
                            <View style={{flexDirection:'row',alignItems:'center',width: '92%', height: 50,
                            backgroundColor: 'white',marginTop: 15, elevation: 5,borderRadius: 10, borderWidth: 1,}}>
                            <TextInput
                                style={{
                                    width: '92%', height: 50, 
                                     paddingLeft: 20, fontSize: 18, borderRadius:10
                                }}
                                placeholder={'Enter your Password'}
                                value={password}
                                secureTextEntry={!hide}
                                onChangeText={(val) => { setPassword(val), setInvalid(!invalid[1]) }}
                            />
                            <TouchableOpacity style={{
                                            alignItems: 'center',right:10
                                        }} onPress={() => setHide(!hide)}>
                                            <Image source={!hide ? AppImage.show : AppImage.hide}
                                                style={{ width: 25, height: 25 }}
                                            />
                                        </TouchableOpacity>
                                        </View>   
                            {invalid[1] && <Text style={{ color: 'darkred', alignSelf: 'flex-end', marginRight: 20, marginTop: 5 }}>{error}</Text>}
                            {invalid[2] && <Text style={{ color: 'darkred', alignSelf: 'flex-end', marginRight: 20, marginTop: 5 }}>{error}</Text>}
                            <Pressable style={{
                                width: '40%', backgroundColor: '#003727', alignSelf: 'center',
                                alignItems: 'center', borderRadius: 15, marginTop: 30
                            }} onPress={() => onSubmit()}>
                                <Text style={{ color: 'white', margin: 10, fontSize: 18 }}>Submit</Text>
                            </Pressable>
                            <Pressable style={{ alignSelf: 'flex-start', flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 10 }}
                                onPress={() => { navigation.goBack(), setModalVisible(false) }}
                            >
                                <Image source={AppImage.back_arrow_white}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                />
                                <Text style={{ color: 'white', fontSize: 18, marginLeft: 20 }}>Back</Text>
                            </Pressable>
                        </View>
                    </View>

                </Modal>
            </View>
        );
    }
    const renderCheckBox = (item, index) => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                    <TouchableOpacity style={styles.box} onPress={() => onTick(item.id)}>
                        {item.selected &&
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />}
                    </TouchableOpacity>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ color: AppColor.appColor, fontSize: 16, fontFamily: AppFonts.medium }}>{item.service_name}</Text>
                        <Text style={{ fontSize: 16, fontFamily: AppFonts.medium }}>Rs. {item.service_price}</Text>
                    </View>
                </View>
                <View style={{ width: 50, height: 50, backgroundColor: AppColor.appColor, justifyContent: 'center', alignItems: 'center', marginRight: 15, borderRadius: 7 }}>
                    <Image source={{ uri: item.service_photo }}
                        style={{ width: 30, height: 30, resizeMode: 'contain', }}
                    />
                </View>
            </View>
        )
    }
    const renderHouse = () => {
        return (
            <View style={{ alignItems: 'center', marginTop: 10 }}>
                <Text style={{ fontSize: 20, color: '#003727' }}>What you want us to do for you today ?</Text>
                <Text style={{
                    fontSize: 13, color: AppColor.appColor, fontFamily: AppFonts.light,
                    textAlign: 'center', margin: 10, marginLeft: 20, marginRight: 20
                }}>You can request for multiple service.Tick the check box and
                    submit to raise your request.
             </Text>
                <View style={{
                    width: '98%', backgroundColor: 'white', elevation: 5,
                    alignSelf: 'center', marginTop: 10, borderRadius: 5
                }}>
                    {/* <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        
                    </View> */}
                    <FlatList
                        data={services}
                        renderItem={({ item, index }) => renderCheckBox(item, index)}
                    />
                    {/* <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                       
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Pick plates from my room</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Save complementary products for today</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Check Ac of my room</Text>
                    </View>*/}
                </View>
            </View>
        )
    }
    const renderTravel = () => {
        return (
            <View style={{ alignItems: 'center', marginTop: 10 }}>
                <Text style={{ fontSize: 20, color: '#003727' }}>What you want us to do for you today ?</Text>
                <Text style={{
                    fontSize: 13, color: AppColor.appColor, fontFamily: AppFonts.light,
                    textAlign: 'center', margin: 10, marginLeft: 20, marginRight: 20
                }}>You can request for multiple service.Tick the check box and
                    submit to raise your request.
             </Text>
                <View style={{
                    width: '95%', backgroundColor: 'white', elevation: 5,
                    alignSelf: 'center', marginTop: 10, borderRadius: 5
                }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Picker
                                selectedValue={cab}
                                style={{ height: 35, width: '88%', alignSelf: 'center', color: 'grey', marginLeft: 10, fontSize: 16, fontFamily: AppFonts.medium, backgroundColor: 'white' }}
                                mode='dropdown'
                                //placeholder='Book a cab'
                                onValueChange={(itemValue, itemIndex) =>
                                    setCab(itemValue)
                                }>
                                <Picker.Item label="Book a cab" value="Book a cab" />
                                <Picker.Item label="Car" value="Car" />
                            </Picker>
                        </View>
                        <Image source={AppImage.back_arrow}
                            style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: "-90deg" }], }}
                        />
                    </View>
                </View>
            </View>
        )
    }
    const renderLanudary = () => {
        return (
            <View style={{ alignItems: 'center', marginTop: 10 }}>
                <Text style={{ fontSize: 20, color: '#003727' }}>What you want us to do for you today ?</Text>
                <Text style={{
                    fontSize: 13, color: AppColor.appColor, fontFamily: AppFonts.light,
                    textAlign: 'center', margin: 10, marginLeft: 20, marginRight: 20
                }}>You can request for multiple service.Tick the check box and
                    submit to raise your request.
             </Text>
                <View style={{
                    width: '98%', backgroundColor: 'white', elevation: 5,
                    alignSelf: 'center', marginTop: 10, borderRadius: 5
                }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <TouchableOpacity style={styles.box} onPress={() => onTick()}>
                            {tick &&
                                <Image source={AppImage.tickmark_icon}
                                    style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                                />}
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 10 }}>Dry Cleaning</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Iron</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Wash Cloths</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
                        <View style={styles.box}>
                            <Image source={AppImage.tickmark_icon}
                                style={{ width: 35, height: 30, resizeMode: 'contain', left: 10, bottom: 3 }}
                            />
                        </View>
                        <Text style={{ marginLeft: 10 }}>Roll press</Text>
                    </View>
                </View>
            </View>
        )
    }
    const renderButton = (item, index) => {
        return (
            <Pressable style={{ flexDirection: 'row' }} onPress={() => setButton(index, item.department_name, item.id)}>
                <View style={{
                    alignItems: 'center', backgroundColor: 'white', margin: 5, marginLeft: 2, marginRight: 2,
                    elevation: id === index ? 5 : 0, borderWidth: id === index ? 1 : 0.5, borderColor: id === index ? AppColor.appColor : '#04db89'
                }}>
                    <Text style={{ fontSize: 16, margin: 8, color: '#003022' }}>{item.department}</Text>

                </View>

            </Pressable>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#f0fcf6' }}>
            <Header
                title='Room Service'
                goBack={() => navigation.goBack()}
                isView={true}
                onDeliverypress={() => navigation.navigate('ViewRequest')}
            />
            <View style={{ flexDirection: 'row' }}>
                {/* <Pressable style={{ flexDirection: 'row' }}
                    onPress={() => setButton(-1, 'Front Desk')}
                >
                    <View style={{
                        alignItems: 'center', backgroundColor: 'white', margin: 5, marginLeft: 2, marginRight: 2,
                        elevation: id === -1 ? 5 : 0, borderWidth: id === -1 ? 1 : 0.5, borderColor: id === -1 ? AppColor.appColor : '#04db89'
                    }}>
                        <Text style={{ fontSize: 16, margin: 8, color: '#003022' }}>Front Desk</Text>

                    </View>

                </Pressable> */}
                <FlatList
                    data={serviceList}
                    horizontal
                    keyExtractor={(item, index) => String(index)}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => renderButton(item, index)}
                />
                {/* <Pressable style={{ flexDirection: 'row' }}
                    onPress={() => setButton(-2, 'Travel Desk')}
                >
                    <View style={{
                        alignItems: 'center', backgroundColor: 'white', margin: 5, marginLeft: 0, right: 7,
                        elevation: id === -2 ? 5 : 0, borderWidth: id === -2 ? 1 : 0.5, borderColor: id === -2 ? AppColor.appColor : '#04db89'
                    }}>
                        <Text style={{ fontSize: 16, margin: 8, color: '#003022' }}>Travel Desk</Text>

                    </View>

                </Pressable> */}
            </View>
            {services.length > 0 ?
                <ScrollView>
                    <View>
                        {/* {id === -1 &&
                        <View>
                            <Text style={{ fontSize: 18, marginLeft: 15, marginTop: 10, fontFamily: AppFonts.medium }}>Luggage</Text>
                            <View style={{
                                width: '95%', height: 180, backgroundColor: 'white', elevation: 5,
                                alignSelf: 'center', marginTop: 10, alignItems: 'center', borderRadius: 10
                            }}>
                                <Text style={{ color: AppColor.appColor, marginTop: 10 }}>Pick my luggage from my room</Text>
                                <TouchableOpacity style={{ backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 5, elevation: 5, marginTop: 10 }}
                                //onPress={() => toggleModal()}
                                >
                                    <Text style={{ color: 'white', margin: 6, fontSize: 16, marginLeft: 15, marginRight: 15, fontFamily: AppFonts.bold }}>Pick</Text>
                                </TouchableOpacity>
                                <Text style={{ color: AppColor.appColor, marginTop: 30 }}>Drop my Luggage at my room</Text>
                                <TouchableOpacity style={{ backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 5, elevation: 5, marginTop: 10 }}>
                                    <Text style={{ color: 'white', margin: 6, fontSize: 16, marginLeft: 15, marginRight: 15, fontFamily: AppFonts.bold }}>Drop</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 30 }}>
                                <Text style={{ fontSize: 18, marginLeft: 15, marginTop: 10, fontFamily: AppFonts.medium, marginBottom: 5 }}>Room</Text>
                                <View style={{
                                    width: '95%', alignItems: 'center', borderWidth: 1, borderColor: AppColor.appColor, alignSelf: 'center',
                                    flexDirection: 'row', borderRadius: 8, elevation: 1, backgroundColor: 'white'
                                }}>
                                    <Text style={{ margin: 12, marginLeft: 20 }}>You are at Room Number </Text>
                                    <View style={{ borderBottomWidth: 1, width: 40, alignItems: 'center' }}>
                                        <Text>{serviceUser.room?serviceUser.room.room_no:''}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>} */}
                        {(id !== -1 && id !== -2) && renderHouse()}
                        {/* {id === 1 && renderLanudary()}*/}
                        {/* {id === -2 && renderTravel()} */}
                        <Text style={{ color: AppColor.appColor, fontSize: 18, marginTop: 20, marginLeft: 15, marginRight: 50 }}>Let us know your time for the above service</Text>
                        <Pressable style={{
                            width: '95%', height: 35, elevation: 5, backgroundColor: 'white', alignSelf: 'center',
                            justifyContent: 'space-between', marginTop: 10, flexDirection: 'row', alignItems: 'center'
                        }}
                            onPress={() => showDatepicker()}
                        >
                            <Text style={{ marginLeft: 15 }}>{moment(time).format('h:mm a')}</Text>
                            {show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={time}
                                    mode={'time'}
                                    //is24Hour={true}
                                    display="default"
                                    onChange={onChange}
                                />
                            )
                            }
                            <Image source={AppImage.back_arrow}
                                style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: '-90deg' }], marginRight: 20 }}
                            />
                        </Pressable>
                        <Text style={{ color: AppColor.appColor, fontSize: 18, marginTop: 20, marginLeft: 15, marginRight: 50 }}>Comment</Text>
                        <View style={{ width: '95%', elevation: 5, backgroundColor: 'white', alignSelf: 'center', justifyContent: 'center', marginTop: 10 }}>
                            <TextInput
                                style={{ width: '90%', margin: 10, textAlignVertical: 'top' }}
                                placeholder={'Add your comment (if any)'}
                                value={comment}
                                onChangeText={(val) => setComment(val)}
                            />

                        </View>
                        <Pressable style={{
                            width: '30%', backgroundColor: '#003727', alignSelf: 'center',
                            alignItems: 'center', borderRadius: 15, marginTop: 15
                        }} onPress={() => SubmitResponse()}>
                            <Text style={{ color: 'white', margin: 5, fontSize: 18 }}>Submit</Text>
                        </Pressable>

                        <View>
                            <Text style={{ fontSize: 18, marginLeft: 15, marginTop: 10, fontFamily: AppFonts.medium }}>Accountant</Text>
                            <Pressable style={{
                                width: '95%', alignSelf: 'center', justifyContent: 'space-between', flexDirection: 'row', height: 40
                                , backgroundColor: 'white', elevation: 5, alignItems: 'center'
                            }} onPress={() => navigation.navigate('Mybill')}>
                                <Text style={{ fontSize: 18, marginLeft: 30 }}>View my bill</Text>
                                <Image source={AppImage.back_arrow}
                                    style={{ width: 15, height: 15, resizeMode: 'contain', marginRight: 30, transform: [{ rotate: '180deg' }] }}
                                />
                            </Pressable>
                        </View>

                    </View>
                    <View style={{ width: '100%', height: 20 }} />
                </ScrollView>
                :
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: 'red', fontSize: 18 }}>No data Available</Text>
                </View>
            }
            <Loader loading={loader} />
            {renderModal()}
        </View>
    )
}

const styles = StyleSheet.create({
    box: {
        width: 30,
        height: 30,
        borderRadius: 5,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: AppColor.appColor
    }
})

export default Service;