import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground, Pressable
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker'
import Header from '../../../component/header/Header';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import moment from 'moment';
import AppImage from '../../../utils/AppImage';
import { simpleGetCall, simplePostCall } from '../../../webservice/APIServices';
import AppStrings from '../../../utils/AppStrings';
import APIStrings from '../../../webservice/APIStrings';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../../../component/loader/Loader';

const arr = [{ id: '1', name: 'Dry Cleaning', img: AppImage.laundary_icon, date: '06/05/2021' }, { id: '2', name: 'Dinner', img: AppImage.dinner_icon, date: '06/05/2021' },
{ id: '3', name: 'Dry Cleaning', img: AppImage.laundary_icon, date: '06/05/2021' }, { id: '4', name: 'Dinner', img: AppImage.dinner_icon, date: '06/05/2021' },
{ id: '5', name: 'Dry Cleaning', img: AppImage.laundary_icon, date: '06/05/2021' }, { id: '6', name: 'Dinner', img: AppImage.dinner_icon, date: '06/05/2021' }
]

const Mybill = ({ navigation }) => {
    const [data, setData] = useState([])
    const [loader, setLoader] = useState(false);
    const [total, setTotal]=useState(null)

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            Fetch();
        });

        return unsubscribe;
    }, [navigation])

    const Fetch = async () => {
        const user_id = await AsyncStorage.getItem(AppStrings.SERVICE_USERID, '')
        setLoader(true)
        let requsetBody = JSON.stringify({
            user_id: user_id,
        })
        console.log(requsetBody, APIStrings.BillOfServicesForEndUser)
        simplePostCall(APIStrings.BillOfServicesForEndUser, requsetBody)
            .then((data) => {
                setLoader(false)

                if (data.status.code === 200) {

                    setData(data.list_of_services)
                    setTotal(data.total_services_price);
                    console.log("success", JSON.stringify(data));

                    //alert('success')
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)
                }

            }).catch((error) => {
                console.log("error", error);
                alert('Something went wrong please try again ...')
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const renderList = (item, index) => {
        return (
            <View style={{ width: '100%', height: 60, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'center', marginTop: 10, alignItems: 'center' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 15 }}>
                    <View style={{
                        backgroundColor: AppColor.appColor, width: 43, height: 43, borderRadius: 5,
                        alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Image source={{uri:APIStrings.Image_Url+item.service_photo}}
                            style={{ width: 27, height: 27, resizeMode: 'contain' }}
                        />
                    </View>
                    <View>
                        <Text style={{ fontSize: 15, color: AppColor.appColor, marginLeft: 15 }}>{item.service_name}</Text>
                        <Text style={{ fontSize: 12, fontFamily: AppFonts.medium, marginLeft: 15 }}>Date: {item.service_get_date}</Text>
                    </View>
                </View>
                <Text style={{ marginRight: 20, fontSize: 15, fontFamily: AppFonts.medium }}><Text style={{ color: AppColor.appColor }}>₹ </Text>{item.service_price}</Text>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#F2FFFB' }}>
            <Header
                title='My Bill'
                goBack={() => navigation.goBack()}
            />
            <View>
                {data.length > 0 ?
                    <FlatList
                        data={data}
                        keyExtractor={item => item.id}
                        contentContainerStyle={{paddingBottom:65}}
                        renderItem={({ item, index }) => renderList(item, index)}
                    />
                    :
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: '50%' }}>
                        <Text style={{ color: 'red', fontSize: 20 }}>No data available</Text>
                    </View>
                }
            </View>
            {data.length > 0 &&
                <View style={{
                    width: '100%', height: '8%', alignItems: 'center',backgroundColor:'white',borderTopWidth:1,borderTopColor:'#E5E5E5',
                    position: 'absolute', bottom: 0, flexDirection: 'row', justifyContent: 'space-between',elevation:5
                }}>
                    <Text style={{ fontSize: 18, color: '#003727', fontFamily: AppFonts.medium, marginLeft: 50 }}>Total</Text>
                    <View style={{
                        width: 100, height: 35, backgroundColor: 'white', marginRight: 5, alignItems: 'center',
                        justifyContent: 'center', elevation: 5, borderRadius: 5,borderColor:'#E5E5E5',borderWidth:0.5
                    }}>
                        <Text style={{ fontSize: 15, fontFamily: AppFonts.medium, }}><Text style={{ color: AppColor.appColor }}>₹ </Text>{total}</Text>
                    </View>
                </View>}
            <Loader loading={loader} />
        </View>
    )
}

export default Mybill;