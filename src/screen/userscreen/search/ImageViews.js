import AsyncStorage from '@react-native-community/async-storage';
import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, StyleSheet, Pressable
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux'
import RazorpayCheckout from 'react-native-razorpay';
import Header from '../../../component/header/Header';
import { checkLogin, checkScreen } from '../../../redux/reducer';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import AppStrings from '../../../utils/AppStrings';
import Loader from '../../../component/loader/Loader';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import ImageView from 'react-native-image-view';

const { width, height } = Dimensions.get('window')

const ImageViews = ({ navigation, route }) => {
    const [isImageViewVisible,setisImageViewVisible]=useState(true);
    const { data } = route.params;
const images=data.map((item,index)=>{
    return{
        source: {
                         uri: item.image,
                     },
                     title: 'Paris',
                     width: 806,
                     height: 720,
    }
})
useEffect(()=>{
    const unsubscribe = navigation.addListener('focus', () => {
        setisImageViewVisible(true)
    });
    setisImageViewVisible(true)

    return unsubscribe;
    
},[])
    // const images = [
    //     {
    //         source: {
    //             uri: 'https://cdn.pixabay.com/photo/2017/08/17/10/47/paris-2650808_960_720.jpg',
    //         },
    //         title: 'Paris',
    //         width: 806,
    //         height: 720,
    //     },
    // ];
    return (
         <View style={{ flex: 1,backgroundColor:'black' }}>
            
            <ImageView
            //glideAlways
                images={images}
                animationType="fade"
                imageIndex={0}
                isVisible={isImageViewVisible}
                onClose={() => {setisImageViewVisible(false),navigation.goBack()}}
                //renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
            />
         </View>
    )
}

export default ImageViews;