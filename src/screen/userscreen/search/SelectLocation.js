import React, { useState } from "react";
import {
    View, StyleSheet, TouchableOpacity, Image, ScrollView, Text,
    TextInput, TouchableWithoutFeedback, FlatList
} from "react-native";
import Header from "../../../component/header/Header";
// import Header from "../../../component/header/Header";
// import AppColor from "../../../utils/AppColor";
// import AppImage from "../../../utils/AppImage";

const arr = [{ id: '1', name: 'Mumbai',  },
{ id: '2', name: 'Delhi',},
{ id: '3', name: 'Kolkata',  },
{ id: '4', name: 'Hyderabad',  },
{ id: '5', name: 'Pune',  },
{ id: '6', name: 'Goa', },
{ id: '7', name: 'Chennai',  },
]
const SelectLocation = ({ navigation,route }) => {

    const [country, setCountries] = useState(arr);
    const [searchInput, setSearchInput] = useState('');
    const [isTrue, setisTrue] = useState(true)

    const onItemPress = (country) => {
        // const { navigation } = this.props;

        route.params.onCountrySelect(country.name);
        navigation.goBack();
    }
    const searchCountry = (input) => {
        //this.setState({ searchInput: input });
        setSearchInput(input)
        let filteredData = arr.filter(item => item.name.toLowerCase().includes(input.trim().toLowerCase()));
        //this.setState({ countries: filteredData });
        setCountries(filteredData)
        setisTrue(false)
    }

    const renderItem = (item, index) => {
        return (
            <TouchableWithoutFeedback
            onPress={() => onItemPress(item)}
            >
                <View style={styles.item}>
                    <Text style={{ margin: 5, flex: 1 }}>{item.name}</Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }


    return (
        <View style={{ flex: 1, }}>
            <Header
            title='Select Places'
                goBack={() => navigation.goBack()}
            />
            <View style={styles.search}>
                <Image
                    style={styles.searchIcon}
                    source={null} />
                <TextInput
                     flex={1}
                    placeholder='Search'
                    //placeholderTextColor={AppColors.colorLightText}
                    // backgroundColor='red'
                    padding={10}
                    keyboardType='default'
                    autoCapitalize='none'
                    autoCorrect={false}
                    maxLength={20}
                    value={searchInput}
                    onChangeText={(value) => searchCountry(value)}
                    returnKeyType='done'
                    // onSubmitEditing={() => { }}
                    //ref={(ref) => { this.searchRef = ref }}
                    underlineColorAndroid="transparent"
                />

            </View>
            {isTrue || searchInput.trim().length === 0 &&
                <View style={{ width: '100%', height: 40, backgroundColor: 'white', justifyContent: 'center' }}>
                    <Text style={{ color: AppColor.appColor, marginLeft: 20, fontSize: 16 }}>Top Cities</Text>
                </View>}
            <FlatList
                nestedScrollEnabled={true}
                data={country}
                horizontal={false}
                renderItem={({ item, index }) => renderItem(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    search: {
        borderRadius: 50,
        elevation: 2,
        zIndex: 1,
        backgroundColor: 'white',
        margin: 20,
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchIcon: {
        width: 20,
        height: 20,
        right: 10,
        resizeMode: 'contain',
        marginLeft: 25
        //flex:1
    },
    item: {
        flex: 1,
        backgroundColor: 'white',
        minHeight: 60,
        padding: 15,
        alignItems: 'center',
        flexDirection: 'row',
        margin: 1.5
    }
});

export default SelectLocation;