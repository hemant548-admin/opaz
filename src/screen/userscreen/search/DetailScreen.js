import React, { useState, useEffect, useRef, } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, Animated,
    ScrollView, ImageBackground, StyleSheet, SafeAreaView, Pressable
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import APIStrings from '../../../webservice/APIStrings';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import Loader from '../../../component/loader/Loader';
import MapView, { Marker } from 'react-native-maps';
import ImageSlider from 'react-native-image-slider';
import axios from 'axios';
import Stars from 'react-native-stars';
import { Picker } from '@react-native-community/picker';
import ImageView from 'react-native-image-view';
import { useIsFocused } from '@react-navigation/native';

const { width, height } = Dimensions.get('window')
const imgArray = [AppImage.hotel_bg2, AppImage.hotel_bg1,]
const star = [AppImage.star_orang, AppImage.star_orang, AppImage.star_orang, AppImage.star_orang, AppImage.star_icon]
const aminity = [{ id: '1', img: AppImage.bowl_icon, name: 'Breakfast' }, { id: '2', img: AppImage.bar_icon, name: 'Bar' },
{ id: '3', img: AppImage.pool_icon, name: 'Pool' }, { id: '4', img: AppImage.spa_icon, name: 'spa' }
]

const HEADER_HEIGHT = height * 0.38;
const HEADER_MAX_HEIGHT = height * 0.38; // max header height
const HEADER_MIN_HEIGHT = 10; // min header height
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT; // header scrolling value


const DetailScreen = ({ navigation, route }) => {
    //console.log(width, height)
    const offset = useRef(new Animated.Value(0)).current;
    const [address1, setAddress1] = useState('');
    const [language, setLanguage] = useState('Java');
    const [room, setRoom] = useState('Single room');
    const [aminities, setAminities] = useState([]);
    //const [isImageViewVisible, setisImageViewVisible] = useState(false)
    const [loader, setLoader] = useState()
    const { hotelData,facility } = route.params;
    const isFocused = useIsFocused();
    const images = hotelData.branch.branch_image.map((item, index) => {
        return {
            source: {
                uri: item.image,
            },
            title: 'Paris',
            width: 806,
            height: 720,
        }
    })
    console.log("hoteldata", JSON.stringify(hotelData))
    useEffect(() => {


    }, [isFocused])
    // const ApiFetch = () => {
    //     setLoader(true)
    //     let requsetBody = JSON.stringify({
    //         room_id: String(item)
    //     })

    //     console.log(APIStrings.EndUserRoomDetails)
    //     console.log(requsetBody);

    //     simplePostCall(APIStrings.EndUserRoomDetails, requsetBody)
    //         .then((data) => {

    //             setLoader(false)
    //             //console.log(data.token);
    //             if (data.status.code === 200) {
    //                 console.log("success", JSON.stringify(data));
    //                 setHotelData(data.Room)
    //             }
    //             else {
    //                 //this.setState({ invalid: true, errorMessage: data.message });
    //                 console.log(data.status.message)

    //             }

    //         }).catch((error) => {
    //             console.log("error ", error);
    //             setLoader(false)
    //             alert('api error')
    //             //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
    //         });

    // }
    const insets = useSafeAreaInsets();

    const headerHeight = offset.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE + insets.top],
        outputRange: [0, -HEADER_SCROLL_DISTANCE + insets.top],
        extrapolate: 'clamp'
    });
    // const fadeOut = offset.interpolate({
    //     inputRange: [0, HEADER_HEIGHT / 2],
    //     outputRange: [0, -HEADER_HEIGHT/2],
    //     extrapolate: 'clamp',
    // });

    const AnimatedHeader = () => {


        return (
            <Animated.View style={
                {
                    width: '100%',
                    //position: 'absolute',
                    top: 32,
                    //bottom:HEADER_HEIGHT,
                    height: HEADER_MAX_HEIGHT,
                    transform: [{ translateY: headerHeight }],
                    backgroundColor: 'red'
                }
            }>
                {/* <Pressable onPress={() => navigation.navigate('ImageView', { data: hotelData.branch.branch_image })}> */}
                {hotelData.branch.branch_image.length > 0 ?
                    <ImageSlider
                        loopBothSides
                        autoPlayWithInterval={1000}
                        images={hotelData.branch.branch_image}

                        customSlide={({ index, item, style, width }) => (
                            //console.log('data ',hotelData.branch.branch_imag),
                            // It's important to put style here because it's got offset inside
                            <Pressable key={index} style={{ flex: 1 }} onPress={() => navigation.navigate('ImageView', { data: hotelData.branch.branch_image })}>
                                <Image source={{ uri: item.image }} style={{ flex: 1, width: width, height: 100 }} />
                            </Pressable>
                        )}
                    />

                    :
                    <View style={{}}>
                        <Image source={null}
                            style={{ flex: 1, resizeMode: 'cover', backgroundColor: 'silver' }}
                        />
                    </View>}

            </Animated.View>

        );
    };

    const renderAminity = (item, index) => {
        return (
            <View style={{ margin: 10, marginLeft: 10, alignItems: 'center', width: 68, marginBottom: 0 }}>
                <View style={styles.aminity}>
                    <Image source={{ uri: item.service_photo }}
                        style={{ width: 30, height: 30, resizeMode: 'contain' }}
                    />
                </View>
                <Text style={styles.aminityTxt}>{item.service_name}</Text>
            </View>
        )
    }

    return (
        // Object.keys(hotelData).length > 0 ?
        hotelData !== '' ?
            <SafeAreaView style={{ flex: 1, }}>

                <View style={{ flex: 1, backgroundColor: AppColor.appColor }}>

                    {/* <Pressable onPress={() => navigation.navigate('ImageView', { data: hotelData.branch.branch_image })}> */}

                    {/* </Pressable> */}
                    {/* <ImageView
                        //glideAlways
                        images={images}
                        animationType="fade"
                        imageIndex={0}
                    //isVisible={this.state.isImageViewVisible}
                    //renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
                    /> */}
                    <Animated.ScrollView contentContainerStyle={{ flexGrow: 1, }} style={{}}
                        showsVerticalScrollIndicator={false}
                        scrollEventThrottle={16}
                    // onScroll={Animated.event(
                    //     [{ nativeEvent: { contentOffset: { y: offset } } }],
                    //     { useNativeDriver: true }
                    // )}
                    >
                        {AnimatedHeader()}
                        <View style={{
                            flex: 1, backgroundColor: AppColor.appColor, borderTopLeftRadius: 40, borderTopRightRadius: 40,
                            borderTopColor: AppColor.appColor,
                        }}>
                            <View style={{ marginTop: 20, marginLeft: 20 }}>
                                <Text style={{ fontSize: 16, color: 'white' }}>Branch</Text>
                                <Text style={styles.title}>{hotelData.branch.branch_name}</Text>
                                <Text style={styles.txt}>{hotelData.branch.first_address}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                    <Image source={AppImage.location_icon}
                                        style={{ width: 20, height: 20, resizeMode: 'contain', }}
                                    />
                                    <Text style={{
                                        color: '#ADBDB9', fontFamily: AppFonts.light,
                                        marginLeft: 10, fontSize: 17
                                    }}>{hotelData.landmark}</Text>

                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10, marginTop: 5, alignSelf: 'flex-end' }}>
                                    {/* {star.map(item => (
                                        <Image source={item}
                                            style={{ width: 15, height: 15, resizeMode: 'contain', marginLeft: 5 }}
                                        />
                                    ))} */}
                                    <Stars
                                        //half={true}
                                        default={hotelData.branch.total_rating}
                                        // update={(val) => { setStars(val), setInValid(!inValid[1]) }}
                                        spacing={4}
                                        disabled={true}
                                        starSize={15}
                                        count={5}
                                        fullStar={AppImage.star_orang}
                                        emptyStar={AppImage.star_icon}
                                    //halfStar={require('./images/starHalf.png')} 
                                    />
                                </View>
                                <Text style={{
                                    color: '#ADBDB9', fontFamily: AppFonts.light,
                                    fontSize: 17, marginRight: 20, marginTop: 35
                                }}>{hotelData.description}
                                </Text>
                                <Text style={styles.title}>Amenities</Text>
                                <View>
                                    {facility.length>0 &&
                                    <FlatList
                                        data={facility}
                                        horizontal
                                        showsHorizontalScrollIndicator={false}
                                        keyExtractor={item => item.id}
                                        renderItem={({ item, index }) => renderAminity(item, index)}
                                    />}
                                </View>

                                <Text style={styles.title}>Policies</Text>
                                <View style={{
                                    width: '94%',  backgroundColor: 'white', borderRadius: 10,
                                    textAlignVertical: 'top', marginTop: 10, alignItems: 'center'
                                }}>{hotelData.branch.policies &&
                                    <Text style={{ fontSize: 16, color: 'grey', margin: 10, marginTop: 15 }}>{hotelData.branch.policies.terms_and_policies}</Text>}
                                </View>
                                <Text style={styles.title}>Check-in/Check-out</Text>
                                {hotelData.branch.policies &&
                                    <React.Fragment>
                                        <View style={styles.picker}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={AppImage.clock_white}
                                                        style={styles.timeImg}
                                                    />
                                                    <Text style={{ color: 'white', fontSize: 16, marginLeft: 15 }}>{hotelData.branch.policies.check_in}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.picker}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={AppImage.timer_white}
                                                        style={styles.timeImg}
                                                    />
                                                    <Text style={{ color: 'white', fontSize: 16, marginLeft: 15 }}>{hotelData.branch.policies.check_out}</Text>

                                                </View>
                                                {/* <Image source={AppImage.arrow_white}
                                        style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: "90deg" }], right: 35 }}
                                    /> */}
                                            </View>
                                        </View>
                                    </React.Fragment>
                                }
                                {/* <Text style={[styles.title, { marginTop: 20 }]}>Contact</Text>
                                <View style={styles.contact}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <Image source={AppImage.profile_white}
                                            style={styles.timeImg}
                                        />
                                        <Text style={styles.contactTxt}>{hotelData.front_desk.first_name}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        <Image source={AppImage.help}
                                            style={styles.timeImg}
                                        />
                                        <Text style={styles.contactTxt}>{hotelData.branch.contact_number}</Text>
                                    </View>
                                </View> */}
                                <Text style={styles.title}>Map</Text>
                                <MapView
                                    style={{ height: 120, width: '95%', marginTop: 10 }}
                                    initialRegion={{
                                        latitude: 18.508482,
                                        longitude: 73.795823,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: Number(hotelData.branch.latitude),
                                            longitude: Number(hotelData.branch.longitude),
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        title={hotelData.branch.branch_name}
                                    //description={marker.description}
                                    />
                                </MapView>
                                {/* <Text style={styles.title}>Select room</Text>
                                <View style={styles.picker}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={AppImage.room_icon}
                                                style={styles.timeImg}
                                            />
                                            <Picker
                                                selectedValue={room}
                                                style={{ height: 40, width: '88%', alignSelf: 'center', color: 'white', marginLeft: 10, fontSize: 16, backgroundColor: '#028B63' }}
                                                mode='dropdown'
                                                onValueChange={(itemValue, itemIndex) =>
                                                    setRoom(itemValue)
                                                }>
                                                <Picker.Item label="Double room" value="Double room" />
                                                <Picker.Item label="Single room" value="Single room" />
                                            </Picker>
                                        </View>
                                        <Image source={AppImage.arrow_white}
                                            style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: "90deg" }], right: 35 }}
                                        />
                                    </View>
                                </View> */}
                                <Pressable style={{
                                    width: '50%', alignSelf: 'center', alignItems: 'center',
                                    backgroundColor: 'white', borderRadius: 20, marginTop: 20, justifyContent: 'center'
                                }} onPress={() => navigation.navigate('Calendar', { id: hotelData.branch.id })}
                                >
                                    <Text style={{ margin: 7, fontSize: 18, fontFamily: AppFonts.medium }}>Find Room</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: '100%', height: 40 }} />
                        </View>

                        {/* <Loader loading={loader} /> */}
                        {/* <View style={{ width: '100%', height: 40 }} /> */}
                    </Animated.ScrollView>
                    <TouchableOpacity style={{ top: 10, left: 15, width: 16, height: 16, position: 'absolute' }} activeOpacity={0.7}
                        onPress={() => navigation.goBack()}
                    >
                        <Image source={AppImage.back_arrow_white}
                            style={{ height: 15, width: 15, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>
                </View>

                <Loader loading={loader} />

            </SafeAreaView>
            : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>No data Available For the room</Text>
            </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '38%',
        top: 30,
    },
    topHeader: {
        //marginTop: 10,
        //marginLeft: 30,
        // backgroundColor:'yellow',
        position: 'absolute',
        justifyContent: 'center',
        width: '100%',
        height: '22%',
        alignSelf: 'center',
        top: 0
        //marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {

        color: 'white',
        fontFamily: AppFonts.light,
        fontSize: 17
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        backgroundColor: AppColor.appColor,
        width: '50%',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20,
        alignSelf: 'center'
    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 7
    },
    title: {
        fontSize: 22,
        color: 'white',
        marginTop: 15,
        // fontFamily:AppFonts.medium
    },
    aminity: {
        width: 55,
        height: 55,
        borderRadius: 10,
        elevation: 4,
        borderWidth: 1,
        borderColor: AppColor.appColor,
        backgroundColor: '#028B63',
        alignItems: 'center',
        justifyContent: 'center'

    },
    aminityTxt: {
        color: 'white',
        textAlign: 'center',
        marginTop: 10
    },
    picker: {
        width: '94%',
        height: 40,
        elevation: 3,
        backgroundColor: '#028B63',
        borderRadius: 5,
        justifyContent: 'center',
        marginTop: 10

    },
    timeImg: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        marginLeft: 10
    },
    contact: {
        width: '94%',
        height: 85,
        elevation: 3,
        backgroundColor: '#028B63',
        borderRadius: 5,
        justifyContent: 'space-evenly',
        marginTop: 10

    },
    contactTxt: {
        textAlign: 'center',
        fontSize: 16,
        color: 'white',
        marginLeft: 15,
        marginTop: 4
    }

})

export default DetailScreen;