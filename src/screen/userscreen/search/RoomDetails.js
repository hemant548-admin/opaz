import AsyncStorage from '@react-native-community/async-storage';
import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, StyleSheet, Pressable
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux'
import RazorpayCheckout from 'react-native-razorpay';
import Header from '../../../component/header/Header';
import { checkLogin, checkScreen } from '../../../redux/reducer';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import AppStrings from '../../../utils/AppStrings';
import Loader from '../../../component/loader/Loader';
import APIStrings from '../../../webservice/APIStrings';
import ImageSlider from 'react-native-image-slider';
import { simplePostCall } from '../../../webservice/APIServices';
import { AuthContext } from '../../../Routes/AuthContext';

const { width, height } = Dimensions.get('window')

const RoomDetails = ({ navigation, route }) => {

    const [selected, setSelected] = useState(null)
    const [discountPrice, setDiscountPrice] = useState(null);
    const [finalPrice, setFinalPrice] = useState(null);
    const { item } = route.params;
    const [isModalVisible, setModalVisible] = useState(false);
    const [isFilterVisible, setFilterVisible] = useState(false);
    const [btn, setBtn] = useState(null);
    const [role, setRole] = useState('')
    const [userId, setUserId] = useState('');
    const [phone, setPhone] = useState('');
    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [room, setRoom] = useState([]);
    const [roomId, setRoomId] = useState('');
    const [branchId, setbranchId] = useState('');
    const [price, setPrice] = useState('');
    const [isSuccess, setIsSuccess] = useState(false);
    const [bookingId, setBookingId] = useState('');
    const [loader, setLoader] = useState(false)
    const [isOfferVisible, setOfferVisible] = useState(false);
    const [offerData, setOfferData]=useState([]);
    //const [selected, setSelected] = useState(null)
    //const [finalPrice, setFinalPrice] = useState(null);
    const [isCancel, setIsCancel] = useState(false);
    const [adultCount, setAdultCount] = useState(2);
    const [childCount, setChildCount] = useState(0);
    const [bedCount, setBedCount] = useState(1);
    const [roomData, setRoomData] = useState([]);
    const [type, setType] = useState('');
    const [isTick, setIsTick] = useState(false);
    //const [discountPrice, setDiscountPrice] = useState(null);
    //const {data}=route.params;
    const { signOut } = React.useContext(AuthContext);
    const data = useSelector(state => state.requestData)
    const checkIn = useSelector(state => state.checkIn)
    const checkOut = useSelector(state => state.checkOut)

    const dispatch = useDispatch()
    //console.log(item);

    useEffect(()=>{
        const unsubscribe = navigation.addListener('focus', () => {
            setFinalPrice(item.final_price);
            setDiscountPrice(item.final_price)
            setRoomId(item.id);
            setbranchId(item.branch);
            setSelected(null);
            userData();
        });
        userData();
        FetchingOffer();

        return unsubscribe;
        
    },[navigation,item.id])

    const userData = async () => {
        const role = await AsyncStorage.getItem(AppStrings.ROLE, '')
        const id = await AsyncStorage.getItem(AppStrings.USER_ID, '')
        const fName = await AsyncStorage.getItem(AppStrings.FIRST_NAME, '');
        const lName = await AsyncStorage.getItem(AppStrings.LAST_Name, '')
        const eml = await AsyncStorage.getItem(AppStrings.EMAIL, '');
        const mobile = await AsyncStorage.getItem(AppStrings.PHONE, '');
        const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, '');

        setRole(role);
        setUserId(id);
        setPhone(mobile);
        setfirstName(fName);
        setlastName(lName);
        setEmail(eml);
        setAddress(addr);
    }
    const FetchingOffer = () => {
        
        setLoader(true)
        

        let requsetBody = JSON.stringify({
            
            branch_id: item.branch,
           
        })
        console.log(APIStrings.BranchPersonalOffer);
        console.log(requsetBody);

        simplePostCall(APIStrings.BranchPersonalOffer, requsetBody)
            .then((data) => {

                setLoader(false)
                console.log(data);
                if (data.status.code === 200) {
                    setOfferData(data.output)
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const FetchingData = () => {
        // if (roomId === '') {
        //     alert("Please select Room")
        //     return
        // }
        setLoader(true)
        toggleModal();

        let requsetBody = JSON.stringify({
            room_id: roomId,
            branch_id: branchId,
            user_id: userId,
            email: email,
            first_name: firstName,
            last_name: lastName,
            phone_number: phone,
            amount: discountPrice,
            checkin_date: checkIn,
            check_out_date: checkOut
        })
        console.log(APIStrings.EndUserBookForSelf);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserBookForSelf, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    setBookingId(data.Booking_Obj.id)
                    console.log("success", JSON.stringify(data));
                    console.log("type", type)
                    if (type === 'Pay Now') {
                        paymentGetway(data)
                    }
                    else {
                        BookandPayatHotel(data)
                    }

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const paymentGetway = (value) => {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: data.razor_pay_key_for_app, // Your api key
            amount: String(value.amount),
            order_id: value.payment.id,
            name: 'foo',
            prefill: {
                email: 'void@razorpay.com',
                contact: '9191919191',
                name: 'Razorpay Software'
            },
            theme: { color: AppColor.appColor }
        }
        RazorpayCheckout.open(options).then((data) => {
            //handle success
            afterPayment(value.Booking_Obj.id)
            console.log(`Success: ${data.razorpay_payment_id}`);
        }).catch((error) => {
            // handle failure
            toggleCancelModal()
            //alert(`Error: ${error.code} | ${error.description}`);
        });
    }
    const afterPayment = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value,
            mobile: phone
        })
        console.log(requsetBody)
        console.log(APIStrings.RazoyRoomPaySucess)
        simplePostCall(APIStrings.RazoyRoomPaySucess, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("Razorpay", JSON.stringify(data1));
                    toggleSuccessModal()
                    navigation.navigate('Tab')
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const BookandPayatHotel = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value.Booking_Obj.id,
            login_user_mobile_number:phone
        })
        console.log(requsetBody)
        console.log(APIStrings.BookAndPayAtHotel)
        simplePostCall(APIStrings.BookAndPayAtHotel, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("BookandPayatHotel", JSON.stringify(data1));
                    toggleSuccessModal()
                    navigation.navigate('Tab')
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const toggleModal = () => {
        if (role === 'EndUser') {
            setModalVisible(!isModalVisible);
        }
        else {
            dispatch(checkLogin(1, true, ''));
            dispatch(checkScreen('Login',null,false))
            //navigation.navigate('Login');
            signOut();

        }
    };
    const toggleFilter = () => {
        setFilterVisible(!isFilterVisible)
    }
    const toggleCancelModal = () => {
        setIsCancel(!isCancel);
    }
    const toggleSuccessModal = () => {
        setIsSuccess(!isSuccess);
    }
    const toggleButton = (index, item,) => {
        if (index === btn) {
            setBtn(null)
            setRoomId('')
            setbranchId('')
            setPrice('');
        }
        else {
            setBtn(index)
            setRoomId(item.id)
            setbranchId(item.branch)
            setPrice(item.final_price);
            setDiscountPrice(item.final_price)
        }

    }
    const onOfferPress = (val,id) => {
        console.log('id ', id)
        if(id === selected){
            setSelected(null);
            setDiscountPrice(finalPrice);
        }
        else {
            setSelected(id);
            let tmp = finalPrice * Number(val)/100;
             setDiscountPrice(finalPrice - tmp);
        }
        
        
        

        // if (id === selected) {
        //     setSelected(null);
        //     //let tmp=finalPrice *0.5
        //     setDiscountPrice(finalPrice);

        // }
        // else if (id === 1) {
        //     setSelected(id);
        //     let tmp = finalPrice * Number(val)/100;
        //     setDiscountPrice(finalPrice - tmp);

        // }
        // else if (id === 2) {
        //     setSelected(id);
        //     let tmp = finalPrice * Number(val)/100;
        //     setDiscountPrice(finalPrice - tmp);

        // }
    }
    const cancelModal = () => {
        return (
            <View>
                <Modal isVisible={isCancel}
                    onBackdropPress={() => setIsCancel(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '30%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your Transaction is not Completed!"} </Text>
                                {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text> */}
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleCancelModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const successModal = () => {
        return (
            <View>
                <Modal isVisible={isSuccess}
                    onBackdropPress={() => setIsSuccess(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '50%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your room has been booked\n successfully."} </Text>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text>
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleSuccessModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const renderModal = () => {
        return (
            <View>
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width, height: '40%', backgroundColor: AppColor.appColor, alignItems: 'center', }}>
                            <Text style={{ fontSize: 22, margin: 10, color: 'white', marginTop: 20 }}>This Booking is for whom ?</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                    onPress={() => FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller', { roomId: roomId, branchId: branchId, userId: userId, price: discountPrice, type: type }), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    return (
        <View style={{ flex: 1, }} key={item.id}>
            <Header
                title="Room Information"
                goBack={() => navigation.goBack()}
            />
            <ScrollView contentContainerStyle={{paddingBottom:20}}>
                <View style={{width:'100%',height:220}}>
                {item.images.length > 0 ?
                    <ImageSlider
                        loopBothSides
                        autoPlayWithInterval={1000}
                        images={item.images}
                        customSlide={({ index, item, style, width }) => (
                            //console.log('data ',hotelData.branch.branch_imag),
                            // It's important to put style here because it's got offset inside
                            <Pressable key={index} style={{ flex: 1 }} 
                            //onPress={() => navigation.navigate('ImageView', { data: item.images })}
                            >
                                <Image source={{ uri: item.images }} style={{ flex: 1, width: width, height: 100 }} />
                            </Pressable>
                        )}
                    />

                    :
                    <View style={{}}>
                        <Image source={null}
                            style={{ flex: 1, resizeMode: 'cover', backgroundColor: 'silver' }}
                        />
                    </View>}
                    {/* <Image
                        source={AppImage.BranchList_img}
                        style={{ width: '100%', height: 200, resizeMode: 'cover' }}
                    /> */}
                </View>
                <View>
                    <View style={{ backgroundColor: 'white', paddingBottom: 18 }}>
                        <Text style={{ fontSize: 20, fontFamily: AppFonts.regular, marginLeft: 20, marginTop: 20 }}>{item.room_type} Room</Text>
                        <Text style={{ marginTop: 10, marginLeft: 20 }}>Max. guest: {item.adult} adults {item.children} children</Text>
                    </View>
                    <View style={{ marginTop: 10, width: '100%', backgroundColor: 'white', }}>
                       <View style={{flexDirection:'row',alignItems:'center',marginTop: 15,marginBottom:15,marginLeft:20}}>
                        <Image source={item.cancellation_status==='Not Refundable'?AppImage.cancel_icon:AppImage.free_icon}
                        style={{width:13,height:13,resizeMode:'contain'}}
                        />
                        <Text style={{ marginLeft: 5,color:item.cancellation_status==='Not Refundable'?'red':'green'  }}>{item.cancellation_status}</Text>
                        </View>
                    </View>
                    <View style={{backgroundColor:'white',marginTop:2}}>
                        <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Room Description</Text>
                        <Text style={{margin:20,marginTop:7}}>{item.description}</Text>
                    </View>
                    <View style={{backgroundColor:'white',marginTop:2}}>
                        <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Pricing details</Text>
                        {/* <TouchableOpacity style={{
                                    width: '100%', elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onOfferPress(1)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === 1 ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize:16, marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10 }}>Apply 50% ofF for first time booking</Text>
                                </TouchableOpacity> */}
                             {offerData.length>0 && offerData.map((item,index)=>{

                            return(
                        <TouchableOpacity style={{
                                    width: '100%', height: 50, elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onOfferPress(item.discount,index)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === index ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize: 16, marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10 }}>Apply {item.discount}% off with {item.name} coupon</Text>
                                </TouchableOpacity>
                            )
                                 })   
                                }
                                <View style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, marginLeft: 10 }}>Price to Pay</Text>
                                <Text style={{ fontSize: 18, marginRight: 10, fontFamily: AppFonts.medium }}>₹{discountPrice}</Text>
                            </View>
                    </View>
                </View>
            </ScrollView>
            <View style={{
                width: '100%', height: '9%', backgroundColor: AppColor.appColor
                , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center'
            }}>
                <Pressable style={{ backgroundColor: 'white', alignItems: 'center', borderRadius: 20, }}
                    onPress={() => { toggleModal(), setType('Pay Hotel') }}
                >
                    <Text style={{ margin: 7, marginLeft: 12, marginRight: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.medium }}>Book and Pay at hotel</Text>
                </Pressable>

                <Pressable style={{ width: '30%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, marginLeft: 30 }}
                    onPress={() => { toggleModal(), setType('Pay Now') }}
                >
                    <Text style={{ margin: 7, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.medium }}>Pay Now</Text>
                </Pressable>
            </View>
            {renderModal()}
            {successModal()}
            {cancelModal()}
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '100%',
        backgroundColor: 'white',
        marginTop: 10
    },
    titleTxt: {
        fontSize: 20,
        color: AppColor.appColor,
        margin: 10
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 5, marginLeft: 10
    },
    img: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    txt: {
        fontSize: 16,
        marginLeft: 10
    },
    modalImg: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    circle: {
        width: 80,
        height: 80,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: '#94FFE0',
        justifyContent: 'center'
    },
    filterTxt: {
        fontSize: 16,
        color: '#003022'
    },
    btnContainer1: {
        width: '40%',
        height: 40,
        backgroundColor: '#003727',
        borderRadius: 10,
        elevation: 3,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 10,
        //borderWidth:0.1,
        borderColor: AppColor.appColor
    },
})

export default RoomDetails;