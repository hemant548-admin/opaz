import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, StyleSheet, Pressable, ScrollView
} from 'react-native';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux'
import { CheckData } from '../../../redux/reducer';
import AppColor from '../../../utils/AppColor';
import AppImage from '../../../utils/AppImage';
import AppFonts from '../../../utils/AppFonts';
import { simplePostCall } from '../../../webservice/APIServices';
import CalendarPicker from 'react-native-calendar-picker';
import APIStrings from '../../../webservice/APIStrings';
import Loader from '../../../component/loader/Loader';

const _format = 'YYYY-MM-DD'
const _today = moment().format(_format)
const CalendarScren = ({ navigation, route }) => {
    const initialState = {
        [_today]: { disabled: false, selected: true }
    }
    const [selectedStartDate, setSelectedStartDate] = useState(moment());
    const [selectedEndDate, setSelectedEndDate] = useState(moment().add(1, 'days'));
    const [dates, setDate] = useState(initialState)
    const [dateArray, setDateArray] = useState([_today]);
    const [bedCount, setbedCount] = useState(1);
    const [loader, setLoader] = useState(false);
    const { id } = route.params;
    const dispatch = useDispatch();

    useEffect(() => {
        console.log(dateArray);
        console.log(dates)
        console.log('sort', dateArray.sort((a, b) => { return a > b }))
        //const setData=()=>{
        // const _selectedDay = moment().format(_format);

        // let selected = true;
        // if (dates[_selectedDay]) {
        //   // Already in marked dates, so reverse current marked state
        //   selected = !dates[_selectedDay].selected;
        // }

        // // Create a new object using object property spread since it should be immutable
        // // Reading: https://davidwalsh.name/merge-objects
        // const updatedMarkedDates = {...dates, ...{ [_selectedDay]: { selected } } }

        // // Triggers component to render again, picking up the new state
        // //this.setState({ _markedDates: updatedMarkedDates });
        // setDate(updatedMarkedDates)
        // }
        //setData()
    }, [dateArray, dates])

    const ApiFetch = () => {

        if (selectedEndDate === null) {
            alert('Please select chekout date')
            return
        }
        //setLoader(true)
        const chIn = moment(selectedStartDate).format('YYYY-MM-DD')
        const chOut = moment(selectedEndDate).format('YYYY-MM-DD')

        let requestbody = JSON.stringify({
            checkin_date: chIn,
            check_out_date: chOut,
            extra_bed: String(bedCount),
            branch_id: String(id)
        })
        setLoader(true);
        console.log(requestbody)
        console.log(APIStrings.EndUserRoomFilerByBookingDate)
        simplePostCall(APIStrings.EndUserRoomFilerByBookingDate, requestbody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    dispatch(CheckData(data, chIn, chOut, bedCount))
                    console.log("success", JSON.stringify(data));

                    navigation.navigate('ChooseRoom')
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                 setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const onDateChange = (date, type) => {
        //function to handle the date change
        if (type === 'END_DATE') {
            setSelectedEndDate(date);
        } else {
            setSelectedEndDate(null);
            setSelectedStartDate(date);
        }
    };
    const onDaySelect = (day) => {
        console.log(day)
        let newDate;
        if (dateArray.includes(day.dateString) === false) {
            newDate = [...dateArray, day.dateString]

        }
        else {
            newDate = dateArray.filter(element => element != day.dateString)
        }
        newDate = newDate.sort((a, b) => { return a > b })
        setDateArray(newDate.sort((a, b) => { return a > b }))
        const _selectedDay = moment(day.dateString).format(_format);


        let selected = true;
        let startingDay = true;
        if (dates[_selectedDay]) {
            // Already in marked dates, so reverse current marked state
            selected = !dates[_selectedDay].selected;
        }
        //console.log('new ',convertArrayToObject(dateArray));

        // Create a new object using object property spread since it should be immutable
        // Reading: https://davidwalsh.name/merge-objects
        const updatedMarkedDates = { ...dates, ...{ [_selectedDay]: { selected } } }

        // Triggers component to render again, picking up the new state
        //this.setState({ _markedDates: updatedMarkedDates });
        setDate(updatedMarkedDates)
    }
    const convertArrayToObject = (array, key) => {
        const initialValue = {};
        return array.reduce((obj, item) => {
            return {
                ...obj,
                [item[key]]: item,
            };
        }, initialValue);
    };
    // const selectDate=(day)=> {
    //     let selectedDate = day.dateString;
    //     let newDates = dates;
    //     if (dates[selectedDate]) {
    //       delete newDates[selectedDate]
    //     } else {

    //       newDates[selectedDate]={selected: true,selectedColor: 'yellow',}
    //     //   { selected: true, endingDay: true, selectedColor: 'yellow' }
    //     // ];


    //     }
    //     setDate({...newDates})
    //   }
    return (
        <View style={{ flex: 1 }}>
            <View style={{
                width: '100%', height: '8%', padding: 5,
                backgroundColor: 'white', alignSelf: 'center', flexDirection: 'row', alignItems: 'center', elevation: 5
            }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image source={AppImage.back_arrow}
                        style={{ width: 16, height: 16, resizeMode: 'contain', marginLeft: 10 }}
                    />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: AppColor.appColor, marginLeft: 10 }}>Book a room</Text>
            </View>
            <ScrollView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <CalendarPicker
                        startFromMonday={false}
                        allowRangeSelection={true}
                        minDate={new Date()}
                        maxDate={new Date(2050, 6, 3)}
                        weekdays={['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',]}
                        months={[
                            'January',
                            'Febraury',
                            'March',
                            'April',
                            'May',
                            'June',
                            'July',
                            'August',
                            'September',
                            'October',
                            'November',
                            'December',
                        ]}
                        previousTitle="Previous"
                        nextTitle="Next"
                        todayBackgroundColor="#e6ffe6"
                        selectedDayColor="#66ff33"
                        selectedDayTextColor="#000000"
                        scaleFactor={375}
                        textStyle={{
                            fontFamily: 'Cochin',
                            color: '#000000',
                        }}
                        selectedRangeEndStyle={{ backgroundColor: AppColor.appColor }}
                        selectedRangeStartStyle={{ backgroundColor: AppColor.appColor }}
                        selectedRangeStartTextStyle={{ color: 'white' }}
                        selectedRangeEndTextStyle={{ color: 'white' }}
                        selectedRangeStyle={{ backgroundColor: '#438a75' }}
                        selectedStartDate={selectedStartDate}
                        selectedEndDate={selectedEndDate}
                        onDateChange={onDateChange}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <View>
                            <Text style={{ fontSize: 16 }}>Check-in</Text>
                            <Text style={{ fontSize: 20, color: AppColor.appColor }}>{moment(selectedStartDate).format('DD MMM')}</Text>

                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={AppImage.arrow_green}
                                style={{ width: 18, height: 15, resizeMode: 'contain', transform: [{ rotate: '180deg' }] }}
                            />
                            <Text style={{ fontSize: 16, textAlign: 'center', color: AppColor.appColor, marginTop: 5 }}>{selectedEndDate === null ? '' : moment(selectedEndDate).diff(selectedStartDate, 'days')} Night</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 16 }}>Check-out</Text>
                            <Text style={{ fontSize: 20, color: AppColor.appColor }}>{selectedEndDate === null ? 'Select check-out' : moment(selectedEndDate).format('DD MMM')}</Text>
                        </View>
                    </View>
                    {/* <TouchableOpacity style={selectedEndDate===null? styles.disable:styles.enable} activeOpacity={0.6}
                    onPress={() => onApplyPress()}
                    disabled={selectedEndDate===null? true:false}
                >
                    <Text style={{ color: 'white', margin: 10, fontSize: 18 }}>Apply Dates</Text>
                </TouchableOpacity> */}
                    {/* <View style={styles.textStyle}>
          <Text style={styles.textStyle}>Selected Start Date :</Text>
          <Text style={styles.textStyle}>
            {selectedStartDate ? selectedStartDate.toString() : ''}
          </Text>
          <Text style={styles.textStyle}>Selected End Date : </Text>
          <Text style={styles.textStyle}>
            {selectedEndDate ? selectedEndDate.toString() : ''}
          </Text>
        </View> */}

                    {/* <View style={styles.card}>
                    <View style={{ flex: 1 }}>
                        <Calendar
                            // Initially visible month. Default = Date()
                            //current={'2012-03-01'}
                            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            minDate={new Date()}
                            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            //maxDate={'2012-05-30'}
                            // markingType={'period'}
                            // Handler which gets executed on day press. Default = undefined
                            onDayPress={(day) => { onDaySelect(day) }}
                            // Handler which gets executed on day long press. Default = undefined
                            onDayLongPress={(day) => { console.log('selected day', day) }}
                            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                            monthFormat={'MMMM yyyy'}
                            // Handler which gets executed when visible month changes in calendar. Default = undefined
                            onMonthChange={(month) => { console.log('month changed', month) }}
                            // Hide month navigation arrows. Default = false
                            //hideArrows={true}
                            // Replace default arrows with custom ones (direction can be 'left' or 'right')
                            //renderArrow={(direction) => (<Arrow />)}
                            // Do not show days of other months in month page. Default = false
                            hideExtraDays={true}
                            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                            // day from another month that is visible in calendar page. Default = false
                            //disableMonthChange={true}
                            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                            // firstDay={1}
                            // Hide day names. Default = false
                            //hideDayNames={true}
                            // Show week numbers to the left. Default = false
                            //showWeekNumbers={true}
                            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                            onPressArrowLeft={subtractMonth => subtractMonth()}
                            // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                            onPressArrowRight={addMonth => addMonth()}
                            // Disable left arrow. Default = false
                            //disableArrowLeft={true}
                            // Disable right arrow. Default = false
                            //disableArrowRight={true}
                            // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
                            //disableAllTouchEventsForDisabledDays={true}
                            // Replace default month and year title with custom one. the function receive a date as parameter.
                            //renderHeader={(date) => 
                             Enable the option to swipe between months. Default = false
                            enableSwipeMonths={true}
                            style={{
                                height: 340,
                                width: '95%',
                                alignSelf: 'center'
                            }}
                            theme={{
                                //     backgroundColor: 'red',
                                //     calendarBackground: 'white',
                                //     textSectionTitleColor: '#b6c1cd',
                                //     textSectionTitleDisabledColor: '#d9e1e8',
                                selectedDayBackgroundColor: AppColor.appColor,
                                //     selectedDayTextColor: 'green',
                                //     todayTextColor: 'white',
                                //     dayTextColor: '#2d4150',
                                //     textDisabledColor: '#d9e1e8',
                                //     dotColor: '#00adf5',
                                //     selectedDotColor: 'red',
                                arrowColor: AppColor.appColor,
                                //     disabledArrowColor: '#d9e1e8',
                                //     monthTextColor: 'blue',
                                //     indicatorColor: 'blue',
                                //     //textDayFontFamily: 'monospace',
                                //     //textMonthFontFamily: 'monospace',
                                //     //textDayHeaderFontFamily: 'monospace',
                                //     textDayFontWeight: '300',
                                //     textMonthFontWeight: 'bold',
                                //     textDayHeaderFontWeight: '300',
                                //     textDayFontSize: 16,
                                //     textMonthFontSize: 16,
                                //     textDayHeaderFontSize: 16,
                                //     'stylesheet.day.basic':{
                                //         today: {
                                //             backgroundColor: 'red',
                                //             borderRadius: 25
                                //           },
                                //     }
                            }}

                            markedDates={dates}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 0.23 }}>
                        <View>
                            <Text style={{ fontSize: 16 }}>Check-in</Text>
                            <Text style={{ fontSize: 20, color: AppColor.appColor }}>{moment(dateArray[0]).format('DD MMM')}</Text>
                            <Text style={{ color: 'grey' }}>From 12pm</Text>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={AppImage.arrow_green}
                                style={{ width: 18, height: 15, resizeMode: 'contain', transform: [{ rotate: '180deg' }] }}
                            />
                            <Text style={{ fontSize: 16, textAlign: 'center', color: AppColor.appColor, marginTop: 5 }}>{dateArray.length > 1 ? moment(dateArray[dateArray.length - 1]).diff(dateArray[0], 'days') : ' '} Night</Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 16 }}>Check-out</Text>
                            <Text style={{ fontSize: 20, color: AppColor.appColor }}>{dateArray.length > 1 ? moment(dateArray[dateArray.length - 1]).format('DD MMM') : ''}</Text>
                            <Text style={{ color: 'grey' }}>Before 10am</Text>
                        </View>
                    </View>
                </View> */}
                    <View style={styles.smallCard}>
                        <Text style={{ textAlign: 'center', fontSize: 18, color: AppColor.appColor, marginTop: 10 }}>Additional</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 }}>
                            <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                                <Image source={AppImage.bed_icon}
                                    style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                />
                                <Text style={{
                                    fontSize: 16, color: AppColor.appColor,
                                    fontFamily: AppFonts.light, marginLeft: 10
                                }}>Extra Bed</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginRight: 20 }}>
                                <TouchableOpacity onPress={() => setbedCount(bedCount - 1)}
                                    disabled={bedCount <= 0}
                                >
                                    <Text style={{ fontSize: 18, bottom: 5 }}>_</Text>
                                </TouchableOpacity>
                                <Text style={{ fontSize: 18, marginRight: 10, marginLeft: 10 }}>{bedCount}</Text>
                                <TouchableOpacity onPress={() => setbedCount(bedCount + 1)}>
                                    <Text style={{ fontSize: 18 }}>+</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ alignSelf: 'center', width: '70%', height: 1, backgroundColor: AppColor.appColor, marginTop: 20 }} />
                    </View>
                    <TouchableOpacity style={{
                        backgroundColor: AppColor.appColor, width: '50%', alignSelf: 'center',
                        alignItems: 'center', borderRadius: 10, marginBottom: 20
                    }} activeOpacity={0.6} onPress={() => ApiFetch()}>
                        <Text style={{ color: 'white', fontSize: 18, margin: 7 }}>Book My Room</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <Loader loading={loader}/>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        height: 440,
        width: '95%',
        backgroundColor: 'white',
        elevation: 5,
        alignSelf: 'center',
        marginTop: 10,
        borderRadius: 20,
        justifyContent: 'space-around'
        //alignItems:'center'

    },
    smallCard: {
        width: '100%',
        height: 150,
        alignSelf: 'center',
        elevation: 5,
        backgroundColor: 'white',
        marginTop: 10,
        borderRadius: 15, marginBottom: 20
    },
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#ffffff',
        padding: 16,
    },
})
export default CalendarScren;