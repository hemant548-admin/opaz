import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, StyleSheet, Pressable
} from 'react-native';
import RazorpayCheckout from 'react-native-razorpay';
import Header from '../../../component/header/Header';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import { useDispatch, useSelector } from 'react-redux'
import { simplePostCall } from '../../../webservice/APIServices';
import APIStrings from '../../../webservice/APIStrings';
import AppStyles from '../../../utils/AppStyles';
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { Picker } from '@react-native-community/picker';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';

const { width, height } = Dimensions.get('window');
const arr = [{ id: '1', name: 'Male' }, { id: '2', name: 'Female' }, { id: '3', name: 'Other' }]

const TravellerProfile = ({ navigation, route }) => {
    const [fullName, setFullName] = useState('');
    const [gender, setGender] = useState('Select gender');
    const [age, setAge] = useState('');
    const [mobile, setMobile] = useState('');
    const [idProof, setIdProof] = useState('');
    const [email, setEmail] = useState('');
    const [addr1, setAddr1] = useState('');
    const [addr2, setaddr2] = useState('');
    const [landmark, setLandmark] = useState('');
    const [pincode, setPincode] = useState('');
    const [loader, setLoader] = useState(false);
    const [isCancel, setIsCancel] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [invalid, setInvalid] = useState([false, false, false, false]);
    const [error, setError] = useState('*required');
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [isDate, setisDate] = useState(false);
    const [phone, setPhone] = useState('');
    const [selected, setSelected] = useState(null)
    const { roomId, branchId, userId, price, type } = route.params;

    const data = useSelector(state => state.requestData)
    const extrabed = useSelector(state => state.extraBed)
    const checkIn = useSelector(state => state.checkIn)
    const checkOut = useSelector(state => state.checkOut)

    useEffect(async () => {
        const phon = await AsyncStorage.getItem(AppStrings.PHONE, '');
        setPhone(phon);
    }, [])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        console.log('event ', event.type)
        setShow(Platform.OS === 'ios');
        if (event.type === 'set') {
            //console.log('event ')
            setisDate(true)
            
            setDate(currentDate);
        }


        //setShow(false)
    };
    const showDatepicker = () => {
        setShow(true);
    };
    const FetchingData = () => {
        if (fullName.trim().length === 0) {

            setInvalid([true, false, false, false])
            return
        }
        if (gender.trim().length === 0) {

            setInvalid([false, true, false, false])
            return
        }
        if (mobile.trim().length === 0 || mobile.trim().length < 10) {

            setInvalid([false, false, true, false])
            return
        }
        if (addr1.trim().length === 0) {

            setInvalid([false, false, false, true])
            return
        }

        setLoader(true)
        let requsetBody = JSON.stringify({
            full_name: fullName,
            gender: gender,
            date_of_birth: date,
            mobile: mobile,
            email: email,
            id_proof_name: idProof,
            id_proof_number: '',
            landmark: landmark,
            pincode: pincode,
            room_id: roomId,
            checkin_date: checkIn,
            check_out_date: checkOut,
            extra_bed: extrabed,
            user_id: userId,
            branch_id: branchId,
            address: addr1,
            amount: price,

        })
        console.log(APIStrings.EndUserBookForOther);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserBookForOther, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    //setBookingId(data.Booking_Obj.id)
                    console.log("success", JSON.stringify(data));
                    if (type === 'Pay Now') {
                        paymentGetway(data)
                    }
                    else {
                        BookandPayatHotel(data)
                    }

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const paymentGetway = (value) => {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: data.razor_pay_key_for_app, // Your api key
            amount: String(value.payment.amount),
            order_id: value.payment.id,
            name: 'foo',
            prefill: {
                email: 'void@razorpay.com',
                contact: '9191919191',
                name: 'Razorpay Software'
            },
            theme: { color: AppColor.appColor }
        }
        RazorpayCheckout.open(options).then((data) => {
            //handle success
            afterPayment(value.Booking.id)
            //alert(`Success: ${data.razorpay_payment_id}`);
        }).catch((error) => {
            // handle failure
            toggleCancelModal()
            //alert(`Error: ${error.code} | ${error.description}`);
        });
    }
    const afterPayment = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value,


        })
        console.log(requsetBody)
        console.log(APIStrings.RazoyRoomPaySucess)
        simplePostCall(APIStrings.RazoyRoomPaySucess, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("Razorpay", JSON.stringify(data1));
                    navigation.navigate('ChooseRoom')
                    toggleSuccessModal()
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const BookandPayatHotel = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value.Booking.id,
            login_user_mobile_number: phone
        })
        console.log(requsetBody)
        console.log(APIStrings.BookAndPayAtHotel)
        simplePostCall(APIStrings.BookAndPayAtHotel, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("BookandPayatHotel", JSON.stringify(data1));
                    navigation.navigate('ChooseRoom')
                    toggleSuccessModal()
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const toggleCancelModal = () => {
        setIsCancel(!isCancel);
    }
    const toggleSuccessModal = () => {
        setIsSuccess(!isSuccess);
    }
    const onGenderPress = (id,val) => {
        console.log('id ', id,val.name)
        if(id === selected){
            setSelected(null);
            setGender('');
        }
        else {
            setSelected(id);
            //let tmp = finalPrice * Number(val)/100;
             setGender(val.name);
        }
    }
        
    const cancelModal = () => {
        return (
            <View>
                <Modal isVisible={isCancel}
                    onBackdropPress={() => setIsCancel(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '30%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your Transaction is not Completed!"} </Text>
                                {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text> */}
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleCancelModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const successModal = () => {
        return (
            <View>
                <Modal isVisible={isSuccess}
                    onBackdropPress={() => setIsSuccess(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '50%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your room has been booked\n successfully."} </Text>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text>
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleSuccessModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    return (
        <View style={{ flex: 1 }}>
            <Header
                title="Traveller Profile"
                goBack={() => navigation.goBack()}
            />
            <Text style={{ fontSize: 16, marginTop: 15, marginLeft: 15, marginBottom: 5 }}>Provide the below details for confirm booking</Text>
            <ScrollView>
                <View style={{ alignItems: 'center', marginTop: 10, backgroundColor: 'white' }}>
                    <TextInput style={[styles.mobileContainer, { marginBottom: 5 }]}
                        placeholder='Full Name'
                        value={fullName}
                        onChangeText={(value) => { setFullName(value), setInvalid(!invalid[0]) }}
                    />
                    {invalid[0] && <Text style={[AppStyles.apiErrorStyle, { alignSelf: 'flex-end' }]}>{error}</Text>}
                    {/* <TextInput style={styles.mobileContainer}
                        placeholder='Gender'
                        value={gender}
                        onChangeText={(value) => { setGender(value), setInvalid(!invalid[1]) }}
                    /> */}
                    {/* <View style={{
                        width: '90%', height: 45,
                        marginTop: 10,
                        borderRadius: 10,
                        borderWidth: 1,
                        //paddingLeft: 10,
                        //justifyContent:'center',
                        //textAlign:'center',
                        //color: AppColor.appColor,
                        borderColor: AppColor.appColor,
                    }}> */}
                    <Text style={{ alignSelf: 'flex-start', marginLeft: 20, marginTop: 10, marginBottom: 5 }}>Select Gender</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '97%' }}>
                        {arr.map((item, i) => {
                            return (
                                <Pressable style={{
                                    width: '25%', borderWidth: 1, borderColor: AppColor.appColor,
                                    alignItems: 'center', justifyContent: 'center', borderRadius: 7,backgroundColor:selected===i?AppColor.appColor:'white'
                                }} onPress={()=>onGenderPress(i,item)}>
                                    <Text style={{ margin: 7, color: selected!==i?AppColor.appColor:'white' }}>{item.name}</Text>
                                </Pressable>
                            )
                        })}
                    </View>
                    {/* </View> */}
                    {invalid[1] && <Text style={[AppStyles.apiErrorStyle, { alignSelf: 'flex-end' }]}>{error}</Text>}
                    <Pressable style={{
                        width: '90%', height: 45,
                        marginTop: 10,
                        borderRadius: 10,
                        borderWidth: 1,
                        paddingLeft: 10,
                        justifyContent: 'center',
                        //textAlign:'center',
                        //color: AppColor.appColor,
                        borderColor: AppColor.appColor,
                    }} onPress={() => showDatepicker()}>
                        <Text style={{ color: isDate ? AppColor.appColor : '#a2a2a6' }}>{isDate ? moment(date).format('DD-MM-YYYY') : "DOB"}</Text>
                    </Pressable>
                    {/* <TextInput style={styles.mobileContainer}
                        placeholder='Age/DOB'
                        value={age}
                        onChangeText={(value) => { setAge(value) }}
                    /> */}
                    {show && (
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={new Date}
                            mode={'date'}
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                        />
                    )}
                    {/* <View style={{width:width-40,borderWidth:1,borderColor:AppColor.appColor,marginTop:10,marginBottom:10}}>
                    <Picker
                        selectedValue={gender}
                        //style={{ color: gender !=='Select gender' ? AppColor.appColor : '#a2a2a6', }}
                        //mode='dropdown'
                        onValueChange={(itemValue, itemIndex) =>
                            setGender(itemValue)
                        }>
                        <Picker.Item label="Select gender" value="Select gender" />
                        <Picker.Item label="Male" value="Male" />
                        <Picker.Item label="Female" value="Female" />
                        <Picker.Item label="Other" value="Other" />
                    </Picker>
                    </View> */}
                    <TextInput style={styles.mobileContainer}
                        placeholder='Mobile Number'
                        value={mobile}
                        maxLength={10}
                        onChangeText={(val) => {
                            setMobile(val)
                            setInvalid(!invalid[2])
                            if (isNaN(val)) {
                                setError('Please enter digit')
                                setInvalid([false, false, true, false]);
                            }
                            else {
                                setInvalid([false, false, false, false]);
                            }
                        }
                        }
                    />
                    {invalid[2] && <Text style={[AppStyles.apiErrorStyle, { alignSelf: 'flex-end' }]}>{error}</Text>}
                    <TextInput style={styles.mobileContainer}
                        placeholder='Email Id'
                        value={email}
                        onChangeText={(value) => { setEmail(value) }}
                    />
                    <TextInput style={styles.mobileContainer}
                        placeholder='ID proof'
                        value={idProof}
                        onChangeText={(value) => { setIdProof(value) }}
                    />
                    <TextInput style={styles.mobileContainer}
                        placeholder='Address line1'
                        value={addr1}
                        onChangeText={(value) => { setAddr1(value), setInvalid(!invalid[3]) }}
                    />
                    {invalid[3] && <Text style={[AppStyles.apiErrorStyle, { alignSelf: 'flex-end' }]}>{error}</Text>}
                    <TextInput style={styles.mobileContainer}
                        placeholder='Address line2'
                        value={addr2}
                        onChangeText={(value) => { setaddr2(value) }}
                    />
                    <TextInput style={styles.mobileContainer}
                        placeholder='Landmark'
                        value={landmark}
                        onChangeText={(value) => { setLandmark(value) }}
                    />
                    <TextInput style={[styles.mobileContainer, { marginBottom: 10 }]}
                        placeholder='Pincode'
                        value={pincode}
                        onChangeText={(value) => { setPincode(value) }}
                    />
                </View>
            </ScrollView>
            <View style={{
                width: '100%', height: '8%', backgroundColor: AppColor.appColor
                , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center'
            }}>
                <Pressable style={{ backgroundColor: 'white', alignItems: 'center', borderRadius: 20, }}
                    onPress={() => FetchingData()}
                >
                    <Text style={{ margin: 6, marginLeft: 12, marginRight: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.medium }}>Save and Continue</Text>
                </Pressable>
            </View>
            {successModal()}
            {cancelModal()}
        </View>
    )
}

const styles = StyleSheet.create({

    mobileContainer: {
        //backgroundColor:'red',
        //arginLeft:20,
        //marginRight:20,
        width: '90%',
        height: 45,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        paddingLeft: 10,
        //textAlign:'center',
        color: AppColor.appColor,
        borderColor: AppColor.appColor,
    },
})

export default TravellerProfile;