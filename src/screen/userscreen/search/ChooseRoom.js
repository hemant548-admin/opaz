import AsyncStorage from '@react-native-community/async-storage';
import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, StyleSheet, Pressable
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux'
import RazorpayCheckout from 'react-native-razorpay';
import Header from '../../../component/header/Header';
import { checkLogin, checkScreen } from '../../../redux/reducer';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import AppStrings from '../../../utils/AppStrings';
import Loader from '../../../component/loader/Loader';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';

const { width, height } = Dimensions.get('window')

const arr = [{
    id: '1',
    title: 'Deluxe Twin Room', guest: '2 adult+1 child(5 year)', isCancellation: true,
    facility: 'Free wifi, Flat TV ,AC, Private pool', breakfast: 'included', bed: 'Twin size',
}, {
    id: '2',
    title: 'Deluxe Twin Room', guest: '2 adult+1 child(5 year)', isCancellation: true,
    facility: 'Free wifi, Flat TV ,AC, Private pool', breakfast: 'included', bed: 'Twin size',
}]

const ChooseRoom = ({ navigation, route }) => {

    const [isModalVisible, setModalVisible] = useState(false);
    const [isFilterVisible, setFilterVisible] = useState(false);
    const [btn, setBtn] = useState(null);
    const [role, setRole] = useState('')
    const [userId, setUserId] = useState('');
    const [phone, setPhone] = useState('');
    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [room, setRoom] = useState([]);
    const [roomId, setRoomId] = useState('');
    const [branchId, setbranchId] = useState('');
    const [price, setPrice] = useState('');
    const [isSuccess, setIsSuccess] = useState(false);
    const [bookingId, setBookingId] = useState('');
    const [loader, setLoader] = useState(false)
    const [isOfferVisible, setOfferVisible] = useState(false)
    const [selected, setSelected] = useState(null)
    const [finalPrice, setFinalPrice] = useState(null);
    const [isCancel, setIsCancel] = useState(false);
    const [adultCount, setAdultCount] = useState(2);
    const [childCount, setChildCount] = useState(0);
    const [bedCount, setBedCount] = useState(1);
    const [roomData, setRoomData] = useState([]);
    const [type, setType] = useState('');
    const [isTick, setIsTick] = useState(false);
    const [discountPrice, setDiscountPrice] = useState(null);
    //const {data}=route.params;
    const data = useSelector(state => state.requestData)
    const checkIn = useSelector(state => state.checkIn)
    const checkOut = useSelector(state => state.checkOut)

    const dispatch = useDispatch()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setRoomData(data.Room)
            setBtn(null)
        });
        userData();

        return unsubscribe;
       
    },[navigation,data])
    const userData = async () => {
        const role = await AsyncStorage.getItem(AppStrings.ROLE, '')
        const id = await AsyncStorage.getItem(AppStrings.USER_ID, '')
        const fName = await AsyncStorage.getItem(AppStrings.FIRST_NAME, '');
        const lName = await AsyncStorage.getItem(AppStrings.LAST_Name, '')
        const eml = await AsyncStorage.getItem(AppStrings.EMAIL, '');
        const mobile = await AsyncStorage.getItem(AppStrings.PHONE, '');
        const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, '');

        setRole(role);
        setUserId(id);
        setPhone(mobile);
        setfirstName(fName);
        setlastName(lName);
        setEmail(eml);
        setAddress(addr);
    }
    // const ApiFetch=()=>{

    //     //setLoader(true)
    //     let requestbody=JSON.stringify({
    //         checkin_date:dateArray[0] , 
    //         check_out_date:dateArray.length>1?dateArray[dateArray.length-1]:moment(dateArray[0]).add(1,'days').format('YYYY-MM-DD') , 
    //         extra_bed:bedCount , 
    //         branch_id:id
    //     })

    //     simplePostCall(APIStrings.EndUserRoomFilerByBookingDate,requestbody)
    //     .then((data) => {

    //         setLoader(false)
    //         //console.log(data.token);
    //         if (data.status.code === 200) {
    //             console.log("success", JSON.stringify(data));

    //         }
    //         else {
    //             //this.setState({ invalid: true, errorMessage: data.message });
    //             console.log(data.status.message)

    //         }

    //     }).catch((error) => {
    //         console.log("error ", error);
    //         // setLoader(false)
    //         alert('api error')
    //         //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
    //     });
    // }
    const toggleModal = () => {
        if (role === 'EndUser') {
            setModalVisible(!isModalVisible);
        }
        else {
            dispatch(checkLogin(1, true, 'ChooseRoom'));
            dispatch(checkScreen('Login'))
            navigation.navigate('Login');
        }
    };
    const toggleFilter = () => {
        setFilterVisible(!isFilterVisible)
    }
    const toggleButton = (index, item,) => {
        if (index === btn) {
            setBtn(null)
            setRoomId('')
            setbranchId('')
            setPrice('');
        }
        else {
            setBtn(index)
            setRoomId(item.id)
            setbranchId(item.branch)
            setPrice(item.final_price);
            setDiscountPrice(item.final_price)
        }

    }
    const FilterApi = () => {

        setLoader(true)

        let requsetBody = JSON.stringify({
            branch_id: data.Room[0].branch,
            adults: adultCount,
            children: childCount,
            beds: bedCount,
            breakfast: isTick ? 'breakfast' : ''
        })
        console.log(APIStrings.EndUserOnlyRoomFilterByFacility);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserOnlyRoomFilterByFacility, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    setRoomData(data.Room)
                    console.log("success", JSON.stringify(data));
                    toggleFilter();

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)
                    toggleFilter();
                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                toggleFilter();
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const FetchingData = () => {
        if (roomId === '') {
            alert("Please select Room")
            return
        }
        setLoader(true)
        toggleModal();

        let requsetBody = JSON.stringify({
            room_id: roomId,
            branch_id: branchId,
            user_id: userId,
            email: email,
            first_name: firstName,
            last_name: lastName,
            phone_number: phone,
            amount: discountPrice,
            checkin_date: checkIn,
            check_out_date: checkOut
        })
        console.log(APIStrings.EndUserBookForSelf);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserBookForSelf, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    setBookingId(data.Booking_Obj.id)
                    console.log("success", JSON.stringify(data));
                    console.log("type", type)
                    if (type === 'Pay Now') {
                        paymentGetway(data)
                    }
                    else {
                        BookandPayatHotel(data)
                    }

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const paymentGetway = (value) => {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: data.razor_pay_key_for_app, // Your api key
            amount: String(value.amount),
            order_id: value.payment.id,
            name: 'foo',
            prefill: {
                email: 'void@razorpay.com',
                contact: '9191919191',
                name: 'Razorpay Software'
            },
            theme: { color: AppColor.appColor }
        }
        RazorpayCheckout.open(options).then((data) => {
            //handle success
            afterPayment(value.Booking_Obj.id)
            console.log(`Success: ${data.razorpay_payment_id}`);
        }).catch((error) => {
            // handle failure
            toggleCancelModal()
            //alert(`Error: ${error.code} | ${error.description}`);
        });
    }
    const afterPayment = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value,
            mobile: phone
        })
        console.log(requsetBody)
        console.log(APIStrings.RazoyRoomPaySucess)
        simplePostCall(APIStrings.RazoyRoomPaySucess, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("Razorpay", JSON.stringify(data1));
                    toggleSuccessModal()
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const BookandPayatHotel = (value) => {

        let requsetBody = JSON.stringify({
            booking_id: value.Booking_Obj.id
        })
        console.log(requsetBody)
        console.log(APIStrings.BookAndPayAtHotel)
        simplePostCall(APIStrings.BookAndPayAtHotel, requsetBody)
            .then((data1) => {

                //setLoader(false)
                //console.log(data.token);
                if (data1.status.code === 200) {
                    console.log("BookandPayatHotel", JSON.stringify(data1));
                    toggleSuccessModal()
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data1.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                // setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const onAdultPress = (value) => {
        let temp = [];
        if (value === 'Plus') {
            setAdultCount(adultCount + 1);
        }
        else {
            setAdultCount(adultCount - 1);
        }

    }
    const onChildPress = (value) => {
        let temp = [];
        if (value === 'Plus') {
            setChildCount(childCount + 1);
        }
        else {
            setChildCount(childCount - 1);
        }

    }
    const onBedPress = (value) => {
        let temp = [];
        if (value === 'Plus') {
            setBedCount(bedCount + 1);
        }
        else {
            setBedCount(bedCount - 1);
        }

    }
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.openDrawer());
    };
    const toggleCancelModal = () => {
        setIsCancel(!isCancel);
    }
    const toggleSuccessModal = () => {
        setIsSuccess(!isSuccess);
    }
    const toggleOfferModal = (price) => {
        setOfferVisible(!isOfferVisible)
        setDiscountPrice(price)
    }
    const onOfferPress = (id) => {
        if (id === selected) {
            setSelected(null);
            //let tmp=finalPrice *0.5
            setDiscountPrice(finalPrice);

        }
        else if (id === 1) {
            setSelected(id);
            let tmp = finalPrice * 0.5
            setDiscountPrice(finalPrice - tmp);

        }
        else if (id === 2) {
            setSelected(id);
            let tmp = finalPrice * 0.3
            setDiscountPrice(finalPrice - tmp);

        }
    }
    const cancelModal = () => {
        return (
            <View>
                <Modal isVisible={isCancel}
                    onBackdropPress={() => setIsCancel(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '30%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your Transaction is not Completed!"} </Text>
                                {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text> */}
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleCancelModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const successModal = () => {
        return (
            <View>
                <Modal isVisible={isSuccess}
                    onBackdropPress={() => setIsSuccess(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width - 30, height: '50%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{}}>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your room has been booked\n successfully."} </Text>
                                <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text>
                            </View>
                            <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                                onPress={() => toggleSuccessModal()}
                            >
                                <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                            </Pressable>
                            {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                onPress={()=>FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View> */}

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const renderModal = () => {
        return (
            <View>
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width, height: '40%', backgroundColor: AppColor.appColor, alignItems: 'center', }}>
                            <Text style={{ fontSize: 22, margin: 10, color: 'white', marginTop: 20 }}>This Booking is for whom ?</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                                <Pressable style={{ alignItems: 'center' }}
                                    onPress={() => FetchingData()}
                                >
                                    <View style={styles.circle}>
                                        <Image source={AppImage.myselfBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                                </Pressable>
                                <View style={{ alignItems: 'center' }}>
                                    <Pressable style={styles.circle}
                                        onPress={() => {
                                            navigation.navigate('Traveller', { roomId: roomId, branchId: branchId, userId: userId, price: price, type: type }), toggleModal()
                                        }}
                                    >
                                        <Image source={AppImage.otherBooking_icon}
                                            style={styles.modalImg}
                                        />
                                    </Pressable>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }

    const renderFilterModal = () => {
        return (
            <View >
                <Modal isVisible={isFilterVisible}
                    onBackdropPress={() => setFilterVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{
                            width: width, height: height * 0.6, backgroundColor: 'white', alignSelf: 'center', top: 20,
                            borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopWidth: 1, borderTopColor: AppColor.appColor,
                        }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginTop: 50 }}>
                                <View>
                                    <Text style={{ fontSize: 18 }}>Adults</Text>
                                    <Text style={{ color: 'grey' }}>(5+ years)</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                                    , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor
                                }}>
                                    <Text style={{ bottom: 5, fontSize: 20, margin: 5,color:adultCount<=1?'grey':'black' }} onPress={() => onAdultPress('Minus')} disabled={adultCount === 1 ? true : false}>_</Text>
                                    <Text style={{ fontSize: 20, margin: 5 }}>{adultCount}</Text>
                                    <Text style={{ fontSize: 20, margin: 5, color: adultCount >= 4 ? 'grey' : 'black' }} onPress={() => onAdultPress('Plus')} disabled={adultCount >= 4 ? true : false}>+</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginTop: 50 }}>
                                <View>
                                    <Text style={{ fontSize: 18 }}>Children</Text>
                                    <Text style={{ color: 'grey' }}>(0-5 years)</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                                    , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor
                                }}>
                                    <Text style={{ bottom: 5, fontSize: 20, margin: 5, color: childCount <= 0 ? 'grey' : 'black' }} onPress={() => onChildPress('Minus')} disabled={childCount === 0 ? true : false}>_</Text>
                                    <Text style={{ fontSize: 20, margin: 5 }}>{childCount}</Text>
                                    <Text style={{ fontSize: 20, margin: 5, color: childCount >= 4 ? 'grey' : 'black' }} onPress={() => onChildPress('Plus')} disabled={childCount >= 4 ? true : false}>+</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginTop: 50 }}>
                                <View>
                                    <Text style={{ fontSize: 18 }}>Beds</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                                    , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor, marginLeft: 15
                                }}>
                                    <Text style={{ bottom: 5, fontSize: 20, margin: 5, color: bedCount <= 1 ? 'grey' : 'black' }} onPress={() => onBedPress('Minus')} disabled={bedCount === 1 ? true : false}>_</Text>
                                    <Text style={{ fontSize: 20, margin: 5 }}>{bedCount}</Text>
                                    <Text style={{ fontSize: 20, margin: 5, color: bedCount >= 4 ? 'grey' : 'black' }} onPress={() => onBedPress('Plus')}
                                        disabled={bedCount >= 4 ? true : false}
                                    >+</Text>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 20, marginLeft: 30, alignItems: 'center' }}>
                                <Pressable style={{ width: 28, height: 28, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center' }} onPress={() => setIsTick(!isTick)}>
                                    {isTick &&
                                        <Image source={AppImage.tickmark_icon}
                                            style={{ width: 28, height: 28, resizeMode: 'contain' }}
                                        />}
                                </Pressable>
                                <Text style={{ fontSize: 18, marginLeft: 10 }}>Breakfast included</Text>
                            </View>
                            <Pressable style={styles.btnContainer1} onPress={() => FilterApi()}>
                                <Text style={{ fontSize: 18, color: 'white' }}>Apply Filter</Text>
                            </Pressable>

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const OfferModal = () => {
        return (
            <View >
                <Modal isVisible={isOfferVisible}
                    onBackdropPress={() => setOfferVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{
                            width: width, height: height * 0.25, backgroundColor: 'white', alignSelf: 'center', top: 20,
                        }}>
                            <View style={{ alignItems: 'center', }}>
                                <TouchableOpacity style={{
                                    width: '100%', elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onOfferPress(1)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === 1 ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize: 20, marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10 }}>Apply 50% ofF for first time booking</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ alignItems: 'center', }}>
                                <TouchableOpacity style={{
                                    width: '100%', height: 50, elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onOfferPress(2)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === 2 ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize: 20, marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10 }}>Apply 30% off with FESTIVAL coupon</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, marginLeft: 10 }}>Price to Pay</Text>
                                <Text style={{ fontSize: 18, marginRight: 10, fontFamily: AppFonts.medium }}>₹{discountPrice}</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const renderRoomList = (item, index) => {
        return (
            <View style={styles.card}>
                <Text style={styles.titleTxt}>{item.room_type} Room</Text>
                <View style={styles.row}>
                    <Image source={item.cancellation_status === 'ancellation_fees_applicable' ?AppImage.cancel_icon:AppImage.free_icon}
                        style={styles.img}
                    />
                    <Text style={[styles.txt, { color: item.cancellation_status === 'ancellation_fees_applicable' ? 'red' : '#02C115' }]}>{item.cancellation_status === 'ancellation_fees_applicable' ? 'Cancellation fees Applicable' : 'Free Cancellation'}</Text>
                </View>
                <View style={styles.row}>
                    <Image source={AppImage.branchName_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{item.adult} adults {item.children} child(0-5 year)</Text>
                </View>
                {item.facility.map((item) => {
                    return (
                        <View style={styles.row}>
                            <Image source={item.name === 'free wifi' ? AppImage.time_icon :
                                item.name === 'lunch' ? AppImage.restaurant_icon : AppImage.cardpay_icon}
                                style={styles.img}
                            />
                            <Text style={styles.txt}>{item.name}</Text>
                        </View>
                    )
                })

                }
                {/* <View style={styles.row}>
                    <Image source={null}
                        style={styles.img}
                    />
                    <Text style={{ fontSize: 16, color: "#02C115" }}>Free cancellation <Text style={{ color: 'grey', fontSize: 14, }}>  before 5May 2021</Text></Text>
                </View>
                <View style={styles.row}>
                    <Image source={AppImage.time_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{item.facility}</Text>
                </View>
                <View style={styles.row}>
                    <Image source={AppImage.restaurant_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>Breakfast {item.breakfast}</Text>
                </View>
                <View style={styles.row}>
                    <Image source={AppImage.bed_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{item.bed}</Text>
                </View>
                <View style={styles.row}>
                    <Image source={AppImage.cardpay_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>Pay at the property</Text>
                </View> */}
                <View style={{ alignSelf: 'flex-end', alignItems: 'center', marginBottom: 20, marginRight: 10 }}>
                    {/* <Text>Price for 3 Nights</Text> */}
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginRight: 10,fontFamily:AppFonts.regular }}>
                        <Text style={{ textDecorationLine: 'line-through', color: 'red', fontSize: 14, fontWeight: '300' }}>Rs. {item.price}   </Text>Rs. {item.final_price}</Text>
                    <Text style={{ color: 'grey' }}>+ tax and charges</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                    <TouchableOpacity style={{
                        width: '40%', borderWidth: 1, borderColor: AppColor.appColor, alignSelf: 'center',
                        alignItems: 'center', marginBottom: 10, backgroundColor: btn === index ? AppColor.appColor : 'white'
                    }}
                        onPress={() => navigation.navigate('RoomDetail',{item:item})} activeOpacity={0.6}
                    >
                        <Text style={{ fontSize: 18, margin: 10, color: btn === index ? 'white' : AppColor.appColor }}>{btn === index ? 'Reserved' : 'Reserve Now'}</Text>
                    </TouchableOpacity>
                    {btn === index &&
                        <Pressable style={{ width: '30%', borderWidth: 1, borderColor: '#ad8003', alignItems: 'center', justifyContent: 'center' }}
                            onPress={() => { setFinalPrice(item.final_price), toggleOfferModal(item.final_price) }}
                        >
                            <Text style={{ fontSize: 16, color: '#ad8003', margin: 7 }}>Apply offer</Text>
                        </Pressable>}
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <Header
                title="Choose Your room"
                goBack={() => navigation.goBack()}
            />
            <Pressable style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center', backgroundColor: 'white', justifyContent: 'space-around', height: 40, marginBottom: 5 }}
                onPress={() => toggleFilter()}
            >
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 16, }}>Filter</Text>
                    <Image source={AppImage.filter_icon}
                        style={{ width: 20, height: 20, resizeMode: 'contain', marginLeft: 5 }}
                    />
                </View>
                <View>
                    <Text style={styles.filterTxt}>Breakfast included</Text>
                </View>
                <View style={{ width: 100, alignItems: 'center', borderLeftWidth: 1, borderRightWidth: 1 }}>
                    <Text style={styles.filterTxt}>2 adults</Text>
                </View>
                <View>
                    <Text style={styles.filterTxt}>1 Bed</Text>
                </View>
            </Pressable>
            <View style={{ flex: 1 }}>
                {roomData.length > 0 ?
                    <FlatList
                        data={roomData}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => renderRoomList(item, index)}
                    />
                    :
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: 'red' }}>No data Avilable</Text>
                    </View>}
            </View>
            {/* <View style={{
                width: '100%', height: '9%', backgroundColor: AppColor.appColor
                , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center'
            }}>
                <Pressable style={{ backgroundColor: 'white', alignItems: 'center', borderRadius: 20, }}
                    onPress={() => { toggleModal(), setType('Pay Hotel') }}
                >
                    <Text style={{ margin: 7, marginLeft: 12, marginRight: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.medium }}>Book and Pay at hotel</Text>
                </Pressable>

                <Pressable style={{ width: '30%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, marginLeft: 30 }}
                    onPress={() => { toggleModal(), setType('Pay Now') }}
                >
                    <Text style={{ margin: 7, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.medium }}>Pay Now</Text>
                </Pressable>
            </View> */}
            {renderModal()}
            {renderFilterModal()}
            {successModal()}
            {OfferModal()}
            {cancelModal()}
            <Loader loading={loader} />
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        width: '100%',
        backgroundColor: 'white',
        marginTop: 10
    },
    titleTxt: {
        fontSize: 20,
        color: AppColor.appColor,
        margin: 10
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 5, marginLeft: 10
    },
    img: {
        width: 20,
        height: 20,
        resizeMode: 'contain'
    },
    txt: {
        fontSize: 16,
        marginLeft: 10
    },
    modalImg: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    circle: {
        width: 80,
        height: 80,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: '#94FFE0',
        justifyContent: 'center'
    },
    filterTxt: {
        fontSize: 16,
        color: '#003022'
    },
    btnContainer1: {
        width: '40%',
        height: 40,
        backgroundColor: '#003727',
        borderRadius: 10,
        elevation: 3,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 10,
        //borderWidth:0.1,
        borderColor: AppColor.appColor
    },
})
export default ChooseRoom;