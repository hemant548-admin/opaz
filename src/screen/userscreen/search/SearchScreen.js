import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Image, TouchableOpacity, Dimensions, Animated,
    TextInput, ScrollView, StyleSheet, Pressable
} from 'react-native';
import Modal from 'react-native-modal';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Header from '../../../component/header/Header';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import MapView, { Marker } from 'react-native-maps';
import MyMapView from '../../../component/Map/MyMapView';
import LottieView from 'lottie-react-native';
import { getLocation, geocodeLocationByName } from '../../../component/services/location-service';
import APIStrings from '../../../webservice/APIStrings';
import { multipartPostCall, simplePostCall } from '../../../webservice/APIServices';
import Loader from '../../../component/loader/Loader';

const data1 = [{ latitude: 18.492335, longitude: 73.816304 }, { latitude: 18.92335, longitude: 73.316304 },
{ latitude: 18.722335, longitude: 73.616304 }, { latitude: 18.525053, longitude: 73.817918 }, { latitude: 18.536133, longitude: 73.820816 }
]
const arr = [{
    id: '1', hotelName: 'Maharaja Hotel', address: 'Pune, Maharashtra', rating: '4.2',
    review: '900', landmark: '2 km from station', price: 'Rs. 3200/Night', actualPrice: '5000', cancellation: true
}, {
    id: '2', hotelName: 'Hotel Burj Khalifa', address: 'Palm jumera, Dubai', rating: '4.9',
    review: '1000', landmark: '2 km from Airport', price: 'Rs. 100000/Night', actualPrice: '500000', cancellation: false
}, {
    id: '3', hotelName: 'Hotel Greenland', address: 'Pune, Maharashtra', rating: '3.2',
    review: '400', landmark: '2 km from station', price: 'Rs. 3200/Night', actualPrice: '5000', cancellation: true
},
]
const { width, height } = Dimensions.get('window')
const CARD_HEIGHT = 230;
const CARD_WIDTH = width * 0.8;
const SPACING_FOR_CARD_INSET = width * 0.1 - 10;
const cityArr = [{ id: "1", name: 'Pune' }, { id: "2", name: 'Mumbai' }, { id: "3", name: 'Bangalore' },
{ id: "4", name: 'Delhi' }
]
const facilityArr = [{ id: "1", name: 'Breakfast' }, { id: "2", name: 'Pool' }, { id: "3", name: 'Parking' },
{ id: "4", name: 'Wi-Fi' }
]
const SearchScreen = ({ navigation, route }) => {

    const [isModalVisible, setModalVisible] = useState(false);
    const [selectedCity, setSelectedCity] = useState(false);
    const [selectedMap, setSelectedMap] = useState(false);
    const [region, setRegion] = useState({});
    const [isSortVisible, setSortVisible] = useState(false)
    const [selected, setSelected] = useState(null)
    //const [hotelData, setHotelData]=useState({});
    const [loader, setLoader] = useState(false);
    const [minBudSelected, setMinBudSelected] = useState('0')
    const [maxBudSelected, setMaxBudSelected] = useState('50000')
    const [selectedIndex, setSelectedIndex] = useState(null)
    const [location, setLocation] = useState([]);
    const [places, setPlaces] = useState('');
    const [facility, setFacility] = useState('');
    //const [ArrayFacility,]

    const { date,dateTO, data } = route.params;
    const [hotelData, setHotelData] = useState(data);

    let mapIndex = 0;
    let mapAnimation = new Animated.Value(0);
    useEffect(() => {
        setHotelData(data)
        if (hotelData.length > 0) {
            getInitialState();
            mapAnimation.addListener(({ value }) => {
                let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
                if (index >= hotelData.length) {
                    index = hotelData.length - 1;
                }
                if (index <= 0) {
                    index = 0;
                }

                clearTimeout(regionTimeout);

                const regionTimeout = setTimeout(() => {
                    if (mapIndex !== index) {
                        mapIndex = index;
                        const lat = hotelData[index].latitude;
                        const lon = hotelData[index].longitude;
                        _map.current.animateToRegion(
                            {
                                ...lat,
                                ...lon,
                                latitudeDelta: state.region.latitudeDelta,
                                longitudeDelta: state.region.longitudeDelta,
                            },
                            350
                        );
                    }
                }, 10);
            });
        }
    }, [data])
    const interpolations = hotelData.map((marker, index) => {
        const inputRange = [
            (index - 1) * CARD_WIDTH,
            index * CARD_WIDTH,
            ((index + 1) * CARD_WIDTH),
        ];

        const scale = mapAnimation.interpolate({
            inputRange,
            outputRange: [1, 1.5, 1],
            extrapolate: "clamp"
        });
        //const color=AppColor.appColor
        //setSelectedIndex(index)
        return { scale };
    });

    const onMarkerPress = (mapEventData) => {
        const markerID = mapEventData._targetInst.return.key;

        let x = (markerID * CARD_WIDTH) + (markerID * 20);
        if (Platform.OS === 'ios') {
            x = x - SPACING_FOR_CARD_INSET;
        }

        _scrollView.current.scrollTo({ x: x, y: 0, animated: true });
    }
    const _map = React.useRef(null);
    const _scrollView = React.useRef(null);
    const getInitialState = () => {

        // console.log(data);
        setRegion({
            latitude: hotelData[0].latitude,
            longitude: hotelData[0].longitude,
            latitudeDelta: 0.07864195044303443,
            longitudeDelta: 0.065142817690068,
        })
        // this.setState({
        //     region: {
        //         latitude: data.latitude,
        //         longitude: data.longitude,
        //         latitudeDelta: 0.003,
        //         longitudeDelta: 0.003
        //     }
        // });

    }
    // getCoordsFromName=(loc)=> {
    //     this.setState({
    //         region: {
    //             latitude: loc.lat,
    //             longitude: loc.lng,
    //             latitudeDelta: 0.003,
    //             longitudeDelta: 0.003
    //         }
    //     });
    //     console.log('region', this.state.region)
    // }
    const onLocationSelect = (country) => {

        setPlaces(country)
        setModalVisible(true)
    }

    const onCountryCodePress = () => {
        //const { navigation } = this.props;
        toggleModal()
        navigation.navigate('SelectLocation', { onCountrySelect: (country) => onLocationSelect(country) });
    }
    const goToScreen = (item) => {
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: String(item)
        })

        console.log(APIStrings.EndUserRoomDetails)
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserRoomDetails, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    //setHotelData(data.Room)
                    navigation.navigate('Detail', { hotelData: Object.keys(data.Room).length > 0 ? data.Room : '',facility:data.amenities })
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }

    const applyFilter=()=>{
        setLoader(true)

        const formData=new FormData();
        formData.append('price_from_range', minBudSelected);
        formData.append('location', places);
        formData.append('price_to_range', maxBudSelected);
        formData.append('facility', facility);
        // let requsetBody = JSON.stringify({
        //     price_from_range:minBudSelected,
        //     location:places,
        //     price_to_range:maxBudSelected,
        //     facility:facility

        // })

        console.log(APIStrings.EndUserFilterResult)
        console.log(formData);

        multipartPostCall(APIStrings.EndUserFilterResult, formData)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setHotelData(data.Room_List)
                    setModalVisible(false)
                    
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const onMapRegionChange = (region) => {
        setRegion(region)
    }
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
        setMinBudSelected('0');
        setMaxBudSelected('50000')
    };
    const toggleSort = () => {
        setSortVisible(!isSortVisible)
    }
    const onMapPress = () => {
        setSelectedMap(true)

    }
    const onItemPress = (id,name) => {

        // let service = [...services];
        // for (let data of service) {
        //     if (data.id == id) {
        //         data.selected = (data.selected == null) ? true : !data.selected;
        //         break;
        //     }
        // }
        setSelectedCity(id);
        setFacility(name)

    }
    const goBack = () => {
        if (selectedMap) {
            setSelectedMap(false)
        }
        else {
            navigation.goBack();
        }
    }
    const onSortPress = (id) => {
        if (id === 1) {
            setSelected(id);
            let sort = hotelData.sort((a, b) => a.branch_name > b.branch_name)
            setHotelData(sort);
            toggleSort()
        }
        else {
            setSelected(id);
            let sort = hotelData.sort((a, b) => a.branch_name < b.branch_name)
            setHotelData(sort);
            toggleSort()
        }
    }
    //    const onSortIncrease=()=>{
    //         let sort=data.sort((a, b) => a.branch_name > b.branch_name)
    //         setHotelData(sort);
    //     }
    //     const onSortDecrease=()=>{
    //         let sort=data.sort((a, b) => a.branch_name < b.branch_name)
    //         setHotelData(sort);
    //     }
    const nonCollidingMultiSliderValuesChange = values => {
        setMinBudSelected(values[0]);
        setMaxBudSelected(values[1]);
        // this.setState({
        //   minBudSelected: values[0],
        //   maxBudSelected: values[1]
        // }, () => this.saveMinMaxBudget());  // Saving the min & max budget on changing the slider
        // this.props.setBudget(values[0], values[1]);
    };

    const renderCity = (item, index) => {
        return (
            <View>
                <TouchableOpacity style={[styles.btnContainer, { backgroundColor: selectedCity === index ? AppColor.appColor : 'white' }]}
                    onPress={() => onItemPress(index,item.name)}
                >
                    <Text style={{
                        color: selectedCity === index ? 'white' : AppColor.appColor,
                        fontSize: 16
                    }}>{item.name}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    const renderModal = () => {
        return (
            <View >
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{
                            width: width, height: height * 0.7, backgroundColor: 'white', alignSelf: 'center', top: 20,
                            borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopWidth: 2,
                        }}>
                            <Text style={{ marginTop: 25, marginLeft: 25, fontSize: 18 }}>Location</Text>
                            <View style={{ alignSelf: 'center', width: '100%', alignItems: 'center' }}>
                                <Pressable style={{ width: '88%', height: 40, justifyContent: 'center', borderWidth: 1, borderColor: AppColor.appColor, borderRadius: 5, marginTop: 10 }}
                                onPress={()=>onCountryCodePress()}
                                >
                                    <Text style={{ marginLeft: 10, color: 'grey' }}>{places!==''? places:"Select Location here"}</Text>
                                </Pressable>
                                {/* <FlatList
                                    data={cityArr}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    // backgroundColor={'red'}
                                    renderItem={({ item, index }) => renderCity(item, index)}
                                    keyExtractor={item => item.id}
                                /> */}
                            </View>
                            <View style={{ margin: 10, marginTop: 20 }}>
                                <Text style={{ marginLeft: 10, fontSize: 18 }}>Price Range</Text>
                                <View style={{ marginTop: 40 }}>
                                    <MultiSlider
                                        values={[
                                            0,
                                            10000
                                        ]}
                                        sliderLength={width - 100}
                                        onValuesChangeFinish={nonCollidingMultiSliderValuesChange}
                                        min={0}
                                        max={10000}
                                        step={50}
                                        allowOverlap={false}
                                        snapped
                                        //enableLabel
                                        minMarkerOverlapDistance={30}
                                        //customMarker={CustomMarker}
                                        selectedStyle={{
                                            backgroundColor: AppColor.appColor,
                                            borderWidth: 2,
                                            borderColor: AppColor.appColor
                                        }}
                                        unselectedStyle={{
                                            backgroundColor: 'silver',
                                            borderWidth: 2,
                                            borderColor: 'silver'
                                        }}
                                        containerStyle={{ alignItems: 'center', }}
                                    />
                                    <Text style={{ textAlign: 'center', fontSize: 18 }}>{minBudSelected} Rs - {maxBudSelected} Rs</Text>
                                </View>
                            </View>
                            <Text style={{ marginTop: 25, marginLeft: 20, fontSize: 18 }}>Facilities</Text>
                            <View style={{ marginTop: 8, marginLeft: 10 }}>
                                <FlatList
                                    data={facilityArr}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    // backgroundColor={'red'}
                                    renderItem={({ item, index }) => renderCity(item, index)}
                                    keyExtractor={item => item.id}
                                />
                            </View>
                            <Pressable style={styles.btnContainer1} onPress={()=>applyFilter()}>
                                <Text style={{ fontSize: 18, color: 'white' }}>Apply Filter</Text>
                            </Pressable>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const Sort = () => {
        return (
            <View >
                <Modal isVisible={isSortVisible}
                    onBackdropPress={() => setSortVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{
                            width: width, height: height * 0.18, backgroundColor: 'white', alignSelf: 'center', top: 20,

                        }}>
                            <View style={{ alignItems: 'center', }}>
                                <TouchableOpacity style={{
                                    width: '100%', height: 50, elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onSortPress(1)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === 1 ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize: 20, marginLeft: 20 }}>Sort (A-Z)</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ alignItems: 'center', }}>
                                <TouchableOpacity style={{
                                    width: '100%', height: 50, elevation: 2, marginTop: 5,
                                    backgroundColor: 'white', marginBottom: 5, flexDirection: 'row', alignItems: 'center'
                                }}
                                    activeOpacity={0.8}
                                    onPress={() => onSortPress(2)}
                                >
                                    <View style={{
                                        width: 18, height: 18, borderWidth: 1, borderColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center',
                                        borderRadius: 9, marginLeft: 15
                                    }}>
                                        <View style={{ backgroundColor: selected === 2 ? AppColor.appColor : 'white', width: 10, height: 10, borderRadius: 5 }} />
                                    </View>
                                    <Text style={{ fontSize: 20, marginLeft: 20 }}>Sort (Z-A)</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const renderList = (item, index) => {
        return (
            <Pressable style={styles.Card}
                onPress={() => goToScreen(item.id)}
            >
                <Image source={{ uri: item.branch_image[0].image }}
                    style={{ flex: 0.7, height: '100%' }}
                />
                <View style={styles.innercard}>
                    <Text style={{ fontSize: 18, color: AppColor.appColor }}>{item.branch_name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5,}}>
                        <Image source={AppImage.marker}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                        <Text style={{ color: AppColor.input_placeholderColor, marginLeft: 5,marginRight:22 }}>{item.first_address}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Image source={AppImage.star_orang}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                        <Text style={{ margin: 5, }}>{item.branch_room.length > 0 ? item.total_rating : ''}</Text>
                        <Text style={{ marginLeft: 20, fontSize: 11, color: 'grey' }}> review</Text>
                    </View>
                    <Text style={{ marginTop: 5, marginBottom: 5, color: AppColor.input_placeholderColor }}>{item.branch_room.length > 0 ? item.branch_room[0].landmark : ''}</Text>
                    <View style={{}}>
                        <Text style={{ marginTop: 5, marginBottom: 5, color: AppColor.appColor, fontWeight: 'bold' }}>{item.branch_room.length > 0 ? item.branch_room[0].final_price : ''}   <Text
                            style={{ color: 'grey', marginLeft: 30, textDecorationLine: 'line-through' }}>   {item.branch_room.length > 0 ? item.branch_room[0].price : ''}</Text></Text>
                        <Text style={{ color: AppColor.appColor, marginLeft: 80, fontFamily: AppFonts.light, fontSize: 11, bottom: 2 }}>+tax and charges</Text>
                    </View>
                    <View style={{ flexDirection: 'row',alignItems:'center' }}>
                        <Image source={item.cancellation_status === 'ancellation_fees_applicable' ?AppImage.free_icon:AppImage.cancel_icon}
                            style={{ width: 12, height: 12, resizeMode: 'contain',marginRight:5 }}
                        />
                        <Text style={{ marginTop: 5, marginBottom: 5, color: item.cancellation_status === 'ancellation_fees_applicable' ? '#02C115' : 'red',fontSize:12,marginRight:10 }}>{item.cancellation_status === 'ancellation_fees_applicable' ? "Free Cancellation" : "Cancellation fees applicable"}</Text>
                    </View>
                </View>
            </Pressable>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <Header
                title={date+" - "+dateTO}
                goBack={() => goBack()}
            />
            {hotelData.length > 0 ?
                <View style={{ flex: 1 }}>
                    <View style={{ width: "100%", backgroundColor: 'white', flexDirection: 'row', marginTop: 5 }}>
                        <Pressable style={styles.subContainer}
                            onPress={() => toggleSort()}
                        >
                            <Text>Sort</Text>
                        </Pressable>
                        <Pressable style={styles.subContainer}
                            onPress={() => toggleModal()}
                        >
                            <Text>Filter</Text>
                        </Pressable>
                        <Pressable style={styles.subContainer} onPress={() => onMapPress()}>
                            <Text>Map</Text>
                        </Pressable>
                    </View>
                    {!selectedMap ?
                        <View style={{ marginBottom: 10, flex: 1 }}>
                            <FlatList
                                data={hotelData}
                                key={item => item.id}
                                renderItem={({ item, index }) => renderList(item, index)}
                            />

                        </View> :
                        <View style={{ flex: 1 }}>
                            <MapView
                                ref={_map}
                                region={{
                                    latitude: Number(hotelData[0].latitude),
                                    longitude: Number(hotelData[0].longitude),
                                    latitudeDelta: 0.07864195044303443,
                                    longitudeDelta: 0.065142817690068,
                                }}
                                style={{ flex: 1 }}
                            //onRegionChange={(reg) => onMapRegionChange(reg)} 
                            >
                                {hotelData.map((marker, index) => {
                                    const scaleStyle = {
                                        transform: [
                                            {
                                                scale: interpolations[index].scale,
                                            },
                                        ],
                                        //backgroundColor:AppColor.appColor
                                    };
                                    return (
                                        <Marker
                                            key={index}
                                            coordinate={{
                                                latitude: Number(marker.latitude),
                                                longitude: Number(marker.longitude),
                                                //latitudeDelta: 0.04864195044303443,
                                                //longitudeDelta: 0.040142817690068,
                                            }}
                                            onPress={(e) => onMarkerPress(e)}
                                        //s title={String("abc")}
                                        // description={marker.title}
                                        >
                                            <Animated.View style={[styles.markerWrap, scaleStyle]}>

                                                <Animated.View style={[styles.arrow]}>
                                                    <Text style={{ margin: 5 }}>₹{marker.branch_room[0].final_price}</Text>
                                                </Animated.View>
                                                <Animated.View style={[styles.arrowBorder,]}>

                                                </Animated.View>
                                                {/* <Animated.Image
                  source={AppImage.marker}
                  style={[styles.marker, scaleStyle]}
                  resizeMode="cover"
                /> */}
                                            </Animated.View>
                                        </Marker>
                                    )
                                })}

                            </MapView>
                            <Animated.ScrollView
                                ref={_scrollView}
                                horizontal
                                pagingEnabled
                                scrollEventThrottle={1}
                                showsHorizontalScrollIndicator={false}
                                snapToInterval={CARD_WIDTH + 20}
                                snapToAlignment="center"
                                style={styles.scrollView}
                                contentInset={{
                                    top: 0,
                                    left: SPACING_FOR_CARD_INSET,
                                    bottom: 0,
                                    right: SPACING_FOR_CARD_INSET
                                }}
                                contentContainerStyle={{
                                    paddingHorizontal: Platform.OS === 'android' ? SPACING_FOR_CARD_INSET : 0
                                }}
                                onScroll={Animated.event(
                                    [
                                        {
                                            nativeEvent: {
                                                contentOffset: {
                                                    x: mapAnimation,
                                                }
                                            },
                                        },
                                    ],
                                    { useNativeDriver: true }
                                )}
                            >
                                {hotelData.map((marker, index) => (
                                    <View style={styles.card} key={index}>
                                        <Image
                                            source={{ uri: marker.branch_image[0].image }}
                                            style={styles.cardImage}
                                            resizeMode="cover"
                                        />
                                        <View style={{ flex: 1, }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Text style={{ fontSize: 16, fontWeight: 'bold', padding: 2, margin: 5, marginLeft: 10, color: AppColor.appColor }}>{marker.branch_name}</Text>
                                                <Image source={AppImage.star_orang}
                                                    style={{ width: 15, height: 15, resizeMode: 'contain', marginLeft: 10 }}
                                                />
                                                <Text style={{ marginLeft: 5 }}>{marker.branch_room.length > 0 ? marker.total_rating : ''}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={AppImage.marker}
                                                    style={{ width: 15, height: 15, resizeMode: 'contain', marginLeft: 10 }}
                                                />
                                                <Text style={{ marginLeft: 5, color: AppColor.input_placeholderColor,marginRight:5 }}>{marker.first_address}</Text>

                                            </View>
                                            <View>
                                                <Text style={{
                                                    marginTop: 5, marginBottom: 5, color: AppColor.appColor, fontWeight: 'bold',
                                                    fontSize: 16, marginLeft: 10
                                                }}>RS.{marker.branch_room[0].final_price}/night   <Text
                                                    style={{ color: 'grey', textDecorationLine: 'line-through' }}>   {marker.branch_room[0].price}</Text></Text>
                                            </View>
                                            {/* <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
                      <StarRating ratings={marker.rating} reviews={marker.reviews} />
                      <Text numberOfLines={1} style={styles.cardDescription}>{marker.description}</Text>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {}}
                          style={[styles.signIn, {
                            borderColor: '#FF6347',
                            borderWidth: 1
                          }]}
                        >
                          <Text style={[styles.textSign, {
                            color: '#FF6347'
                          }]}>Order Now</Text>
                        </TouchableOpacity>
                      </View> */}
                                        </View>
                                    </View>
                                ))}
                            </Animated.ScrollView>
                        </View>}
                    {renderModal()}
                    {Sort()}
                    <Loader loading={loader}/>
                </View>
                :
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                    <LottieView source={require('../../../assets/not-found.json')} autoPlay loop
                        style={{ width: '100%', height: 200 }} />
                </View>}

        </View>



    )
}

const styles = StyleSheet.create({
    subContainer: {
        flex: 1,
        alignItems: 'center',
        borderRightWidth: 1,
        marginTop: 12,
        borderColor: 'grey',
        marginBottom: 12

    },
    Card: {
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 5,

    },
    innercard: {
        marginLeft: 15,
        marginBottom: 5,
        marginTop: 10,
        flex: 1
    },
    btnContainer: {
        width: 120,
        height: 40,
        // backgroundColor:'red',
        borderRadius: 10,
        elevation: 3,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        //borderWidth:0.1,
        borderColor: AppColor.appColor
    },
    btnContainer1: {
        width: '40%',
        height: 40,
        backgroundColor: '#003727',
        borderRadius: 10,
        elevation: 3,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 10,
        //borderWidth:0.1,
        borderColor: AppColor.appColor
    },
    markerWrap: {
        alignItems: "center",
        //justifyContent: "center",
        width: 80,
        height: 80,
        justifyContent: 'center',
        //backgroundColor:'red'
    },
    marker: {
        width: 30,
        height: 30,
    },
    scrollView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    card: {
        // padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 1.35,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    arrow: {
        backgroundColor: 'white',
        borderColor: 'transparent',
        borderTopColor: '#fff',
        //borderWidth:16,
        alignSelf: 'center',
        // width:55,
        //height:35,
        //right:5,
        alignItems: 'center',
        justifyContent: 'center',

        //marginTop:-32
    },
    arrowBorder: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderBottomWidth: 12,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: "white",
        transform: [
            { rotate: '180deg' }
        ],
        margin: 0,
        //right:5,
        //marginLeft: -4,
        borderWidth: 0,
        //borderColor:"red"
    }
})

export default SearchScreen;