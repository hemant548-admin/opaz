import React, { useState } from 'react';
import {
    View, Text, FlatList, BackHandler, Platform, PermissionsAndroid, Alert,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import moment from 'moment'
import { SliderBox } from "react-native-image-slider-box";
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import IconLoader from '../../../component/loader/IconLoader';
import Toast from 'react-native-toast-message';
import toastConfig from '../../../component/Toast/Toast';
import { getLocation, geocodeLocationByName } from '../../../component/services/location-service';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import Loader from '../../../component/loader/Loader';
import Modal from 'react-native-modal';
import Header from '../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import Geolocation from 'react-native-geolocation-service';
import AppStrings from '../../../utils/AppStrings';

const { height, width } = Dimensions.get('window');

const arr = [{ id: '1', name: 'Mumbai', },
{ id: '2', name: 'Delhi', },
{ id: '3', name: 'Kolkata', },
{ id: '4', name: 'Hyderabad', },
{ id: '5', name: 'Pune', },
{ id: '6', name: 'Goa', },
{ id: '7', name: 'Chennai', },
]
const HomeScreen = ({ navigation }) => {
    const [iconloader, seticonLoader] = useState(false);
    const [loader, setLoader] = useState(false);
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(new Date());
    const [showto, setShowto] = useState(false);
    const [dateto, setDateto] = useState(moment().add(1, 'days'));
    const [destination, setDestination] = useState('');
    const [location, setLocation] = useState();
    const [rooms, setRooms] = useState('1 room-2 adults-0 children');
    const [hotelData, setHotelData] = useState([]);
    const [promotion, setPromotion] = useState([]);
    const [roomArray, setRoomArray] = useState([{ adults: 2, child: 0 }]);
    const [count, setCount] = useState(1)
    const [adultCount, setAdultCount] = useState(2);
    const [childCount, setChildCount] = useState(0);
    const [placesData, setPlacesData] = useState(arr);
    const [isModalVisible, setModalVisible] = useState(false);
    const [imageArray, setImageArray]=useState([]);
    const [isSearchModalVisible, setSearchModalVisible] = useState(false);

    React.useEffect(async () => {
        requestLocationPermission();
        const token = await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN, '');

        console.log(token)
        const unsubscribe = navigation.addListener('focus', () => {
            getInitialState();
        });



        return () => {
            Geolocation.clearWatch(watchID);
            unsubscribe;
        };
    }, [navigation]);

    // React.useEffect(()=>{
    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         closeApp
    //       );

    //       return () => backHandler.remove();
    // },[])

    // const closeApp = () => {

    //     Alert.alert(
    //       'Exit',
    //       'Are you sure you want to exit the App?',
    //       [
    //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //         { text: 'Yes', onPress: () => { BackHandler.exitApp(); } },
    //       ],
    //       { cancelable: false });
    //     return true;

    //   }
    const requestLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            getInitialState();
            subscribeLocationLocation();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: 'Location Access Required',
                        message: 'This App needs to Access your location',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    //To Check, If Permission is granted
                    getInitialState();
                    subscribeLocationLocation();
                } else {
                    //setLocationStatus('Permission Denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };
    const onRoomClick = (value) => {
        if (value === 'Plus') {

            setRooms(roomArray.push({ adults: 2, child: 0 }))

            console.log("roomArray ", roomArray)
        }
        else {
            setRooms(roomArray.pop({ adults: 2, child: 0 }))
        }
    }
    const getInitialState = () => {
        getLocation().then(
            (data) => {
                console.log(data);
                setLocation(
                    {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                );

                NearLocation(data.latitude, data.longitude)
                promotionalOffer(data.latitude, data.longitude)

            }
        );
    }
    const subscribeLocationLocation = () => {
        watchID = Geolocation.watchPosition(
            (position) => {
                //Will give you the location on location change

                //setLocationStatus('You are Here');
                console.log(position);

                //getting the Longitude from the location json        
                const currentLongitude =
                    JSON.stringify(position.coords.longitude);

                //getting the Latitude from the location json
                const currentLatitude =
                    JSON.stringify(position.coords.latitude);

                //Setting Longitude state
                //setCurrentLongitude(currentLongitude);

                //Setting Latitude state
                //setCurrentLatitude(currentLatitude);
            },
            (error) => {
                //setLocationStatus(error.message);
                console.log(error)
            },
            {
                enableHighAccuracy: false,
                maximumAge: 1000
            },
        );
    };
    const NearLocation = (lat, lon) => {

        let requsetBody = JSON.stringify({
            latitude: lat,
            longitude: lon
        });

        setLoader(true)
        console.log(APIStrings.HomeListOfHoteNearLocation);
        console.log(requsetBody);

        simplePostCall(APIStrings.HomeListOfHoteNearLocation, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setHotelData(data.Branch_List)
                }
                else {
                    //this.setState({ invalid: true, error Message: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });

    }
    const promotionalOffer = (lat, lon) => {

        let requsetBody = JSON.stringify({
            latitude: lat,
            longitude: lon
        });
        setLoader(true)
        console.log(APIStrings.Opaz_promotional_offer_Ads);
        console.log(requsetBody);

        simplePostCall(APIStrings.Opaz_promotional_offer_Ads, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setPromotion(data.output)
                    const temp=data.output.map((item,index)=>{
                        return item.banner_image;
                    })
                    setImageArray(temp)
                    console.log('temp ', temp)
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });

    }
    const onSearchPress = () => {
        seticonLoader(true)
        let requsetBody = JSON.stringify({
            address: destination,
            latitude: destination.trim().length === 0 ? location.latitude : '',
            longitude: destination.trim().length === 0 ? location.longitude : '',
            adult: adultCount,
            children: childCount,
            start_date: moment(date).format('YYYY-MM-DD'),
            end_date: moment(dateto).format('YYYY-MM-DD')
        });

        console.log(APIStrings.EndUserHotelFilter);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserHotelFilter, requsetBody)
            .then((data) => {

                // seticonLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setTimeout(() => {
                        seticonLoader(false)
                        navigation.navigate('Search', { date: moment(date).format('DD-MMM-YYYY'), dateTO: moment(dateto).format('DD-MMM-YYYY'), data: data.Branch_List })
                    }, 9500);
                }
                else {
                    setTimeout(() => {
                        seticonLoader(false)
                        //navigation.navigate('Search', { date: moment(date).format('ddd DD-MMM-YYYY'), data: data.Branch_List })
                    }, 9500);
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)


                }

            }).catch((error) => {
                console.log("error ", error);
                setTimeout(() => {
                    seticonLoader(false)
                    alert('Something went wrong please try after some time ...')
                    //navigation.navigate('Search', { date: moment(date).format('ddd DD-MMM-YYYY'), data: data.Branch_List })
                }, 9500);
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        //setShow(Platform.OS === 'ios');
        setDate(currentDate);
        setShow(false)
    };
    const onChangeto = (event, selectedDate) => {
        const currentDate = selectedDate || dateto;
        //setShow(Platform.OS === 'ios');
        setDateto(currentDate);
        setShowto(false)
    };
    const showDatepicker = () => {
        setShow(true);
    };
    const showDatepickerto = () => {
        setShowto(true);
    };
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.openDrawer());

    };
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const toggleSearchModal = () => {
        setSearchModalVisible(!isSearchModalVisible);
        return true
    };
    const onAdultPress = (index, value) => {
        let temp = [];
        if (value === 'Plus') {
            temp = roomArray.map((item, i) => {
                return {
                    adults: i === index ? item.adults + 1 : item.adults,
                    child: item.child
                }
            })
        }
        else {
            temp = roomArray.map((item, i) => {
                return {
                    adults: i === index ? item.adults - 1 : item.adults,
                    child: item.child
                }
            })
        }
        console.log('tmp ', temp)
        setRoomArray(temp);
    }
    const onChildPress = (index, value) => {
        let temp = [];
        if (value === 'Plus') {
            temp = roomArray.map((item, i) => {
                return {
                    child: i === index ? item.child + 1 : item.child,
                    adults: item.adults
                }
            })
        }
        else {
            temp = roomArray.map((item, i) => {
                return {
                    child: i === index ? item.child - 1 : item.child,
                    adults: item.adults
                }
            })
        }
        console.log('tmp ', temp)
        setRoomArray(temp);
    }
    const onApplyPress = () => {
        let acount = 0;
        let ccount = 0;
        roomArray.forEach(ele => {
            acount += ele.adults
            ccount += ele.child
        })
        console.log(acount, ccount)
        setAdultCount(acount);
        setChildCount(ccount);
        toggleModal()
    }
    const onDateSelect = (checkIn, checkOut) => {
        // alert(JSON.stringify(country));
        //this.setState({ flag: country.flag, countryCode: '+' + country.countryCode });
        setDate(checkIn);
        setDateto(checkOut)
        // setCountryCode('+' + country.countryCode)
        // setFlag(country.flag)
    }
    const onDatePress = () => {
        //const { navigation } = this.props;
        navigation.navigate('Calender', { onDateSelect: (checkIn, checkOut) => onDateSelect(checkIn, checkOut) });
    }
    const onPlacePress = async (details) => {
        console.log(details)
        if (details) {
            setDestination(details.structured_formatting.main_text)
            //   this.setState({
            //     cityName: details.structured_formatting.main_text,
            //     isLocationModalVisible: false
            //   });
            toggleSearchModal();

        }
    }
    const searchCountry = (input) => {
        //this.setState({ searchInput: input });
        setDestination(input)
        let filteredData = arr.filter(item => item.name.toLowerCase().includes(input.trim().toLowerCase()));
        //this.setState({ countries: filteredData });
        setPlacesData(filteredData)
        //setisTrue(false)
    }
    const onItemPress = (item) => {
        setDestination(item);
        toggleSearchModal();
    }
    const goToScreen = (item) => {
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: String(item)
        })

        console.log(APIStrings.EndUserRoomDetails)
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserRoomDetails, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    //setHotelData(data.Room)
                    navigation.navigate('Detail', { hotelData: Object.keys(data.Room).length > 0 ? data.Room : '',facility:data.amenities })
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const renderRoom = (item, index) => {
        return (
            <View style={{
                height: 150, width: '90%', elevation: 5, backgroundColor: 'white',
                alignSelf: 'center', marginTop: 10, justifyContent: 'space-between', marginBottom: 10
            }}>
                <Text style={{ marginLeft: 10, marginTop: 10 }}>Room{index + 1}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Text style={{ fontSize: 18, marginLeft: 20 }}>Adults</Text>
                    <View style={{
                        flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                        , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor, marginRight: 20
                    }}>
                        <Text style={{ bottom: 5, fontSize: 20, margin: 5, color: item.adults > 1 ? 'black' : 'grey' }}
                            onPress={() => onAdultPress(index, 'Subtract')} disabled={item.adults > 1 ? false : true}
                        >_</Text>
                        <Text style={{ fontSize: 20, margin: 5 }} >{item.adults}</Text>
                        <Text style={{ fontSize: 20, margin: 5, color: item.adults < 4 ? 'black' : 'grey' }} onPress={() => onAdultPress(index, 'Plus')}
                            disabled={item.adults < 4 ? false : true}
                        >+</Text>
                    </View>

                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 12, alignItems: 'center' }}>
                    <View>
                        <Text style={{ fontSize: 18, marginLeft: 20 }}>Children</Text>
                        <Text style={{ marginLeft: 20, color: 'grey' }}>(0-5 Age)</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                        , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor, marginRight: 20
                    }}>
                        <Text style={{ bottom: 5, fontSize: 20, margin: 5, color: item.child > 0 ? 'black' : 'grey' }} onPress={() => onChildPress(index, 'Subtract')}
                            disabled={item.child > 0 ? false : true}
                        >_</Text>
                        <Text style={{ fontSize: 20, margin: 5 }}>{item.child}</Text>
                        <Text style={{ fontSize: 20, margin: 5, color: item.child < 4 ? 'black' : 'grey' }} onPress={() => onChildPress(index, 'Plus')}
                            disabled={item.child < 4 ? false : true}
                        >+</Text>
                    </View>

                </View>

            </View>
        )
    }
    const renderSearchModal = () => {
        return (
            <View >
                <Modal isVisible={isSearchModalVisible}
                    onBackdropPress={() => setSearchModalVisible(false)}
                    onBackButtonPress={() => setSearchModalVisible(false)}
                    deviceWidth={width}
                    deviceHeight={height}
                    style={{ margin: 0 }}
                >

                    <View style={styles.container}>
                        <View style={{ marginLeft: 5, marginRight: 5, marginTop: 10, flexDirection: 'row', justifyContent: 'center' }}>
                            <Pressable
                                //onPress={() => toggleSearchModal()}
                                onPress={() => toggleSearchModal()}
                            >
                                <Image
                                    style={styles.searchIcon}
                                    source={AppImage.arrow_green} />
                            </Pressable>
                            <TextInput
                                //flex={1}
                                style={{ width: '85%', borderWidth: 1, borderColor: AppColor.appColor, borderRadius: 5 }}
                                placeholder='Search places here ...'
                                //placeholderTextColor={AppColors.colorLightText}
                                // backgroundColor='red'
                                padding={10}
                                keyboardType='default'
                                autoCapitalize='none'
                                autoCorrect={false}
                                maxLength={20}
                                value={destination}
                                onChangeText={(value) => searchCountry(value)}
                                returnKeyType='done'
                                // onSubmitEditing={() => { }}
                                //ref={(ref) => { this.searchRef = ref }}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        <FlatList
                            nestedScrollEnabled={true}
                            data={placesData}
                            contentContainerStyle={{ paddingTop: 20 }}
                            horizontal={false}
                            renderItem={({ item, index }) => {
                                return (
                                    <Pressable
                                        onPress={() => onItemPress(item.name)}
                                    >
                                        <View style={styles.item}>
                                            <Text style={{ margin: 5, flex: 1 }}>{item.name}</Text>
                                        </View>
                                    </Pressable>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        {/* <View style={{ flex: 1, marginLeft: 5, marginRight: 5, marginTop: 10, flexDirection: 'row', justifyContent: 'center' }}>
                            <Pressable 
                            //onPress={() => toggleSearchModal()}
                            onPress={()=>toggleSearchModal()}
                            >
                                <Image
                                    style={styles.searchIcon}
                                    source={AppImage.arrow_green} />
                            </Pressable>
                            <GooglePlacesAutocomplete
                                placeholder='Search places here...'
                                onPress={(data, details = null) => {
                                    // 'details' is provided when fetchDetails = true
                                    onPlacePress(details)
                                    // console.log("data: ", data, "details: ", details);
                                }}
                                onFail={error => console.error(error)}
                                query={{
                                    key: 'AIzaSyDOi_uZzGJGFwWaDV4BVLb5NpzMju-8Qq8',
                                    language: 'en'
                                }}
                                listEmptyComponent={() => {
                                    return (
                                        <View>
                                            <Text style={styles.noDataText}>No Data Available</Text>
                                        </View>
                                    )
                                }}
                                styles={autoCompleteStyle}
                            />



                        </View> */}

                    </View>
                </Modal>
            </View>
        );
    }
    const renderModal = () => {
        return (
            <View >
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                    deviceWidth={width}
                    deviceHeight={height}
                    style={{ margin: 0 }}
                >

                    <View style={{ flex: 1, backgroundColor: '#f7f7f7' }}>
                        <Header
                            title='Rooms and Guests'
                            goBack={() => setModalVisible(false)}
                        />
                        <View style={{
                            height: 60, width: '90%', elevation: 5, backgroundColor: 'white',
                            alignSelf: 'center', marginTop: 20, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginBottom: 10
                        }}>
                            <Text style={{ fontSize: 18, marginLeft: 20 }}>Rooms</Text>
                            <View style={{
                                flexDirection: 'row', width: 90, alignItems: 'center', justifyContent: 'space-around'
                                , borderRadius: 10, borderWidth: 1, borderColor: AppColor.appColor, marginRight: 20
                            }}>
                                <Text style={{ bottom: 5, fontSize: 20, margin: 5, color: roomArray.length < 2 ? 'grey' : 'black' }}
                                    onPress={() => onRoomClick('Subtract')} disabled={roomArray.length < 2 ? true : false}>_</Text>
                                <Text style={{ fontSize: 20, margin: 5 }}>{roomArray.length}</Text>
                                <Text style={{ fontSize: 20, margin: 5, color: roomArray.length < 4 ? 'black' : 'grey' }} onPress={() => onRoomClick('Plus')}
                                    disabled={roomArray.length < 4 ? false : true}
                                >+</Text>
                            </View>
                        </View>

                        <FlatList
                            data={roomArray}
                            keyExtractor={(item, index) => String(index)}
                            renderItem={({ item, index }) => renderRoom(item, index)}
                        />
                    </View>
                    <TouchableOpacity style={{
                        width: '60%', alignSelf: 'center', alignItems: 'center', backgroundColor: '#003727',
                        position: 'absolute', bottom: 10, borderRadius: 10
                    }} onPress={() => onApplyPress()}
                    >
                        <Text style={{ color: 'white', fontSize: 18, margin: 7 }}>Apply</Text>
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
    const renderList = (item, index) => {
        return (
            <Pressable style={styles.Card} onPress={() => goToScreen(item.id)}>
                <View>
                    <Image source={{ uri: item.branch_image[0].image }}
                        style={{ width: '98%', height: 170, borderRadius: 20, alignSelf: 'center', resizeMode: 'cover' }}
                    />
                    <View style={{ flexDirection: 'row', position: 'absolute', bottom: 10, right: 15, alignItems: 'center', backgroundColor: AppColor.appColor, borderRadius: 10 }}>
                        <Image source={AppImage.star_icon}
                            style={{ width: 13, height: 13, resizeMode: 'contain', marginLeft: 5 }}
                        />
                        <Text style={{ color: 'white', marginLeft: 10, marginRight: 5 }}>{Number(item.total_rating).toFixed(1)}</Text>
                    </View>
                </View>
                <View style={{
                    flexDirection: 'row', marginLeft: 5, marginRight: 5, borderBottomLeftRadius: 20, borderBottomRightRadius: 20,
                    justifyContent: 'space-between', alignItems: 'center'
                }}>
                    <View style={{ marginTop: 10, marginBottom: 10, marginLeft: 15, width: '70%' }}>
                        <Text style={{ fontSize: 20, color: AppColor.appColor }}>{item.branch_name}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 5, }}>
                            <Image source={AppImage.marker}
                                style={{ width: 20, height: 20, resizeMode: 'contain', marginTop: 4 }}
                            />
                            <Text style={{ color: AppColor.appColor, fontFamily: AppFonts.light, marginLeft: 5 }}>{item.first_address}</Text>
                        </View>
                    </View>
                    <View style={{}}>
                        <Pressable style={{
                            justifyContent: 'center', alignItems: 'center', backgroundColor: '#003727',
                            borderRadius: 15, width: 65, marginRight: 5
                        }} onPress={() => goToScreen(item.id)}>
                            <Text style={{ color: 'white', margin: 5 }}>More</Text>
                        </Pressable>
                    </View>
                </View>
            </Pressable>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={styles.header}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => toggleDrawer()}
                    >
                        <Image source={AppImage.hamburger_icon}
                            style={{ width: 25, height: 22, resizeMode: 'contain', marginLeft: 7 }}
                        />
                    </TouchableOpacity>
                    <Text style={styles.headerTxt}>Home</Text>
                </View>
                <TouchableOpacity style={{
                    flexDirection: 'row', height: 30,
                    borderWidth: 1, borderRadius: 12, borderColor: 'white', alignItems: 'center', justifyContent: 'space-around'
                }} activeOpacity={0.8} onPress={() => navigation.navigate('Services')}>
                    <Image source={AppImage.folder_icon}
                        style={{ width: 18, height: 18, resizeMode: 'contain', margin: 2, marginLeft: 3 }}
                    />
                    <Text style={{ color: 'white', margin: 2, marginRight: 4 }}>Room Service</Text>
                </TouchableOpacity>
            </View>

            <ScrollView contentContainerStyle={{ flexGrow: 1, }}>
                <View style={{ marginTop: 20, alignItems: 'center', backgroundColor: 'white' }}>
                    <Pressable style={[styles.box, { height: 35, justifyContent: 'flex-start' }]} onPress={() => toggleSearchModal()}>
                        <Image source={AppImage.search_icon}
                            style={[styles.boxImg, { marginLeft: 10, marginRight: 10 }]}
                        />
                        <Text style={{ color: destination === '' ? 'grey' : AppColor.appColor }}>{destination === '' ? "Enter Your destination" : destination}</Text>
                    </Pressable>
                    <Pressable style={[styles.box, { borderTopWidth: 0, borderBottomWidth: 0, height: 30, justifyContent: 'flex-start' }]}
                        onPress={() => toggleModal()}
                    >
                        <Image source={AppImage.name_icon}
                            style={[styles.boxImg, { marginLeft: 10, marginRight: 10 }]}
                        />
                        <Text style={{ marginLeft: 10, color: AppColor.appColor }}>{roomArray.length} room {adultCount}-adults {childCount}-child</Text>
                        {/* <TextInput
                            style={styles.boxInput}
                            placeholder='1 room-2 adults-0 children'
                            value={rooms}
                            onChangeText={(value)=>setRooms(value)}
                        /> */}
                    </Pressable>
                    <Pressable style={{
                        width: '90%',
                        flexDirection: 'row',
                        //backgroundColor: 'red',
                        //justifyContent: 'space-evenly',
                        height: 35,
                        alignItems: 'center',
                        borderWidth: 1,
                        borderColor: AppColor.appColor
                    }} onPress={() => onDatePress()}>
                        <Image source={AppImage.calenderBooking}
                            style={{ width: 25, height: 25, resizeMode: 'contain', marginLeft: 10 }}
                        />
                        <Text style={{ color: AppColor.appColor, marginLeft: 5 }}>{moment(date).format('ddd DD-MMM-YYYY')}</Text>
                        <Text style={{ color: AppColor.appColor, marginLeft: 5 }}> - {moment(dateto).format('ddd DD-MMM-YYYY')}</Text>

                    </Pressable>

                    <Pressable style={[styles.box, { backgroundColor: '#003727' }]}
                        onPress={() => onSearchPress()}
                    //onPress={()=>alert('needs to be change from backend side')}
                    >
                        <Text style={{ color: 'white', margin: 7 }}>Search</Text>
                    </Pressable>
                </View>
                <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
                <Text style={{ fontSize: 18, marginLeft: 15, marginBottom: 7, fontFamily: AppFonts.regular,marginTop:10 }}>Promotions and Discount</Text>
                <View>
                <SliderBox
                    images={imageArray}
                    sliderBoxHeight={160}
                    onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                    dotColor={AppColor.appColor}
                    inactiveDotColor="#90A4AE"
                    //autoplayInterval={4000}
                    //paginationBoxVerticalPadding={20}
                    autoplay={true}
                    circleLoop
                    resizeMethod={'resize'}
                    resizeMode={'cover'}
                    // paginationBoxStyle={{
                    //   position: "absolute",
                    //   bottom: 0,
                    //   padding: 0,
                    //   alignItems: "center",
                    //   alignSelf: "center",
                    //   justifyContent: "center",
                    //   paddingVertical: 10
                    // }}
                    dotStyle={{
                      width: 10,
                      height: 10,
                      borderRadius: 5,
                      marginHorizontal: 0,
                      padding: 0,
                      margin: 0,
                      backgroundColor: "rgba(128, 128, 128, 0.92)"
                    }}
                    ImageComponentStyle={{borderRadius: 15, width: '90%', marginTop: 5,marginBottom:5,}}
                    imageLoadingColor={AppColor.appColor}
                />
                </View>
                <View style={{ flex: 1, }}>
                    <Text style={{ margin: 10, fontSize: 18 }}>Hotels near you</Text>
                    <FlatList
                        data={hotelData}
                        renderItem={({ item, index }) => renderList(item, index)}
                    />
                    <View style={{ width: '100%', height: 10, marginTop: 10, }} />
                </View>
                
                {/* <FlatList
                    data={promotion}
                    contentContainerStyle={{ }}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id}
                    renderItem={({ item, index }) => {
                        return (
                            <Pressable style={[styles.banner,{marginLeft:index===0?15:10}]} >
                                <Image source={{ uri: item.banner_image }}
                                    style={{ width:'100%',height:160 }}
                                />
                            </Pressable>
                        )
                    }
                    }

                /> */}
                {/* </View> */}
            </ScrollView>
            {renderModal()}
            {renderSearchModal()}
            <IconLoader loading={iconloader} />
            <Loader loading={loader} />
        </View >
    )
}
const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: '8%',
        padding: 10,
        backgroundColor: AppColor.appColor,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerTxt: {
        color: 'white',
        marginLeft: 15,
        fontSize: 18
    },
    box: {
        width: '90%',
        flexDirection: 'row',
        // backgroundColor:'red',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: AppColor.appColor

    },
    boxImg: {
        width: 18,
        height: 18,
        resizeMode: 'contain'
    },
    boxInput: {
        width: '80%',
        height: 38,
        justifyContent: 'center',
        color: AppColor.appColor
        // backgroundColor:'yellow'
    },
    Card: {
        width: '95%',
        elevation: 3,
        borderRadius: 20,
        backgroundColor: 'white',
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 5
    },
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    search: {
        borderRadius: 50,
        elevation: 2,
        zIndex: 1,
        backgroundColor: 'white',
        margin: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchIcon: {
        width: 18,
        height: 18,
        marginRight: 20,
        resizeMode: 'contain',
        marginLeft: 0,
        marginTop: 14
        //flex:1
    },
    item: {
        flex: 1,
        backgroundColor: 'white',
        minHeight: 60,
        padding: 15,
        alignItems: 'center',
        flexDirection: 'row',
        margin: 1.5
    },
    noDataText: {
        alignSelf: 'center',
        marginTop: 10,
        color: 'red'
    },
    banner: {
        width: width - 60,
        height: 160,
        borderRadius: 15,
        backgroundColor: 'white',
        alignSelf: 'center',
        elevation: 5,
        marginLeft: 10,
        marginRight: 8,
        overflow: 'hidden'
    }

})
const autoCompleteStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 5
    },
    textInputContainer: {
        flexDirection: 'row',
    },
    textInput: {
        //backgroundColor: '#FFFFFF',
        height: 44,
        paddingVertical: 5,
        paddingHorizontal: 8,
        fontSize: 15,
        flex: 1,
        top: 3,
        // borderWidth: 2,
        borderRadius: 10,
        right: 5,
        borderColor: 'grey'
    },
    poweredContainer: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderColor: '#c8c7cc',
        borderTopWidth: 0.5,
    },
    row: {
        backgroundColor: '#FFFFFF',
        padding: 13,
        height: 44,
        flexDirection: 'row',
    },
    separator: {
        height: 0.5,
        backgroundColor: '#c8c7cc',
    },
    loader: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        height: 20,
    },
})


export default HomeScreen;