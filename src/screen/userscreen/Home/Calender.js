// React Native Calendar Picker using react-native-calendar-picker
// https://aboutreact.com/react-native-calendar-picker/

// import React in our code
import moment from 'moment';
import React, { useState } from 'react';

// import all the components we are going to use
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity,Image } from 'react-native';

//import CalendarPicker from the package we installed
import CalendarPicker from 'react-native-calendar-picker';
import AppColor from '../../../utils/AppColor';
import AppImage from '../../../utils/AppImage';

const Calender = ({ navigation, route }) => {
    const [selectedStartDate, setSelectedStartDate] = useState(moment());
    const [selectedEndDate, setSelectedEndDate] = useState(moment().add(1, 'days'));

    const onDateChange = (date, type) => {
        //function to handle the date change
        if (type === 'END_DATE') {
            setSelectedEndDate(date);
        } else {
            setSelectedEndDate(null);
            setSelectedStartDate(date);
        }
    };
    const onApplyPress = () => {
        // const { navigation } = this.props;

        route.params.onDateSelect(selectedStartDate, selectedEndDate);
        navigation.goBack();
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <CalendarPicker
                    startFromMonday={false}
                    allowRangeSelection={true}
                    minDate={new Date()}
                    maxDate={new Date(2050, 6, 3)}
                    weekdays={['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',]}
                    months={[
                        'January',
                        'Febraury',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December',
                    ]}
                    previousTitle="Previous"
                    nextTitle="Next"
                    todayBackgroundColor="#e6ffe6"
                    selectedDayColor="#66ff33"
                    selectedDayTextColor="#000000"
                    scaleFactor={375}
                    textStyle={{
                        fontFamily: 'Cochin',
                        color: '#000000',
                    }}
                    selectedRangeEndStyle={{ backgroundColor: AppColor.appColor }}
                    selectedRangeStartStyle={{ backgroundColor: AppColor.appColor }}
                    selectedRangeStartTextStyle={{ color: 'white' }}
                    selectedRangeEndTextStyle={{ color: 'white' }}
                    selectedRangeStyle={{ backgroundColor: '#2f964a' }}
                    selectedStartDate={selectedStartDate}
                    selectedEndDate={selectedEndDate}
                    onDateChange={onDateChange}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between',marginTop:20 }}>
                    <View>
                        <Text style={{ fontSize: 16 }}>Check-in</Text>
                        <Text style={{ fontSize: 20, color: AppColor.appColor }}>{moment(selectedStartDate).format('DD MMM')}</Text>
                        
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={AppImage.arrow_green}
                            style={{ width: 18, height: 15, resizeMode: 'contain', transform: [{ rotate: '180deg' }] }}
                        />
                        <Text style={{ fontSize: 16, textAlign: 'center', color: AppColor.appColor, marginTop: 5 }}>{selectedEndDate===null? '':moment(selectedEndDate).diff(selectedStartDate, 'days')} Night</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: 16 }}>Check-out</Text>
                        <Text style={{ fontSize: 20, color: AppColor.appColor }}>{selectedEndDate===null? 'Select check-out':moment(selectedEndDate).format('DD MMM')}</Text>
                    </View>
                </View>
                <TouchableOpacity style={selectedEndDate===null? styles.disable:styles.enable} activeOpacity={0.6}
                    onPress={() => onApplyPress()}
                    disabled={selectedEndDate===null? true:false}
                >
                    <Text style={{ color: 'white', margin: 10, fontSize: 18 }}>Apply Dates</Text>
                </TouchableOpacity>
                {/* <View style={styles.textStyle}>
          <Text style={styles.textStyle}>Selected Start Date :</Text>
          <Text style={styles.textStyle}>
            {selectedStartDate ? selectedStartDate.toString() : ''}
          </Text>
          <Text style={styles.textStyle}>Selected End Date : </Text>
          <Text style={styles.textStyle}>
            {selectedEndDate ? selectedEndDate.toString() : ''}
          </Text>
        </View> */}
            </View>
        </SafeAreaView>
    );
};
export default Calender;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#ffffff',
        padding: 16,
    },
    textStyle: {
        marginTop: 10,
    },
    titleStyle: {
        textAlign: 'center',
        fontSize: 20,
        margin: 20,
    },
    enable:{
        width: '80%', backgroundColor: AppColor.appColor, borderRadius: 10, alignSelf: 'center',
        position: 'absolute', bottom: 0, alignItems: "center"
    },
    disable:{
        width: '80%', 
        backgroundColor: 'grey', 
        borderRadius: 10, 
        alignSelf: 'center',
        position: 'absolute', 
        bottom: 0, 
        alignItems: "center"
    }
});
