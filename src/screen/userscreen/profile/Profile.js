import React, { useState, useRef, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ActivityIndicator,
    ScrollView, ImageBackground, StyleSheet, Pressable, Animated
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppStyles from '../../../utils/AppStyles';
import { simplePostCall } from '../../../webservice/APIServices';
import APIStrings from '../../../webservice/APIStrings';
import Loader from '../../../component/loader/Loader';
import ThemeUtils from '../../../utils/ThemeUtils';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import { useDispatch } from 'react-redux'
import axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import { checkScreen } from '../../../redux/reducer';
import { Picker } from '@react-native-community/picker';
import moment from 'moment';
import { AuthContext } from '../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')

const Profile = ({ navigation }) => {
    const [mobile, setMobile] = useState('');
    const [name, setName] = useState('');
    const [userId, setUserId] = useState('');
    const [lastName, setLastName] = useState('');
    const [id_name, setIdName] = useState('');
    const [id_number, setIdNumber] = useState('');
    const [gender, setGender] = useState('');
    const [email, setEmail] = useState('');
    const [dob, setDob] = useState('');
    const [anniversary, setAnniversary] = useState(null);
    const [address, setAddress] = useState('');
    const [isModalVisible, setModalVisible] = useState(false);
    const [filePath, setFilePath] = useState(null);
    const [error, setError] = useState('');
    const [profile, setProfile] = useState(null);
    const [isEdit, setIsEdit] = useState(false);
    const [inValid, setInValid] = useState([false, false, false, false])
    const [loader, setLoader] = useState(false)
    const [isRender, setisRender] = useState(false);
    const [isDate, setisDate] = useState(false);
    const [isAnn, setIsAnn] = useState(false);
    const [date, setDate] = useState(null);
    const [show, setShow] = useState(false);
    const [showAnn, setShowAnn] = useState(false);
    const [isEditModalVisible, setEditModalVisible] = useState(false);
    //const scrollY = useRef(new Animated.Value(0)).current
    const dispatch = useDispatch();
    const { signOut } = React.useContext(AuthContext);

    useEffect(() => {

        const unsubscribe = navigation.addListener('focus', () => {
            check();
        });
        check();

        return unsubscribe;


    }, [navigation])

    const check = async () => {
        id = await AsyncStorage.getItem(AppStrings.USER_ID);
        setUserId(id)
        setIsEdit(false)
        setShow(false);
        setShowAnn(false);
        if (id !== null) {
            Fetch(id);
        }
        else {
            setisRender(true)
        }
    }

    const onLogin = () => {
        dispatch(checkScreen('Login'))
        signOut();
    }
    const Fetch = async (id) => {

        //const id = await AsyncStorage.getItem(AppStrings.USER_ID, '');
        //setUserId(id)
        console.log('id ..', id)
        setLoader(true)
        let requestbody = JSON.stringify({
            user_id: id ? id : userId
        })
        console.log(requestbody)
        console.log(APIStrings.EndUserProfileView)
        simplePostCall(APIStrings.EndUserProfileView, requestbody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                // if (data.status.code === 200) {
                setMobile(data.output.EndUser.user.phone_number);
                setProfile(data.output.EndUser.user.profile_pic);
                setName(data.output.EndUser.first_name);
                setLastName(data.output.EndUser.last_name);
                setEmail(data.output.EndUser.email);
                setAddress(data.output.EndUser.address);
                setDate(data.output.EndUser.date_of_birth ? data.output.EndUser.date_of_birth : new Date());
                setGender(data.output.EndUser.gender);
                setAnniversary(data.output.EndUser.anniversary_date ? data.output.EndUser.anniversary_date : new Date());
                setIdName(data.output.EndUser.id_proof_name);
                setIdNumber(data.output.EndUser.id_proof_number);


                console.log("success", JSON.stringify(data));



                // }
                // else {
                //     //this.setState({ invalid: true, errorMessage: data.message });
                //     console.log(data.status.message)

                // }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                //alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const multipleImage = () => {
        ImagePicker.openPicker({
            includeBase64: false,
            multiple: false

        }).then(images => {
            console.log("multiple pick imges", images);

            setFilePath(images)
            toggleModal()
            //setimg(images.path)
            //console.log("img ",img)
        });
    }
    const CameraImage = () => {
        ImagePicker.openCamera({
            multiple: false,
            cropping: true,
        }).then(image => {
            console.log(image);
            let file = [];
            //file.push(image)
            setFilePath(image)
            toggleModal()
            //setimg(image.path)
            //console.log(file)
        });
    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        console.log('event ', event.type)
        if (event.type === 'set') {
            //console.log('event ')
            setisDate(true)
            setShow(Platform.OS === 'ios');
            setDate(currentDate);
        }

        setShow(Platform.OS === 'ios');
        //setShow(false)
    };
    const showDatepicker = () => {
        setShow(true);
    };

    const onChangeAnn = (event, selectedDate) => {
        const currentDate = selectedDate || anniversary;
        console.log('event ', event.type)
        if (event.type === 'set') {
            //console.log('event ')
            setIsAnn(true)
            setShowAnn(Platform.OS === 'ios');
            setAnniversary(currentDate);
        }

        setShowAnn(Platform.OS === 'ios');
        //setShow(false)
    };
    const showAnniverSaryPicker = () => {
        setShowAnn(true);
    };

    const onSubmit = () => {
        if (name === null || name.trim().length === 0) {
            setError('Please Enter name')
            setInValid([true, false, false, false])
            return;

        }
        if (lastName === null || lastName.trim().length === 0) {
            setError('Please Enter name')
            setInValid([false, true, false, false])
            return;

        }

        let emailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        if (!emailFormat.test(email)) {
            setError('Please enter valid email id')
            setInValid([false, false, true, false])
            return;
        }
        if (mobile.trim().length === 0 || mobile.trim().length < 10 || isNaN(mobile)) {
            setError('Please enter valid mobile number')
            setInValid([true, false, false, true])
            //hotelRef.current.focus();
            return;
        }


        // let requsetBody = JSON.stringify({
        //     user_id: userId,
        //     first_name , 
        //     last_name , 
        //     profile_pic , 
        //     email , 
        //     date_of_birth , 
        //     anniversary_date , 
        //     id_proof_name , 
        //     id_proof_number  ,
        //     address , 
        //     gender
        // });

        setLoader(true)
        const formData = new FormData();
        formData.append('user_id', userId);
        formData.append('first_name', name);
        formData.append('last_name', lastName);
        formData.append('email', email);
        formData.append('date_of_birth', moment(date).format('YYYY-MM-DD'));
        formData.append('anniversary_date', moment(anniversary).format('YYYY-MM-DD'));
        formData.append('id_proof_name', id_name);
        formData.append('id_proof_number', id_number);
        formData.append('address', address);
        formData.append('gender', gender);
        formData.append('profile_pic', filePath ? {

            name: 'image.jpg',
            type: filePath.mime,
            uri: filePath.path

        } : '')
        formData.append('address', address)

        console.log(APIStrings.EditEndUserProfile);
        console.log(formData);
        axios.post(APIStrings.EditEndUserProfile, formData)
            .then(res => {
                //response
                toggleEditModal();
                console.log(res.data)
                alert(res.data.message)
                setIsEdit(false)
                setLoader(false)
                setFilePath(null)
                Fetch()
            }).catch(err => {
                setLoader(false)
                console.log(err)
                //error
            })
    }
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const toggleEditModal = () => {
        setEditModalVisible(!isEditModalVisible);
    };
    const renderModal = () => {
        return (
            <View>
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: '95%', height: '22%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
                                <TouchableOpacity onPress={() => CameraImage()}
                                >
                                    <Image source={AppImage.camera_display}
                                        style={{ width: 50, height: 50, marginRight: 50 }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => multipleImage()}>
                                    <Image source={AppImage.gallery_icon}
                                        style={{ width: 50, height: 50, marginLeft: 50 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    const editModal = () => {
        return (

            <View >
                <Modal isVisible={isEditModalVisible}
                    // animationIn='sl'
                    animationInTiming={1000}
                    animationOutTiming={1000}
                // onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <ScrollView style={{
                            width: width, backgroundColor: 'white', alignSelf: 'center', top: 20,
                            borderTopLeftRadius: 20, borderTopRightRadius: 20, paddingBottom: 40
                        }} showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1,paddingBottom:60}}
                        >
                            <LinearGradient colors={['#007654', '#003727',]} style={[styles.topView, {
                                borderTopLeftRadius: 20,
                                borderTopRightRadius: 20
                            }]}>
                                {/* <View style={styles.header}>
                                <Pressable style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 15, }} onPress={() => navigation.goBack()}>
                                    <Image source={AppImage.back_arrow_white}
                                        style={{ width: 16, height: 16, resizeMode: 'contain' }}
                                    />
                                    <Text style={{ color: 'white', fontSize: 17, fontFamily: AppFonts.medium, marginLeft: 10 }}>Profile</Text>
                                </Pressable>
                                {!isEdit &&
                                    <Pressable style={{ marginRight: 15, flexDirection: 'row', alignItems: 'center' }} onPress={() => setIsEdit(true)}>
                                        <Image source={AppImage.edit_white}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                        <Text style={{ color: 'white', marginLeft: 10, fontSize: 16 }}>Edit</Text>
                                    </Pressable>}
                            </View> */}
                                <View style={[styles.imgContainer,{marginTop:10}]}>
                                   
                                        <Image source={{ uri: filePath ? filePath.path : profile }}
                                            style={{ flex: 1 }}
                                        />
                                    
                                        <Pressable style={{ position: 'absolute', right: 20, bottom: 20 }} onPress={() => toggleModal()}>
                                            <Image source={AppImage.edit_green}
                                                style={{ width: 27, height: 27, resizeMode: 'contain', }}
                                            />
                                        </Pressable>
                                </View>
                            </LinearGradient>
                            <View style={{ alignItems: 'center' }}>
                            <Text style={styles.nameTitle}>First name</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='User Name'
                                    value={name ? name : ''}
                                    onChangeText={(value) => { setName(value), setInValid(!inValid[0]) }}
                                    //editable={isEdit}

                                />
                                {inValid[0] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                                <Text style={styles.nameTitle}>Last name</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='last Name'
                                    value={lastName ? lastName : ''}
                                    onChangeText={(value) => { setLastName(value), setInValid(!inValid[1]) }}
                                    //editable={isEdit}

                                />
                                {inValid[1] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                                <Text style={styles.nameTitle}>Email Id</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='Email ID'
                                    value={email ? email : ''}
                                    onChangeText={(value) => { setEmail(value), setInValid(!inValid[2]) }}
                                    //editable={isEdit}


                                />
                                {inValid[2] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                                <Text style={styles.nameTitle}>Mobile</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='Mobile Number'
                                    value={mobile ? mobile : ''}
                                    onChangeText={(value) => { setMobile(value), setInValid(!inValid[3]) }}
                                    maxLength={10}
                                    //editable={isEdit}

                                />
                                {inValid[3] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between',width:'100%' }}>
                                    {/* <TextInput style={[styles.mobileContainer1, { marginRight: 40 }]}
                                    placeholder='Enter DOB'
                                    value={dob ? dob : ''}
                                    onChangeText={(value) => { setDob(value), setInValid(false) }}
                                    editable={isEdit}
                                    onFocus={()=>showDatepicker()}

                                /> */}
                                <View style={{width:'50%',marginLeft:20}}>
                                <Text style={[styles.nameTitle,{marginLeft:0}]}>DOB</Text>
                                    <Pressable style={[styles.mobileContainer1, { marginRight: 40,width:'80%' }]} onPress={() => showDatepicker()} disabled={!isEdit}>
                                        <Text style={{ color: isDate || date ? AppColor.appColor : 'grey', opacity: 1 }}>{date ? moment(date).format('DD-MM-YYYY') : "Enter DOB"}</Text>
                                    </Pressable>
                                </View>
                                    {show && (
                                        <DateTimePicker
                                            testID="dateTimePicker"
                                            value={new Date()}
                                            mode={'date'}
                                            is24Hour={true}
                                            display="default"
                                            onChange={onChange}
                                        />
                                    )}
                                    {/* <TextInput style={styles.mobileContainer1}
                                    placeholder='Enter Gender'
                                    value={gender ? gender : ''}
                                    onChangeText={(value) => { setGender(value), setInValid(false) }}
                                    editable={isEdit}

                                /> */}
                                    <View style={{width:'100%'}}>
                                    <Text style={[styles.nameTitle,{marginLeft:0}]}>Gender</Text>
                                    <View style={[styles.mobileContainer1,{width:'40%'}]}>
                                         <Picker
                                            selectedValue={gender}
                                            style={{ color: gender.trim().length !== 0 ? AppColor.appColor : 'grey', }}
                                            mode='dropdown'
                                            onValueChange={(itemValue, itemIndex) =>
                                                setGender(itemValue)
                                            }>
                                            <Picker.Item label="Male" value="Male" />
                                            <Picker.Item label="Female" value="Female" />
                                            <Picker.Item label="Other" value="Other" />
                                        </Picker>
                                           
                                    </View>
                                    </View>
                                </View>
                                <Text style={styles.nameTitle}>Anniversary Date</Text>
                                <Pressable style={[styles.mobileContainer, { justifyContent: 'center' }]} onPress={() => showAnniverSaryPicker()} >
                                    <Text style={{ color: anniversary ? AppColor.appColor : 'grey', opacity: 1 }}>{anniversary ? moment(anniversary !== '' ? anniversary : new Date()).format('DD-MM-YYYY') : "Enter Anniversary Date"}</Text>
                                </Pressable>
                                {showAnn && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={new Date()}
                                        mode={'date'}
                                        is24Hour={true}
                                        display="default"
                                        onChange={onChangeAnn}
                                    />
                                )}
                                {/* <TextInput style={styles.mobileContainer}
                                placeholder='Enter Anniversary Date'
                                value={anniversary ? anniversary : ''}
                                onChangeText={(value) => { setAnniversary(value), setInValid(false) }}
                                editable={isEdit}
                            /> */}
                            <Text style={styles.nameTitle}>ID proof Name</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='ID proof Name'
                                    value={id_name ? id_name : ''}
                                    onChangeText={(value) => { setIdName(value), setInValid(false) }}
                                    //editable={isEdit}
                                />
                                <Text style={styles.nameTitle}>ID proof Number</Text>
                                <TextInput style={styles.mobileContainer}
                                    placeholder='ID proof number'
                                    value={id_number ? id_number : ''}
                                    onChangeText={(value) => { setIdNumber(value), setInValid(false) }}
                                    //editable={isEdit}

                                />
                                {/* <TouchableOpacity style={styles.changeContainer}
                                    onPress={() => navigation.navigate('UserChangePassword1')}
                                >

                                    <Text style={styles.changeTxt}>Change Password</Text>
                                    <Image source={AppImage.arrow_right}
                                        style={{ width: 10, height: 15, resizeMode: 'contain' }} />
                                </TouchableOpacity> */}
                                <Text style={styles.nameTitle}>Address</Text>
                                <TextInput style={styles.addrContainer}
                                    placeholder='Enter Address ...'
                                    value={address ? address : ''}
                                    //editable={isEdit}
                                    onChangeText={(value) => { setAddress(value), setInValid(false) }}

                                />
                                
                                    <Pressable style={{ width: '40%', borderRadius: 10, backgroundColor: '#003727', alignItems: 'center', marginTop: 10 }}
                                        onPress={() => onSubmit()}
                                    >
                                        <Text style={{ margin: 5, color: 'white', fontSize: 16 }}>Save profile</Text>
                                    </Pressable>
                            </View>
                        </ScrollView>
                        <Pressable style={{ position: 'absolute', top:35,left:-7}} onPress={() => toggleEditModal()}>
                            <Image source={AppImage.cross_icon}
                                style={{ width: 30, height: 30,}}
                            />
                        </Pressable>
                    </View>
                </Modal>
            </View>

        )

    }

    return (
        !isRender ?
            (
                <View style={{ flex: 1, backgroundColor: 'white', }}>
                    <ScrollView contentContainerStyle={{ paddingBottom: 20 }}>
                        <LinearGradient colors={['#007654', '#003727',]} style={styles.topView}>
                            <View style={styles.header}>
                                <Pressable style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 15, }} onPress={() => navigation.goBack()}>
                                    <Image source={AppImage.back_arrow_white}
                                        style={{ width: 16, height: 16, resizeMode: 'contain' }}
                                    />
                                    <Text style={{ color: 'white', fontSize: 17, fontFamily: AppFonts.medium, marginLeft: 10 }}>Profile</Text>
                                </Pressable>
                                
                                    <Pressable style={{ marginRight: 15, flexDirection: 'row', alignItems: 'center' }} onPress={() => { setIsEdit(true), toggleEditModal() }}>
                                        <Image source={AppImage.edit_white}
                                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                                        />
                                        <Text style={{ color: 'white', marginLeft: 10, fontSize: 16 }}>Edit</Text>
                                    </Pressable>
                            </View>
                            <View style={styles.imgContainer}>
                                
                                    <Image source={{ uri: profile !== null ? profile : null }}
                                        style={{ flex: 1 }}
                                    />
                                   
                                {/* {isEdit &&
                                    <Pressable style={{ position: 'absolute', right: 20, bottom: 20 }} onPress={() => toggleModal()}>
                                        <Image source={AppImage.edit_green}
                                            style={{ width: 27, height: 27, resizeMode: 'contain', }}
                                        />
                                    </Pressable>} */}
                            </View>
                        </LinearGradient>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={styles.nameTitle}>First name</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='User Name'
                                value={name ? name : ''}
                                onChangeText={(value) => { setName(value), setInValid(!inValid[0]) }}
                                editable={isEdit}

                            />
                            {inValid[0] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                            <Text style={styles.nameTitle}>Last name</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='last Name'
                                value={lastName ? lastName : ''}
                                onChangeText={(value) => { setLastName(value), setInValid(!inValid[1]) }}
                                editable={isEdit}

                            />
                            {inValid[1] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                            <Text style={styles.nameTitle}>Email</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='Email ID'
                                value={email ? email : ''}
                                onChangeText={(value) => { setEmail(value), setInValid(!inValid[2]) }}
                                editable={isEdit}


                            />
                            {inValid[2] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                            <Text style={styles.nameTitle}>Mobile</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='Mobile Number'
                                value={mobile ? mobile : ''}
                                onChangeText={(value) => { setMobile(value), setInValid(!inValid[3]) }}
                                maxLength={10}
                                editable={isEdit}

                            />
                            {inValid[3] && <Text style={styles.apiErrorStyle}>{error}</Text>}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between',width:'100%' }}>
                                {/* <TextInput style={[styles.mobileContainer1, { marginRight: 40 }]}
                                    placeholder='Enter DOB'
                                    value={dob ? dob : ''}
                                    onChangeText={(value) => { setDob(value), setInValid(false) }}
                                    editable={isEdit}
                                    onFocus={()=>showDatepicker()}

                                /> */}
                                <View style={{width:'50%',marginLeft:20}}>
                                <Text style={[styles.nameTitle,{marginLeft:0}]}>DOB</Text>
                                <Pressable style={[styles.mobileContainer1, { width:'80%' }]} onPress={() => showDatepicker()} disabled={!isEdit}>
                                    <Text style={{ color: isDate || date ? AppColor.appColor : 'grey', opacity: 1 }}>{date ? moment(date).format('DD-MM-YYYY') : "Enter DOB"}</Text>
                                </Pressable>
                                </View>
                                {show && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={new Date()}
                                        mode={'date'}
                                        is24Hour={true}
                                        display="default"
                                        onChange={onChange}
                                    />
                                )}
                                {/* <TextInput style={styles.mobileContainer1}
                                    placeholder='Enter Gender'
                                    value={gender ? gender : ''}
                                    onChangeText={(value) => { setGender(value), setInValid(false) }}
                                    editable={isEdit}

                                /> */}
                                <View style={{width:'50%'}}>
                                <Text style={[styles.nameTitle,{marginLeft:0}]}>Gender</Text>
                                <View style={[styles.mobileContainer1,{width:'80%'}]}>
                                    {/* {isEdit ? <Picker
                                        selectedValue={gender}
                                        style={{ color: gender.trim().length !== 0 ? AppColor.appColor : 'grey',backgroundColor:'red' }}
                                        mode='dropdown'
                                        onValueChange={(itemValue, itemIndex) =>
                                            setGender(itemValue)
                                        }>
                                        <Picker.Item label="Male" value="Male" />
                                        <Picker.Item label="Female" value="Female" />
                                        <Picker.Item label="Other" value="Other" />
                                    </Picker>
                                        :
                                        <Text style={{ color: gender ? AppColor.appColor : 'grey' }}>{gender ? gender : "Select Gender"}</Text>
                                    } */}
                                    <Text style={{ color: gender ? AppColor.appColor : 'grey' }}>{gender ? gender : "Select Gender"}</Text>
                                </View>
                                </View>
                            </View>
                            <Text style={styles.nameTitle}>Anniversary Date</Text>
                            <Pressable style={[styles.mobileContainer, { justifyContent: 'center' }]} onPress={() => showAnniverSaryPicker()} disabled={!isEdit}>
                                <Text style={{ color: anniversary ? AppColor.appColor : 'grey', opacity: 1 }}>{anniversary ? moment(anniversary !== '' ? anniversary : new Date()).format('DD-MM-YYYY') : "Enter Anniversary Date"}</Text>
                            </Pressable>
                            {showAnn && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={new Date()}
                                    mode={'date'}
                                    is24Hour={true}
                                    display="default"
                                    onChange={onChangeAnn}
                                />
                            )}
                            {/* <TextInput style={styles.mobileContainer}
                                placeholder='Enter Anniversary Date'
                                value={anniversary ? anniversary : ''}
                                onChangeText={(value) => { setAnniversary(value), setInValid(false) }}
                                editable={isEdit}
                            /> */}
                            <Text style={styles.nameTitle}>ID proof Name</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='ID proof Name'
                                value={id_name ? id_name : ''}
                                onChangeText={(value) => { setIdName(value), setInValid(false) }}
                                editable={isEdit}
                            />
                            <Text style={styles.nameTitle}>ID proof Number</Text>
                            <TextInput style={styles.mobileContainer}
                                placeholder='ID proof number'
                                value={id_number ? id_number : ''}
                                onChangeText={(value) => { setIdNumber(value), setInValid(false) }}
                                editable={isEdit}

                            />
                            <TouchableOpacity style={styles.changeContainer}
                                onPress={() => navigation.navigate('UserChangePassword1')}
                            >

                                <Text style={styles.changeTxt}>Change Password</Text>
                                <Image source={AppImage.arrow_right}
                                    style={{ width: 10, height: 15, resizeMode: 'contain' }} />
                            </TouchableOpacity>
                            <Text style={styles.nameTitle}>Address</Text>
                            <TextInput style={styles.addrContainer}
                                placeholder='Enter Address ...'
                                value={address ? address : ''}
                                editable={isEdit}
                                onChangeText={(value) => { setAddress(value), setInValid(false) }}

                            />
                            {isEdit &&
                                <Pressable style={{ width: '40%', borderRadius: 10, backgroundColor: '#003727', alignItems: 'center', marginTop: 10 }}
                                    onPress={() => onSubmit()}
                                >
                                    <Text style={{ margin: 5, color: 'white', fontSize: 16 }}>Save profile</Text>
                                </Pressable>}
                        </View>

                        {/* <View style={{ width: '100%', height: 100 }} /> */}

                    </ScrollView>
                    {renderModal()}
                    {editModal()}
                    <ActivityIndicator
                        animating={loader}
                        size={'large'}
                        color={AppColor.appColor}
                        style={{ position: 'absolute', alignSelf: 'center', top: '50%' }}
                    />
                    {/* <Loader loading={loader} /> */}

                </View>)
            :
            (<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 18, margin: 10, textAlign: 'center' }}>You don't have access to see your bookings please
                    <Text style={{ color: "blue" }} onPress={() => onLogin()}> click here to login.</Text></Text>
            </View>
            )
    )
}
const styles = StyleSheet.create({
    topView: {
        width: width,
        height: 180,

    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor:'red',
        marginTop: 7,
        justifyContent: 'space-between'
    },
    imgContainer: {
        width: 130,
        height: 130,
        borderRadius: 65,
        borderWidth: 2,
        borderColor: '#FFA800',
        alignSelf: 'center',
        backgroundColor: 'silver',
        elevation: 5,
        overflow: 'hidden'

    },
    mobileContainer: {
        //backgroundColor:'red',
        //arginLeft:20,
        //marginRight:20,
        width: '90%',
        height: 45,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        paddingLeft: 10,
        //textAlign:'center',
        color: AppColor.appColor,
        borderColor: AppColor.appColor,
    },
    mobileContainer1: {
        //backgroundColor:'white',
        //arginLeft:20,
        //marginRight:20,
        width: '40%',
        height: 45,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        paddingLeft: 10,
        //textAlign:'center',
        //alignItems:'center',
        justifyContent: 'center',
        color: AppColor.appColor,
        borderColor: AppColor.appColor,
    },
    changeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#003727',
        width: '90%',
        marginTop: 10,
        borderRadius: 10,
    },
    changeTxt: {
        textAlign: 'center',
        fontSize: 16,
        margin: 5,
        //fontFamily: AppFonts.medium,
        fontSize: 22,
        color: 'white'
    },
    addrContainer: {
        //backgroundColor:'red',
        //arginLeft:20,
        //marginRight:20,
        width: '90%',
        height: 100,
        marginTop: 10,
        borderRadius: 10,
        textAlignVertical: 'top',
        borderWidth: 1,
        paddingLeft: 10,
        //textAlign:'center',
        color: AppColor.appColor,
        borderColor: AppColor.appColor,
    },
    nameTitle:{
        alignSelf:'flex-start',
        marginLeft:20,
        marginTop:10,
        color:'black'
    }


})

export default Profile;