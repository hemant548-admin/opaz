import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList, StyleSheet, Pressable,
    Dimensions, Image, TouchableOpacity,
} from 'react-native';
import { useDispatch } from 'react-redux'
import Header from '../../../component/header/Header';
import Loader from '../../../component/loader/Loader';
import { checkScreen } from '../../../redux/reducer';
import { AuthContext } from '../../../Routes/AuthContext';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppImage from '../../../utils/AppImage';
import AppStrings from '../../../utils/AppStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import APIStrings from '../../../webservice/APIStrings';
import { useIsFocused } from '@react-navigation/native';

const arr = [{
    id: '1', hotelName: 'Maharaja Hotel', address: 'Pune, Maharashtra', rating: '4.2',
    review: '900', landmark: '2 km from station', price: 'Rs. 3200/Night', actualPrice: '5000', cancellation: true
}, {
    id: '2', hotelName: 'Hotel Burj Khalifa', address: 'Palm jumera, Dubai', rating: '4.9',
    review: '1000', landmark: '2 km from Airport', price: 'Rs. 100000/Night', actualPrice: '500000', cancellation: false
}, {
    id: '3', hotelName: 'Hotel Greenland', address: 'Pune, Maharashtra', rating: '3.2',
    review: '400', landmark: '2 km from station', price: 'Rs. 3200/Night', actualPrice: '5000', cancellation: true
},
]
const Mybooking = ({ navigation }) => {
    const [isColor, setisColor] = useState(1);
    const [userId, setUserId]=useState('');
    const [loader,setLoader]=useState();
    const [message,setMessage]=useState('');
    const [upcoming,setUpcoming]=useState([]);
    const [isRender, setisRender]=useState(false)
    const [type, setType]=useState('')
    const dispatch=useDispatch();
    const { signOut } = React.useContext(AuthContext);
    const isFocused = useIsFocused();

    useEffect( async()=>{
        id= await AsyncStorage.getItem(AppStrings.USER_ID);
        setUserId(id)
        if(id!==null){
        UpcomingBooking(id);
        }
        else{
            setisRender(true)
        }
    },[isFocused])
    //const change=React.useMemo(()=>{ setisColor(1), UpcomingBooking(userId)},[isFocused])
    const onLogin=()=>{
        dispatch(checkScreen('Login'))
        signOut()
    }
    const UpcomingBooking=async ()=>{
        setisColor(1)
        setType(1);
        id= await AsyncStorage.getItem(AppStrings.USER_ID);
        setLoader(true)
        let requsetBody=JSON.stringify({
            user_id:id,
          })
          console.log(requsetBody)
          console.log(APIStrings.EnUserUpcomingBooking)
          simplePostCall(APIStrings.EnUserUpcomingBooking, requsetBody)
          .then((data) => {

              setLoader(false)
              //console.log(data.token);
              if (data.status.code === 200) {
                  console.log("Success", JSON.stringify(data));
                  setUpcoming(data.booking_list)
                  if(data.booking_list.length===0){
                      setMessage("You don't have any upcoming booking")
                  }
                 // alert(`Success: ${data.razorpay_payment_id}`);
              }
              else {
                  //this.setState({ invalid: true, errorMessage: data.message });
                  console.log(data.status.message)

              }

          })
          .catch((error) => {
              console.log("error ", error);
               setLoader(false)
              alert('api error')
              //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
          });
      }
      const pastBooking= async ()=>{
        setisColor(2)
        setType(2)
        id= await AsyncStorage.getItem(AppStrings.USER_ID);
        setLoader(true)
        let requsetBody=JSON.stringify({
            user_id:id,
          })
          console.log(requsetBody)
          console.log(APIStrings.EndUserPastBooking)
          simplePostCall(APIStrings.EndUserPastBooking, requsetBody)
          .then((data) => {

              setLoader(false)
              //console.log(data.token);
              if (data.status.code === 200) {
                  console.log("Success", JSON.stringify(data));
                  setUpcoming(data.booking_list)
                  if(data.booking_list.length===0){
                    setMessage("You don't have any past booking")
                }
                 // alert(`Success: ${data.razorpay_payment_id}`);
              }
              else {
                  //this.setState({ invalid: true, errorMessage: data.message });
                  console.log(data.status.message)

              }

          })
          .catch((error) => {
              console.log("error ", error);
               setLoader(false)
              alert('api error')
              //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
          });
      }
      const CancelBooking= async ()=>{
        setisColor(3)
        setType(3)
        id= await AsyncStorage.getItem(AppStrings.USER_ID);
        setLoader(true)
        let requsetBody=JSON.stringify({
            user_id:id,
          })
          console.log(requsetBody)
          console.log(APIStrings.EnduserBookingCancelList)
          simplePostCall(APIStrings.EnduserBookingCancelList, requsetBody)
          .then((data) => {

              setLoader(false)
              //console.log(data.token);
              if (data.status.code === 200) {
                  console.log("Success", JSON.stringify(data));
                  setUpcoming(data.booking_list)
                  if(data.booking_list.length===0){
                    setMessage("You don't have any Cancel booking")
                }
                 // alert(`Success: ${data.razorpay_payment_id}`);
              }
              else {
                  //this.setState({ invalid: true, errorMessage: data.message });
                  console.log(data.status.message)

              }

          })
          .catch((error) => {
              console.log("error ", error);
               setLoader(false)
              alert('api error')
              //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
          });
      }
    const renderList = (item, index) => {
        return (
            <Pressable style={styles.card} onPress={() => navigation.navigate('BookingDetail',{data:item,type:isColor})}>
                <Image source={{uri:item.branch.branch_image[0].image}}
                    style={{ flex: 0.7, height: '100%' }}
                />
                <View style={styles.innercard}>
                    <Text style={{ fontSize: 16, color: AppColor.appColor }}>Booking ID: {item.id}</Text>
                    <Text style={{ color: AppColor.appColor,fontSize:11,marginTop:6,marginBottom:6 }}>Booking Person Name: {item.user.first_name}</Text>
                    <Text style={{ fontSize: 16, color: AppColor.appColor }}>{item.branch.branch_name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                        <Image source={AppImage.marker}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                        <Text style={{ color: AppColor.input_placeholderColor, marginLeft: 5,marginRight:23 }}>{item.branch.first_address}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Image source={AppImage.star_orang}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                        <Text style={{ margin: 5 }}>{item.branch.total_rating}</Text>
                        <Text style={{ marginLeft: 20, fontSize: 11, color: 'grey' }}> review</Text>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Image source={AppImage.calenderBooking}
                        style={{width:17,height:17,resizeMode:'contain',marginRight:5}}
                        />
                    <Text>{moment(item.from_booking_date).format('DD MMM YYYY')} - {moment(item.to_booking_date).format('DD MMM YYYY')}</Text>
                    </View>
                    {/* <Text style={{ marginTop: 5, marginBottom: 5, color: AppColor.input_placeholderColor }}>{item.landmark}</Text> */}
                    <View style={{}}>
                        <Text style={{ marginTop: 5, marginBottom: 5, color: AppColor.appColor, fontWeight: 'bold' }}>{item.room_no.final_price}/Night   <Text
                            style={{ color: 'grey', marginLeft: 30, textDecorationLine: 'line-through' }}>   {item.room_no.price}</Text></Text>
                        <Text style={{ color: AppColor.appColor, marginLeft: 80, fontFamily: AppFonts.light, fontSize: 11, bottom: 2 }}>+tax and charges</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={null}
                            style={{ width: 15, height: 15, resizeMode: 'contain' }}
                        />
                        <Text style={{ marginTop: 5, marginBottom: 5, color: item.room_no.cancellation_status!=="ancellation_fees_applicable" ? '#02C115' : 'red' }}>{item.room_no.cancellation_status!=="ancellation_fees_applicable" ? "Free Cancellation" : "Cancellation fees applicable"}</Text>
                    </View>
                    <TouchableOpacity style={styles.btnContainer1} onPress={()=>navigation.navigate('BookingDetail',{data:item,type:isColor})}>
                        <Text style={{color:'white',fontFamily:AppFonts.medium}}>See Details</Text>
                    </TouchableOpacity>
                </View>
            </Pressable>
        )
    }

    return (
        !isRender? (
        <View style={{ flex: 1 }}>
            <Header
                title='My Booking'
                goBack={() => navigation.goBack()}
            />
            <View style={{ width: "100%", backgroundColor: 'white', flexDirection: 'row', marginTop: 5 }}>
                <Pressable style={styles.subContainer} onPress={() => UpcomingBooking()}>
                    <Text style={{ fontSize: 18, color: isColor === 1 ? 'black' : 'grey', fontWeight: isColor === 1 ? 'bold' : '300' }}>Upcoming</Text>
                </Pressable>
                <Pressable style={styles.subContainer}
                    onPress={() => pastBooking()}
                >
                    <Text style={{ fontSize: 18, color: isColor === 2 ? 'black' : 'grey', fontWeight: isColor === 2 ? 'bold' : '300' }}>Past booking</Text>
                </Pressable>
                <Pressable style={styles.subContainer} onPress={() => CancelBooking()}>
                    <Text style={{ fontSize: 18, color: isColor === 3 ? 'black' : 'grey', fontWeight: isColor === 3 ? 'bold' : '300' }}>Cancel</Text>
                </Pressable>
            </View>
            <View style={{ flex: 1, }}>
                {upcoming.length>0 ?
                <FlatList
                    data={upcoming}
                    key={item => item.id}
                    renderItem={({ item, index }) => renderList(item, index)}
                />
                :
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:"red"}}>{message}</Text>
                    </View>
                }
            </View>
            <Loader loading={loader}/>
        </View>)
        :
           ( <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:18,margin:10,textAlign:'center'}}>You don't have access to see your bookings please 
                <Text style={{color:"blue"}} onPress={()=>onLogin()}> click here to login.</Text></Text>
            </View>
           )
    )
}
const styles = StyleSheet.create({
    subContainer: {
        flex: 1,
        alignItems: 'center',
        borderRightWidth: 1,
        marginTop: 12,
        borderColor: 'grey',
        marginBottom: 12

    },
    card: {
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 5

    },
    innercard: {
        marginLeft: 15,
        marginBottom: 5,
        marginTop: 10,
        flex: 1,
        //backgroundColor:'red'
    },
    btnContainer: {
        width: 120,
        height: 40,
        // backgroundColor:'red',
        borderRadius: 10,
        elevation: 3,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        //borderWidth:0.1,
        borderColor: AppColor.appColor
    },
    btnContainer1: {
        width: '55%',
        height: 34,
        backgroundColor: '#003727',
        borderRadius: 20,
        elevation: 3,
       // margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:5,
        borderColor: AppColor.appColor
    },

})

export default Mybooking;