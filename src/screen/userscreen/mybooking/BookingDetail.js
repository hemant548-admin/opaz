import React, { useState, useEffect, useRef } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, Animated, Alert,
    ScrollView, ImageBackground, StyleSheet, SafeAreaView, Pressable
} from 'react-native';
import { Container } from 'native-base';
import Toast from 'react-native-simple-toast';
import LinearGradient from 'react-native-linear-gradient';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import APIStrings from '../../../webservice/APIStrings';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import Loader from '../../../component/loader/Loader';
import MapView, { Marker } from 'react-native-maps';
import ImageSlider from 'react-native-image-slider';
import axios from 'axios';
import Stars from 'react-native-stars';
import moment from 'moment';
import { useIsFocused } from '@react-navigation/native';
import AppStyles from '../../../utils/AppStyles';

const { width, height } = Dimensions.get('window')
const imgArray = [AppImage.hotel_bg2, AppImage.hotel_bg1,]
const star = [AppImage.star_orang, AppImage.star_orang, AppImage.star_orang, AppImage.star_orang, AppImage.star_icon]
const aminity = [{ id: '1', img: AppImage.bowl_icon, name: 'Breakfast' }, { id: '2', img: AppImage.bar_icon, name: 'Bar' },
{ id: '3', img: AppImage.pool_icon, name: 'Pool' }, { id: '4', img: AppImage.spa_icon, name: 'spa' }
]
const rating = [{ id: '1', review: 'Lorem ipsum is the dummy text lorem dolar sit emet' },
{ id: '1', review: 'Lorem ipsum is the dummy text lorem dolar sit emet' }, { id: '1', review: 'Lorem ipsum is the dummy text lorem dolar sit emet' },
{ id: '1', review: 'Lorem ipsum is the dummy text lorem dolar sit emet' },
]
const HEADER_HEIGHT = height * 0.38;


const BookingDetail = ({ navigation, route }) => {
    //console.log(width, height)
    const offset = useRef(new Animated.Value(0)).current;
    const [stars, setStars] = useState(0);
    const [loader, setLoader] = useState(false);
    const [tab, setTab] = useState(1);
    const [ratings, setRatings] = useState('');
    const [review, setReview] = useState('');
    const [error, setError] = useState('');
    const [inValid, setInValid] = useState([false, false])
    const { data, type } = route.params;
    const [item, setItem] = useState(data)
    const [isComment, setIsComment] = useState(false);
    const [comment, setComment] = useState([]);
    const [name, setName] = useState('');
    const isFocused = useIsFocused();
    // console.log('item', JSON.stringify(data))

    useEffect(() => {

        setItem(data);
        // nameData();
        setComment(data.branch.rating_and_review);
        BookingDetails(data.id);

    }, [data.id, isFocused])
    const onTabPress = (tab) => {
        setTab(tab)
    }

    const CancelConfirmation = () => {

        Alert.alert(
            'Note !',
            'Are you sure you want to Cancel Booking?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => { BookinCancel(); } },
            ],
            { cancelable: false });
        return true;

    }
    const BookinCancel = async () => {
        const id = await AsyncStorage.getItem(AppStrings.USER_ID);
        const num = await AsyncStorage.getItem(AppStrings.PHONE);
        setLoader(true)
        let requsetBody = JSON.stringify({
            booking_id: item.id,
            login_user_mobile_number: num
        })
        console.log(requsetBody)
        console.log(APIStrings.EnUserBookingCancel)
        simplePostCall(APIStrings.EnUserBookingCancel, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Success", JSON.stringify(data));
                    //setItem(data.booking_list)
                    Toast.show('Booking Cancel Sucessfully');

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const BookingDetails = async (id) => {
        const first = await AsyncStorage.getItem(AppStrings.FIRST_NAME, '');
        const last = await AsyncStorage.getItem(AppStrings.LAST_Name, '');
        const names = first + " " + last;
        setName(names);
        //id= await AsyncStorage.getItem(AppStrings.USER_ID);
        setLoader(true)
        let requsetBody = JSON.stringify({
            booking_id: id,
        })
        console.log(requsetBody)
        console.log(APIStrings.BookingDetails)
        simplePostCall(APIStrings.BookingDetails, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Success", JSON.stringify(data));
                    setItem(data.booking_obj);
                    setComment(data.booking_obj.branch.rating_and_review)
                    let temp;
                    data.booking_obj.branch.rating_and_review.forEach(element => {
                        temp = element.name === names ? true : false
                    });
                    setIsComment(temp);
                    //const temp=data.booking_obj.branch.rating_and_review.includes(names);
                    console.log('bvnvj ', temp, names)
                    //Toast.show('Booking Cancel Sucessfully');

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const RatingandReview = async () => {

        if (review.trim().length === 0) {
            setError('Please Write a review')
            setInValid([true, false])
            return
        }
        if (stars === 0) {
            setError('Please select star rating')
            setInValid([false, true])
            return
        }
        const id = await AsyncStorage.getItem(AppStrings.USER_ID);
        const nam = await AsyncStorage.getItem(AppStrings.FIRST_NAME, '');
        const last = await AsyncStorage.getItem(AppStrings.LAST_Name, '');
        setLoader(true)
        let requsetBody = JSON.stringify({
            user_id: id,
            branch_id: item.branch.id,
            rating: Math.floor(stars),
            review: review,
            person_name: nam + " " + last
        })
        console.log(requsetBody)
        console.log(APIStrings.EndUserRatingAndReview)
        simplePostCall(APIStrings.EndUserRatingAndReview, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Success", JSON.stringify(data));
                    BookingDetails(item.id);
                    Toast.show(data.message);
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            })
            .catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }

    const AnimatedHeader = () => {
        const insets = useSafeAreaInsets();

        const headerHeight = offset.interpolate({
            inputRange: [0, HEADER_HEIGHT + insets.top],
            outputRange: [HEADER_HEIGHT + insets.top, insets.top + 10],
            extrapolate: 'clamp'
        });

        return (
            <Animated.View style={
                {
                    width: '100%',
                    //position: 'absolute',
                    top: 32,
                    height: headerHeight,
                    backgroundColor: 'lightblue'
                }
            }>

                {item.branch.branch_image.length > 0 ?
                    <ImageSlider
                        loopBothSides
                        autoPlayWithInterval={1000}
                        images={item.branch.branch_image}
                        customSlide={({ index, item, style, width }) => (
                            //console.log('data ',item),
                            // It's important to put style here because it's got offset inside
                            <View key={index} style={{ flex: 1 }}>
                                <Image source={{ uri: item.image }} style={{ flex: 1, width: width, height: 100 }} />
                            </View>
                        )}
                    />

                    :
                    <View style={{}}>
                        <Image source={null}
                            style={{ flex: 1, resizeMode: 'cover', backgroundColor: 'silver' }}
                        />
                    </View>}

            </Animated.View>

        );
    };

    const renderAminity = (item, index) => {
        return (
            <View style={{ margin: 10, marginLeft: 20 }}>
                <View style={styles.aminity}>
                    <Image source={{ uri: item.service_photo }}
                        style={{ width: 30, height: 30, resizeMode: 'contain' }}
                    />
                </View>
                <Text style={styles.aminityTxt}>{item.service_name}</Text>
            </View>
        )
    }
    const renderComment = (item, index) => {
        return (
            <View style={{ backgroundColor: 'white', width: '95%', marginTop: 10, alignSelf: 'center', borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', margin: 15, alignItems: 'center', flex: 1 }}>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{
                            width: 35, height: 35, borderRadius: 18, backgroundColor: 'silver',
                            alignItems: 'center', justifyContent: 'center',
                        }}>
                            <Image source={AppImage.name_icon}
                                style={{ width: 18, height: 18, resizeMode: 'contain' }}
                            />
                        </View>
                        <Text style={{ marginTop: 5 }}>{item.name}</Text>
                    </View>
                    <View style={{ marginLeft: 10, marginRight: 20, paddingLeft: 10, width: '75%' }}>
                        <Text style={{}}>{item.review}</Text>
                    </View>
                </View>

            </View>
        )
    }

    const reviewScreen = () => {
        return (
            <View style={{ alignItems: 'center' }}>
                {!isComment &&
                    <View style={{
                        alignItems: 'center', width: '90%', backgroundColor: 'white', marginTop: 10,
                        borderRadius: 8, elevation: 5
                    }}>
                        <TextInput
                            style={{ width: '90%', height: 120, textAlignVertical: 'top', borderBottomWidth: 1, borderBottomColor: 'grey' }}
                            placeholder='Write your review here'
                            value={review}
                            onChangeText={(value) => { setReview(value), setInValid(!inValid[0]) }}
                            multiline={true}
                            numberOfLines={5}
                        />
                        {inValid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                        <View style={{
                            backgroundColor: AppColor.appColor, width: '80%', marginTop: 10, height: 30,
                            alignItems: 'center', justifyContent: 'center'
                        }}>
                            <Stars
                                //half={true}
                                default={0}
                                update={(val) => { setStars(val), setInValid(!inValid[1]) }}
                                spacing={4}
                                starSize={20}
                                count={5}
                                fullStar={AppImage.star_orang}
                                emptyStar={AppImage.star_icon}
                            //halfStar={require('./images/starHalf.png')} 
                            />
                        </View>
                        {inValid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                        <Pressable style={{ width: '30%', backgroundColor: '#003727', alignItems: 'center', borderRadius: 20, marginTop: 10, marginBottom: 10 }}
                            onPress={() => RatingandReview()}
                        >
                            <Text style={{ margin: 5, fontSize: 18, color: "white", }}>Submit</Text>
                        </Pressable>
                    </View>}
                <View>
                    <FlatList
                        data={item.branch.rating_and_review}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => renderComment(item, index)}
                    />
                </View>

            </View>
        )
    }
    return (
        <View style={{ flex: 1, }}>
            {/* <View style={{height:20,width:'100%',backgroundColor:'red',top:0}}/> */}
            <View style={{ flex: 1, backgroundColor: AppColor.appColor }}>


                <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ flex: 1, }}
                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={16}
                // onScroll={Animated.event(
                //     [{ nativeEvent: { contentOffset: { y: offset } } }],
                //     { useNativeDriver: false }
                // )}
                >
                    {AnimatedHeader()}
                    <View style={{
                        flex: 1, backgroundColor: AppColor.appColor, borderTopLeftRadius: 40, borderTopRightRadius: 40,
                        borderTopColor: AppColor.appColor,
                    }}>
                        <View style={{ flexDirection: 'row', margin: 5, justifyContent: "space-around", marginTop: 18 }}>
                            <TouchableOpacity style={{ width: '30%', borderRadius: 20, alignItems: 'center', backgroundColor: '#028B63', elevation: 5 }}>
                                <Text style={{ color: 'white', margin: 5, fontSize: 18, fontFamily: AppFonts.medium }}
                                    onPress={() => onTabPress(1)}
                                >Information</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: '23%', borderRadius: 20, alignItems: 'center', backgroundColor: '#028B63', elevation: 5 }}
                                onPress={() => onTabPress(2)}
                            >
                                <Text style={{ color: 'white', margin: 5, fontSize: 18, fontFamily: AppFonts.medium }}>Map</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: '25%', borderRadius: 20, alignItems: 'center', backgroundColor: '#028B63', elevation: 5 }}
                                onPress={() => onTabPress(3)}
                            >
                                <Text style={{ color: 'white', margin: 5, fontSize: 18, fontFamily: AppFonts.medium }}>Review</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 20, marginLeft: 20 }}>
                            <Text style={{ fontSize: 16, color: 'white' }}>{item.branch.branch_name}</Text>
                            <Text style={styles.title}>{item.room_no.room_type} Room</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                <Image source={AppImage.location_icon}
                                    style={{ width: 20, height: 20, resizeMode: 'contain', }}
                                />
                                <Text style={{
                                    color: '#ADBDB9', fontFamily: AppFonts.light,
                                    marginLeft: 10, fontSize: 17
                                }}>{item.room_no.landmark}</Text>

                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10, marginTop: 5, alignSelf: 'flex-end' }}>
                                {/* {star.map(item => (
                                        <Image source={item}
                                            style={{ width: 15, height: 15, resizeMode: 'contain', marginLeft: 5 }}
                                        />
                                    ))} */}
                                <Stars
                                    //half={true}
                                    default={item.branch.total_rating}
                                    // update={(val) => { setStars(val), setInValid(!inValid[1]) }}
                                    spacing={4}
                                    disabled={true}
                                    starSize={15}
                                    count={5}
                                    fullStar={AppImage.star_orang}
                                    emptyStar={AppImage.star_icon}
                                //halfStar={require('./images/starHalf.png')} 
                                />
                            </View>
                            {tab === 1 &&
                                <View>
                                    <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Description:
                                        <Text style={{
                                            color: '#ADBDB9', fontFamily: AppFonts.light,
                                            fontSize: 17, marginRight: 20, marginTop: 35
                                        }}>
                                            {item.room_no.description}
                                        </Text> </Text>
                                    <Text style={styles.title}>Amenities</Text>
                                    <View>
                                        <FlatList
                                            data={item.branch.amenities}
                                            horizontal
                                            showsHorizontalScrollIndicator={false}
                                            keyExtractor={item => item.id}
                                            renderItem={({ item, index }) => renderAminity(item, index)}
                                        />
                                    </View>

                                    {/* <Text style={styles.title}>Covid Policies</Text>
                            <View  style={{
                                    width: '92%', height: 170, backgroundColor: 'white', borderRadius: 10,
                                    textAlignVertical: 'top', marginTop: 10, alignSelf: 'center',alignItems:'center'
                                }}>
                            <Text style={{fontSize:16,color:'grey',margin:10,marginTop:15}}>Lorem ipsum is the dummy text of writing  anythibg,Lorem ipsum is the dummy text of writing  anythibg,
                            Lorem ipsum is the dummy text of writing  anythibg Lorem ipsum is the dummy text of writing  anythibg
                            </Text>
                            </View> */}
                                    <Text style={styles.title}>Date:</Text>
                                    <View style={styles.picker}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={AppImage.calender_white}
                                                    style={styles.timeImg}
                                                />
                                                <Text style={{ color: 'white', fontSize: 18, marginLeft: 15 }}>{moment(item.from_booking_date).format('DD MMM YYYY')} - {moment(item.to_booking_date).format('DD MMM YYYY')}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <Text style={styles.title}>Policies</Text>
                                    <Pressable style={styles.policy} onPress={() => navigation.navigate('CancelPolicy', { name: "Covid Policy", data: item.branch.policies })}>
                                        <Text style={{ fontSize: 21, color: '#ADBDB9', marginLeft: 15 }}>Covid Policy</Text>
                                        <Image source={AppImage.arrow_right}
                                            style={{ width: 12, height: 12, resizeMode: 'contain', marginRight: 20 }}
                                        />
                                    </Pressable>
                                    <Pressable style={styles.policy} onPress={() => navigation.navigate('CancelPolicy', { name: "Cancel Policy", data: item.branch.policies })}>
                                        <Text style={{ fontSize: 21, color: '#ADBDB9', marginLeft: 15 }}>Cancellation Policy</Text>
                                        <Image source={AppImage.arrow_right}
                                            style={{ width: 12, height: 12, resizeMode: 'contain', marginRight: 20 }}
                                        />
                                    </Pressable>
                                </View>}

                            {/* <Text style={[styles.title, { marginTop: 20 }]}>Contact</Text>
                            <View style={styles.contact}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    <Image source={AppImage.profile_white}
                                        style={styles.timeImg}
                                    />
                                    <Text style={styles.contactTxt}>{item.branch.manager_name}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                    <Image source={AppImage.help}
                                        style={styles.timeImg}
                                    />
                                    <Text style={styles.contactTxt}>{item.branch.contact_number}</Text>
                                </View>
                            </View>
                            <Text style={styles.title}>Map</Text>
                            <MapView
                                style={{ height: 120, width: '95%', marginTop: 10 }}
                                initialRegion={{
                                    latitude: 18.508482,
                                    longitude: 73.795823,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}
                            >
                                <Marker
                                    coordinate={{
                                        latitude: Number(item.branch.latitude),
                                        longitude: Number(item.branch.longitude),
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                    title={item.branch.branch_name}
                                //description={marker.description}
                                />
                            </MapView>
                            <Text style={styles.title}>Select room</Text>
                            <View style={styles.picker}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={AppImage.room_icon}
                                            style={styles.timeImg}
                                        />
                                        <Picker
                                            selectedValue={room}
                                            style={{ height: 40, width: '88%', alignSelf: 'center', color: 'white', marginLeft: 10, fontSize: 16, backgroundColor: '#028B63' }}
                                            mode='dropdown'
                                            onValueChange={(itemValue, itemIndex) =>
                                                setRoom(itemValue)
                                            }>
                                            <Picker.Item label="Double room" value="Double room" />
                                            <Picker.Item label="Single room" value="Single room" />
                                        </Picker>
                                    </View>
                                    <Image source={AppImage.arrow_white}
                                        style={{ width: 15, height: 15, resizeMode: 'contain', transform: [{ rotate: "90deg" }], right: 35 }}
                                    />
                                </View>
                            </View>
                            <Pressable style={{
                                width: '50%', alignSelf: 'center', alignItems: 'center',
                                backgroundColor: 'white', borderRadius: 20, marginTop: 20, justifyContent: 'center'
                            }} onPress={()=>navigation.navigate('Calendar')}
                            >
                                <Text style={{ margin: 7, fontSize: 18, fontFamily: AppFonts.medium }}>Find My Room</Text>
                            </Pressable>*/}
                        </View>
                        {tab === 2 &&
                            <View style={{}}>
                                <MapView
                                    style={{ height: 300, width: '100%', marginTop: 10 }}
                                    initialRegion={{
                                        latitude: 18.508482,
                                        longitude: 73.795823,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: Number(item.branch.latitude),
                                            longitude: Number(item.branch.longitude),
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                    //title={}
                                    //description={marker.description}
                                    />
                                </MapView>
                            </View>}
                        {tab === 3 && reviewScreen()}
                        <View style={{ width: '100%', height: 40, }} />
                    </View>
                    <TouchableOpacity style={{ top: 10, left: 15, position: 'absolute' }} activeOpacity={0.7}
                        onPress={() => navigation.goBack()}
                    >
                        <Image source={AppImage.back_arrow_white}
                            style={{ height: 15, width: 15, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>
                    {/* <Loader loading={loader} /> */}
                    {/* <View style={{ width: '100%', height: 40 }} /> */}
                </ScrollView>

                <View style={{
                    width: '100%', height: '8%', backgroundColor: 'white'
                    , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center', elevation: 3
                }}>
                    {type == 1 &&
                        <Pressable style={{ backgroundColor: '#003727', alignItems: 'center', borderRadius: 20, }}
                            onPress={() => CancelConfirmation()}
                        >
                            <Text style={{ margin: 5, marginLeft: 12, marginRight: 10, fontSize: 16, color: 'white', }}>Cancel Booking </Text>
                        </Pressable>
                    }
                    <Pressable style={{ width: '30%', backgroundColor: '#003727', alignItems: 'center', borderRadius: 20, marginLeft: type === 1 ? 30 : 0 }}
                        onPress={() => navigation.navigate('Calendar', { id: item.branch.id })}
                    >
                        <Text style={{ margin: 5, fontSize: 16, color: "white", }}>Book Again</Text>
                    </Pressable>
                </View>
            </View>
            <Loader loading={loader} />
            {/* </SafeAreaView> */}
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '36%',
        top: 30,
    },
    topHeader: {
        //marginTop: 10,
        //marginLeft: 30,
        // backgroundColor:'yellow',
        position: 'absolute',
        justifyContent: 'center',
        width: '100%',
        height: '22%',
        alignSelf: 'center',
        top: 0
        //marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {

        color: 'white',
        fontFamily: AppFonts.light,
        fontSize: 17
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        backgroundColor: AppColor.appColor,
        width: '50%',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20,
        alignSelf: 'center'
    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 7
    },
    title: {
        fontSize: 22,
        color: 'white',
        marginTop: 15,
        // fontFamily:AppFonts.medium
    },
    aminity: {
        width: 55,
        height: 55,
        borderRadius: 10,
        elevation: 4,
        borderWidth: 1,
        borderColor: AppColor.appColor,
        backgroundColor: '#028B63',
        alignItems: 'center',
        justifyContent: 'center'

    },
    aminityTxt: {
        color: 'white',
        textAlign: 'center',
        marginTop: 10
    },
    picker: {
        width: '94%',
        height: 45,
        elevation: 3,
        backgroundColor: '#028B63',
        borderRadius: 5,
        justifyContent: 'center',
        marginTop: 10

    },
    policy: {
        width: '94%',
        height: 50,
        flexDirection: 'row',
        elevation: 3,
        backgroundColor: '#028B63',
        borderRadius: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10

    },
    timeImg: {
        width: 25,
        height: 25,
        resizeMode: 'contain',
        marginLeft: 10
    },
    contact: {
        width: '94%',
        height: 85,
        elevation: 3,
        backgroundColor: '#028B63',
        borderRadius: 5,
        justifyContent: 'space-evenly',
        marginTop: 10

    },
    contactTxt: {
        textAlign: 'center',
        fontSize: 16,
        color: 'white',
        marginLeft: 15,
        marginTop: 4
    }

})

export default BookingDetail;