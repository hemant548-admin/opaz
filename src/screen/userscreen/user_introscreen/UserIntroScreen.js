
import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ImageBackground
} from 'react-native';
import { Container } from 'native-base';
import AppIntroSlider from 'react-native-app-intro-slider';
import AppImage from '../../../utils/AppImage';
import AppFonts from '../../../utils/AppFonts';
import AppColor from '../../../utils/AppColor';
import { AuthContext } from '../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')
const string1 = "Let's find  a perfect hotel for you to stay and relax"
const string2 = 'You enjoy!  We manage everything for you with in this app'

const slides = [
    {
        key: 1,
        //title: 'Manage all Your Bookings',
        text: string1,
        image: AppImage.Beach_curve,
        image1:AppImage.COZY_YOU
        //backgroundColor: '#59b2ab',
    },
    {
        key: 2,
        //title: 'Handle Your Staff from anywhere',
        text: string2,
        image: AppImage.Beach_curve2,
        image1: AppImage.THE_BEST,
        backgroundColor: '#febe29',
    },
    
];

const UserIntroScreen = ({ navigation }) => {
    const [isRender, setRender] = useState(false)
    const { signSkip } = React.useContext(AuthContext);

    const renderIntro = ({ item }) => {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ justifyContent: 'space-between', flex: 1 }}>
                    <View style={{ width: width, height: height * 0.58, }}>
                        <Image
                            source={item.image}
                            style={{ width: '100%', height: '100%', resizeMode: 'stretch' }}
                        />
                        <View style={{ alignItems: 'center', bottom: 70 }}>
                            <Image source={item.image1}
                                style={{ width: 115, height: 22,resizeMode:'contain' }} />
                            <Text style={{
                                fontSize: 18, margin: 25, textAlign: 'center', marginLeft: 35,
                                marginRight: 35, fontFamily: AppFonts.light
                            }}
                            >{item.text}</Text>
                        </View>
                    </View>
                   
                    <View style={{ justifyContent: 'flex-end',flex:1 }}>
                        <Image
                            source={AppImage.vector_curve}  
                            style={{ width: width, height: height * 0.26, justifyContent: 'flex-end', resizeMode: 'stretch' }}
                         />
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between',position:'absolute' }}>

                                <Text style={{ color: 'white', fontSize: 18, marginLeft: 10, marginBottom: 20, marginLeft: 20 }}
                                    onPress={() => signSkip()}
                                >Skip</Text>
                                <Text style={{ color: '#00FFF7', fontSize: 18, marginLeft: 10, marginBottom: 20, marginRight: 20 }}
                                    onPress={() => navigation.navigate('SignUp')}
                                >Register</Text>
                            </View>
                        {/* </Image> */}
                    </View>
                </View>
            </View>
        )
    }
    
    return (
        <AppIntroSlider
            renderItem={renderIntro}
            data={slides}
            showNextButton={false}
            showDoneButton={false}
            //onDone={this._onDone}
            dotStyle={{ bottom: '40%', backgroundColor: '#4D007654' }}
            activeDotStyle={{ bottom: '40%', backgroundColor: AppColor.appColor }}
            />
        // !isRender ?
        //    renderIntro1()
        //     :
        //     renderIntro2()
    )
}

export default UserIntroScreen;

