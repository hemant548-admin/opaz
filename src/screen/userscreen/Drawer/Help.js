import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet,Pressable,Linking
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
//import FabButton from '../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import { DrawerActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';

const { width, height } = Dimensions.get('screen')


const Help = ({ navigation }) => {

    const [hotelName, setHotelName] = useState('');
    const [location, setLocation] = useState('');
    const [state, setState] = useState('');
    const [role, setRole] = useState('');
  
    // useEffect(() => {
    //   const data = async () => {
    //     const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME, '');
    //     const loc = await AsyncStorage.getItem(AppStrings.CITY, '');
    //     const stat = await AsyncStorage.getItem(AppStrings.STATE, '');
    //     const role = await AsyncStorage.getItem(AppStrings.ROLE, '');
    //     setHotelName(hotel);
    //     setLocation(loc);
    //     setState(stat);
    //     setRole(role);
    //   }
    //   data();
    // }, [])
    const shareToWhatsAppWithContact = (text, phoneNumber) => {
        Linking.openURL(`whatsapp://send?text=${text}&phone=${phoneNumber}`);
       }
     const dialCall = () => {

        let phoneNumber = '';
    
        if (Platform.OS === 'android') {
          phoneNumber = 'tel:${+1234567890}';
        }
        else {
          phoneNumber = 'telprompt:${+1234567890}';
        }
    
        Linking.openURL(phoneNumber);
      };
    const gotoScreen = () => {
        const screen = role === 'Hotel' ? 'Homedrawer' : role === 'FrontDesk' ? 'FrontDesk' : 'BranchManagerHome';
        navigation.navigate(screen)
      }
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.closeDrawer());
    };
    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <View style={styles.topView}>
               <View style={{alignItems:'center',marginTop:40}}>
                   <Text style={styles.txt}>Help</Text>
                   <Text style={styles.txt1}>Tell us about your issue. We will try our best to
                       resolve the issue as soon as possible
                   </Text>
               </View>
            </View>
            <View style={{flex:1}}>
            <View style={styles.imgContainer}>
               <Pressable style={{alignItems:'center'}} onPress={()=>dialCall()}> 
                   <Image source={AppImage.mobile_icon}
                   style={styles.img}
                   />
                   <Text style={styles.imgtxt}>1234567890</Text>
               </Pressable>
               <Pressable style={{alignItems:'center'}} onPress={()=>shareToWhatsAppWithContact('Hi','+911234567890')}> 
                   <Image source={AppImage.whatsapp_icon}
                   style={styles.img}
                   />
                   <Text style={styles.imgtxt}>1234567890</Text>
               </Pressable>
               <Pressable style={{alignItems:'center'}} onPress={() => Linking.openURL('mailto:support@example.com') }> 
                   <Image source={AppImage.email_icon}
                   style={styles.img}
                   />
                   <Text style={styles.imgtxt}>abc@gmail.com</Text>
               </Pressable>
            </View>
            <View style={{alignItems:'center',}}>
                <TouchableOpacity style={styles.btnContainer} onPress={()=>{navigation.navigate('Tab')
            ,toggleDrawer()}}>
                    <Text style={styles.btnTxt}>Home</Text>
                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '40%',
        backgroundColor: AppColor.appColor
    },
   
    imgContainer: {
        //position: 'absolute',
        //top: '26%',
        height:'65%',
        bottom:'20%',
        backgroundColor:'white',
        borderWidth:1,
        borderColor:'#d2d9d0',
        elevation:1,
        //justifyContent:'center',
        width:'90%',
        borderRadius:20,
        alignSelf:'center',
        alignItems:'center',
        justifyContent:'space-around'
    },
    txt:{
      fontSize:20,
      color:'white',
      fontFamily:AppFonts.medium,
      textAlign:'center'  
    },
    txt1:{
        color:'white',
        textAlign:'center',
        margin:30
        
    },
    mobileContainer:{
        //backgroundColor:'red',
        marginLeft:20,
        marginRight:20,
        marginTop:80,
        borderRadius:10,
        borderWidth:1,
        borderColor:AppColor.appColor
    },
    mobileTxt:{
        textAlign:'center',
        fontSize:18,
        margin:15,
        color:AppColor.appColor
    },
   img:{
    width:25,
    height:25
   },
   imgtxt:{
        marginTop:5,
        fontFamily:AppFonts.light
   },
    btnContainer:{
        width:'50%',
        backgroundColor:AppColor.appColor,
        alignItems:'center',
        borderRadius:10
        //top: '26%',
        
    },
    btnTxt:{
        color:'white',
        fontSize:20,
        margin:5,
        fontFamily:AppFonts.bold,
        marginLeft:20,
        marginRight:20,
        
    }
})

export default Help;