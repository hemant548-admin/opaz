import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList, Pressable,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
//import FabButton from '../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import { DrawerActions } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import APIStrings from '../../../webservice/APIStrings';
import { simpleGetCall, simplePostCall } from '../../../webservice/APIServices';

const { width, height } = Dimensions.get('screen')


const Policy = ({ navigation }) => {

    const [hotelName, setHotelName] = useState('');
    const [policies, setPolicies] = useState('');
    const [state, setState] = useState('');
    const [role, setRole] = useState('');
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        Policies();
    }, [])
    const Policies = () => {


        setLoader(true)
        console.log(APIStrings.OpazTermsAndPoliciesList);
        //console.log(requsetBody);

        simplePostCall(APIStrings.OpazTermsAndPoliciesList)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setPolicies(data.terms_and_condition[0].terms_and_policies)
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });

    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <View style={styles.topView}>
                <Pressable style={{ marginTop: 17, marginLeft: 20 }} onPress={() => navigation.goBack()}>
                    <Image source={AppImage.back_arrow_white}
                        style={{ width: 18, height: 18, resizeMode: 'contain' }}
                    />
                </Pressable>
                <View style={{ alignItems: 'center', marginTop: 12 }}>
                    <Text style={styles.txt}>Policies</Text>
                    <Text style={styles.txt1}>Tell us about your issue. We will try our best to
                        resolve the issue as soon as possible Lorem Ipsum is the dummy text
                    </Text>
                </View>
            </View>

            <View style={{ flex: 1 }}>
                <View style={styles.imgContainer}>
                    <ScrollView style={{  }} contentContainerStyle={{ flexGrow: 1 ,paddingTop:15,paddingBottom:15}}>
                        <Text style={{ textAlign: 'center', fontSize: 18, color: AppColor.appColor, marginLeft: 20, marginRight: 20 }}
                        >{policies}</Text>
                    </ScrollView>
                </View>

            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '45%',
        //position:'absolute',
        backgroundColor: AppColor.appColor
    },

    imgContainer: {
        //position: 'absolute',
        //top: '26%',
        height: '120%',
        bottom: '34%',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#d2d9d0',
        elevation: 1,
        //justifyContent:'center',
        width: '90%',
        borderRadius: 20,
        alignSelf: 'center',
        alignItems: 'center',
        overflow:'hidden',
        justifyContent: 'space-around'
    },
    txt: {
        fontSize: 20,
        color: 'white',
        fontFamily: AppFonts.medium,
        textAlign: 'center'
    },
    txt1: {
        color: 'white',
        textAlign: 'center',
        margin: 30

    },
    mobileContainer: {
        //backgroundColor:'red',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 80,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppColor.appColor
    },
    mobileTxt: {
        textAlign: 'center',
        fontSize: 18,
        margin: 15,
        color: AppColor.appColor
    },
    img: {
        width: 25,
        height: 25
    },
    imgtxt: {
        marginTop: 5,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        width: '50%',
        backgroundColor: AppColor.appColor,
        alignItems: 'center',
        borderRadius: 10
        //top: '26%',

    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 5,
        fontFamily: AppFonts.bold,
        marginLeft: 20,
        marginRight: 20,

    }
})

export default Policy;