import React, { useEffect, useState } from 'react';
import { View, Text, Image, Linking, Alert, StyleSheet, Pressable, Share ,TouchableOpacity} from 'react-native';
import {
  DrawerContentScrollView, DrawerItem
} from '@react-navigation/drawer';
import { useDispatch } from 'react-redux'
import { DrawerActions } from '@react-navigation/native';
import AppImage from '../../../utils/AppImage';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import { checkScreen } from '../../../redux/reducer';
import { AuthContext } from '../../../Routes/AuthContext';


const DrawerComponent = ({ props, navigation }) => {
  // const dispatch = useDispatch()
  const [hotelName, setHotelName] = useState('');
  const [location, setLocation] = useState('');
  const [state, setState] = useState('');
  const [role, setRole] = useState('');
  const { signOut } = React.useContext(AuthContext);

  const dispatch = useDispatch()
  useEffect(() => {
    const data = async () => {
      const first = await AsyncStorage.getItem(AppStrings.FIRST_NAME, '');
      const last = await AsyncStorage.getItem(AppStrings.LAST_Name, '');
      const loc = await AsyncStorage.getItem(AppStrings.CITY, '');
      //const stat = await AsyncStorage.getItem(AppStrings.STATE, '');
      //const role = await AsyncStorage.getItem(AppStrings.ROLE, '');
      setHotelName(first+" "+last);
      setLocation(loc);
      //setState(stat);
      //setRole(role);
    }
    data();
  }, [])

  // 1.revoke access from user & signout 
  // 2.remove the user from app
  const signOuT = async () => {
    try {
      const token=await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN,'')
      dispatch(checkScreen('Login',token,true))
      AsyncStorage.clear();
      navigation.navigate('min')
      signOut()
    } catch (error) {
      console.error(error);
    }
  };

  const logOutConfirmation = () => {

    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => { signOuT(); } },
      ],
      { cancelable: false });
    return true;

  }
  const toggleDrawer = () => {
    navigation.dispatch(DrawerActions.closeDrawer());
  };
  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          'Download Opaz App For Exciting offers',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const gotoScreen = () => {
    const screen = role === 'Hotel' ? 'Homedrawer' : role === 'FrontDesk' ? 'Front' : 'BranchManagerHome';
    navigation.navigate(screen)
  }
  const ItemSeparator = () => {
    return <View style={DrawerStyles.separator} />
  }

  return (
    <View flex={1} backgroundColor='#01704F'>
      <DrawerContentScrollView {...props}>
        <View style={{ backgroundColor: '#CD9933', width: '100%', justifyContent: 'center', bottom: 4 }}>
          <View style={{ marginTop: 40, marginBottom: 30, marginLeft: 20 }}>
            <Text style={{ fontSize: 25, color: '#014733' }}>{hotelName!=='null null'?hotelName:'User Name'}</Text>
            <Text style={{ fontSize: 12, color: '#014733', marginTop: 5 }}>{location?location:'location'}</Text>
          </View>

        </View>
        <View style={styles.container}>

          {/* home*/}
          <TouchableOpacity style={styles.subContainer} onPress={() => {
            navigation.navigate('Tab'),
            toggleDrawer()
          }}>
            <Image source={AppImage.home}
              style={styles.img}
            />
            <Text style={styles.txt}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.container1}
            onPress={() => navigation.navigate('Policy')}
          >
            <View style={styles.subContainer}>
              <Image source={AppImage.setting_icon}
                style={styles.img}
              />
              <Text style={styles.txt}>Policies</Text>
            </View>
            <Image source={AppImage.arrow_right}
              style={styles.arrowImg}
            />
          </TouchableOpacity>
          {/* help*/}
          <TouchableOpacity style={styles.container1}
            onPress={() => navigation.navigate('Help')}
          >
            <View style={styles.subContainer}>
              <Image source={AppImage.help}
                style={styles.img}
              />
              <Text style={styles.txt}>Help</Text>
            </View>
            <Image source={AppImage.arrow_right}
              style={styles.arrowImg}
            />
          </TouchableOpacity>
          {/* share*/}
          <TouchableOpacity style={styles.container1} onPress={() => onShare()}>
            <View style={styles.subContainer}>
              <Image source={AppImage.share}
                style={styles.img}
              />
              <Text style={styles.txt}>Share</Text>
            </View>
            <Image source={AppImage.arrow_right}
              style={styles.arrowImg}
            />
          </TouchableOpacity>
          {/* signout*/}
          <Pressable style={styles.container1}
            onPress={() => logOutConfirmation()}
          >
            <View style={styles.subContainer}>
              <Image source={AppImage.signout}
                style={styles.img}
              />
              <Text style={styles.txt}>Sign out</Text>
            </View>
            <Image source={AppImage.arrow_right}
              style={styles.arrowImg}
            />
          </Pressable>
        </View>
      </DrawerContentScrollView>

    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    marginTop: 30
  },
  subContainer: {
    flexDirection: 'row',
    marginLeft: 15,
    alignItems: 'center'
  },
  img: {
    width: 22,
    height: 22
  },
  txt: {
    color: 'white',
    fontSize: 21,
    marginLeft: 15,
    textAlign: 'center'
  },
  container1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 25
  },
  arrowImg: {
    height: 12,
    width: 18,
    resizeMode: 'contain',
    marginRight: 30,
    marginTop: 5
  }
})
export default DrawerComponent;