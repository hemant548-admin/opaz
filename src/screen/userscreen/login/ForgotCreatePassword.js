import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { BarPasswordStrengthDisplay } from 'react-native-password-strength-meter';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import AppStyles from '../../../utils/AppStyles';
import Loader from '../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')

const ForgotCreatePassword = ({ navigation,route }) => {

    const [createPassword, setCreatePassword] = useState('');
    const [reEnterPassword, setReEnterPassword] = useState('');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false)
    const [show, setShow]=useState(false);
    const [showRenter, setShowRenter]=useState(false);
    const [invalid, setInValid] = useState([false, false]);
    const {mobile}=route.params;
   
    const onContinuePress = async () => {
        if (createPassword.trim().length === 0 || createPassword.trim().length < 6) {
            setError('Please enter minimum 6 digit password')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (reEnterPassword.trim().length === 0 || reEnterPassword.trim().length < 6) {
            setError('Please enter minimum 6 digit password')
            setInValid([false, true, false])
            return;
        }
        if (createPassword !== reEnterPassword) {
            setError('Please enter same password')
            setInValid([false, false, true])
            return;
        }

        setLoader(true)
        let requsetBody = JSON.stringify({
            password: createPassword,
            mobile:mobile
            
        });

        console.log(APIStrings.EndUserForgotPass);
        console.log(requsetBody);

        simplePostCall(APIStrings.EndUserForgotPass, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status=== 200) {
                    console.log("Registration success", JSON.stringify(data));
                    //parseData(data)
                    //alert(data.status.message)
                    navigation.navigate('Success1')
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log("fali ",JSON.stringify(data))
                    setError(data.message)
                    setInValid([false, false, true])

                }

            }).catch((error) => {
                console.log("error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <ScrollView style={{flex:1,}} contentContainerStyle={{flexGrow:1}}>
            <LinearGradient colors={['#007654', '#003727',]} style={styles.topView}>
                <View style={styles.topHeader}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image source={AppImage.back_arrow_white}
                                style={{ width: 22, height: 22, marginTop: 4 }}
                            />
                        </TouchableOpacity>
                        <Text style={{
                            textAlign: 'center', marginLeft: 10, color: 'white',
                            fontSize: 20, fontFamily: AppFonts.medium
                        }}>Create Password</Text>
                    </View>
                </View>
            </LinearGradient>
            <View style={styles.imgContainer}>
                <Image source={AppImage.enter_password}
                    style={styles.changePsd}
                />
                <View style={{width:'100%'}}>
                {/* <Text style={styles.txt}>Do you want to change your password ? Through 
                    mobile OTP you can change your password.
                </Text> */}
                <View style={styles.mobileContainer}>
                <TextInput style={{width:'90%',borderRadius:10,paddingLeft:10}}
                placeholder='Create Password'
                value={createPassword}
                onChangeText={(text)=>{setCreatePassword(text),setInValid(!invalid[0]) }}
                secureTextEntry={!show}
                />
                 <TouchableOpacity activeOpacity={0.8}
                onPress={()=>setShow(!show)}
                >
                <Image source={!show? AppImage.show: AppImage.hide}
                        style={{width:25,height:25}}
                        />
                </TouchableOpacity>
                </View>
                <BarPasswordStrengthDisplay
                            password={createPassword}
                            wrapperStyle={{alignSelf:'center'}}
                            minLength= {2}
                            width={width*0.8}
                            levels={ [
                                 
                                {
                                  label: 'Weak',
                                  labelColor: '#ff6900',
                                  activeBarColor: '#ff6900',
                                },
                                {
                                    label: 'Average',
                                    labelColor: '#f3d331',
                                    activeBarColor: '#f3d331',
                                },
                                
                                {
                                  label: 'Strong',
                                  labelColor: '#14eb6e',
                                  activeBarColor: '#14eb6e',
                                },
                               
                              ]
                            }
                            variations={{
                                digits: /\d/,
                                lower: /[a-z]/,
                                upper: /[A-Z]/,
                                nonWords: /\W/g,
                              }}
                        />
                {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                <View style={styles.mobileContainer1}>
                <TextInput style={{width:'90%',borderRadius:10,paddingLeft:10}}
                placeholder='Re-enter Password'
                value={reEnterPassword}
                onChangeText={(text)=>{setReEnterPassword(text),setInValid(!invalid[1]) }}
                secureTextEntry={!showRenter}
                />
                <TouchableOpacity activeOpacity={0.8}
                onPress={()=>setShowRenter(!showRenter)}
                >
                <Image source={!showRenter? AppImage.show: AppImage.hide}
                        style={{width:25,height:25}}
                        />
                </TouchableOpacity>
                </View>

                {invalid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                {invalid[2] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <TouchableOpacity style={styles.changeContainer}
                onPress={()=>onContinuePress()}
                >
                    <Text style={styles.changeTxt}>Submit</Text>
                </TouchableOpacity>
            </View>
            <View style={{width:'100%',height:60}}/>
            </View>
            
            </ScrollView>
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '32%',
        backgroundColor: AppColor.appColor
    },
    topHeader: {
        marginTop: 10,
        marginLeft: 30,
        justifyContent: 'center',
        width: '100%',
        height: '16%',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 220,
        height: 170,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        //position: 'absolute',
        bottom: '17%',
        //justifyContent:'center',
        width:'100%',
        alignItems:'center',
        //backgroundColor:'yellow'
    },
    txt:{
      fontSize:17,
      color:AppColor.appColor,
      marginTop:15,
      marginLeft:20,
      marginRight:20,
      fontFamily:AppFonts.regular,
      textAlign:'center'  
    },
    mobileContainer:{
        //backgroundColor:'red',
        flexDirection:'row',
        marginLeft:20,
        marginRight:20,
        marginTop:50,
        borderRadius:10,
        borderWidth:1,
        textAlign:'center',
        color:AppColor.appColor,
        borderColor:AppColor.appColor,
        alignItems:'center'
    },
    mobileContainer1:{
        //backgroundColor:'red',
        flexDirection:'row',
        marginLeft:20,
        marginRight:20, 
        marginTop:30,
        borderRadius:10,
        borderWidth:1,
        textAlign:'center',
        color:AppColor.appColor,
        alignItems:'center',
        borderColor:AppColor.appColor
    },
    mobileTxt:{
        textAlign:'center',
        fontSize:18,
        margin:15,
        color:AppColor.appColor
    },
    changeContainer:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
         backgroundColor:'#003727',
         marginLeft:30,
         marginRight:30,
         marginTop:40,
         width:'45%',
         borderRadius:25,
         alignSelf:'center'
     },
     changeTxt:{
         textAlign:'center',
         fontSize:18,
         margin:10,
        // marginTop:10,
         //marginBottom:10,
         //marginRight:20,
         fontFamily:AppFonts.medium,
         fontSize:22,
         color:'white'
     }
})

export default ForgotCreatePassword;