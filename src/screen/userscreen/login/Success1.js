import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';

const { width, height } = Dimensions.get('screen')

const Success1 = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white',justifyContent:'center' }}>
                <View style={{alignItems:'center',marginBottom:50}}>
                <View>
                        <Image source={AppImage.check_circle}
                        style={{width:110,height:120}}
                        />
                    </View>
                    <Text style={{marginLeft:35,marginRight:35,marginTop:60,
                        textAlign:'center',fontSize:20,color:AppColor.appColor}}>Password Changed Successfully</Text>
                   
                        <TouchableOpacity style={{backgroundColor:'#003727',width:'35%',alignSelf:'center',
                    borderRadius:20,top:80}}
                    onPress={()=>navigation.navigate('Login')}
                    >
                            <Text style={{textAlign:'center',color:'white',fontSize:22,margin:5}}>Ok</Text>
                        </TouchableOpacity>
                </View>
        </View>
    )
}

export default Success1;