import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList, Platform,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import Toast from 'react-native-toast-message';
import FabButton from '../../../component/button/FabButton';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import APIStrings from '../../../webservice/APIStrings';
import Loader from '../../../component/loader/Loader';
import AppStyles from '../../../utils/AppStyles';
import { simplePostCall } from '../../../webservice/APIServices';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import toastConfig from '../../../component/Toast/Toast';
import { AuthContext } from '../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')

const LoginScreen = ({ navigation, route }) => {
    //const { role, screen } = route.params;
    const [userId, SetUserId] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false)
    const [show, setShow] = useState(false);
    const [token, setToken] = useState('');
    const [invalid, setInValid] = useState([false, false, false]);
    const { signIn,signSkip } = React.useContext(AuthContext);

    useEffect(async () => {

        const devicetoken = await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN, '');
        setToken(devicetoken)

    }, [])
    const onLoginPress = () => {
        if (userId.trim().length === 0) {
            setError('Please enter userId')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (password.trim().length === 0) {
            setError('Please enter valid password')
            setInValid([false, true, false])
            return;
        }


        setLoader(true)
        let requsetBody = JSON.stringify({
            password: password,
            mobile: userId,
            deviceToken: token,
            platform: Platform.OS
        });
        // let link;
        //     switch(role){
        //         case 'Hotel':
        //                 link=APIStrings.HotelLoginView;
        //             break;
        //         case 'FrontDesk':
        //                 link=APIStrings.FrontDeskLogin;
        //             break;
        //         case 'BranchManager':
        //                 link=APIStrings.BranchManagerLogin;
        //             break;
        //         case 'Employee':
        //                 link=APIStrings.EmployeeLogin;
        //             break;           
        //     }
        //AsyncStorage.setItem(AppStrings.ROLE, role,null)
        //const link=role==='FrontDesk'?APIStrings.FrontDeskLogin:APIStrings.HotelLoginView
        //console.log(link);
        console.log(requsetBody);
        console.log('url ', APIStrings.EndUserLogin)

        simplePostCall(APIStrings.EndUserLogin, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Login success", JSON.stringify(data));
                    parseHotelData(data.output);
                    signIn(userId,data.Role);
                    // switch(data.output.Role){
                    //     case 'Hotel':
                    //             parseHotelData(data.output);
                    //         break;
                    //     case 'FrontDesk':
                    //             parseFrontData(data.output);
                    //         break;
                    //     case 'BranchManager':
                    //             parseManagerData(data.output);
                    //         break;
                    //     case 'Employee':
                    //             parseEmployeeData(data.output);
                    //         break;           
                    // }
                    // alert("Login " + data.status.message)


                    Toast.show({
                        text1: 'Successfully Login !',

                    });


                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)
                    setError('User id or password is incorrect')
                    setInValid([false, false, true])

                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });




    }
    const parseHotelData = async (data) => {
        //console.log("hotel data ",data.hotel_owner[0])
        try {
            await AsyncStorage.setItem(AppStrings.FIRST_NAME, data.EndUser.first_name + '');
            await AsyncStorage.setItem(AppStrings.LAST_Name, data.EndUser.last_name + '')
            await AsyncStorage.setItem(AppStrings.EMAIL, data.EndUser.email + '');
            await AsyncStorage.setItem(AppStrings.PHONE, data.EndUser.user ? data.EndUser.user.phone_number : '' + '');
            await AsyncStorage.setItem(AppStrings.USER_ID, data.EndUser.id + '');
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.EndUser.address + '');
            await AsyncStorage.setItem(AppStrings.IMAGE, data.EndUser.user ? data.EndUser.user.profile_pic + '' : '');
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');


            navigation.navigate('drawer')
        }
        catch (e) {
            console.log(e)
            setLoader(false)
        }


    }


    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                onPress={() => navigation.goBack()}
            >
                <Image source={AppImage.back_arrow}
                    style={{ height: 16, width: 10 }} />
            </TouchableOpacity>
            <ScrollView style={{}} contentContainerStyle={{ flexGrow: 1 }}>
                <View>
                    <View style={{ alignSelf: 'center', }}>
                        <Image source={AppImage.opaz_logo}
                            style={{ width: 150, height: 150 }} />
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                    <View >
                        <Image source={AppImage.vector_curve2}
                            style={{ width: width, height: height * 0.6, flex: 1, resizeMode: 'stretch' }} />
                        <View style={{ alignSelf: 'center', marginTop: 20, position: 'absolute' }}>
                            <Image source={AppImage.welcom_icon}
                                style={{ width: 120, height: 30 }} />
                        </View>
                        <View style={{ position: 'absolute', top: '28%', width: '100%', }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, marginTop: 10 }}>

                                <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 20 }}
                                    onPress={() => navigation.navigate('SignUp')}
                                >Sign Up</Text>

                                <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 10, width: 80, alignItems: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}
                                    >Sign in</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%' }}>
                                <View style={{}}>
                                    <TextInput
                                        style={{
                                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                                            borderRadius: 5, paddingLeft: 20, color: 'white'
                                        }}
                                        placeholder='Enter User ID'
                                        value={userId}
                                        onChangeText={(text) => { SetUserId(text), setInValid(!invalid[0]) }}
                                        placeholderTextColor={AppColor.inputlight_txtColor}
                                    />
                                    {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                                    <View style={{
                                        backgroundColor: '#039D70', flexDirection: 'row', width: '80%', height: 40,
                                        alignSelf: 'center', elevation: 4, marginTop: invalid[0] ? 0 : 20, alignItems: 'center', borderRadius: 5,
                                    }}>
                                        <TextInput
                                            style={{
                                                width: '80%', height: 40,
                                                paddingLeft: 20, color: 'white'
                                            }}
                                            placeholder='Enter Password'
                                            value={password}
                                            onChangeText={(text) => { setPassword(text), setInValid(!invalid[1]) }}
                                            placeholderTextColor={AppColor.inputlight_txtColor}
                                            secureTextEntry={!show}
                                        />
                                        <TouchableOpacity style={{
                                            alignItems: 'center', marginLeft: 20,
                                        }} onPress={() => setShow(!show)}>
                                            <Image source={!show ? AppImage.show : AppImage.hide}
                                                style={{ width: 25, height: 25 }}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    {invalid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                                    {invalid[2] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                                </View>
                            </View>
                            <Text style={{ textAlign: 'right', color: 'white', marginRight: 40, marginTop: 10 }}
                                onPress={() => navigation.navigate('Forgot')}
                            >Forgot Password ?</Text>
                            <TouchableOpacity style={{ width: '80%', backgroundColor: 'white', alignSelf: 'center', marginTop: 20, elevation: 3, borderRadius: 5 }}
                                onPress={() => onLoginPress()}
                            >
                                <Text style={{ textAlign: 'center', margin: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.bold, }}>Sign in</Text>
                            </TouchableOpacity>
                           
                        </View>
                        <View style={{ alignSelf: 'center', position:'absolute',bottom:10 }}>
                                <Text style={{ color: 'white', fontSize: 18 }}
                                    onPress={() =>signSkip()}
                                >SKIP</Text>
                            </View>
                    </View>
                </View>
            </ScrollView>
            <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
            <Loader loading={loader} />
        </View>
    )

}

export default LoginScreen;