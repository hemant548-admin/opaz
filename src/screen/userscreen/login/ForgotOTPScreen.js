import React, { useState, useEffect, useRef } from 'react';
import {
    View, Text, FlatList, StyleSheet,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import APIStrings from '../../../webservice/APIStrings';
import AppColor from '../../../utils/AppColor';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../utils/AppStrings';
import AppStyles from '../../../utils/AppStyles';
import { simplePostCall } from '../../../webservice/APIServices';

const { width, height } = Dimensions.get('screen')
const RESEND_OTP_TIME_LIMIT = 30; // 30 secs
const AUTO_SUBMIT_OTP_TIME_LIMIT = 4; // 4 secs

let resendOtpTimerInterval;
let autoSubmitOtpTimerInterval;

const ForgotOTPScreen = ({ navigation,route }) => {

    const [otp, setOtp] = useState(['', '', '', '']);
    const [loader, setLoader] = useState(false);
    //const [phone, setPhone] = useState('');
    const [invalid,setInValid]=useState(false)
    const [attemptsRemaining, setAttemptsRemaining] = useState(4);
    const [otpArray, setOtpArray] = useState(['', '', '', '']);
    const [submittingOtp, setSubmittingOtp] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    //console.log(route);
    const {phone}=route.params;

    // in secs, if value is greater than 0 then button will be disabled
    const [resendButtonDisabledTime, setResendButtonDisabledTime] = useState(
        RESEND_OTP_TIME_LIMIT,
    );

    // 0 < autoSubmitOtpTime < 4 to show auto submitting OTP text
    const [autoSubmitOtpTime, setAutoSubmitOtpTime] = useState(
        AUTO_SUBMIT_OTP_TIME_LIMIT,
    );

    // TextInput refs to focus programmatically while entering OTP
    const firstTextInputRef = useRef(null);
    const secondTextInputRef = useRef(null);
    const thirdTextInputRef = useRef(null);
    const fourthTextInputRef = useRef(null);

    // a reference to autoSubmitOtpTimerIntervalCallback to always get updated value of autoSubmitOtpTime
    const autoSubmitOtpTimerIntervalCallbackReference = useRef();

    useEffect(async () => {

        // let phone = await AsyncStorage.getItem(AppStrings.PHONE)
        // setPhone(phone)
        // autoSubmitOtpTime value will be set after otp is detected,
        // in that case we have to start auto submit timer
        autoSubmitOtpTimerIntervalCallbackReference.current = autoSubmitOtpTimerIntervalCallback;
    });

    useEffect(() => {
        startResendOtpTimer();

        return () => {
            if (resendOtpTimerInterval) {
                clearInterval(resendOtpTimerInterval);
            }
        };
    }, [resendButtonDisabledTime]);
    const startResendOtpTimer = () => {
        if (resendOtpTimerInterval) {
            clearInterval(resendOtpTimerInterval);
        }
        resendOtpTimerInterval = setInterval(() => {
            if (resendButtonDisabledTime <= 0) {
                clearInterval(resendOtpTimerInterval);
            } else {
                setResendButtonDisabledTime(resendButtonDisabledTime - 1);
            }
        }, 1000);
    };

    // this callback is being invoked from startAutoSubmitOtpTimer which itself is being invoked from useEffect
    // since useEffect use closure to cache variables data, we will not be able to get updated autoSubmitOtpTime value
    // as a solution we are using useRef by keeping its value always updated inside useEffect(componentDidUpdate)
    const autoSubmitOtpTimerIntervalCallback = () => {
        if (autoSubmitOtpTime <= 0) {
            clearInterval(autoSubmitOtpTimerInterval);

            // submit OTP
            // onSubmitButtonPress();
        }
        setAutoSubmitOtpTime(autoSubmitOtpTime - 1);
    };

    const startAutoSubmitOtpTimer = () => {
        if (autoSubmitOtpTimerInterval) {
            clearInterval(autoSubmitOtpTimerInterval);
        }
        autoSubmitOtpTimerInterval = setInterval(() => {
            autoSubmitOtpTimerIntervalCallbackReference.current();
        }, 1000);
    };

    const refCallback = textInputRef => node => {
        textInputRef.current = node;
    };

    const onResendOtpButtonPress = () => {
        // clear last OTP
        if (firstTextInputRef) {
            setOtpArray(['', '', '', '']);
            firstTextInputRef.current.focus();
        }

        setResendButtonDisabledTime(RESEND_OTP_TIME_LIMIT);
        startResendOtpTimer();

        // resend OTP Api call
        // todo
        console.log('todo: Resend OTP');
    };

    const check=(item)=>{

        return item==='';
    }
    const onSubmitButtonPress = async () => {

        if(otpArray.every(check)) {
            setInValid(true)
            setErrorMessage('Please Enter OTP')

            return;
        }
        setLoader(true)

        let requsetBody = JSON.stringify({
            mobile: phone,
            otp: otpArray[0].toString() + otpArray[1].toString() + otpArray[2].toString() + otpArray[3].toString()
        });

        console.log(APIStrings.VarifyEndUserForgotPassOtp);
        console.log(requsetBody);

        simplePostCall(APIStrings.VarifyEndUserForgotPassOtp, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("otp success", JSON.stringify(data));
                    alert(data.status.message)
                    navigation.navigate('CreateForgot',{mobile:phone})

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    setInValid(true);
                    setErrorMessage('Please enter valid OTP')
                    console.log(data.status.message)
                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
        console.log('todo: Submit OTP', otpArray);
    };

    // this event won't be fired when text changes from '' to '' i.e. backspace is pressed
    // using onOtpKeyPress for this purpose
    const onOtpChange = index => {
        return value => {
            if (isNaN(Number(value))) {
                // do nothing when a non digit is pressed
                return;
            }
            const otpArrayCopy = otpArray.concat();
            otpArrayCopy[index] = value;
            setOtpArray(otpArrayCopy);
            console.log('number', otpArray)
            // auto focus to next InputText if value is not blank
            if (value !== '') {
                if (index === 0) {
                    secondTextInputRef.current.focus();
                } else if (index === 1) {
                    thirdTextInputRef.current.focus();
                } else if (index === 2) {
                    fourthTextInputRef.current.focus();
                }
            }
        };
    };

    // only backspace key press event is fired on Android
    // to have consistency, using this event just to detect backspace key press and
    // onOtpChange for other digits press
    const onOtpKeyPress = index => {
        return ({ nativeEvent: { key: value } }) => {
            // auto focus to previous InputText if value is blank and existing value is also blank
            if (value === 'Backspace' && otpArray[index] === '') {
                if (index === 1) {
                    firstTextInputRef.current.focus();
                } else if (index === 2) {
                    secondTextInputRef.current.focus();
                } else if (index === 3) {
                    thirdTextInputRef.current.focus();
                }

                /**
                 * clear the focused text box as well only on Android because on mweb onOtpChange will be also called
                 * doing this thing for us
                 * todo check this behaviour on ios
                 */
                if (Platform.OS === 'android' && index > 0) {
                    const otpArrayCopy = otpArray.concat();
                    otpArrayCopy[index - 1] = ''; // clear the previous box which will be in focus
                    setOtpArray(otpArrayCopy);
                }
            }
        };
    };


    const renderOTPInputBox = () => {
        return [
            firstTextInputRef,
            secondTextInputRef,
            thirdTextInputRef,
            fourthTextInputRef,
        ].map((textInputRef, index) => {
            return (
                <View>
                    <TextInput
                        style={[styles.optStyle]}
                        // padding={5}
                        value={otpArray[index]}
                        onKeyPress={onOtpKeyPress(index)}
                        onChangeText={onOtpChange(index)}
                        keyboardType={'numeric'}
                        maxLength={1}
                        //style={[styles.otpText, GenericStyles.centerAlignedText]}
                        autoFocus={index === 0 ? true : undefined}
                        ref={refCallback(textInputRef)}
                        key={index}
                    />
                </View>
            )
        })
    }


    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                onPress={() => navigation.goBack()}
            >
                <Image source={AppImage.back_arrow}
                    style={{ height: 16, width: 10 }} />
            </TouchableOpacity>
            <ScrollView style={{}} >
                <View>
                    <View style={{ alignSelf: 'center', }}>
                        <Image source={AppImage.opaz_logo}
                            style={{ width: 150, height: 150 }} />
                    </View>
                </View>
                <View style={{ flex: 1, }}>
                    <Image source={AppImage.vector_curve2}
                        style={{ width: width, height: height * 0.815, flex: 1, resizeMode: 'cover' }} />
                    <View style={{ alignSelf: 'center', marginTop: 20, position: 'absolute' }}>
                        <Image source={AppImage.welcom_icon}
                            style={{ width: 120, height: 30 }} />
                    </View>
                    <View style={{ position: 'absolute', top: '28%', width: '100%', }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, }}>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 50, width: 80 }}>
                                <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}>Sign Up</Text>
                            </View>
                            <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 60 }}>Sign in</Text>
                        </View>
                        <View>
                            <Text style={styles.txt}>OTP sent to Your Mobile Number- {phone}.
                            <Text style={styles.click} onPress={() => navigation.goBack()}> Click here</Text> to change Mobile number
                        </Text>
                        </View>
                        <View style={{ width: '100%' }}>
                            <View flexDirection='row' justifyContent='space-around' margin={10}>
                                {renderOTPInputBox()}
                            </View>
                        </View>
                        {resendButtonDisabledTime > 0 ? (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
                        <Text style={{ marginRight: 20,color:'black' }}>Resend OTP in time {resendButtonDisabledTime} </Text>
                    </View>
                ) : (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
                        <Text style={{ color: 'darkblue', marginRight: 20, fontSize: 16 }} onPress={() => onResendOtpButtonPress()}>
                            Resend OTP</Text>
                    </View>
                )}
                {invalid && <Text style={[AppStyles.apiErrorStyle,{color:'darkred'}]}>{errorMessage}</Text>}
                        <FabButton title='Continue'
                            onItem={() => onSubmitButtonPress()}
                        />
                        <View style={{ alignSelf: 'center', marginTop: 10 }}>
                            <Text style={{ color: 'white', fontSize: 18 }}>SKIP</Text>
                        </View>
                    </View>
                    {/* </ImageBackground> */}
                </View>
            </ScrollView>

        </View>
    )
}
const styles = StyleSheet.create({

    optStyle: {
        //borderBottomColor: AppColors.colorLine,
        //borderBottomWidth: 3,
        width: 50,
        height: 50,
        margin: 5,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 8,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        elevation: 2,
        color: 'white',
        backgroundColor: AppColor.inputColor
    },
    txt: {
        color: 'white',
        //marginTop: 20,
        marginRight: 35,
        marginLeft: 35,
        fontSize: 15,
        textAlign: 'center'
    },
    click: {
        fontSize: 16,
        fontWeight: '700'
    },
    otpTxtContainer: {
        marginTop: 40,
        marginLeft: 30,
        fontSize: 20,
        color: AppColor.appColor,
        fontWeight: '700'
    },
    otpTxt: {
        fontSize: 20,
        color: AppColor.appColor,
        //fontWeight:'700' 
    },

})
export default ForgotOTPScreen;