import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';
import AppStyles from '../../../utils/AppStyles';
import { simplePostCall } from '../../../webservice/APIServices';
import APIStrings from '../../../webservice/APIStrings';
import Loader from '../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')

const ForgotPassword = ({ navigation }) => {
    const [mobile,setMobile]=useState('');
    const [error, setError] = useState('');
    const [inValid, setInValid] = useState(false)
    const [loader, setLoader]=useState(false)

    const onSubmit=()=>{
        if (mobile.trim().length === 0 || mobile.trim().length < 10) {
            setError('Please Enter valid Mobile Number')
            setInValid(true)
            return;
        }

        let requsetBody = JSON.stringify({
            mobile: mobile
        });

        setLoader(true)
        console.log(APIStrings.SendEndUserForgotPassOtp);
        console.log(requsetBody);
        
        simplePostCall(APIStrings.SendEndUserForgotPassOtp, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("otp success", JSON.stringify(data));
                    //alert(data.status.message)

                    navigation.navigate('ForgotOTP',{phone:mobile})
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)
                    setError(data.output.user_message)
                    setInValid(true)
                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });

        
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <ScrollView style={{flex:1,}} contentContainerStyle={{flexGrow:1}}>
            <LinearGradient colors={['#007654', '#003727',]} style={styles.topView}>
                <View style={styles.topHeader}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image source={AppImage.back_arrow_white}
                                style={{ width: 22, height: 22, marginTop: 4 }}
                            />
                        </TouchableOpacity>
                        <Text style={{
                            textAlign: 'center', marginLeft: 10, color: 'white',
                            fontSize: 20, fontFamily: AppFonts.medium
                        }}>Change Password</Text>
                    </View>
                </View>
            </LinearGradient>
            <View style={styles.imgContainer}>
                <Image source={AppImage.change_password}
                    style={styles.changePsd}
                />
                <View>
                <Text style={styles.txt}>Do you want to change your password ? Through 
                    mobile OTP you can change your password.
                </Text>
                <TextInput style={styles.mobileContainer}
                placeholder='Enter Mobile Number'
                value={mobile}
                onChangeText={(value)=>{setMobile(value),setInValid(false)}}
                maxLength={10}

                />
                {inValid && <Text style={styles.apiErrorStyle}>{error}</Text>}
                <TouchableOpacity style={styles.changeContainer}
                onPress={()=> onSubmit()}
                >
                    
                    <Text style={styles.changeTxt}>Change Password</Text>
                    <Image source={AppImage.arrow_right}
                    style={{width:10,height:15,resizeMode:'contain'}}/>
                </TouchableOpacity>
            </View>
            <View style={{width:'100%',height:60}}/>
            </View>
            
            </ScrollView>
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '32%',
        backgroundColor: AppColor.appColor
    },
    topHeader: {
        marginTop: 10,
        marginLeft: 30,
        justifyContent: 'center',
        width: '100%',
        height: '16%',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        //position: 'absolute',
        bottom: '15%',
        //justifyContent:'center',
        width:'100%',
        alignItems:'center',
        //backgroundColor:'yellow'
    },
    txt:{
      fontSize:17,
      color:AppColor.appColor,
      marginTop:15,
      marginLeft:20,
      marginRight:20,
      fontFamily:AppFonts.regular,
      textAlign:'center'  
    },
    mobileContainer:{
        //backgroundColor:'red',
        marginLeft:20,
        marginRight:20,
        marginTop:80,
        borderRadius:10,
        borderWidth:1,
        textAlign:'center',
        color:AppColor.appColor,
        borderColor:AppColor.appColor,
    },
    mobileTxt:{
        textAlign:'center',
        fontSize:18,
        margin:15,
        color:AppColor.appColor
    },
   changeContainer:{
       flexDirection:'row',
       justifyContent:'space-around',
       alignItems:'center',
        backgroundColor:'#003727',
        marginLeft:30,
        marginRight:30,
        marginTop:20,
        borderRadius:17,
    },
    changeTxt:{
        textAlign:'center',
        fontSize:14,
        marginTop:7,
        marginBottom:7,
        marginRight:20,
        //fontFamily:AppFonts.medium,
        fontSize:22,
        color:'white'
    },
    apiErrorStyle:{
        color:'red',
        textAlign:'right',
        marginRight:15,
        marginTop:10

    }
})

export default ForgotPassword;