import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppStyles from '../../../utils/AppStyles';

const { width, height } = Dimensions.get('screen')

const SignUpScreen = ({ navigation }) => {

    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [error, setError] = useState('');
    const [invalid, setInValid] = useState([false, false,]);

    const onContinuePress = () => {
        if (firstName.trim().length === 0) {
            setError('Please enter first name')
            setInValid([true, false,])
            //hotelRef.current.focus();
            return;
        }
        if (lastName.trim().length === 0) {
            setError('Please enter last name')
            setInValid([false, true,])
            return;
        }

        navigation.navigate('SignUp1',{first:firstName,last:lastName})
    }
    

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
             <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
                <ScrollView style={{}} >
            <View>
                <View style={{ alignSelf: 'center',  }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
            </View>
                <View style={{flex:1,}}>
                    <Image source={AppImage.vector_curve2}
                        style={{ width: width, height: height*0.815,flex:1,resizeMode:'cover' }} />
                        <View style={{ alignSelf: 'center', marginTop: 20,position:'absolute' }}>
                            <Image source={AppImage.welcom_icon}
                                style={{ width: 120, height: 30 }} />
                        </View>
                        <View style={{ position: 'absolute', top: '28%', width: '100%', }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, }}>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 50, width: 80 }}>
                                    <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}>Sign Up</Text>
                                </View>
                                <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 60 }}
                                onPress={()=>navigation.navigate('Login')}
                                >Sign in</Text>
                            </View>
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={{
                                        width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', fontFamily: 'Roboto', elevation: 2,
                                        borderRadius: 5, paddingLeft:20,color:'white'
                                    }}
                                    placeholder='First Name'
                                    value={firstName}
                                    onChangeText={(value)=>{setfirstName(value.replace(/[^A-Za-z]/g, '')), setInValid(!invalid[0])}}
                                    placeholderTextColor={AppColor.inputlight_txtColor}
                                />
                            </View>
                            {invalid[0]&& <Text style={[AppStyles.apiErrorStyle,{color:"#850a01"}]}>{error}</Text>}
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={{
                                        width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', marginTop: 20, elevation: 2,
                                        borderRadius: 5,paddingLeft:20,color:'white'
                                    }}
                                    placeholder='Last Name'
                                    value={lastName}
                                    onChangeText={(value)=>{setlastName(value.replace(/[^A-Za-z]/g, '')), setInValid(!invalid[1])}}
                                    placeholderTextColor={AppColor.inputlight_txtColor}
                                />
                            </View>
                            {invalid[1]&& <Text style={[AppStyles.apiErrorStyle,{color:"#850a01"}]}>{error}</Text>}
                            <FabButton title='Continue'
                            onItem={()=>onContinuePress()}
                            />
                            <View style={{ alignSelf: 'center', marginTop: 10 }}>
                                <Text style={{ color: 'white', fontSize: 18 }}
                                onPress={()=>navigation.navigate('drawer')}
                                >SKIP</Text>
                            </View>
                        </View>
                    {/* </ImageBackground> */}
                </View>
            </ScrollView>

        </View>
    )
}

export default SignUpScreen;