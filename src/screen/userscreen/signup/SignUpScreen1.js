import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppStyles from '../../../utils/AppStyles';
import AppColor from '../../../utils/AppColor';
import APIStrings from '../../../webservice/APIStrings';
import { simplePostCall } from '../../../webservice/APIServices';
import Loader from '../../../component/loader/Loader';
import Toast from 'react-native-simple-toast';

const { width, height } = Dimensions.get('screen')

const SignUpScreen1 = ({ navigation,route }) => {
    const [mobile, setMobile] = useState('');
    const [email, setEmail] = useState('');
    const [error, setError] = useState('');
    const [loader,setLoader] =useState('');
    const [countryCode, setCountryCode] = useState('+91');
    const [flag, setFlag] = useState('');
    const [invalid, setInValid] = useState([false, false,false]);

    const {first,last}=route.params;

    const onContinuePress = () => {
        let emailFormat=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

        if (mobile.trim().length === 0 || mobile.trim().length<10 || isNaN(mobile)) {
            setError('Please enter valid mobile number')
            setInValid([true, false,false])
            //hotelRef.current.focus();
            return;
        }
        if (!emailFormat.test(email)) {
            setError('Please enter valid email id')
            setInValid([false, true,false])
            return;
        }
        setLoader(true)
        let requsetBody = JSON.stringify({
            mobile:mobile ,
            first_name:first , 
            last_name:last , 
            email:email
        });

        console.log(APIStrings.SendEndUserOtp);
        console.log(requsetBody);

        simplePostCall(APIStrings.SendEndUserOtp, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                 if (data.status.code === 200) {
                    console.log("Registration success", JSON.stringify(data));
                    //parseData(data)
                   // alert(data.status.message)
                  // Toast.show('Registered Successfully')
                    navigation.navigate('OTP',{phone:mobile})



                }
                 else {
                //     //this.setState({ invalid: true, errorMessage: data.message });

                     console.log(data)
                     setError('Mobile number is already registered')
                    setInValid([false,false,true])

                 }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const onCountrySelect = (country) => {
        // alert(JSON.stringify(country));
        //this.setState({ flag: country.flag, countryCode: '+' + country.countryCode });
        setCountryCode('+' + country.countryCode)
        setFlag(country.flag)
    }
    const onCountryCodePress = () => {
        //const { navigation } = this.props;
        navigation.navigate('CountrySelect', { onCountrySelect: (country) => onCountrySelect(country) });
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
             <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
                <ScrollView style={{}} >
            <View>
                <View style={{ alignSelf: 'center',  }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
            </View>
                <View style={{flex:1,}}>
                    <Image source={AppImage.vector_curve2}
                        style={{ width: width, height: height*0.815,flex:1,resizeMode:'cover' }} />
                        <View style={{ alignSelf: 'center', marginTop: 20,position:'absolute' }}>
                            <Image source={AppImage.welcom_icon}
                                style={{ width: 120, height: 30 }} />
                        </View>
                        <View style={{ position: 'absolute', top: '28%', width: '100%', }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, }}>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 50, width: 80 }}>
                                    <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}>Sign Up</Text>
                                </View>
                                <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 60 }}
                                onPress={()=>navigation.navigate('Login')}
                                >Sign in</Text>
                            </View>
                            <View style={{
                        flexDirection: 'row', backgroundColor: '#039D70', width: "80%",
                        alignSelf: 'center', borderRadius: 5, elevation: 4, marginTop: 20
                    }}>
                        <TouchableOpacity
                            onPress={() => onCountryCodePress()}>
                            <View
                                style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={{ padding: 10, minWidth: 40, color: 'white' }}>
                                    {countryCode}
                                </Text>
                                <Image style={{ width: 10, height: 10, resizeMode: 'contain', transform: [{ rotate: "90deg" }] }}
                                    source={AppImage.arrow_white} />
                            </View>
                        </TouchableOpacity>
                        <TextInput
                            style={{
                                width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center',
                                borderRadius: 5, paddingLeft: 20, color: 'white'
                            }}
                            placeholder='Mobile'
                            placeholderTextColor={AppColor.inputlight_txtColor}
                            value={mobile}
                            keyboardType='phone-pad'
                            autoCapitalize='none'
                            autoCorrect={false}
                            maxLength={10}
                            onChangeText={(value) => { setMobile(value.replace(/[^0-9]/g, '')), setInValid(!invalid[0]) }}
                            //onSubmitEditing={() => { mobileRef.focus(); }}
                            //ref={mobileRef}
                        />
                    </View>
                            {invalid[0]&& <Text style={[AppStyles.apiErrorStyle,{color:"#850a01"}]}>{error}</Text>}
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={{
                                        width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', marginTop:invalid[0]? 0: 20, elevation: 2,
                                        borderRadius: 5,paddingLeft:20,color:'white'
                                    }}
                                    placeholder='Email Id'
                                    value={email}
                                    onChangeText={(value)=>{setEmail(value), setInValid(!invalid[1])}}
                                    placeholderTextColor={AppColor.inputlight_txtColor}
                                />
                            </View>
                            {invalid[1]&& <Text style={[AppStyles.apiErrorStyle,{color:"#850a01"}]}>{error}</Text>}
                            {invalid[2]&& <Text style={[AppStyles.apiErrorStyle,{color:"#850a01"}]}>{error}</Text>}
                            <FabButton title='Get OTP'
                            onItem={()=>onContinuePress()}
                            />
                            {/* <View style={{ alignSelf: 'center', marginTop: 10 }}>
                                <Text style={{ color: 'white', fontSize: 18 }}>SKIP</Text>
                            </View> */}
                        </View>
                    {/* </ImageBackground> */}
                </View>
            </ScrollView>
                <Loader loading={loader}/>
        </View>
    )
}

export default SignUpScreen1;