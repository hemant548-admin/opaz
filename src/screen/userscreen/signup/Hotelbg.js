import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';

const { width, height } = Dimensions.get('screen')

const Hotelbg = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <ImageBackground source={AppImage.hotel_bg2}
             style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'flex-end'}}
            >
            <View style={{marginBottom:'10%'}}>
                <Text style={styles.headertxt}>You are all set</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.containerTxt}>Thanks for registering ! Now let's
                    find hotel for you.
                </Text>
            </View>
            <TouchableOpacity style={styles.btnContainer} onPress={()=>navigation.navigate('Login')}>
                <Text style={styles.btnTxt}>Let's Start</Text>
            </TouchableOpacity>
            </ImageBackground>
        </View>
    )
}
const styles = StyleSheet.create({
    headertxt:{
        color:'white',
        fontSize:30,
        fontFamily:AppFonts.bold
    },
    container:{
     //backgroundColor:'rgba(100, 100, 100, 0.5)' ,
     marginBottom:40  
    },
    containerTxt:{
        color:'white',
        fontSize:20,
        marginTop:5,
        marginBottom:5,
        marginLeft:30,
        marginRight:30,
        textAlign:'center'
    },
    btnContainer:{
        backgroundColor:AppColor.appColor,
        width:'60%',
        alignItems:'center',
        borderRadius:20,
        marginBottom:50
    },
    btnTxt:{
        color:'white',
        fontSize:22,
        margin:10
    }
})

export default Hotelbg;