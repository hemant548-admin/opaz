import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { BarPasswordStrengthDisplay } from 'react-native-password-strength-meter';
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import APIStrings from '../../../webservice/APIStrings';
import AppStyles from '../../../utils/AppStyles';
import Loader from '../../../component/loader/Loader';
import Toast from 'react-native-simple-toast';
import { simplePostCall } from '../../../webservice/APIServices';

const { width, height } = Dimensions.get('window')

const PasswordScreen = ({ navigation, route }) => {
    const { mobile } = route.params;
    const [createPassword, setCreatePassword] = useState('');
    const [reEnterPassword, setReEnterPassword] = useState('');
    const [invalid, setInValid] = useState([false, false, false]);
    const [error, setError] = useState('');
    const [show, setShow] = useState(false);
    const [showRenter, setShowRenter] = useState(false);
    const [loader, setLoader] = useState(false);


    const onContinuePress = async () => {
        var passw=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;;
        if (createPassword.trim().length === 0 || createPassword.trim().length < 6 ) {
            setError('Please enter minimum 6 digit password')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (!passw.test(createPassword)) {
            setError('Password must contain char,digit and a Special charcter')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (reEnterPassword.trim().length === 0 || reEnterPassword.trim().length < 6) {
            setError('Please enter minimum 6 digit password')
            setInValid([false, true, false])
            return;
        }
        if (createPassword !== reEnterPassword) {
            setError('Please enter same password')
            setInValid([false, false, true])
            return;
        }


        setLoader(true)
        let requsetBody = JSON.stringify({
            password: createPassword,
            mobile: mobile,

        });

        console.log(APIStrings.RegisterEndUser);
        console.log(requsetBody);

        simplePostCall(APIStrings.RegisterEndUser, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Registration success", JSON.stringify(data));
                    //parseData(data)
                    //alert(data.status.message)
                    Toast.show('Registered Successfully')
                    navigation.navigate('success')



                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <Loader loading={loader} />
            <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                onPress={() => navigation.goBack()}
            >
                <Image source={AppImage.back_arrow}
                    style={{ height: 16, width: 10 }} />
            </TouchableOpacity>
            <ScrollView style={{}} >
                <View>
                    <View style={{ alignSelf: 'center', }}>
                        <Image source={AppImage.opaz_logo}
                            style={{ width: 150, height: 150 }} />
                    </View>
                </View>
                <View style={{ flex: 1, }}>
                    <Image source={AppImage.vector_curve2}
                        style={{ width: width, height: height * 0.815, flex: 1, resizeMode: 'cover' }} />
                    <View style={{ alignSelf: 'center', marginTop: 20, position: 'absolute' }}>
                        <Image source={AppImage.welcom_icon}
                            style={{ width: 120, height: 30 }} />
                    </View>
                    <View style={{ position: 'absolute', top: '36%', width: '100%', }}>
                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, }}>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 50, width: 80 }}>
                                <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}>Sign Up</Text>
                            </View>
                            <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 60 }}>Sign in</Text>
                        </View> */}
                        <View style={{
                            backgroundColor: '#039D70', flexDirection: 'row', width: '80%', height: 40,
                            alignSelf: 'center', elevation: 4, marginTop: 0, alignItems: 'center', borderRadius: 5,
                        }}>
                            <TextInput
                                style={{
                                    width: '80%', height: 40,
                                    paddingLeft: 20, color: 'white'
                                }}
                                placeholder='Enter Password'
                                value={createPassword}
                                onChangeText={(text) => { setCreatePassword(text), setInValid(!invalid[0]) }}
                                placeholderTextColor={AppColor.inputlight_txtColor}
                                secureTextEntry={!show}
                            />
                            <TouchableOpacity style={{
                                alignItems: 'center', marginLeft: 20,
                            }} onPress={() => setShow(!show)}>
                                <Image source={!show ? AppImage.show : AppImage.hide}
                                    style={{ width: 25, height: 25 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <BarPasswordStrengthDisplay
                            password={createPassword}
                            wrapperStyle={{alignSelf:'center'}}
                            minLength= {2}
                            width={width*0.8}
                            levels={ [
                                 
                                {
                                  label: 'Weak',
                                  labelColor: '#ff6900',
                                  activeBarColor: '#ff6900',
                                },
                                {
                                    label: 'Average',
                                    labelColor: '#f3d331',
                                    activeBarColor: '#f3d331',
                                },
                                
                                {
                                  label: 'Strong',
                                  labelColor: '#14eb6e',
                                  activeBarColor: '#14eb6e',
                                },
                               
                              ]
                            }
                            variations={{
                                digits: /\d/,
                                lower: /[a-z]/,
                                upper: /[A-Z]/,
                                nonWords: /\W/g,
                              }}
                        />
                        {invalid[0] && <Text style={[AppStyles.apiErrorStyle, { color: 'darkred' }]}>{error}</Text>}
                        <View style={{
                            backgroundColor: '#039D70', flexDirection: 'row', width: '80%', height: 40,
                            alignSelf: 'center', elevation: 4, marginTop: invalid[0] ? 0 : 20, alignItems: 'center', borderRadius: 5,
                        }}>
                            <TextInput
                                style={{
                                    width: '80%', height: 40,
                                    paddingLeft: 20, color: 'white'
                                }}
                                placeholder='Re-enter Password'
                                value={reEnterPassword}
                                onChangeText={(text) => { setReEnterPassword(text), setInValid(!invalid[1]) }}
                                placeholderTextColor={AppColor.inputlight_txtColor}
                                secureTextEntry={!showRenter}
                            />
                            <TouchableOpacity style={{
                                alignItems: 'center', marginLeft: 20,
                            }} onPress={() => setShowRenter(!showRenter)}>
                                <Image source={!showRenter ? AppImage.show : AppImage.hide}
                                    style={{ width: 25, height: 25 }}
                                />
                            </TouchableOpacity>
                        </View>
                        {invalid[1] && <Text style={[AppStyles.apiErrorStyle, { color: 'darkred' }]}>{error}</Text>}
                        {invalid[2] && <Text style={[AppStyles.apiErrorStyle, { color: 'darkred' }]}>{error}</Text>}
                        <FabButton title='Register'
                            onItem={() => onContinuePress()}
                        />
                        {/* <View style={{ alignSelf: 'center', marginTop: 10 }}>
                            <Text style={{ color: 'white', fontSize: 18 }}>SKIP</Text>
                        </View> */}
                    </View>
                    {/* </ImageBackground> */}
                </View>

            </ScrollView>

        </View>
    )
}

export default PasswordScreen;