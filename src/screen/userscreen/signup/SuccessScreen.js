import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';
import AppColor from '../../../utils/AppColor';
import AppFonts from '../../../utils/AppFonts';

const { width, height } = Dimensions.get('screen')

const SuccessScreen = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center' }}>
            <View style={{ alignItems: 'center', marginBottom: 50 }}>
                <View style={{
                    width: 170, height: 170, borderRadius: 85, backgroundColor: '#E0FFF3',
                    alignItems: 'center', justifyContent: 'center'
                }}>

                    <View style={{
                        width: 100, height: 100, borderRadius: 50,
                        backgroundColor: AppColor.appColor, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Image source={AppImage.check_icon}
                            style={{ width: 30, height: 30, resizeMode: 'contain' }}
                        />
                    </View>
                </View>
                <Text style={{
                    marginLeft: 35, marginRight: 35, marginTop: 60,
                    textAlign: 'center', fontSize: 28, color: AppColor.appColor, fontFamily: AppFonts.medium
                }}>Congratulation !!!</Text>
                <Text style={{ margin: 7, fontSize: 22, color: AppColor.appColor, 
                    fontFamily: AppFonts.light,textAlign:'center' }}>{"You have successfully\n registered with us."}</Text>
                <TouchableOpacity style={{
                    backgroundColor: AppColor.appColor, width: '35%', alignSelf: 'center',
                    borderRadius: 20, top: 60
                }}
                    onPress={() => navigation.navigate('Login')}
                >
                    <Text style={{
                        textAlign: 'center', color: 'white',
                        fontSize: 22, margin: 5,
                    }}>Ok</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default SuccessScreen;