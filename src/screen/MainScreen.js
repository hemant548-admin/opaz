import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,Alert,
    Dimensions, Image, TouchableOpacity, TextInput, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import NetInfo from "@react-native-community/netinfo";
import AppImage from '../utils/AppImage';
import AppFonts from '../utils/AppFonts';
import { useSelector, useDispatch } from 'react-redux';
import ConnectionModal from '../component/loader/ConnectionModal';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../utils/AppStrings';
import {checkScreen} from '../redux/reducer';
const { width, height } = Dimensions.get('screen')

const MainScreen = ({ navigation }) => {
    const [isConnected, setIsConnected]=useState(null);
    const isToken=useSelector(state=>state.isToken);
    const token=useSelector(state=>state.token);
    const dispatch=useDispatch();

    useEffect( async ()=>{
        // const backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     closeApp
        //   );
        const unsubscribe = NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
            setIsConnected(state.isConnected)
            // if(!state.isConnected){
            //     alert('internet connection')
            // }
          });
          if(isToken){
            await AsyncStorage.setItem(AppStrings.DEVICE_TOKEN,token);
          }

          return () => {unsubscribe();
           // backHandler.remove();
        }
    },[])
    // React.useEffect(()=>{
    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         closeApp
    //       );

    //       return () => backHandler.remove();
    // },[])

    // const closeApp = () => {

    //     Alert.alert(
    //       'Exit',
    //       'Are you sure you want to exit the App?',
    //       [
    //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //         { text: 'Yes', onPress: () => { BackHandler.exitApp(); } },
    //       ],
    //       { cancelable: false });
    //     return true;
    
    //   }
    const hotelRoute=()=>{
        dispatch(checkScreen('',token,true));
        navigation.navigate('HotelRoute')
    }
    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <ImageBackground source={AppImage.Landing_bg}
                style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1, }} >
                <View style={{ width: '90%', height: '30%', alignSelf: 'center', marginTop: 20 }}>
                    <Image source={AppImage.Landing_bg2}
                        style={{ width: '100%', height: '100%', resizeMode: 'contain', alignSelf: 'center', marginTop: 20 }} />

                </View>
                <View style={{ alignItems: 'flex-end' }}>
                    <TouchableOpacity style={{ justifyContent: 'center' }}
                        onPress={() =>hotelRoute()}
                    >
                        <Image
                            source={AppImage.rectangle_green}
                            style={{ width: 200, height: 38, }}
                        />
                        <Text style={{ position: 'absolute', color: 'white', alignSelf: 'center', fontSize: 20, fontFamily: AppFonts.bold, }}>HOTEL OWNER</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'flex-end', justifyContent: 'space-between' }}>
                    <View style={{ width: '28%', height: '38%', marginLeft: 20 }}>
                        <Image source={AppImage.Landing_bg1}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                    </View>
                    <View style={{ marginBottom: 65, }}>
                        <TouchableOpacity style={{ justifyContent: 'center' }}
                            onPress={() => navigation.navigate('UserRoute')}
                        >
                            <Image
                                source={AppImage.rectangle_green}
                                style={{ width: 200, height: 38, }}
                            />
                            <Text style={{ position: 'absolute', color: 'white', alignSelf: 'center', fontSize: 20, fontFamily: AppFonts.bold, }}>USER</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
            {!isConnected &&
            <ConnectionModal  connection={!isConnected}
            backdrop={()=>setIsConnected(!isConnected)}
            />
            }
        </View>
    )
}

export default MainScreen;