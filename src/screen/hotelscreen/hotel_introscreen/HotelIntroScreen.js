import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../utils/AppImage';
import AppFonts from '../../../utils/AppFonts';
import AppIntroSlider from 'react-native-app-intro-slider';
import AppColor from '../../../utils/AppColor';

const string1 = 'We make it Simple for you to Manage all your upcoming and existing bookings'
const string2 = 'Let us know about your employees and we will help you to handle.'
const string3 = 'Register with us and we will do the rest for you for all your hotel needs.'

const slides = [
    {
        key: 1,
        title: 'Manage all Your Bookings',
        text: string1,
        image: AppImage.intro1_hotel,
        //backgroundColor: '#59b2ab',
    },
    {
        key: 2,
        title: 'Handle Your Staff from anywhere',
        text: string2,
        image: AppImage.intro2_hotel,
        backgroundColor: '#febe29',
    },
    {
        key: 3,
        title: 'Choose the Best App',
        text: string3,
        image: AppImage.intro3_hotel,
        backgroundColor: '#22bcb5',
    }
];
const HotelIntroScreen = ({ navigation }) => {
    const [isRender, setRender] = useState(false)
    const [count, setCount] = useState(0);

    const _renderItem = ({ item }) => {
        return (
            <View style={{flex:1}}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ marginTop: 20 }}>
                        <Image
                            source={item.image}
                            style={item.key === 3 ? Styles.img3 : Styles.img1}
                        />
                    </View>

                    <View>
                        <Text style={{
                            fontSize: 25, marginLeft: 20, marginRight: 30, textAlign: 'center', fontFamily: AppFonts.regular,
                            marginTop: 0
                        }}>{item.title}</Text>
                    </View>
                    <View>
                        <Text style={{
                            fontSize: 18, marginLeft: 80, marginRight: 80, marginTop: 40, marginBottom: 90, textAlign: 'center', fontFamily: AppFonts.thin,
                            fontWeight: '200'
                        }}>{item.text}</Text>
                    </View>


                </View>
                {item.key === 3 &&

                    <TouchableOpacity style={Styles.button}
                        onPress={() => navigation.navigate('AllProfile')}
                    >
                        <Text style={Styles.btnTxt}>Get Started</Text>
                    </TouchableOpacity>

                }
            </View>
        )
    }

    const _renderNextButton = () => {
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{
                    fontSize: 20, paddingLeft: 25, paddingRight: 25, backgroundColor: '#33007654', borderRadius: 30,
                    marginBottom: 10, paddingTop: 10, paddingBottom: 10, marginRight: 15
                }}
                >Next</Text>
            </View>
        )
    }
    const _renderSkipButton = () => {
        return (
            <View style={{ marginTop: 15 }}>
                <Text style={{ marginLeft: 15, fontSize: 15, textAlign: 'center', }}
                >Skip</Text>
            </View>
        )
    }
    return (

        <LinearGradient colors={['#DBFFF3', '#E8FFFFFF', '#EBFFFFFF', '#FFE2A1']} style={{ flex: 1 }}>
            <AppIntroSlider renderItem={_renderItem} data={slides}
                dotStyle={{ bottom: '45%', backgroundColor: '#97b8a5', width: 38, height: 9, marginLeft: 10, marginRight: 10 }}
                activeDotStyle={{ bottom: '45%', backgroundColor: AppColor.appColor, width: 38, height: 9 }}
                renderNextButton={_renderNextButton}
                showSkipButton={true}
                renderSkipButton={_renderSkipButton}
                showDoneButton={false}
                onSkip={() => navigation.navigate('AllProfile')}
            />

        </LinearGradient>


    )
}

const Styles = StyleSheet.create({

    img1: {
        width: 250,
        height: 210,
        marginTop: 10,
        resizeMode: 'contain'
    },
    img2: {
        width: 250,
        height: 190,
        marginTop: 40
    },
    img3: {
        width: 220,
        height: 160,
        marginTop: 50,
        resizeMode: 'contain'
    },
    button: {
        width: '60%',
        //height:30,
        backgroundColor: '#007654',
        alignSelf: 'center',
        //marginBottom: 25,
        borderRadius: 18,
        position: 'absolute',
        bottom: 10
        //top: '25%'
    },
    btnTxt: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        margin: 10
    }
})

export default HotelIntroScreen;