import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import FabButton from '../../../component/button/FabButton';

const { width, height } = Dimensions.get('screen')

const SignUpScreen1 = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
                <View style={{ alignSelf: 'center', marginTop: 8, }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
            </View>
            <ScrollView style={{}}>
                <View style={{ marginBottom: '150%', width: '100%', height: '100%', }}>
                    <ImageBackground source={AppImage.vector_curve2}
                        style={{ width: '103%', height: '101%', }} >
                        <View style={{ alignSelf: 'center', position: 'absolute', marginTop: 20 }}>
                            <Image source={AppImage.welcom_icon}
                                style={{ width: 120, height: 30 }} />
                        </View>
                        <View style={{ position: 'absolute', top: '28%', width: '100%', }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: 30, }}>
                                <View style={{ borderBottomWidth: 1, borderBottomColor: 'rgba(255,255,255,0.54)', marginRight: 50, width: 80 }}>
                                    <Text style={{ color: 'white', fontSize: 20, marginBottom: 5 }}>Sign Up</Text>
                                </View>
                                <Text style={{ color: 'rgba(0,173,123,1)', fontSize: 16, marginRight: 60 }}>Sign in</Text>
                            </View>
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={{
                                        width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', fontFamily: 'Roboto', elevation: 2,
                                        borderRadius: 5, paddingLeft: 20
                                    }}
                                    placeholder='Mobile Number'
                                    placeholderTextColor="#FFFFFF"
                                />
                            </View>
                            <View style={{ width: '100%' }}>
                                <TextInput
                                    style={{
                                        width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', marginTop: 20, elevation: 2,
                                        borderRadius: 5, paddingLeft: 20
                                    }}
                                    placeholder='Email ID'
                                    placeholderTextColor='#FFFFFF'
                                />
                            </View>
                            <FabButton title='GET OTP' />
                            <View style={{ alignSelf: 'center', marginTop: 10 }}>
                                <Text style={{ color: 'white', fontSize: 18 }}>SKIP</Text>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            </ScrollView>

        </View>
    )
}

export default SignUpScreen1;