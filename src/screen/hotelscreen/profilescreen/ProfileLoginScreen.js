import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, StyleSheet, ScrollView
} from 'react-native';
import { Container } from 'native-base'
import AppImage from '../../../utils/AppImage';
import AppFonts from '../../../utils/AppFonts';

const { width, height } = Dimensions.get('screen')

const ProfileLoginScreen = ({ navigation }) => {

    return (
        <View style={{ flex: 1,backgroundColor:'white' }}>
            <ScrollView>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={Styles.topTxt}>Choose one from the below profile to continue</Text>
                    <View>
                        <Image source={AppImage.frontdesk}
                            style={Styles.frntdsk} />
                        <TouchableOpacity style={Styles.btnContainer}
                        onPress={()=>navigation.navigate('Login',{role:'FrontDesk',screen:'Front'})}
                        //onPress={()=>navigation.navigate('Front')}
                        >
                            <Text style={Styles.txt}>FRONT DESK</Text>
                            <Image source={AppImage.arrow}
                                style={{ width: 5, height: 10 }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={Styles.imgContainer}>
                        <Image source={AppImage.hotel}
                            style={Styles.hotel} />
                        <TouchableOpacity style={Styles.btnContainer}
                        onPress={()=>navigation.navigate('Login',{role:'Hotel',screen:'home'})}
                        >
                            <Text style={[Styles.txt,{marginLeft:30}]}>HOTEL</Text>
                            <Image source={AppImage.arrow}
                                style={{ width: 5, height: 10 }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={Styles.imgContainer}>
                        <Image source={AppImage.branch_manager}
                            style={Styles.branch} />
                        <TouchableOpacity style={Styles.btnContainer}
                        //onPress={()=>navigation.navigate('Login',{role:'BranchManager',screen:'Manager'})}
                        onPress={()=>navigation.navigate('Login',{role:'BranchManager'})}
                        >
                            <Text style={[Styles.txt,{marginLeft:2}]}>BRANCH MANAGER</Text>
                            <Image source={AppImage.arrow}
                                style={{ width: 5, height: 10 }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center',marginBottom:30 }}>
                    <View style={Styles.imgContainer}>
                        <Image source={AppImage.employee}
                            style={Styles.empl} />
                        <TouchableOpacity style={Styles.btnContainer}
                        onPress={()=>navigation.navigate('Login',{role:'Employee'})}
                        >
                            <Text style={Styles.txt}>EMPLOYEE</Text>
                            <Image source={AppImage.arrow}
                                style={{ width: 5, height: 10 }} />
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView>
        </View>
    )
}

const Styles = StyleSheet.create({
    topTxt: {
        fontSize: 20,
        color: '#007654',
        marginTop: 50,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 20,
        textAlign: 'center'
    },
    frntdsk: {
        width: 180,
        height: 150,
        resizeMode: 'contain'
    },
    hotel: {
        width: 180,
        height: 150,
        resizeMode: 'contain',
        marginBottom:40
    },
    branch: {
        width: 180,
        height: 150,
        resizeMode: 'contain',
        marginBottom:30
    },
    empl: {
        width: 180,
        height: 150,
        resizeMode: 'contain',
        marginBottom:42
    },
    imgContainer: {
        marginTop: 20,
    },
    btnContainer: {
        width: 180,
        //height:30,
        flexDirection: 'row',
        backgroundColor: '#D09831',
        position: 'absolute',
        bottom: 0,
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 7
    },
    txt: {
        fontSize: 15,
        margin: 10,
        marginLeft: 20,
        color: '#003223',
        fontWeight: '700',
        fontFamily:AppFonts.slab,
    }
})

export default ProfileLoginScreen;