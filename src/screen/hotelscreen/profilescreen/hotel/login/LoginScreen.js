import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground, Platform
} from 'react-native';
import { Container } from 'native-base'
import { useDispatch } from 'react-redux';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-toast-message';
import AppStrings from '../../../../../utils/AppStrings';
import APIStrings from '../../../../../webservice/APIStrings';
import Loader from '../../../../../component/loader/Loader';
import AppStyles from '../../../../../utils/AppStyles';
import { simplePostCall } from '../../../../../webservice/APIServices';
import AppColor from '../../../../../utils/AppColor';
import toastConfig from '../../../../../component/Toast/Toast';
import { forgotCheckRole } from '../../../../../redux/reducer';
import { AuthContext } from '../../../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')

const LoginScreen = ({ navigation, route }) => {
    const { role } = route.params;
    const [userId, SetUserId] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false)
    const [show, setShow] = useState(false);
    const [token, setToken]=useState('');
    const [invalid, setInValid] = useState([false, false, false]);
    const dispatch=useDispatch();
    const { signInHotel} = React.useContext(AuthContext);

    useEffect( async()=>{
       const tok=await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN,'');
        setToken(tok);
    },[])

    const goToScreen=()=>{
            dispatch(forgotCheckRole(role));
            navigation.navigate('ChangedPassword1')
    }
    const onLoginPress = () => {
        if (userId.trim().length === 0) {
            setError('Please enter userId')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (password.trim().length === 0) {
            setError('Please enter valid password')
            setInValid([false, true, false])
            return;
        }


        setLoader(true)
        let requsetBody = JSON.stringify({
            password: password,
            mobile: userId,
            deviceToken:token,
            platform:Platform.OS
            
        });
        let link;
        switch (role) {
            case 'Hotel':
                link = APIStrings.HotelLoginView;
                break;
            case 'FrontDesk':
                link = APIStrings.FrontDeskLogin;
                break;
            case 'BranchManager':
                link = APIStrings.BranchManagerLogin;
                break;
            case 'Employee':
                link = APIStrings.EmployeeLogin;
                break;
        }
        //AsyncStorage.setItem(AppStrings.ROLE, role,null)
        //const link=role==='FrontDesk'?APIStrings.FrontDeskLogin:APIStrings.HotelLoginView
        console.log(link);
        console.log(requsetBody);

        simplePostCall(link, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Login success", JSON.stringify(data));
                    
                    switch (data.output.Role) {
                        case 'Hotel':
                            parseHotelData(data.output);
                            break;
                        case 'FrontDesk':
                            parseFrontData(data.output);
                            break;
                        case 'BranchManager':
                            parseManagerData(data.output);
                            break;
                        case 'Employee':
                            parseEmployeeData(data.output);
                            break;
                    }

                    Toast.show({
                        text1: 'Login Successfull !',

                    });



                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)
                    setError('User id or password is incorrect')
                    setInValid([false, false, true])

                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });




    }
    const parseHotelData = async (data) => {
        console.log("hotel data ", data.hotel_owner[0])
        try {
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, data.hotel_owner[0].hotel_name, +'');
            await AsyncStorage.setItem(AppStrings.WEB, data.hotel_owner[0].website + '');
            await AsyncStorage.setItem(AppStrings.PHONE, data.phone_number + '');
            await AsyncStorage.setItem(AppStrings.USER_ID, data.hotel_owner[0].id + "");
            await AsyncStorage.setItem(AppStrings.PINCODE, data.hotel_owner[0].pincode + "");
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.hotel_owner[0].address_first + '');
            await AsyncStorage.setItem(AppStrings.CITY, data.hotel_owner[0].city + "");
            await AsyncStorage.setItem(AppStrings.STATE, data.hotel_owner[0].state + '');
            await AsyncStorage.setItem(AppStrings.SOCIAL_MEDIA, data.hotel_owner[0].social_media + '');
            await AsyncStorage.setItem(AppStrings.VERIFIED_STATUS, data.hotel_owner[0].verified_status + '');
            await AsyncStorage.setItem(AppStrings.IMAGE, data.hotel_owner[0].hotel_photo + '');
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');
            signInHotel(data.Role);

        } catch (e) {
            console.log(e)
            setLoader(false)
        }


    }
    const parseFrontData = async (data) => {
        console.log("fornt ", data.hotel_name)
        try {
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, data.hotel_name + "");
            //await AsyncStorage.setItem(AppStrings.WEB, data[0].website, '');
            await AsyncStorage.setItem(AppStrings.PHONE, String(data.Front_Desk_User.phone_number), '');
            await AsyncStorage.setItem(AppStrings.USER_ID, data.Front_Desk_Branch.id + "");
            await AsyncStorage.setItem(AppStrings.CITY, data.Front_Desk_Branch.city + "");
            await AsyncStorage.setItem(AppStrings.STATE, data.Front_Desk_Branch.state + "");
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.Front_Desk_Branch.first_address + '');
            await AsyncStorage.setItem(AppStrings.BRANCH_NAME, data.Front_Desk_Branch.branch_name + '');
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');
            signInHotel(data.Role);
            //navigation.navigate('Front')

        } catch (e) {
            setLoader(false)
        }


    }
    const parseManagerData = async (data) => {
        console.log("manager ", data)
        try {
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, data.hotel_obj[0].hotel_name + "");
            //await AsyncStorage.setItem(AppStrings.WEB, data[0].website, '');
            await AsyncStorage.setItem(AppStrings.CITY, data.hotel_obj[0].city + "");
             await AsyncStorage.setItem(AppStrings.STATE, data.hotel_obj[0].state + "");
             await AsyncStorage.setItem(AppStrings.PHONE, String(data.branch_obj.contact_number), '');
             await AsyncStorage.setItem(AppStrings.USER_ID, data.branch_obj.id + "");
            //await AsyncStorage.setItem(AppStrings.PINCODE, data[0].Front_Desk_Branch + "");
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.branch_obj.first_address + '');
            await AsyncStorage.setItem(AppStrings.BRANCH_NAME, data.branch_obj.branch_name + '');
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');
            signInHotel(data.Role);

        } catch (e) {
            setLoader(false)
        }


    }
    const parseEmployeeData = async (data) => {
        console.log("Employee ", data)
        try {
            await AsyncStorage.setItem(AppStrings.EMPLOYEE_NAME, data.Employee.emp_user ? data.Employee.emp_user.first_name + "" : '');
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, data.hotel_name + "");
            await AsyncStorage.setItem(AppStrings.IMAGE, data.Employee.emp_user ? data.Employee.emp_user.profile_pic+"" :"");
            await AsyncStorage.setItem(AppStrings.EMAIL, data.Employee.email + '');
            await AsyncStorage.setItem(AppStrings.DOB, data.Employee.date_of_birth + '');
            await AsyncStorage.setItem(AppStrings.ANNIVERSARY_DATE, data.Employee.anniversary_date + '');
            await AsyncStorage.setItem(AppStrings.PHONE, data.Employee.emp_user ? data.Employee.emp_user.phone_number + "" : '');
            await AsyncStorage.setItem(AppStrings.USER_ID, data.Employee.id + "");
            await AsyncStorage.setItem(AppStrings.CITY, data.Employee.city + "");
            await AsyncStorage.setItem(AppStrings.AGE, data.Employee.age + "");
            await AsyncStorage.setItem(AppStrings.STATE, data.Employee.state + "");
            await AsyncStorage.setItem(AppStrings.PINCODE, data.Employee.pincode + "");
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.Employee.address + '');
            await AsyncStorage.setItem(AppStrings.Gender, data.Employee.gender + '');
            await AsyncStorage.setItem(AppStrings.BRANCH_NAME, data.Branch_Name + "");
            await AsyncStorage.setItem(AppStrings.BRANCH_ID, data.Employee.branch.id + "");
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');
            signInHotel(data.Role);

        } catch (e) {
            console.log(e)
            setLoader(false)
        }


    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.navigate('AllProfile')}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ alignSelf: 'center', marginTop: 8, }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
                <View style={{ alignSelf: 'center', marginTop: 20, }}>
                    <Image source={AppImage.welcom_icon}
                        style={{ width: 120, height: 30 }} />
                </View>

                <View style={{ marginTop: 40 }}>
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, color: 'white'
                        }}
                        placeholder='Enter User ID'
                        value={userId}
                        onChangeText={(text) => { SetUserId(text), setInValid(!invalid[0]) }}
                        placeholderTextColor={AppColor.inputlight_txtColor}
                    />
                    {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{
                        backgroundColor: '#039D70', flexDirection: 'row', width: '80%', height: 40,
                        alignSelf: 'center', elevation: 4, marginTop: invalid[0] ? 0 : 20, alignItems: 'center', borderRadius: 5,
                    }}>
                        <TextInput
                            style={{
                                width: '80%', height: 40,
                                paddingLeft: 20, color: 'white'
                            }}
                            placeholder='Enter Password'
                            value={password}
                            onChangeText={(text) => { setPassword(text), setInValid(!invalid[1]) }}
                            placeholderTextColor={AppColor.inputlight_txtColor}
                            secureTextEntry={!show}
                        />
                        <TouchableOpacity style={{
                            alignItems: 'center', marginLeft: 20,
                        }} onPress={() => setShow(!show)}>
                            <Image source={!show ? AppImage.show : AppImage.hide}
                                style={{ width: 25, height: 25 }}
                            />
                        </TouchableOpacity>
                    </View>
                    {invalid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {invalid[2] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                </View>
                <View style={{
                    flexDirection: 'row', justifyContent: 'flex-end', marginRight: 40,
                    marginTop: 20
                }}>
                    <Text style={{ color: 'blue' }} onPress={() => goToScreen()}>Forgot Password ?</Text>
                </View>
                <FabButton title='Login'
                    onItem={() => onLoginPress()}
                />

                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: width, height: height * 0.169, alignSelf: 'center', }}>
                        <ImageBackground source={AppImage.vector_curve3}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 45, }}>
                                <TouchableOpacity style={{ marginRight: 150, borderBottomWidth: 1, borderBottomColor: 'white', }}
                                    onPress={() => navigation.navigate('SignUp')}
                                    disabled={role !== 'Hotel'}
                                >
                                    <Text style={{ color: 'white', fontSize: 20, marginRight: 5, marginLeft: 5, }}

                                    >{role !== 'Hotel' ? 'Login' : 'Register'}</Text>
                                </TouchableOpacity>
                                <Text style={{ color: '#00B580', top: 10, }}>{role !== 'Hotel' ? '' : 'Login'}</Text>
                            </View>
                        </ImageBackground>

                    </View>
                </View>
                <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
            </ScrollView>
            <Loader loading={loader} />
        </View>
    )
}

export default LoginScreen;