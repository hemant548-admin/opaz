import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';

const { width, height } = Dimensions.get('screen')

const Hotelbg1 = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <ImageBackground source={AppImage.hotel_bg1}
             style={{flex:1,alignItems:'center'}}
            >
            <View style={{marginTop:100}}>
                <Text style={styles.headertxt}>You are all set</Text>
            </View>
            <View style={styles.container}>
                <Text style={styles.containerTxt}>Thanks for registering ! Now let's
                    help people to find you and enjoy at your place.
                </Text>
            </View>
            <TouchableOpacity style={styles.btnContainer} onPress={()=>navigation.navigate('Login',{role:'Hotel'})}
            activeOpacity={0.8}
            >
                <Text style={styles.btnTxt}>Let's  Start</Text>
            </TouchableOpacity>
            </ImageBackground>
        </View>
    )
}
const styles = StyleSheet.create({
    headertxt:{
        color:'white',
        fontSize:30,
        fontFamily:AppFonts.bold
    },
    container:{
     backgroundColor:'rgba(100, 100, 100, 0.5)' ,
     marginTop:100  
    },
    containerTxt:{
        color:'white',
        fontSize:20,
        marginTop:5,
        marginBottom:5,
        marginLeft:30,
        marginRight:30,
        textAlign:'center'
    },
    btnContainer:{
        backgroundColor:AppColor.appColor,
        width:'50%',
        alignItems:'center',
        borderRadius:20,
        marginTop:130
    },
    btnTxt:{
        color:'white',
        fontSize:20,
        margin:7
    }
})

export default Hotelbg1;