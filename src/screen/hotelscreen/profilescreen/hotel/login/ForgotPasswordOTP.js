import React, { useState, useRef, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView,
    ImageBackground, StyleSheet
} from 'react-native';
import { useSelector } from 'react-redux';
import RNOtpVerify from 'react-native-otp-verify';
import AsyncStorage from '@react-native-community/async-storage';
import AppImage from '../../../../../utils/AppImage';
import AppColor from '../../../../../utils/AppColor';
import FabButton from '../../../../../component/button/FabButton';
import APIStrings from '../../../../../webservice/APIStrings';
import AppStrings from '../../../../../utils/AppStrings';
import { simplePostCall } from '../../../../../webservice/APIServices';
import Loader from '../../../../../component/loader/Loader';
import AppStyles from '../../../../../utils/AppStyles';

const { width, height } = Dimensions.get('screen')
const RESEND_OTP_TIME_LIMIT = 30; // 30 secs
const AUTO_SUBMIT_OTP_TIME_LIMIT = 4; // 4 secs

let resendOtpTimerInterval;
let autoSubmitOtpTimerInterval;

const ForgotPasswordOTp = ({ navigation, route }) => {
    const [otp, setOtp] = useState(['', '', '', '']);
    const OTPRefs = useRef([null, null, null, null]);
    const [loader, setLoader] = useState(false);
    const [phone, setPhone] = useState('');
    const [attemptsRemaining, setAttemptsRemaining] = useState(4);
    const [otpArray, setOtpArray] = useState(['', '', '', '']);
    const [submittingOtp, setSubmittingOtp] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [invalid,setInValid]=useState(false);
    const role=useSelector(state=>state.forgotRole);

    const { mobile } = route.params;

    // in secs, if value is greater than 0 then button will be disabled
    const [resendButtonDisabledTime, setResendButtonDisabledTime] = useState(
        RESEND_OTP_TIME_LIMIT,
    );

    // 0 < autoSubmitOtpTime < 4 to show auto submitting OTP text
    const [autoSubmitOtpTime, setAutoSubmitOtpTime] = useState(
        AUTO_SUBMIT_OTP_TIME_LIMIT,
    );

    // TextInput refs to focus programmatically while entering OTP
    const firstTextInputRef = useRef(null);
    const secondTextInputRef = useRef(null);
    const thirdTextInputRef = useRef(null);
    const fourthTextInputRef = useRef(null);

    // a reference to autoSubmitOtpTimerIntervalCallback to always get updated value of autoSubmitOtpTime
    const autoSubmitOtpTimerIntervalCallbackReference = useRef();

    useEffect(async () => {

        let phone = await AsyncStorage.getItem(AppStrings.PHONE)
        setPhone(phone)
        // autoSubmitOtpTime value will be set after otp is detected,
        // in that case we have to start auto submit timer
        autoSubmitOtpTimerIntervalCallbackReference.current = autoSubmitOtpTimerIntervalCallback;
    });

    useEffect(() => {
        startResendOtpTimer();

        return () => {
            if (resendOtpTimerInterval) {
                clearInterval(resendOtpTimerInterval);
            }
        };
    }, [resendButtonDisabledTime]);

    const getHash = () =>
    RNOtpVerify.getHash()
    .then(console.log)
    .catch(console.log);

    useEffect(() => {
        // docs: https://github.com/faizalshap/react-native-otp-verify
        getHash();
        RNOtpVerify.getOtp()
          .then(p =>
            RNOtpVerify.addListener(message => {
              try {
                if (message) {
                  const messageArray = message.split('\n');
                  if (messageArray[2]) {
                    const otp = messageArray[2].split(' ')[0];
                    if (otp.length === 4) {
                      setOtpArray(otp.split(''));
    
                      // to auto submit otp in 4 secs
                      setAutoSubmitOtpTime(AUTO_SUBMIT_OTP_TIME_LIMIT);
                      startAutoSubmitOtpTimer();
                    }
                  }
                }
              } catch (error) {
                logErrorWithMessage(
                  error.message,
                  'RNOtpVerify.getOtp - read message, OtpVerification',
                );
              }
            }),
          )
          .catch(error => {
            logErrorWithMessage(
              error.message,
              'RNOtpVerify.getOtp, OtpVerification',
            );
          });
    
        // remove listener on unmount
        return () => {
          RNOtpVerify.removeListener();
        };
      }, []);
    
    const startResendOtpTimer = () => {
        if (resendOtpTimerInterval) {
            clearInterval(resendOtpTimerInterval);
        }
        resendOtpTimerInterval = setInterval(() => {
            if (resendButtonDisabledTime <= 0) {
                clearInterval(resendOtpTimerInterval);
            } else {
                setResendButtonDisabledTime(resendButtonDisabledTime - 1);
            }
        }, 1000);
    };

    // this callback is being invoked from startAutoSubmitOtpTimer which itself is being invoked from useEffect
    // since useEffect use closure to cache variables data, we will not be able to get updated autoSubmitOtpTime value
    // as a solution we are using useRef by keeping its value always updated inside useEffect(componentDidUpdate)
    const autoSubmitOtpTimerIntervalCallback = () => {
        if (autoSubmitOtpTime <= 0) {
            clearInterval(autoSubmitOtpTimerInterval);

            // submit OTP
            // onSubmitButtonPress();
        }
        setAutoSubmitOtpTime(autoSubmitOtpTime - 1);
    };

    const startAutoSubmitOtpTimer = () => {
        if (autoSubmitOtpTimerInterval) {
            clearInterval(autoSubmitOtpTimerInterval);
        }
        autoSubmitOtpTimerInterval = setInterval(() => {
            autoSubmitOtpTimerIntervalCallbackReference.current();
        }, 1000);
    };

    const refCallback = textInputRef => node => {
        textInputRef.current = node;
    };

    const onResendOtpButtonPress = () => {
        // clear last OTP
        if (firstTextInputRef) {
            setOtpArray(['', '', '', '']);
            firstTextInputRef.current.focus();
        }

        setResendButtonDisabledTime(RESEND_OTP_TIME_LIMIT);
        startResendOtpTimer();

        // resend OTP Api call
        // todo
        console.log('todo: Resend OTP');
    };
    const check=(item)=>{

        return item==='';
    }
    const onSubmitButtonPress = async () => {
        
        if(otpArray.every(check)) {
            setInValid(true)
            setErrorMessage('Please Enter OTP')

            return;
        }
        setLoader(true)

        let requsetBody = JSON.stringify({
            mobile: mobile,
            otp: otpArray[0].toString() + otpArray[1].toString() + otpArray[2].toString() + otpArray[3].toString()
        });
        let link;
        switch (role) {
            case 'Hotel':
                link = APIStrings.VarifyHotelForgotPassOtp;
                break;
            case 'FrontDesk':
                link = APIStrings.VarifyFrontDeskForgotPassOtp;
                break;
            case 'BranchManager':
                link = APIStrings.VarifyBranchManagerForgotPassOtp;
                break;
            case 'Employee':
                link = APIStrings.VarifyEmployeeForgotPassOtp;
                break;
        }
        console.log(link);
        console.log(requsetBody);
       

        simplePostCall(link, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("otp success", JSON.stringify(data));
                    alert(data.status.message)
                    navigation.navigate('ChangedPassword2', { mobile: mobile })

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    setInValid(true);
                    setErrorMessage('Please enter valid OTP')
                    console.log(data.status.message)
                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
        console.log('todo: Submit OTP', otpArray);
    };

    // this event won't be fired when text changes from '' to '' i.e. backspace is pressed
    // using onOtpKeyPress for this purpose
    const onOtpChange = index => {
        return value => {
            if (isNaN(Number(value))) {
                // do nothing when a non digit is pressed
                return;
            }
            const otpArrayCopy = otpArray.concat();
            otpArrayCopy[index] = value;
            setOtpArray(otpArrayCopy);
            console.log('number', otpArray)
            // auto focus to next InputText if value is not blank
            if (value !== '') {
                if (index === 0) {
                    secondTextInputRef.current.focus();
                } else if (index === 1) {
                    thirdTextInputRef.current.focus();
                } else if (index === 2) {
                    fourthTextInputRef.current.focus();
                }
            }
        };
    };

    // only backspace key press event is fired on Android
    // to have consistency, using this event just to detect backspace key press and
    // onOtpChange for other digits press
    const onOtpKeyPress = index => {
        return ({ nativeEvent: { key: value } }) => {
            // auto focus to previous InputText if value is blank and existing value is also blank
            if (value === 'Backspace' && otpArray[index] === '') {
                if (index === 1) {
                    firstTextInputRef.current.focus();
                } else if (index === 2) {
                    secondTextInputRef.current.focus();
                } else if (index === 3) {
                    thirdTextInputRef.current.focus();
                }

                /**
                 * clear the focused text box as well only on Android because on mweb onOtpChange will be also called
                 * doing this thing for us
                 * todo check this behaviour on ios
                 */
                if (Platform.OS === 'android' && index > 0) {
                    const otpArrayCopy = otpArray.concat();
                    otpArrayCopy[index - 1] = ''; // clear the previous box which will be in focus
                    setOtpArray(otpArrayCopy);
                }
            }
        };
    };

    const onKeyPress = (keyName, index) => {

        if (keyName === 'Backspace' && otp[index].length === 0) {
            if (index != 0) {
                OTPRefs[index - 1].current.focus();
            }
        }
    }

    const onChangeText = (value, index) => {

        if (value === ' ')
            return;

        let otp1 = otp;
        otp1[index] = value;
        setOtp(otp1)
        //this.setState({ otp: otp });

        if (otp[index].length === 1) {
            secondTextInputRef.current.focus();
        }
    }


    const renderOTPInputBox = () => {
        return [
            firstTextInputRef,
            secondTextInputRef,
            thirdTextInputRef,
            fourthTextInputRef,
        ].map((textInputRef, index) => {
            return (
                <View>
                    <TextInput
                        style={[styles.optStyle]}
                        // padding={5}
                        value={otpArray[index]}
                        onKeyPress={onOtpKeyPress(index)}
                        onChangeText={onOtpChange(index)}
                        keyboardType={'numeric'}
                        maxLength={1}
                        //style={[styles.otpText, GenericStyles.centerAlignedText]}
                        autoFocus={index === 0 ? true : undefined}
                        ref={refCallback(textInputRef)}
                        key={index}
                    />
                </View>
            )
        })
    }


    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ alignSelf: 'center', marginTop: 8, }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
                <View style={{ alignSelf: 'center', marginTop: 20, }}>
                    <Image source={AppImage.welcom_icon}
                        style={{ width: 120, height: 30 }} />
                </View>
                <View>
                    <Text style={styles.txt}>OTP sent to Your Mobile Number- {mobile}.
                            <Text style={styles.click} onPress={() => navigation.goBack()}> Click here</Text> to change Mobile number
                        </Text>
                </View>
                <View style={styles.otpTxtContainer}>
                    <Text style={styles.otpTxt}>Enter OTP</Text>
                </View>
                <View flexDirection='row' justifyContent='space-around' margin={10}>
                    {renderOTPInputBox()}
                </View>
                {resendButtonDisabledTime > 0 ? (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
                        <Text style={{ marginRight: 20, color: 'black' }}>Resend OTP in time {resendButtonDisabledTime} </Text>
                    </View>
                ) : (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', }}>
                        <Text style={{ color: 'blue', marginRight: 20, fontSize: 16 }} onPress={() => onResendOtpButtonPress()}>
                            Resend OTP</Text>
                    </View>
                )}
                {invalid && <Text style={[AppStyles.apiErrorStyle]}>{errorMessage}</Text>}
                <FabButton title='Verify OTP'
                    onItem={() => onSubmitButtonPress()}
                />

                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: width, height: height * 0.169, alignSelf: 'center', }}>
                        <ImageBackground source={AppImage.vector_curve3}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 45, }}>
                                <TouchableOpacity style={{ marginRight: 150, borderBottomWidth: 1, borderBottomColor: 'white', }}
                                    onPress={() => navigation.navigate('SignUp')}
                                >
                                    <Text style={{ color: 'white', fontSize: 20, marginRight: 5, marginLeft: 5, }}

                                    >Register</Text>
                                </TouchableOpacity>
                                <Text style={{ color: '#00B580', top: 10, }}
                                    onPress={() => navigation.navigate('Login')}
                                >Login</Text>
                            </View>
                        </ImageBackground>

                    </View>
                </View>

            </ScrollView>
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({

    optStyle: {
        //borderBottomColor: AppColors.colorLine,
        //borderBottomWidth: 3,
        width: 50,
        height: 50,
        margin: 5,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 8,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        elevation: 2,
        color: 'white',
        backgroundColor: AppColor.inputColor
    },
    txt: {
        color: AppColor.appColor,
        marginTop: 20,
        marginRight: 35,
        marginLeft: 35,
        fontSize: 15,
        textAlign: 'center'
    },
    click: {
        fontSize: 16,
        fontWeight: '700'
    },
    otpTxtContainer: {
        marginTop: 40,
        marginLeft: 30,
        fontSize: 20,
        color: AppColor.appColor,
        fontWeight: '700'
    },
    otpTxt: {
        fontSize: 20,
        color: AppColor.appColor,
        //fontWeight:'700' 
    },

})
export default ForgotPasswordOTp;