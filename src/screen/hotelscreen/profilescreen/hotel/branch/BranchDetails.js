import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
//import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import APIStrings from '../../../../../webservice/APIStrings';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../../utils/AppStrings';
import Modal from 'react-native-modal';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { multipartPostCall, simplePostCall } from '../../../../../webservice/APIServices';
import Loader from '../../../../../component/loader/Loader';
import ImagePicker from 'react-native-image-crop-picker';
import ImageSlider from 'react-native-image-slider';
import axios from 'axios';

const { width, height } = Dimensions.get('screen')

//const arr = ["https://source.unsplash.com/1024x768/?nature", "file:///data/user/0/com.opaz/cache/react-native-image-crop-picker/ciudad-maderas-MXbM1NrRqtI-unsplash.jpg", "file:///data/user/0/com.opaz/cache/react-native-image-crop-picker/eddi-aguirre-ZAVHbpOn4Jk-unsplash.jpg"]

const BranchDetails = ({ navigation, route }) => {
    //console.log(width, height)
    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [web, setWeb] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [branchName, setBranchName] = useState('');
    const [contactNumber, setContactNumber] = useState('');
    const [lat, setLat] = useState('18.504613');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false);
    const [invalid, setInValid] = useState([false, false]);
    const [filePath, setFilePath] = useState([]);
    const [hotelID, setHotelID] = useState('');
    const [data, setData] = useState({});
    const [imgArray, setimgArray] = useState([]);
    const { id } = route.params;
    useEffect(() => {

        Fetch();   // let hotelId = await AsyncStorage.getItem(AppStrings.USER_ID," ")
        // setHotelID(hotelId)

    }, []);


    const Fetch = async () => {
        const hotelId = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setHotelID(hotelId)

        //const hotelId=await AsyncStorage.getItem(AppStrings.USER_ID," ")
        setLoader(true)
        let requsetBody = JSON.stringify({
            hotel_id: hotelId,
            branch_id: id,

        });



        // console.log("url ", APIStrings.BranchAdd)
        // multipartPostCall(APIStrings.BranchAdd, data)
        //     .then((data) => {
        //         setLoader(false)
        //         console.log("addbranch response success", JSON.stringify(data));
        //     }).catch((error) => {
        //         setLoader(false)
        //         console.log("Used api response", error);
        //     });
        //this.setState({toggle:false})


        //AsyncStorage.setItem(AppStrings.ROLE, role,null)
        console.log("Api", APIStrings.BranchDetails);
        console.log(requsetBody);

        simplePostCall(APIStrings.BranchDetails, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("details success", JSON.stringify(data));
                    setData(data.branch_list)
                    setimgArray(data.branch_list.branch_image)
                    console.log(data.branch_list.branch_image)
                    //navigation.navigate('home')



                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)
                    //alert("Api response " + data.status.message)

                }

            }).catch((error) => {
                console.log("api error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }

    // const setValue = async (pin) => {
    //     setPincode(pin)
    //     if (pin.trim().length === 6) {
    //         // setError('Please Enter valid Pincode')
    //         // setInValid([false, true, false])
    //         // return;
    //         console.log(pin)

    //         let link = 'https://api.postalpincode.in/pincode/' + `${pin}`;
    //         console.log(link)
    //         fetch(link)
    //             .then((response) => response.json())
    //             .then((json) => {
    //                 parseData(json);
    //                 console.log(json.map(item => {
    //                     return item.PostOffice[0]
    //                 }))
    //             })
    //             .catch((error) => console.error(error))

    //     }
    // }
    // const parseData = (data) => {
    //     const arr = data.map(item => {
    //         return item.PostOffice[0]



    //     })

    //     setCity(arr[0].District)
    //     setState(arr[0].State)
    //     console.log('data', arr[0].District)

    // }

    // const toggleModal = () => {
    //     setModalVisible(!isModalVisible);
    // };

    // const renderModal = () => {
    //     return (
    //         <View>
    //             <Modal isVisible={isModalVisible}
    //                 onBackdropPress={() => setModalVisible(false)}
    //             >
    //                 <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    //                     <View style={{ width: '95%', height: '22%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
    //                         <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
    //                             <TouchableOpacity onPress={() => CameraImage()}
    //                             >
    //                                 <Image source={AppImage.camera_display}
    //                                     style={{ width: 50, height: 50, marginRight: 50 }}
    //                                 />
    //                             </TouchableOpacity>
    //                             <TouchableOpacity onPress={() => multipleImage()}>
    //                                 <Image source={AppImage.gallery_icon}
    //                                     style={{ width: 50, height: 50, marginLeft: 50 }}
    //                                 />
    //                             </TouchableOpacity>
    //                         </View>
    //                     </View>
    //                 </View>
    //             </Modal>
    //         </View>
    //     );
    // }
    return (
        <View style={{ flex: 1, backgroundColor: AppColor.appColor, }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.topView}>

                    {imgArray.length > 0 ?
                        <ImageSlider
                            loopBothSides
                            autoPlayWithInterval={3000}
                            images={imgArray}
                            customSlide={({ index, item, style, width }) => (
                                //console.log('data ',item),
                                // It's important to put style here because it's got offset inside
                                <View key={index} style={{ flex: 1 }}>
                                    <Image source={{ uri: item.image }} style={{ flex: 1, width: width, height: 100 }} />
                                </View>
                            )}
                        />

                        :
                        <View style={{}}>

                            <View style={{ flexDirection: 'row', width: '100%', height: '50%', }}>
                                <View style={{ width: '45%', height: '100%' }}>
                                    <Image source={!filePath[0] ? AppImage.img1 : { uri: filePath[0].path }}
                                        style={{ width: '100%', height: '100%', resizeMode: 'cover' }}
                                    />
                                </View>
                                <View style={{ width: '55%', height: '100%' }}>
                                    <Image source={!filePath[1] ? AppImage.img2 : { uri: filePath[1].uri }}
                                        style={{ width: '100%', height: '100%', resizeMode: 'cover', }}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', width: '100%', height: '57%', }}>
                                <View style={{ width: '33%', height: '100%' }}>
                                    <Image source={!filePath[2] ? AppImage.img3 : { uri: filePath[2].uri }}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View>
                                <View style={{ width: '33%', height: '100%' }}>
                                    <Image source={AppImage.img4}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View>
                                <View style={{ width: '34%', height: '100%' }}>
                                    <Image source={AppImage.img5}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View>
                            </View>
                        </View>}
                    <View style={styles.topHeader}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', marginLeft: 30, }}>
                                <TouchableOpacity onPress={() => navigation.goBack()}>
                                    <Image source={AppImage.arrow_green}
                                        style={{ width: 16, height: 16, marginTop: 4 }}
                                    />
                                </TouchableOpacity>
                                <Text style={{
                                    textAlign: 'center', marginLeft: 15, color: AppColor.appColor,
                                    fontSize: 16, fontFamily: AppFonts.medium
                                }}>View Branch</Text>
                            </View>
                            {/* <TouchableOpacity style={{
                            width: 32, height: 32, borderRadius: 16, backgroundColor: 'red',
                            marginRight: 20, alignItems: 'center', justifyContent: 'center'
                        }} onPress={() => toggleModal()}>
                            <Image source={AppImage.camera_icon}
                                style={{ width: 20, height: 20 }} />
                        </TouchableOpacity> */}
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40,
                    borderTopColor: AppColor.appColor, bottom: 25
                }}>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.branchName_icon}
                                style={{ width: 26, height: 18 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Branch Name'
                            //placeholderTextColor={AppColor.appColor}
                            value={data.branch_name}
                            onChangeText={(value) => { setBranchName(value) }}
                            editable={false}

                        />
                    </View>
                    {/* <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Name'
                            placeholderTextColor={AppColor.appColor}
                            value={address1}
                            onChangeText={(value)=>{setAddress1(value)}}

                        />
                    </View> */}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Address Line1'
                            //placeholderTextColor={AppColor.appColor}
                            value={data.first_address}
                            editable={false}
                            onChangeText={(value) => { setAddress1(value) }}

                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Address Line2'
                            //placeholderTextColor={AppColor.appColor}
                            value={data.second_address}
                            editable={false}
                            onChangeText={(value) => { setAddress2(value) }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Pincode'
                            //placeholderTextColor={AppColor.appColor}
                            value={data.pincode}
                            editable={false}
                            onChangeText={(pin) => { setValue(pin), setInValid(!invalid[1]) }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TextInput style={styles.mobileContainer1}
                                placeholder='City'
                                // placeholderTextColor={AppColor.appColor}
                                value={data.city}
                                editable={false}
                                onChangeText={(value) => { setCity(value) }}
                            />
                            <TextInput style={styles.mobileContainer1}
                                placeholder='State'
                                //placeholderTextColor={AppColor.appColor}
                                value={data.state}
                                editable={false}
                                onChangeText={(value) => { setState(value) }}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.web_icon}
                                style={{ width: 25, height: 25 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Website'
                            // placeholderTextColor={AppColor.appColor}
                            value={data.website}
                            editable={false}
                            onChangeText={(value) => { setWeb(value) }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.call_green}
                                style={{ width: 25, height: 25, marginRight: 5 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Contact Number'
                            value={data.contact_number}
                            editable={false}
                            onChangeText={(value) => { setContactNumber(value) }}
                        //placeholderTextColor={AppColor.appColor}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}
                        >
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='lat/lon'
                            //placeholderTextColor={AppColor.appColor}
                            value={data.latitude + "  " + data.longitude}
                            editable={false}
                            onChangeText={(value) => { setLat(value) }}

                        />
                    </View>
                    {/* <TouchableOpacity style={styles.btnContainer}
                        onPress={() => onSavePress()}
                    >
                        <Text style={styles.btnTxt}>Save</Text>
                    </TouchableOpacity> */}
                    <View style={{ width: '100%', height: 60, marginBottom: 130 }} />
                </View>
            </ScrollView>
            <Loader loading={loader} />
        </View>

    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '30%',
        //position:'absolute',
        //top:30
        //backgroundColor: 'red'
    },
    topHeader: {
        //marginTop: 10,
        //marginLeft: 30,
        // backgroundColor:'yellow',
        position: 'absolute',
        justifyContent: 'center',
        width: '100%',
        height: '22%',
        alignSelf: 'center',
        top: 0
        //marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        color: AppColor.appColor
        //textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        backgroundColor: AppColor.appColor,
        width: '50%',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20,
        alignSelf: 'center'
    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 7
    }

})

export default BranchDetails;