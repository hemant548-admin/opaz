import React from 'react';
import { View } from 'react-native';
import FabButton from '../../../../../component/button/FabButton';
import MapInput from '../../../../../component/Map/MapInput';
import MyMapView from '../../../../../component/Map/MyMapView';
import { getLocation, geocodeLocationByName } from '../../../../../component/services/location-service';

class MapScreen extends React.Component {
    state = {
        region: {}
    };

    componentDidMount() {
        this.getInitialState();
    }

    getInitialState() {
        getLocation().then(
            (data) => {
                console.log(data);
                this.setState({
                    region: {
                        latitude: data.latitude,
                        longitude: data.longitude,
                        latitudeDelta: 0.003,
                        longitudeDelta: 0.003
                    }
                });
            }
        );
    }

    getCoordsFromName(loc) {
        this.setState({
            region: {
                latitude: loc.lat,
                longitude: loc.lng,
                latitudeDelta: 0.003,
                longitudeDelta: 0.003
            }
        });
        console.log('region', this.state.region)
    }

    onMapRegionChange(region) {
        this.setState({ region });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{flex:1 }}>
                    <MapInput notifyChange={(loc) => this.getCoordsFromName(loc)}
                    />
                </View>

                {
                    this.state.region['latitude'] ?
                        <View style={{ flex: 8 }}>
                            <MyMapView
                                region={this.state.region}
                                onRegionChange={(reg) => this.onMapRegionChange(reg)} 
                                />
                        </View> : null}
                        <View style={{flex:1,bottom:10}}>
                            <FabButton 
                            title='Ok'
                            />
                        </View>
            </View>
        );
    }
}

export default MapScreen;