import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,Linking,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import APIStrings from '../../../../../webservice/APIStrings';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../../utils/AppStrings';
import Modal from 'react-native-modal';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { multipartPostCall, simplePostCall } from '../../../../../webservice/APIServices';
import Loader from '../../../../../component/loader/Loader';
import ImagePicker from 'react-native-image-crop-picker';
import ImageSlider from 'react-native-image-slider';
import DocumentPicker from 'react-native-document-picker';
import axios from 'axios';
import AppStyles from '../../../../../utils/AppStyles';
import ImageUploader from '../../../../../component/loader/ImageUploader';

const { width, height } = Dimensions.get('screen')

const arr = ["https://source.unsplash.com/1024x768/?nature", "file:///data/user/0/com.opaz/cache/react-native-image-crop-picker/ciudad-maderas-MXbM1NrRqtI-unsplash.jpg", "file:///data/user/0/com.opaz/cache/react-native-image-crop-picker/eddi-aguirre-ZAVHbpOn4Jk-unsplash.jpg"]

const URL='https://dashboard.razorpay.com/'
const AddBranch = ({ navigation }) => {
    //console.log(width, height)
    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [web, setWeb] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [branchName, setBranchName] = useState('');
    const [contactNumber, setContactNumber] = useState('');
    const [lat, setLat] = useState('18.504613');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false);
    const [invalid, setInValid] = useState([false, false, false, false, false, false,false,false,false,false]);
    const [filePath, setFilePath] = useState([]);
    const [hotelID, setHotelID] = useState('');
    const [isModalVisible, setModalVisible] = useState(false);
    const [isPin, setisPin] = useState(false);
    const [color, setColor] = useState('#add9ab');
    const [phone, setPhone]=useState('');
    const [razorPayId, setRazorPayId]=useState('');
    const [razorPayKey, setRazorPayKey]=useState('');
    const [noOfRooms, setNoOfRooms]=useState('');
    const [singleFile, setSingleFile] = useState({});


    useEffect(() => {

        const Fetch = async () => {
            const hotelId = await AsyncStorage.getItem(AppStrings.USER_ID, "")
            const mob = await AsyncStorage.getItem(AppStrings.PHONE, "")
            setHotelID(hotelId)
            setPhone(mob);
        }
        Fetch();   // let hotelId = await AsyncStorage.getItem(AppStrings.USER_ID," ")
        // setHotelID(hotelId)

    }, []);

    const selectOneFile = async () => {
        //Opening Document Picker for selection of one file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
                //There can me more options as well
                // DocumentPicker.types.allFiles
                // DocumentPicker.types.images
                // DocumentPicker.types.plainText
                // DocumentPicker.types.audio
                // DocumentPicker.types.pdf
            });
            //Printing the log realted to the file
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.name);
            console.log('File Size : ' + res.size);
            //Setting the state to show single file attributes
            setSingleFile(res);
            setColor(AppColor.appColor)
            //this.setState({ singleFile: res, color: AppColor.appColor })
        } catch (err) {
            //Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                //If user canceled the document selection
                //alert('Canceled from single doc picker');
            } else {
                //For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    };
    // const chooseGallery = () => {
    //     setModalVisible(false)
    //     let options = {
    //       title: 'Select Image',
    //       customButtons: [
    //         {
    //           name: 'customOptionKey',
    //           title: 'Choose Photo from Custom Option'
    //         },
    //       ],
    //       storageOptions: {
    //         skipBackup: true,
    //         path: 'images',
    //       },
    //     };
    //     launchImageLibrary(options, (response) => {
    //       console.log('Response = ', response);

    //       if (response.didCancel) {
    //         console.log('User cancelled image picker');
    //       } else if (response.error) {
    //         console.log('ImagePicker Error: ', response.error);
    //       } else if (response.customButton) {
    //         console.log(
    //           'User tapped custom button: ',
    //           response.customButton
    //         );
    //         alert(response.customButton);
    //       } else {
    //         let source = response;
    //         // You can also display the image using data:
    //         // let source = {
    //         //   uri: 'data:image/jpeg;base64,' + response.data
    //         // };
    //         setFilePath([source,...filePath]);
    //         console.log('file', [source,...filePath])
    //       }
    //     });
    //   };
    const chooseCamera = () => {
        //setModalVisible(false)
        let options = {
            title: 'Select Image',
            customButtons: [
                {
                    name: 'customOptionKey',
                    title: 'Choose Photo from Custom Option'
                },
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        }

        launchCamera(options, (response) => {

            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log(
                    'User tapped custom button: ',
                    response.customButton
                );
                alert(response.customButton);
            } else {
                let source = response;
                // You can also display the image using data:
                // let source = {
                //   uri: 'data:image/jpeg;base64,' + response.data
                // };
                setFilePath([source, ...filePath]);
                console.log('file', [source, ...filePath])
            }
        });
    };
    const multipleImage = () => {

        ImagePicker.openPicker({
            multiple: true,
            includeBase64: false,

        }).then(images => {
            console.log("multiple pick imges", images);

            setFilePath(images)
            //console.log("img ",img)
        });
        toggleModal()
    }
    const CameraImage = () => {
        ImagePicker.openCamera({

            cropping: true,
        }).then(image => {
            console.log(image);
            let file = [];
            file.push(image)
            setFilePath(filePath.concat(image))
            console.log(file)
        });
        toggleModal()
    }
    const onSavePress = () => {
        let regex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
        let sum = 0;
        filePath.forEach((element, i) => {
            sum += element.size;
        });
        if (sum > 10485800) {
            //setError('Please Enter valid website or domain name')
            //setInValid([true, false, false, false, false])
            //hotelRef.current.focus();
            alert('All the image size will not be exceeds to more than 10 Mb')
            return;
        }
        if(filePath.length===0){
            alert('Please upload hotel image')
            return;
        }
        if (branchName.trim().length === 0) {
            setError('Please Enter Branch Name')
            setInValid([true, false, false, false, false, false,false,false,false,false])
            //hotelRef.current.focus();
            return;
        }
        if (address1.trim().length === 0) {
            setError('Please enter address')
            setInValid([false, true, false, false, false, false,false,false,false,false])
            return;
        }
        if (pincode.trim().length === 0 || pincode.trim().length < 6
            || isNaN(pincode) || isPin) {
            setError('Please Enter valid Pincode')
            setInValid([false, false, true, false, false, false,false,false,false,false])
            return;
        }
        if (web.trim().length > 0 && !regex.test(web)) {
            setError('Please Enter valid website or domain name')
            setInValid([false, false, false, true, false, false,false,false,false,false])
            return;
        }
        if (contactNumber.trim().length === 0 || contactNumber.trim().length < 10 || isNaN(contactNumber)) {
            setError('Please enter valid number')
            setInValid([false, false, false, false, true, false,false,false,false,false])
            return;
        }
        if (!lat) {
            setError('Please select address from map')
            setInValid([false, false, false, false, false, true,false,false,false,false])
            return;
        }
        if (noOfRooms.trim().length===0 || isNaN(noOfRooms)) {
            setError('Please Enter valid number')
            setInValid([false, false, false, false, false, false,true,false,false,false])
            return;
        }
        if (razorPayId.trim().length===0) {
            setError('*required')
            setInValid([false, false, false, false, false, false,false,true,false,false])
            return;
        }
        if (razorPayKey.trim().length===0) {
            setError('*required')
            setInValid([false, false, false, false, false, false,false,false,true,false])
            return;
        }
        if(Object.keys(singleFile).length===0){
            setError('Please select address from map')
            setInValid([false, false, false, false, false, false,false,false,false,true])
            return;
        }

        //const hotelId=await AsyncStorage.getItem(AppStrings.USER_ID," ")
        setLoader(true)
        // let requsetBody = JSON.stringify({
        //     hotel_id:hotelID , 
        //     branch_name:branchName , 
        //     manager_name:'' , 
        //     first_address:address1 , 
        //     second_address:address2 , 
        //     pincode:pincode , 
        //     city:city , 
        //     state:state , 
        //     website:web , 
        //     contact_number:contactNumber, 
        //     latitude:lat , 
        //     longitude:"73.798308" , 
        //     branch_image:filePath ,
        // });

        const data = new FormData();
        data.append('hotel_id', hotelID,);
        data.append('branch_name', branchName);
        data.append('manager_name', "");
        data.append('first_address', address1);
        data.append('second_address', address2);
        data.append('pincode', pincode);
        data.append('city', city);
        data.append('state', state);
        data.append('website', web);
        data.append('latitude', lat);
        data.append('longitude', "73.798308");
        data.append('contact_number', contactNumber);
        data.append('razorpay_id', razorPayId);
        data.append('razorpay_secret_key', razorPayKey);
        data.append('number_of_room', noOfRooms);
        data.append('login_user_mobile_number',phone );

        data.append('document', {

            name: singleFile.name,
            type: singleFile.type,
            uri: singleFile.uri
      
          })
        // Please change file upload URL

        filePath.forEach((element, i) => {

            const newFile = {
                name: 'image.jpg',
                type: element.mime,
                uri: Platform.OS === 'ios' ? element.path.replace('file://', '') : element.path,
            }
            data.append('branch_image', newFile)
        });

        console.log("data1 ", data)
        console.log("data ", JSON.stringify(data));
        axios.post(APIStrings.BranchAdd, data)
            .then(res => {
                //response
                console.log(res.data)
                alert('Branch created successfully')
                setLoader(false)
                navigation.navigate('BranchList')
            }).catch(err => {
                setLoader(false)
                console.log(err)
                //error
            })


    }

    const setValue = async (pin) => {
        setPincode(pin)
        if (isNaN(pin)) {
            console.log("pineee ")
            setError('Please Enter 6 digit Pincode')
            setisPin(true)
            setInValid([false, false, true, false, false])
            return;
        }
        setisPin(false)

        if (pin.trim().length === 6) {
            // setError('Please Enter valid Pincode')
            // setInValid([false, true, false])
            // return;
            console.log(pin)

            let link = 'https://api.postalpincode.in/pincode/' + `${pin}`;
            console.log(link)
            fetch(link)
                .then((response) => response.json())
                .then((json) => {
                    parseData(json);
                    console.log(json.map(item => {
                        return item.PostOffice[0]
                    }))
                })
                .catch((error) => {
                    console.error(error)
                    setError('Please Enter Valid Pincode')
                    setisPin(true)
                })

        }
    }
    const parseData = (data) => {
        const arr = data.map(item => {
            return item.PostOffice[0]



        })

        setCity(arr[0].District)
        setState(arr[0].State)
        console.log('data', arr[0].District)

    }

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const renderModal = () => {
        return (
            <View>
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: '95%', height: '22%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
                                <TouchableOpacity onPress={() => CameraImage()}
                                >
                                    <Image source={AppImage.camera_display}
                                        style={{ width: 50, height: 50, marginRight: 50 }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => multipleImage()}>
                                    <Image source={AppImage.gallery_icon}
                                        style={{ width: 50, height: 50, marginLeft: 50 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    return (
        <View style={{ flex: 1, backgroundColor:'white' }}>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1, paddingBottom: 10 }}>
                <View style={styles.topView}>

                    {filePath.length > 0 ?
                        <ImageSlider
                            loopBothSides
                            autoPlayWithInterval={2000}
                            images={filePath}
                            customSlide={({ index, item, style, width }) => (
                                //console.log('data ',item),
                                // It's important to put style here because it's got offset inside
                                <View key={index} style={{ flex: 1 }}>
                                    <Image source={{ uri: item.path }} style={{ flex: 1, width: width, height: 100 }} />
                                </View>
                            )}
                        />

                        :
                        <Pressable style={{flex:1,alignItems:'center',backgroundColor:'#D7D7FF',justifyContent:'center'}} onPress={()=>toggleModal()}>

                            <Text style={{fontSize:22,opacity:5}}>Please Upload the Image</Text>
                            <Image source={AppImage.camera_display}
                            style={{width:50,height:50,resizeMode:'contain'}}
                            />
                            {/* <View style={{ flexDirection: 'row', width: '100%', height: '57%', }}>
                                <View style={{ width: '33%', height: '100%' }}>
                                    <Image source={!filePath[2] ? AppImage.img3 : { uri: filePath[2].uri }}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View>
                                <View style={{ width: '33%', height: '100%' }}>
                                    <Image source={AppImage.img4}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View>
                                <View style={{ width: '34%', height: '100%' }}>
                                    <Image source={AppImage.img5}
                                        style={{ width: '100%', height: '100%', resizeMode: 'stretch', }}
                                    />
                                </View> 
                            </View>*/}
                        </Pressable>}
                    <View style={styles.topHeader}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', marginLeft: 30, }}>
                                <TouchableOpacity onPress={() => navigation.goBack()}>
                                    <Image source={AppImage.arrow_green}
                                        style={{ width: 16, height: 16, marginTop: 4 }}
                                    />
                                </TouchableOpacity>
                                <Text style={{
                                    textAlign: 'center', marginLeft: 15, color: AppColor.appColor,
                                    fontSize: 16, fontFamily: AppFonts.medium
                                }}>Add Branch</Text>
                            </View>
                            <TouchableOpacity style={{
                                width: 32, height: 32, borderRadius: 16, backgroundColor: 'red',
                                marginRight: 20, alignItems: 'center', justifyContent: 'center'
                            }} onPress={() => toggleModal()}>
                                <Image source={AppImage.camera_icon}
                                    style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={{
                    flex: 1, backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40,
                    borderTopColor: AppColor.appColor, bottom: 30, marginBottom: 50
                }}>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.branchName_icon}
                                style={{ width: 26, height: 18 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Branch Name'
                            placeholderTextColor={AppColor.appColor}
                            value={branchName}
                            onChangeText={(value) => { setBranchName(value) }}

                        />
                    </View>
                    {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {/* <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Name'
                            placeholderTextColor={AppColor.appColor}
                            value={address1}
                            onChangeText={(value)=>{setAddress1(value)}}

                        />
                    </View> */}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Address Line1'
                            placeholderTextColor={AppColor.appColor}
                            value={address1}
                            onChangeText={(value) => { setAddress1(value) }}

                        />
                    </View>
                    {invalid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {/* <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Address Line2'
                            placeholderTextColor={AppColor.appColor}
                            value={address2}
                            onChangeText={(value) => { setAddress2(value) }}
                        />
                    </View> */}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Pincode'
                            placeholderTextColor={AppColor.appColor}
                            value={pincode}
                            onChangeText={(pin) => { setValue(pin), setInValid(!invalid[1]) }}
                        />
                    </View>
                    {invalid[2] && !isPin && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {isPin && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TextInput style={styles.mobileContainer1}
                                placeholder='City'
                                placeholderTextColor={AppColor.appColor}
                                value={city}
                                onChangeText={(value) => { setCity(value) }}
                            />
                            <TextInput style={styles.mobileContainer1}
                                placeholder='State'
                                placeholderTextColor={AppColor.appColor}
                                value={state}
                                onChangeText={(value) => { setState(value) }}
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.web_icon}
                                style={{ width: 25, height: 25 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Website'
                            placeholderTextColor={AppColor.appColor}
                            value={web}
                            onChangeText={(value) => { setWeb(value) }}
                        />
                    </View>
                    {invalid[3] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{ flexDirection: 'row', marginTop: invalid[3] ? 10 : 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.call_green}
                                style={{ width: 25, height: 25, marginRight: 5 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Contact Number'
                            value={contactNumber}
                            maxLength={10}
                            keyboardType='phone-pad'
                            onChangeText={(value) => { setContactNumber(value) }}
                            placeholderTextColor={AppColor.appColor}
                        />
                    </View>
                    {invalid[4] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <TouchableOpacity style={{ justifyContent: 'center', marginRight: 5 }}
                            onPress={() => navigation.navigate('Map')}
                        >
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </TouchableOpacity>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='lat/lon'
                            placeholderTextColor={AppColor.appColor}
                            value={lat}
                            onChangeText={(value) => { setLat(value) }}

                        />

                    </View>
                    {invalid[5] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}
                            onPress={() => navigation.navigate('Map')}
                        >
                            <Image source={AppImage.room_icon}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                    
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Total rooms'
                            placeholderTextColor={AppColor.appColor}
                            value={noOfRooms}
                            keyboardType='numeric'
                            onChangeText={(value) => { setNoOfRooms(value) }}

                        />
                        
                    </View>
                    {invalid[6] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}
                            
                        >
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Razorpay Id'
                            placeholderTextColor={AppColor.appColor}
                            value={razorPayId}
                            onChangeText={(value) => { setRazorPayId(value) }}

                        />
                        
                    </View>
                    {invalid[7] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <Text style={{textAlign:'right',marginRight:10,color:'blue'}} onPress={()=>Linking.openURL(URL)}>Click here to create.</Text>
                    <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}
                            
                        >
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <TextInput
                            style={styles.mobileContainer}
                            placeholder='Razorpay key'
                            placeholderTextColor={AppColor.appColor}
                            value={razorPayKey}
                            onChangeText={(value) => { setRazorPayKey(value) }}

                        />
                        
                    </View>
                    {invalid[8] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <Text style={{ marginLeft: 60, marginTop: 10, fontSize: 16, marginBottom: 5, color:AppColor.appColor }}>Upload Licence (.pdf file)</Text>
                    <View style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'center'}}>
                    <Image source={AppImage.pdf_icom}
                                style={{ width: 30, height: 30,}}
                            />
                        <TouchableOpacity style={{
                            borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center', alignItems: 'center',
                            borderRadius: 10
                        }} onPress={() => selectOneFile()}>
                            <Text style={{ fontSize: 16, margin: 10, color: color }}>{singleFile.name?singleFile.name : 'Browse'}</Text>
                        </TouchableOpacity>
                    </View>
                    {invalid[9] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <TouchableOpacity style={styles.btnContainer}
                        onPress={() => onSavePress()} activeOpacity={0.8}
                    >
                        <Text style={styles.btnTxt}>Save</Text>
                    </TouchableOpacity>
                    {/* <View style={{ width: '100%', height: filePath.length > 0 ? 100 : 180, }} /> */}
                </View>
                {renderModal()}
            </ScrollView>
            {loader &&
                <ImageUploader loading={loader} />}
        </View>

    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: 260,
        //backgroundColor: 'red'
    },
    topHeader: {
        //marginTop: 10,
        //marginLeft: 30,
        // backgroundColor:'yellow',
        position: 'absolute',
        justifyContent: 'center',
        width: '100%',
        height: '22%',
        alignSelf: 'center',
        top: 0
        //marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        //textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        paddingLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        //textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        backgroundColor: AppColor.appColor,
        width: '50%',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 15,
        //marginBottom: 20,
        alignSelf: 'center'
    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 7
    },

})

export default AddBranch;