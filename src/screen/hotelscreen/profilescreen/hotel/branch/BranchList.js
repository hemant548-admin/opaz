import React, { useEffect, useState } from "react";
import { View, StyleSheet, TouchableOpacity, Image, ScrollView, FlatList, Pressable,ActivityIndicator,Text } from "react-native";
// import {
//     Container, Content, Text,
//     Card, CardItem, Left, Right, Fab, Icon
// } from "native-base";

import FastImage from 'react-native-fast-image'
import AppColor from "../../../../../utils/AppColor";
import AppFonts from "../../../../../utils/AppFonts";
import AppImage from "../../../../../utils/AppImage";
import AsyncStorage from "@react-native-community/async-storage";
import APIStrings from "../../../../../webservice/APIStrings";
import { simplePostCall } from "../../../../../webservice/APIServices";
import AppStrings from "../../../../../utils/AppStrings";
import Loader from "../../../../../component/loader/Loader";

const dataArray = [
    { id: "1", BranchName: "Branch Name", BranchCode: '102' },
    { id: "2", BranchName: "Branch Name", BranchCode: '102' },
    { id: "3", BranchName: "Branch Name", BranchCode: '102' },
];

const BranchList = ({ navigation }) => {

    const [loader, setLoader] = useState(false);
    const [roomData, setRoomData] = useState([]);
    const [imgLoader, setImgLoader]=useState(true)

    useEffect(() => {

        const unsubscribe = navigation.addListener('focus', () => {
            branchFetch();
        });

        return unsubscribe;


    }, [navigation])


    const branchFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            hotel_id: id
        });
        console.log(requsetBody, APIStrings.BranchListView)

        simplePostCall(APIStrings.BranchListView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setRoomData(data.branch_list)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)

                }

            }).catch((error) => {
                console.log("Listview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
   const _onLoadEnd = () => {
       setImgLoader(false)
    }
    const renderCard = (item, index) => {
        //console.log("item",item)
        return (
            <Pressable style={styles.card} onPress={() => navigation.navigate('BranchDetail', { id: item.id })}>
                <View style={styles.imageView}>
                    <Image source={{ uri: item.branch_image.length>0 ? item.branch_image[0].image : null }} style={{ flex: 1, resizeMode: 'cover', }}
                        onLoadEnd={_onLoadEnd}
                        
                    />
                    <ActivityIndicator size="small" color={AppColor.appColor} style={{position:'absolute',alignSelf:'center',marginTop:20}}
                    animating={imgLoader}
                    />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '92%', marginTop: 20, marginBottom: 30, alignItems: 'center' }}>
                    <View style={{}}>
                        <Text style={{ fontSize: 18, fontFamily: AppFonts.regular }}>{item.branch_name}</Text>
                        <Text style={{ fontSize: 14, fontFamily: AppFonts.regular, color: AppColor.appColor, marginLeft: 3 }}>Branch code : {item.id}</Text>
                    </View>
                    <Pressable style={{ backgroundColor: item.status === 'verified' ? AppColor.appColor : 'red', borderRadius: 7, alignItems: 'center', justifyContent: 'center', height: 30 }}>
                        <Text style={{ margin: 5, marginLeft: 15, marginRight: 15, color: 'white', fontSize: 14 }}>{item.status === 'verified' ? 'Verified' : 'Not verified'}</Text>
                    </Pressable>
                </View>
            </Pressable>
            // <Content style={{}} contentContainerStyle={{ alignItems: 'center' }} padder>
            //     <Card style={{ width: '99%', borderRadius: 10, marginTop: -10 }}>
            //         {/* <CardItem>
            //         <Left>
            //             <Thumbnail source={{ uri: 'Image URL' }} />
            //             <Body>
            //                 <Text>NativeBase</Text>
            //                 <Text note>GeekyAnts</Text>
            //             </Body>
            //         </Left>
            //     </CardItem> */}
            //     <TouchableOpacity activeOpacity={0.8} style={{backgroundColor:'red'}}
            //     onPress={()=>navigation.navigate('BranchDetail',{id:item.id})}>
            //         <CardItem style={{ borderRadius: 10,}}

            //         >
            //             {/* <View style={{backgroundColor:'red',justifyContent:'center',width:'100%',borderRadius:10}}> */}
            //             <Image source={{uri:item.branch_image?item.branch_image[0].image:null}} style={{ height: 150, width: '100%', flex: 1, borderRadius: 15, marginTop: -5,resizeMode:'cover' }} />
            //             {/* </View> */}
            //         </CardItem>
            //         </TouchableOpacity>
            //         <TouchableOpacity activeOpacity={0.8}
            //         onPress={()=>navigation.navigate('BranchDetail',{id:item.id})}>
            //         <CardItem style={{ borderRadius: 10, marginTop: -10 }}

            //         >
            //             <Left style={{ flexDirection: 'column', }}>

            //                 <Text style={{ alignSelf: 'flex-start' }}>{item.branch_name}</Text>
            //                 <Text style={{
            //                     color: AppColor.appColor, fontSize: 14,
            //                     fontFamily: AppFonts.light, textAlign: 'center', alignSelf: 'flex-start', marginLeft: 20
            //                 }}>{item.id}</Text>

            //             </Left>
            //             <Right>
            //                 <TouchableOpacity style={{ backgroundColor: item.status==='verified' ? AppColor.appColor : 'red', borderRadius: 7 }}>
            //                     <Text style={{ margin: 5, marginLeft: 15, marginRight: 15, color: 'white', fontSize: 14 }}>{item.status==='verified' ? 'Verified' : 'Not verified'}</Text>
            //                 </TouchableOpacity>
            //             </Right>
            //         </CardItem>
            //         </TouchableOpacity>
            //     </Card>
            // </Content>
        )
    }

    return (
        <View flex={1} backgroundColor={'#F2FFFB'}>
            <View style={styles.topHeader}>
                <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={AppImage.arrow_green}
                            style={{ width: 16, height: 16, marginTop: 4 }}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        textAlign: 'center', marginLeft: 15, color: AppColor.appColor,
                        fontSize: 18, fontFamily: AppFonts.medium
                    }}>Branch List</Text>
                </View>
                <TouchableOpacity style={{
                    backgroundColor: 'white', alignItems: 'center', borderWidth: 0.5, borderColor: AppColor.appColor,
                    width: 40, height: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', elevation: 3, marginRight: 15,
                }}
                    onPress={() => navigation.navigate('AddBranch')}>
                    <Text style={{ fontSize: 25, fontFamily: AppFonts.light, bottom: 1, color: AppColor.appColor }}>+</Text>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, marginTop: 15 }}>
                {roomData.length > 0 ?
                    <FlatList
                        data={roomData}
                        contentContainerStyle={{paddingTop:5}}
                        renderItem={({ item, index }) => renderCard(item, index)}
                        keyExtractor={item => item.id}
                    //stickyHeaderIndices={this.state.stickyHeaderIndices}
                    />
                    :
                    <View>
                        <Text style={{ textAlign: 'center', marginTop: '50%', color: 'red' }}>You dont have any branch</Text>
                    </View>}
            </View>
            <Loader loading={loader} />
        </View>
    );
}

const styles = StyleSheet.create({
    topView: {
        //width: '100%',
        //height: '36%',
        backgroundColor: 'red'
    },
    card: {
        width: '95%',
        marginBottom: 15,
        alignSelf: 'center',
        backgroundColor: 'white',
        borderRadius: 15,
        elevation: 5,
        alignItems: 'center'

    },
    topHeader: {
        //marginTop: 10,
        //marginLeft: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        height: '8%',
        alignSelf: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '85%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '42%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    btnContainer: {
        backgroundColor: AppColor.appColor,
        width: '50%',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20,
        alignSelf: 'center'
    },
    btnTxt: {
        color: 'white',
        fontSize: 20,
        margin: 7
    },
    imageView: {
        //backgroundColor: 'red',
        width: '97%',
        height: 150,
        marginTop: 4,
        overflow: 'hidden',
        borderRadius: 15,
        //alignItems:'center',
        justifyContent:'center'
    }

})

export default BranchList;