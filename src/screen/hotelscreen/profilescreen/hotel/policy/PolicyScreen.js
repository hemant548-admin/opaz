import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native";
import {
  Container, Content, Text,
  Picker, Form, Input, Textarea
} from "native-base";
import DocumentPicker from 'react-native-document-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import AppColor from "../../../../../utils/AppColor";
import AppFonts from "../../../../../utils/AppFonts";
import AppImage from "../../../../../utils/AppImage";
import APIStrings from "../../../../../webservice/APIStrings";
import Loader from "../../../../../component/loader/Loader";
import { simplePostCall } from "../../../../../webservice/APIServices";
import AsyncStorage from "@react-native-community/async-storage";
import AppStrings from "../../../../../utils/AppStrings";
import axios from "axios";
import Toast from 'react-native-simple-toast';
import moment from "moment";
import AppStyles from "../../../../../utils/AppStyles";

export default class PolicyScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      branches: [
        { address_line: 'Select Time', id: 1 },
        { address_line: '10', id: 2 },
        { address_line: '11', id: 3 },
        { address_line: '12', id: 3 }
      ],
      branches1: [
        { address_line: 'Select Time', id: 1 },
        { address_line: '4', id: 2 },
        { address_line: '5', id: 3 },
        { address_line: '6', id: 3 }],
      branches2: [
        { address_line: 'Select Document', id: 1 },
        { address_line: 'Aadahr Card', id: 2 },
        { address_line: 'Pan Card', id: 3 },
        { address_line: 'Passport', id: 3 }],
      selected1: 1,
      checkedout: new Date(),
      checkinTime: new Date(),
      documentList: '',
      singleFile: { name: "Browse" },
      cancellation: '',
      modification: '',
      loader: false,
      color: '#add9ab',
      show: false,
      istrue: false,
      showout: false,
      isCheck: false,
      error:'',
      inValid:[false,false,false,false,false]

    };
  }
  onChange = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.checkinTime;
    //setShow(Platform.OS === 'ios');
    this.setState({
      checkinTime: currentDate, show: false,
      istrue: true
    });
  };

  showTimepicker = () => {
    this.setState({ show: true });
  };
  onChekout = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.checkedout;
    //setShow(Platform.OS === 'ios');
    this.setState({
      checkedout: currentDate, showout: false,
      isCheck: true
    });
  };
  onSavePress = async () => {
    if (!this.state.istrue) {
      this.setState({inValid:[true,false,false,false,false],
        error:'required'
      })
      //hotelRef.current.focus();
      return;
    }
    if (!this.state.isCheck) {
      this.setState({inValid:[false,true,false,false,false],
        error:'required'
      })
      return;
    }
    if (this.state.cancellation.trim().length === 0) {
      this.setState({inValid:[false,false,true,false,false],
        error:'required'
      })
      return;
    }
    if (this.state.modification.trim().length === 0) {
      this.setState({inValid:[false,false,false,true,false],
        error:'required'
      })
      return;
    }
    // if (this.state.documentList.trim().length === 0 ) {
    //   this.setState({inValid:[false,false,false,false,true],
    //     error:'required'
    //   })
    //   return;
    // }
    // if(this.state.singleFile.size>3145728) {
    //   this.setState({inValid:[false,false,false,false,true],
    //     error:'file size will not exceeds to more than 3Mb'
    //   })
    //   return;
    // }
    const id = await AsyncStorage.getItem(AppStrings.USER_ID, '')

    this.setState({ loader: true })

   const formData = new FormData();
    formData.append('hotel_id', id);
    formData.append('booking_modification', this.state.modification)
    formData.append('checkin_time', moment(this.state.checkinTime).format('h A'));
    formData.append('checkout_time', moment(this.state.checkedout).format('h A'));
    formData.append('cancellation', this.state.cancellation)
    // formData.append('document_pdf', {

    //   name: this.state.singleFile.name,
    //   type: this.state.singleFile.type,
    //   uri: this.state.singleFile.uri

    // })
    //formData.append('document_list', this.state.documentList)
    // let requsetBody = JSON.stringify({
    //   hotel_id: id,
    //   checkin_time: String(this.state.checkinTime),
    //   checkout_time: String(this.state.checkedout),
    //   cancellation: this.state.cancellation,
    //   booking_modification: this.state.modification,
    //   document_list: this.state.documentList,
    //   document_pdf: this.state.singleFile.uri
    // });

    //AsyncStorage.setItem(AppStrings.ROLE, role,null)
    console.log(APIStrings.BranchTermsAndPolicies);
    console.log(formData);
    axios.post(APIStrings.BranchTermsAndPolicies, formData)
      .then(res => {
        //response
        console.log(res.data)
        // alert(res.data.output.user_message)
        Toast.show(res.data.output.user_message)
        this.setState({ loader: false })
        this.props.navigation.navigate('HomeScreen')
        
      }).catch(err => {
        this.setState({ loader: false })
        console.log(err)
        //error
      })


  }
  selectOneFile = async () => {
    //Opening Document Picker for selection of one file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
        //There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      //Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      console.log('URI : ' + res.uri);
      console.log('Type : ' + res.type);
      console.log('File Name : ' + res.name);
      console.log('File Size : ' + res.size);
      //Setting the state to show single file attributes
      //setSingleFile(res);
      this.setState({ singleFile: res, color: AppColor.appColor })
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        //alert('Canceled from single doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  onBranchSelected(value) {
    this.setState({
      selectedBranch: value
    });
  }

  render() {
    const { navigation } = this.props
    return (
      <Container flex={1}>
        <View style={styles.topHeader}>
          <View style={{ flexDirection: 'row', marginLeft: 20 }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image source={AppImage.back_arrow_white}
                style={{ width: 16, height: 16, marginTop: 4 }}
              />
            </TouchableOpacity>
            <Text style={{
              textAlign: 'center', marginLeft: 15, color: 'white',
              fontSize: 18, fontFamily: AppFonts.medium
            }}>Policy</Text>
          </View>
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: 'white', }} contentContainerStyle={{ flexGrow: 1 }}>
          <Content>
            {this.state.show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={this.state.checkinTime}
                mode={'time'}
                //is24Hour={true}
                display="default"
                onChange={this.onChange}
              />
            )}
            <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Check-in</Text>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.showTimepicker()}
              style={{
                borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center', height: 40,
                borderRadius: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
              }}>
              <Text style={{ marginLeft: 10, color: this.state.istrue ? AppColor.appColor : '#add9ab' }}>{this.state.istrue ? moment(this.state.checkinTime).format('h A') : 'Select Time'}</Text>
              <Image source={AppImage.arrow}
                style={{
                  width: 10, height: 10, resizeMode: 'contain', marginRight: 20,
                  transform: [{ rotate: '90deg' }]
                }}
              />
            </TouchableOpacity>
                {this.state.inValid[0] && <Text style={[AppStyles.apiErrorStyle,{marginRight:20}]}>{this.state.error}</Text>}
          </Content>
          <Content>
            {this.state.showout && (
              <DateTimePicker
                testID="dateTimePicker"
                value={this.state.checkedout}
                mode={'time'}
                //is24Hour={true}
                display="default"
                onChange={this.onChekout}
              />
            )}
            <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Check-out</Text>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.setState({ showout: true })}
              style={{
                borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center', height: 40,
                borderRadius: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
              }}>
              <Text style={{ marginLeft: 10, color: this.state.isCheck ? AppColor.appColor : '#add9ab' }}>{this.state.isCheck ? moment(this.state.checkedout).format('h A') : 'Select Time'}</Text>
              <Image source={AppImage.arrow}
                style={{
                  width: 10, height: 10, resizeMode: 'contain', marginRight: 20,
                  transform: [{ rotate: '90deg' }]
                }}
              />
            </TouchableOpacity>
            {this.state.inValid[1] && <Text style={[AppStyles.apiErrorStyle,{marginRight:20}]}>{this.state.error}</Text>}
          </Content>
          <Content>
            <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Cancellation</Text>
            <Form style={{
              borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center',
              borderRadius: 10
            }}>
              <Textarea
                rowSpan={5}
                style={{ width: '100%', height: 150, textAlignVertical: 'top', paddingLeft: 20, paddingRight: 50, color: AppColor.appColor }}
                placeholder='Write Your Cancellation policy here for customer'
                placeholderTextColor='#add9ab'
                value={this.state.cancellation}
                onChangeText={(value) => this.setState({ cancellation: value })}
              />
            </Form>
            {this.state.inValid[2] && <Text style={[AppStyles.apiErrorStyle,{marginRight:20}]}>{this.state.error}</Text>}
          </Content>
          <Content >
            <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Booking Modification</Text>
            <Form style={{
              borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center',
              borderRadius: 10
            }}>
              <Textarea
                rowSpan={5}
                style={{ width: '100%', height: 150, textAlignVertical: 'top', paddingLeft: 20, paddingRight: 50, color: AppColor.appColor }}
                placeholder='Write Your booking modification policies here for customer'
                placeholderTextColor='#add9ab'
                value={this.state.modification}
                onChangeText={(value) => this.setState({ modification: value })}
              />
            </Form>
            {this.state.inValid[3] && <Text style={[AppStyles.apiErrorStyle,{marginRight:20}]}>{this.state.error}</Text>}
          </Content>
          <Loader loading={this.state.loader} />
          {/* <Content >
            <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Document List</Text>
            <Form style={{
              borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center',
              borderRadius: 10
            }}>
              <Picker
                style={{ width: '95%', height: 40, color: '#add9ab', }}
                iosHeader="Branch"
                Header="Branch"
                mode="dropdown"
                textStyle={{ color: 'grey' }}
                placeholder='Select branch'
                headerBackButtonText='Geri'
                selectedValue={this.state.documentList}
                onValueChange={(value) => this.setState({ documentList: value })}
              >
                {this.state.branches2.map((branches, i) => {
                  return (
                    <Picker.Item label={branches.address_line} value={branches.address_line} key={i} />
                  );
                }
                )}
              </Picker>
            </Form>
            {this.state.inValid[4] && <Text style={[AppStyles.apiErrorStyle,{marginRight:20}]}>{this.state.error}</Text>}
          </Content> */}
          <Content >
            {/* <Text style={{ marginLeft: 40, marginTop: 10, fontSize: 18, marginBottom: 5 }}>Upload PDF</Text>
            <TouchableOpacity style={{
              borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center', alignItems: 'center',
              borderRadius: 10
            }} onPress={() => this.selectOneFile()}>
              <Text style={{ fontSize: 16, margin: 10, color: this.state.color }}>{this.state.singleFile.name !== undefined || this.state.singleFile.name !== null
                || this.state.singleFile.name !== '' ? this.state.singleFile.name : 'Browse'}</Text>
            </TouchableOpacity> */}
          </Content>
          <Content >
            <TouchableOpacity style={styles.btnContainer}
              onPress={() => this.onSavePress()}
            >
              <Text style={styles.btnTxt}>Save</Text>
            </TouchableOpacity>
          </Content>
          <View style={{ width: '100%', height: 20 }} />
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    //width: '100%',
    //height: '36%',
    backgroundColor: 'red'
  },
  topHeader: {
    //marginTop: 10,
    //marginLeft: 30,
    justifyContent: 'center',
    width: '100%',
    height: '8%',
    alignSelf: 'center',
    backgroundColor: AppColor.appColor,
    marginHorizontal: 10,
  },
  changePsd: {
    width: 210,
    height: 150,
    resizeMode: 'contain'
    // bottom:0
  },
  imgContainer: {
    width: 45,
    height: 45,
    borderRadius: 25,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  txt: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    fontFamily: AppFonts.slab,
    marginLeft: 15
  },
  mobileContainer: {
    //backgroundColor:'red',
    //marginTop:50,
    borderRadius: 10,
    borderWidth: 1,
    width: '85%',
    borderColor: AppColor.appColor,
    textAlign: 'center'
  },
  mobileContainer1: {
    //backgroundColor:'red',
    //marginTop:50,
    borderRadius: 10,
    borderWidth: 1,
    width: '42%',
    borderColor: AppColor.appColor,
    textAlign: 'center'
  },
  mobileTxt: {
    //textAlign:'center',
    fontSize: 15,
    margin: 10,
    marginTop: 12,
    color: AppColor.appColor,
    fontFamily: AppFonts.light
  },
  btnContainer: {
    backgroundColor: AppColor.appColor,
    width: '50%',
    alignItems: 'center',
    borderRadius: 20,
    marginTop: 20,
    alignSelf: 'center'
  },
  btnTxt: {
    color: 'white',
    fontSize: 20,
    margin: 7
  }

})