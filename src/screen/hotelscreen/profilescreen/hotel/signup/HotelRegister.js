import React, { useState } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';

import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import APIStrings from '../../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../../webservice/APIServices';
import AppStyles from '../../../../../utils/AppStyles';

const { width, height } = Dimensions.get('screen')

const HotelRegister = ({ navigation }) => {
    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [landMark, setLandMark] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [error, setError] = useState('');
    const [invalid, setInValid] = useState([false, false]);
    const[isPin, setisPin]=useState(false);

    const onContinuePress = () => {
        if (address1.trim().length === 0) {
            setError('Please Enter Address')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (pincode.trim().length === 0 || pincode.trim().length < 6 
        || isNaN(pincode) || isPin) {
            setError('Please Enter valid Pincode')
            setInValid([false, true, false])
            return;
        }

        navigation.navigate('CreatePassword',{
            addr1:address1,
            addr2:address2,
            pin:pincode,
            land:landMark,
            city:city,
            state:state
        })

        // setIsProcessing(true)
        // let requsetBody = JSON.stringify({
        //     address_first:address1 , 
        //     address_second:address2 , 
        //     pincode:pincode , 
        //     land_mark:landMark , 
        //     state:state , 
        //     city:city , 
        //     password: , 
        //     mobile_number , 
        //     hotel_name , 
        //     website
        // });

        // console.log(APIStrings.RegisterHotel);
        // console.log(requsetBody);

        // simplePostCall(APIStrings.RegisterHotel, requsetBody)
        //     .then((data) => {

        //         setIsProcessing(false)
        //         //console.log(data.token);
        //         if (data.status.code === 200) {
        //             console.log("Registration success", JSON.stringify(data));
        //             parseData(data)
        //             alert(data.status.message)



        //         }
        //         else {
        //             //this.setState({ invalid: true, errorMessage: data.message });
        //             console.log(data.status.message)
        //         }

        //     }).catch((error) => {
        //         console.log("login error", error);
        //         setIsProcessing(false)
        //         //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
        //     });


    }
    const setValue = async (pin) => {
        setPincode(pin)
        if(isNaN(pin)){
            console.log("pineee ")
            setError('Please Enter 6 digit Pincode')
            setisPin(true)
            setInValid([false, true, false])
            return;
        }
        setisPin(false)
        
        if (pin.trim().length === 6) {
            // setError('Please Enter valid Pincode')
            // setInValid([false, true, false])
            // return;
            console.log(pin)

            let link = 'https://api.postalpincode.in/pincode/' + `${pin}`;
            console.log(link)
            fetch(link)
                .then((response) => response.json())
                .then((json) => {
                    parseData(json);
                    console.log(json.map(item => {
                        return item.PostOffice[0]
                    }))
                })
                .catch((error) => {console.log("post api error",error),
                setError('Please Enter Valid Pincode')
                setisPin(true)
            })
                
        }
        
    }
    const parseData = (data) => {
        
        const arr = data.map(item => {
            return item.PostOffice[0]



        })

        setCity(arr[0].District)
        setState(arr[0].State)
        console.log('data', arr[0].District)

    }


    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ alignSelf: 'center', }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
                <View style={{ alignSelf: 'center', marginTop: 20, }}>
                    <Image source={AppImage.welcom_icon}
                        style={{ width: 120, height: 30 }} />
                </View>

                <View style={{ marginTop: 40 }}>
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, color: 'white'
                        }}
                        placeholder='Address Line 1'
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        value={address1}
                        onChangeText={(text) => {setAddress1(text),setInValid(!invalid[0]) }}
                    />
                    {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, marginTop: invalid[0] ? 0 : 20, color: 'white'
                        }}
                        placeholder='Address Line 2'
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        value={address2}
                        onChangeText={(text) => setAddress2(text)}
                    />
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, marginTop: 20, color: 'white'
                        }}
                        placeholder='Pincode'
                        value={pincode}
                        maxLength={6}
                        keyboardType='number-pad'
                        onChangeText={(pin) => {setValue(pin), setInValid(!invalid[1]) }}
                        placeholderTextColor={AppColor.inputlight_txtColor}
                    />
                    {invalid[1] && !isPin && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {isPin && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, marginTop: invalid[1] || isPin ? 0 : 20, color: 'white'
                        }}
                        placeholder='Land mark'
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        value={landMark}
                        onChangeText={(text) => setLandMark(text)}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <TextInput
                            style={{
                                width: '35%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                                borderRadius: 5, paddingLeft: 20, marginTop: 20, marginLeft: 20, color: 'white'
                            }}
                            placeholder='City'
                            value={city}
                            onChangeText={(text) => setCity(text)}
                            placeholderTextColor={AppColor.inputlight_txtColor}
                        />
                        <TextInput
                            style={{
                                width: '35%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                                borderRadius: 5, paddingLeft: 20, marginTop: 20, marginRight: 15, color: 'white'
                            }}
                            placeholder='State'
                            value={state}
                            onChangeText={(text) => setState(text)}
                            placeholderTextColor={AppColor.inputlight_txtColor}
                        />
                    </View>
                </View>
                <FabButton title='Continue'
                    onItem={() => onContinuePress()}
                />
                 <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: width, height: height * 0.169, alignSelf: 'center', }}>
                        <ImageBackground source={AppImage.vector_curve3}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 45, }}>
                                <TouchableOpacity style={{ marginRight: 150, borderBottomWidth: 1, borderBottomColor: 'white', }}
                               // onPress={() => navigation.navigate('Otp')}
                                >
                                    <Text style={{ color: 'white', fontSize: 20, marginRight: 5, marginLeft: 5, }}
                                        
                                    >Register</Text>
                                </TouchableOpacity>
                                <Text style={{ color: '#00B580', top: 10, }}
                                onPress={() => navigation.navigate('Login')}
                                >Login</Text>
                            </View>
                        </ImageBackground>

                    </View>
                </View>

            </ScrollView>
        </View>
    )
}

export default HotelRegister;