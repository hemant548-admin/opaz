import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,Platform,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground
} from 'react-native';

import { BarPasswordStrengthDisplay } from 'react-native-password-strength-meter';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import APIStrings from '../../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../../webservice/APIServices';
import Loader from '../../../../../component/loader/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../../utils/AppStrings';
import AppStyles from '../../../../../utils/AppStyles';
import { AuthContext } from '../../../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')

const CreatePassword = ({ navigation, route }) => {
    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [phone, setPhone] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [createPassword, setCreatePassword] = useState('');
    const [reEnterPassword, setReEnterPassword] = useState('');
    const [error, setError] = useState('');
    const [loader, setLoader] = useState(false)
    const [show, setShow]=useState(false);
    const [token, setToken]=useState('');
    const [showRenter, setShowRenter]=useState(false);
    const [invalid, setInValid] = useState([false, false,false]);
    const { signInHotel} = React.useContext(AuthContext);

    useEffect(async () => {

        let phone = await AsyncStorage.getItem(AppStrings.PHONE)
        setPhone(phone)
        const tok=await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN,'');
        setToken(tok);
        // autoSubmitOtpTime value will be set after otp is detected,
        // in that case we have to start auto submit timer
    });
    const onContinuePress = async () => {
        if (createPassword.trim().length === 0 || createPassword.trim().length < 6) {
            setError('Please enter minimum 6 digit password')
            setInValid([true, false, false])
            //hotelRef.current.focus();
            return;
        }
        if (reEnterPassword.trim().length === 0 || reEnterPassword.trim().length < 6) {
            setError('Please enter minimum 6 digit password')
            setInValid([false, true, false])
            return;
        }
        if (createPassword !== reEnterPassword) {
            setError('Please enter same password')
            setInValid([false, false, true])
            return;
        }

        const hotelName=await AsyncStorage.getItem(AppStrings.HOTEL_NAME)
        const weB=await AsyncStorage.getItem(AppStrings.WEB)

        setLoader(true)
        let requsetBody = JSON.stringify({
            address_first: route.params.addr1,
            address_second: route.params.addr2,
            pincode: route.params.pin,
            land_mark: route.params.land,
            state: route.params.state,
            city: route.params.city,
            password: createPassword,
            mobile_number: phone,
            hotel_name:hotelName,
            website: weB,
            deviceToken:token,
            platform:Platform.OS

        });

        console.log(APIStrings.RegisterHotel);
        console.log(requsetBody);

        simplePostCall(APIStrings.RegisterHotel, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("Registration success", JSON.stringify(data));
                    //parseData(data)
                   console.log(data.status.message)
                   parseHotelData(data)
                    //navigation.navigate('hotelbg1')



                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)

                }

            }).catch((error) => {
                console.log("login error", error);
                setLoader(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const parseHotelData = async (data) => {
        console.log("hotel data ", data.hotel_owner[0])
        try {
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, data.hotel_owner[0].hotel_name, +'');
            await AsyncStorage.setItem(AppStrings.WEB, data.hotel_owner[0].website + '');
            await AsyncStorage.setItem(AppStrings.PHONE, data.phone_number + '');
            await AsyncStorage.setItem(AppStrings.USER_ID, data.hotel_owner[0].id + "");
            await AsyncStorage.setItem(AppStrings.PINCODE, data.hotel_owner[0].pincode + "");
            await AsyncStorage.setItem(AppStrings.ADDRESS, data.hotel_owner[0].address_first + '');
            await AsyncStorage.setItem(AppStrings.CITY, data.hotel_owner[0].city + "");
            await AsyncStorage.setItem(AppStrings.STATE, data.hotel_owner[0].state + '');
            await AsyncStorage.setItem(AppStrings.SOCIAL_MEDIA, data.hotel_owner[0].social_media + '');
            await AsyncStorage.setItem(AppStrings.VERIFIED_STATUS, data.hotel_owner[0].verified_status + '');
            await AsyncStorage.setItem(AppStrings.IMAGE, data.hotel_owner[0].hotel_photo + '');
            await AsyncStorage.setItem(AppStrings.ROLE, data.Role + '');
            signInHotel(data.Role);

        } catch (e) {
            console.log(e)
            setLoader(false)
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1, }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ alignSelf: 'center', marginTop: 8, }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
                <View style={{ alignSelf: 'center', marginTop: 20, }}>
                    <Image source={AppImage.welcom_icon}
                        style={{ width: 120, height: 30 }} />
                </View>

                <View style={{ marginTop: 40 }}>
                <View style={{backgroundColor: '#039D70',flexDirection:'row',width: '80%', height: 40,
                    alignSelf: 'center', elevation: 4,marginTop: invalid[0] ? 0 : 20,alignItems:'center',borderRadius: 5,}}>
                    <TextInput
                        style={{
                            width: '80%', height: 40,  
                             paddingLeft: 20, color:'white'
                        }}
                        placeholder='Enter Password'
                        value={createPassword}
                        onChangeText={(text)=>{setCreatePassword(text),setInValid(!invalid[0]) }}
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        secureTextEntry={!show}
                    />
                    <TouchableOpacity style={{alignItems:'center',marginLeft:20,
                }} onPress={()=>setShow(!show)}>
                    <Image source={!show? AppImage.show:AppImage.hide}
                        style={{width:25,height:25}}
                    />
                </TouchableOpacity>
                    </View>
                    <BarPasswordStrengthDisplay
                            password={createPassword}
                            wrapperStyle={{alignSelf:'center'}}
                            minLength= {2}
                            width={width*0.8}
                            levels={ [
                                 
                                {
                                  label: 'Weak',
                                  labelColor: '#ff6900',
                                  activeBarColor: '#ff6900',
                                },
                                {
                                    label: 'Average',
                                    labelColor: '#f3d331',
                                    activeBarColor: '#f3d331',
                                },
                                
                                {
                                  label: 'Strong',
                                  labelColor: '#14eb6e',
                                  activeBarColor: '#14eb6e',
                                },
                               
                              ]
                            }
                            variations={{
                                digits: /\d/,
                                lower: /[a-z]/,
                                upper: /[A-Z]/,
                                nonWords: /\W/g,
                              }}
                        />
                    {invalid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{backgroundColor: '#039D70',flexDirection:'row',width: '80%', height: 40,
                    alignSelf: 'center', elevation: 4,marginTop: invalid[0] ? 0 : 20,alignItems:'center',borderRadius: 5,}}>
                    <TextInput
                        style={{
                            width: '80%', height: 40,  
                             paddingLeft: 20, color:'white'
                        }}
                        placeholder='Re-enter Password'
                        value={reEnterPassword}
                        onChangeText={(text)=>{setReEnterPassword(text),setInValid(!invalid[1]) }}
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        secureTextEntry={!showRenter}
                    />
                    <TouchableOpacity style={{alignItems:'center',marginLeft:20,
                }} onPress={()=>setShowRenter(!showRenter)}>
                    <Image source={!showRenter? AppImage.show:AppImage.hide}
                        style={{width:25,height:25}}
                    />
                </TouchableOpacity>
                    </View>
                    {invalid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    {invalid[2] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                </View>
                <FabButton title='Register'
                    onItem={() => onContinuePress()}
                />
                
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: width, height: height * 0.169, alignSelf: 'center', }}>
                        <ImageBackground source={AppImage.vector_curve3}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 45, }}>
                                <TouchableOpacity style={{ marginRight: 150, borderBottomWidth: 1, borderBottomColor: 'white', }}
                               onPress={() => navigation.navigate('SignUp')}
                                >
                                    <Text style={{ color: 'white', fontSize: 20, marginRight: 5, marginLeft: 5, }}
                                        
                                    >Register</Text>
                                </TouchableOpacity>
                                <Text style={{ color: '#00B580', top: 10, }}
                                onPress={() => navigation.navigate('Login')}
                                >Login</Text>
                            </View>
                        </ImageBackground>

                    </View>
                </View>

            </ScrollView>
            <Loader loading={loader} />
        </View>
    )
}

export default CreatePassword;