import React, { useState, useRef } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput, ScrollView, ImageBackground, Pressable
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import { simplePostCall } from '../../../../../webservice/APIServices';
import APIStrings from '../../../../../webservice/APIStrings';
import Loader from '../../../../../component/loader/Loader';
import AppStyles from '../../../../../utils/AppStyles';
import AppStrings from '../../../../../utils/AppStrings';

const { width, height } = Dimensions.get('screen')

const SignUpScreen = ({ navigation }) => {
    //console.log(height,width)
    const [hotelName, setHotelName] = useState('');
    const [mobile, setMobile] = useState('');
    const [web, setWeb] = useState('');
    const [error, setError] = useState('');
    const [isProcessing, setIsProcessing] = useState(false);
    const [inValid, setInValid] = useState([false, false, false])
    const [countryCode, setCountryCode] = useState('+91');
    const [flag, setFlag] = useState('');
    const hotelRef = useRef(null);
    const mobileRef = useRef(null);

    const onContinuePress = () => {
        if (hotelName.trim().length === 0) {
            setError('Please Enter hotel name')
            setInValid([true, false, false])
            hotelRef.current.focus();
            return;
        }
        if (mobile.trim().length === 0 || mobile.trim().length < 10) {
            setError('Please Enter valid Mobile Number')
            setInValid([false, true, false])
            return;
        }
        setIsProcessing(true)
        let requsetBody = JSON.stringify({
            hotel_name: hotelName,
            website: web,
            mobile: mobile
        });

        console.log(APIStrings.SendHotelOtp);
        console.log(requsetBody);

        simplePostCall(APIStrings.SendHotelOtp, requsetBody)
            .then((data) => {

                setIsProcessing(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("otp success", JSON.stringify(data));
                    parseData(data)
                    //alert(data.status.message)



                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data)
                    setError('Mobile Number is already registered')
                    setInValid([false, false, true])
                }

            }).catch((error) => {
                console.log("login error", error);
                setIsProcessing(false)
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const parseData = async () => {

        try {
            await AsyncStorage.setItem(AppStrings.HOTEL_NAME, hotelName, '');
            await AsyncStorage.setItem(AppStrings.WEB, web, '');
            await AsyncStorage.setItem(AppStrings.PHONE, mobile, '');

            navigation.navigate('Otp')
        } catch (e) {
            //this.setState({ isProcessing: false, invalid: true, errorMessage: AppStrings.something_went_wrong });
            console.log('error', e)
        }

    }

    const onCountrySelect = (country) => {
        // alert(JSON.stringify(country));
        //this.setState({ flag: country.flag, countryCode: '+' + country.countryCode });
        setCountryCode('+' + country.countryCode)
        setFlag(country.flag)
    }

    const onCountryCodePress = () => {
        //const { navigation } = this.props;
        navigation.navigate('CountrySelect', { onCountrySelect: (country) => onCountrySelect(country) });
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View>
                <TouchableOpacity style={{ marginTop: 25, marginLeft: 30 }}
                    onPress={() => navigation.goBack()}
                >
                    <Image source={AppImage.back_arrow}
                        style={{ height: 16, width: 10 }} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ alignSelf: 'center', marginTop: 8, }}>
                    <Image source={AppImage.opaz_logo}
                        style={{ width: 150, height: 150 }} />
                </View>
                <View style={{ alignSelf: 'center', marginTop: 20, }}>
                    <Image source={AppImage.welcom_icon}
                        style={{ width: 120, height: 30 }} />
                </View>

                <View style={{ marginTop: 40 }}>

                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center',
                            paddingLeft: 20, color: 'white', elevation: 4, borderRadius: 5
                        }}
                        placeholder='Hotel Name'
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        value={hotelName}
                        onChangeText={(value) => { setHotelName(value), setInValid(!inValid[0]) }}
                        returnKeyType='next'
                        onSubmitEditing={() => { mobileRef.current.focus(); }}
                        ref={hotelRef}

                    />

                    {inValid[0] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <View style={{
                        flexDirection: 'row', backgroundColor: '#039D70', width: "80%",
                        alignSelf: 'center', borderRadius: 5, elevation: 4, marginTop: inValid[0] ? 0 : 20
                    }}>
                        <TouchableOpacity
                            onPress={() => onCountryCodePress()}>
                            <View
                                style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={{ padding: 10, minWidth: 40, color: 'white' }}>
                                    {countryCode}
                                </Text>
                                <Image style={{ width: 10, height: 10, resizeMode: 'contain', transform: [{ rotate: "90deg" }] }}
                                    source={AppImage.arrow_white} />
                            </View>
                        </TouchableOpacity>
                        <TextInput
                            style={{
                                width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center',
                                borderRadius: 5, paddingLeft: 20, color: 'white'
                            }}
                            placeholder='Mobile'
                            placeholderTextColor={AppColor.inputlight_txtColor}
                            value={mobile}
                            keyboardType='phone-pad'
                            autoCapitalize='none'
                            autoCorrect={false}
                            maxLength={10}
                            onChangeText={(value) => { setMobile(value), setInValid(!inValid[1]) }}
                            //onSubmitEditing={() => { mobileRef.focus(); }}
                            ref={mobileRef}
                        />
                    </View>
                    {inValid[1] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                    <TextInput
                        style={{
                            width: '80%', height: 40, backgroundColor: '#039D70', alignSelf: 'center', elevation: 4,
                            borderRadius: 5, paddingLeft: 20, marginTop: inValid[1] ? 0 : 20, color: 'white'
                        }}
                        placeholder='Website/URL'
                        placeholderTextColor={AppColor.inputlight_txtColor}
                        value={web}
                        onChangeText={(value) => setWeb(value)}
                    />
                </View>
                {inValid[2] && <Text style={AppStyles.apiErrorStyle}>{error}</Text>}
                <FabButton title='Continue'
                    onItem={() => onContinuePress()}
                />

                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: width, height: height * 0.169, alignSelf: 'center', }}>
                        <ImageBackground source={AppImage.vector_curve3}
                            style={{ width: '100%', height: '100%', resizeMode: 'contain', flex: 1 }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 45, }}>
                                <Pressable style={{ marginRight: 150, }}
                                // onPress={() => navigation.navigate('Otp')}
                                >
                                    <Text style={{ color: '#00B580', marginRight: 5, marginLeft: 5, top: 10 }}

                                    >Register</Text>
                                </Pressable>
                                <Pressable style={{ borderBottomWidth: 1, borderColor: 'white', top: 10, }}
                                    onPress={() => navigation.navigate('Login', { role: 'Hotel', screen: '' })}
                                >
                                    <Text style={{ color: 'white', fontSize: 20, }}
                                    >Login</Text>
                                </Pressable>
                            </View>
                        </ImageBackground>

                    </View>
                </View>
            </ScrollView>
            <Loader loading={isProcessing} />
        </View>
    )
}

export default SignUpScreen;