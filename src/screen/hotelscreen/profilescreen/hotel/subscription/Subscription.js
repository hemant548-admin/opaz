import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import { Container } from 'native-base';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import AppStrings from '../../../../../utils/AppStrings';
import AsyncStorage from '@react-native-community/async-storage';
import APIStrings from '../../../../../webservice/APIStrings';
import { simpleGetCall } from '../../../../../webservice/APIServices';
import Loader from '../../../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')
const str = "Lorem Ipsum is simply dummy text of the printing and typesetting industry." +
    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ..."

const arr = [{ id: '1', title: 'Subscription Model 1', description: str },
{ id: '2', title: 'Subscription Model 2', description: str },
{ id: '3', title: 'Subscription Model 3', description: str }
]

const Subscription = ({ navigation }) => {

    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [landMark, setLandMark] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [web, setWeb] = useState('');
    const [hotelName, setHotelName] = useState('');
    const [phone, setPhone] = useState('');
    const [list, setList] = useState([]);
    const [loader, setLoader] = useState(false);
    const [invalid, setInValid] = useState([false, false]);

    useEffect(async () => {

        subscriptionList();
        const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME)
        const phNumber = await AsyncStorage.getItem(AppStrings.PHONE)
        const addr1 = await AsyncStorage.getItem(AppStrings.ADDRESS)
        const pincode = await AsyncStorage.getItem(AppStrings.PINCODE)
        const city = await AsyncStorage.getItem(AppStrings.CITY)
        const state = await AsyncStorage.getItem(AppStrings.STATE)
        const web = await AsyncStorage.getItem(AppStrings.WEB)
        //const S=await AsyncStorage.getItem(AppStrings.PINCODE)
        setHotelName(hotel)
        setPhone(phNumber)
        setAddress1(addr1)
        setPincode(pincode)
        setCity(city)
        setState(state)
        setWeb(web)
        //console.log(addr1)

    }, []);
    const subscriptionList = () => {



        setLoader(true)
        console.log(APIStrings.SubscriptionList);
        //console.log(requsetBody);

        simpleGetCall(APIStrings.SubscriptionList)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setList(data.output)
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });

    }
    const VirtualizedView = (props) => {
        return (
            <FlatList
                data={[]}
                ListEmptyComponent={null}
                keyExtractor={() => "dummy"}
                renderItem={null}
                style={{  }}
                contentContainerStyle={{  flexGrow: 1 }}
                ListHeaderComponent={() => (
                    <React.Fragment>{props.children}</React.Fragment>
                )}
            />
        );
    }
    const _renderItem = (item, index) => {
        return (
            <View style={{
                width: '100%',  backgroundColor: 'white',overflow:'hidden',
                flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', marginBottom: 10
            }}>
                <Image source={AppImage.subscription_img}
                    style={styles.innerImg}
                />
                <Pressable style={styles.card} onPress={() => navigation.navigate('SubscriptionDetail', { detail: item })}>
                    <Text style={styles.subscriptionTxt}>{item.plan_name}</Text>
                    <Text style={styles.innerTxt}>{item.description}</Text>
                </Pressable>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: AppColor.appColor, }}>
            <View style={styles.topHeader}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={AppImage.back_arrow_white}
                            style={{ width: 16, height: 16, marginTop: 4 }}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        textAlign: 'center', marginLeft: 15, color: 'white',
                        fontSize: 16, fontFamily: AppFonts.medium
                    }}>Subscription</Text>
                </View>
            </View>
            <View style={{ flex: 1 }}>
                <VirtualizedView >
                    <View style={styles.topView}>

                        <View style={{ marginTop: 25, marginLeft: 25 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={styles.imgContainer}>
                                    <Image source={AppImage.hotel_icon}
                                        style={{ width: 30, height: 30 }}
                                    />
                                </View>
                                <Text style={styles.txt}>{hotelName}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                                <View style={styles.imgContainer}>
                                    <Image source={AppImage.subscription_img}
                                        style={{ width: 30, height: 30 }}
                                    />
                                </View>
                                <Text style={styles.txt}>Subscription Name</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopColor: AppColor.appColor, }}>
                        <View style={{ flex: 1, marginTop: 32, marginBottom: 20 }}>
                            {list.length > 0 ?
                                <FlatList
                                    data={list}
                                    keyExtractor={item => item.id}
                                    contentContainerStyle={{ paddingBottom: 20 }}
                                    renderItem={({ item, index }) => _renderItem(item, index)}
                                /> :
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                                    <Text style={{ color: 'red' }}>No data available</Text>
                                </View>}
                        </View>
                        {/* <View style={{width:'100%',height:140,backgroundColor:'white',
                marginTop:10,flexDirection:'row',justifyContent:'space-evenly',alignItems:'center'}}>
                   <Image source={AppImage.subscription_img}
                     style={styles.innerImg}
                   />
                    <View style={styles.card}>
                        <Text style={styles.subscriptionTxt}>Subscription Model 2</Text>
                        <Text style={styles.innerTxt}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ...</Text>
                    </View>
                </View>
                <View style={{width:'100%',height:140,backgroundColor:'white',
                marginTop:10,flexDirection:'row',justifyContent:'space-evenly',alignItems:'center'}}>
                   <Image source={AppImage.subscription_img}
                     style={styles.innerImg}
                   />
                    <View style={styles.card}>
                        <Text style={styles.subscriptionTxt}>Subscription Model 3</Text>
                        <Text style={styles.innerTxt}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ...</Text>
                    </View>
                </View> */}

                        {list.length === 0 && <View style={{ height: height - 280 }} />}
                    </View>
                </VirtualizedView>
            </View>
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: 160,
        //backgroundColor: 'red'
    },
    topHeader: {
        //marginTop: 10,
        marginLeft: 30,
        justifyContent: 'center',
        width: '100%',
        height: '9%',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    card: {
        width: '82%',
        //height: 136,
        //backgroundColor:'silver',
        alignItems: 'center',
        borderRadius: 10,
        margin:5,
        borderWidth: 1,
        borderColor: AppColor.appColor
    },
    subscriptionTxt: {
        fontSize: 25,
        margin: 6,

    },
    innerTxt: {
        color: AppColor.appColor,
        margin: 3,
        marginLeft: 7,
        marginRight: 7,
        marginBottom:10

    },
    innerImg: {
        width: 30,
        height: 30,
        marginBottom: 70
    }

})

export default Subscription;