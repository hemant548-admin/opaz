import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import { Container } from 'native-base';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import AppStrings from '../../../../../utils/AppStrings';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';
import { simplePostCall } from '../../../../../webservice/APIServices';
import APIStrings from '../../../../../webservice/APIStrings';
import Loader from '../../../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')

const SubscriptionDetail = ({ navigation,route }) => {

    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [landMark, setLandMark] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [web, setWeb] = useState('');
    const [hotelName, setHotelName] = useState('');
    const [phone, setPhone] = useState('');
    const [isCheck, setIsCheck] = useState(false);
    const [list ,setList]=useState([]);
    const [loader, setLoader] = useState(false);
    const {detail}=route.params;

    useEffect(async () => {

        const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME)
        const phNumber = await AsyncStorage.getItem(AppStrings.PHONE)
        const addr1 = await AsyncStorage.getItem(AppStrings.ADDRESS)
        const pincode = await AsyncStorage.getItem(AppStrings.PINCODE)
        const city = await AsyncStorage.getItem(AppStrings.CITY)
        const state = await AsyncStorage.getItem(AppStrings.STATE)
        const web = await AsyncStorage.getItem(AppStrings.WEB)
        //const S=await AsyncStorage.getItem(AppStrings.PINCODE)
        setHotelName(hotel)
        setPhone(phNumber)
        setAddress1(addr1)
        setPincode(pincode)
        setCity(city)
        setState(state)
        setWeb(web)
        console.log(addr1)

    }, []);

    const createOrder = () => {

    }

    
    const checkBranch= async ()=>{

        const id=await AsyncStorage.getItem(AppStrings.USER_ID)
        let requestBody=JSON.stringify({
            hotel_id:id
        })
        setLoader(true)
        console.log(APIStrings.BranchListForHsubbscription);
        console.log(requestBody);

        simplePostCall(APIStrings.BranchListForHsubbscription, requestBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("success", JSON.stringify(data));
                    setList(data.output)
                    if(data.output.length===0){
                        togglecheckModal()
                    }
                    else{
                        navigation.navigate('Payment',{data:data.output,cost:detail.price,planeId:detail.id})
                    }
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("error ", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });
    }
    const togglecheckModal = () => {
        setIsCheck(!isCheck)
    }
    const checkModal = () => {
        return (
            <View>
                <Modal isVisible={isCheck}
                    onBackdropPress={() => setIsCheck(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: width, height: '50%', backgroundColor: AppColor.appColor, alignItems: 'center', }}>
                            <Text style={{ fontSize: 22, margin: 10, color: 'white', marginTop: 20 }}>Note !</Text>
                            <View style={{ marginLeft: 5, marginRight: 5 }}>
                                <Text style={{ fontSize: 17, color: 'white', margin: 5 }}>If you want to Purchase Subscription then creating a branch is
                                mandatory .
                                </Text>
                                <Text style={{ fontSize: 17, color: 'white', margin: 5 }}>If You don't have branch then you have to create your main hotel as a branch.</Text>
                                <Pressable style={{ width: '40%', backgroundColor: 'white', borderRadius: 5, alignItems: 'center', justifyContent: 'center', alignSelf: 'center', marginTop: '20%' }}
                                onPress={()=>{navigation.navigate('AddBranch'), setIsCheck(false)}}
                                >
                                    <Text style={{ margin: 7, color: AppColor.appColor, fontSize: 17 }}> Add Branch</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    return (
        <View style={{ flex: 1, backgroundColor: AppColor.appColor, }}>
            <View style={styles.topHeader}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={AppImage.back_arrow_white}
                            style={{ width: 16, height: 16, marginTop: 4 }}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        textAlign: 'center', marginLeft: 15, color: 'white',
                        fontSize: 16, fontFamily: AppFonts.medium
                    }}>Subscription</Text>
                </View>
            </View>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.topView}>

                    <View style={{ marginTop: 25, marginLeft: 25 }}>
                        {/* <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.imgContainer}>
                            <Image source={AppImage.hotel_icon}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <Text style={styles.txt}>{hotelName}</Text>
                    </View> */}
                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <View style={styles.imgContainer}>
                                <Image source={AppImage.subscription_img}
                                    style={{ width: 30, height: 30 }}
                                />
                            </View>
                            <Text style={styles.txt}>{detail.plan_name}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopColor: AppColor.appColor, }}>

                    <View style={{
                        width: '90%',
                        marginTop: 30, alignItems: 'center', alignSelf: 'center', borderRadius: 20, borderWidth: 1, borderColor: AppColor.appColor
                    }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 5 }}>
                            <Text style={styles.subscriptionTxt}>Description</Text>
                            <Text style={styles.innerTxt}>{detail.description}</Text>
                        </View>
                    </View>
                    <View style={{
                        width: '90%',
                        marginTop: 30, alignItems: 'center', alignSelf: 'center', borderRadius: 20, borderWidth: 1, borderColor: AppColor.appColor
                    }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 5 }}>
                            <Text style={styles.subscriptionTxt}>Service/Feature</Text>
                            <Text style={styles.innerTxt}>{detail.services}</Text>
                            {/* <Text style={styles.innerTxt}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ...</Text>
                            <Text style={styles.innerTxt}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when ...</Text> */}
                        </View>
                    </View>

                    
                    <View style={{ width: '100%', height: 80 }} />
                </View>
            </ScrollView>
            <View style={{
                width: '100%', height: '10%', backgroundColor: AppColor.appColor
                , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center'
            }}>
                <Text style={{ fontSize: 25, color: 'white' }}>
                    ₹ {detail.price}/yr
                </Text>
                <Pressable style={{ width: '30%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, marginLeft: 30 }}
                    onPress={() => checkBranch()}
                >
                    <Text style={{ margin: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.bold }}>Continue</Text>
                </Pressable>
            </View>
            {checkModal()}
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '15%',
        //backgroundColor: 'red'
    },
    topHeader: {
        //marginTop: 10,
        marginLeft: 30,
        justifyContent: 'center',
        width: '100%',
        height: '9%',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },
    card: {
        width: '82%',
        height: 136,
        //backgroundColor:'silver',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: AppColor.appColor
    },
    subscriptionTxt: {
        fontSize: 25,
        margin: 6,

    },
    innerTxt: {
        color: AppColor.appColor,
        margin: 3,
        marginLeft: 7,
        marginRight: 7,
        fontSize: 15

    },
    innerImg: {
        width: 30,
        height: 30,
        marginBottom: 70
    }

})

export default SubscriptionDetail;