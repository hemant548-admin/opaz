import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet,Pressable
} from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import AppImage from '../../../../../utils/AppImage';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import Header from '../../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import APIStrings from '../../../../../webservice/APIStrings';
import RazorpayCheckout from 'react-native-razorpay';
import Modal from 'react-native-modal';
import { simplePostCall } from '../../../../../webservice/APIServices';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', Branch:'A'
},
{
    id: '2', Branch:'B'
},
{
    id: '3', Branch:'C'
}]
const arr1=arr.map(item=>{
    return{
        id:item.id,
        Branch:item.Branch,
        selected:item.id==='1'?true:null
    }
}
)

const PaymentScreen = ({ navigation,route }) => {

    const [loader, setLoader] = useState(false);
    const [Data, setData] = useState(data);
    const [selected, setSelected]=useState(null);
    const [price, setPrice]=useState(null);
    const [branch, setBranch]=useState([]);
    const [isSuccess, setIsSuccess]=useState(false);
    const [isCancel, setIsCancel]=useState(false);
    const {data,cost,planeId}=route.params; 

    useEffect(()=>{
        const temp=data.map((item,i)=>{
            return{
                id:item.id,
                Branch:item.branch_name,
                selected:i===0?true:null
            }
        }
        )
        const res = temp.map(item => {
            return item.id
        })
        setData(temp);
        setBranch(res)
        setPrice(cost)

    },[])
   const onItemPress=(id)=>{
            //setSelected(id);
            let service = [...Data];
        for (let data of service) {
            if (data.id == id) {
                data.selected = (data.selected == null) ? true : !data.selected;
                break;
            }
        }
        let count=0;
        const res = service.filter(item => item.selected === true);
        //setServicesID()
       let temp = res.map(item => {
            return item.id
        })
        service.forEach((item,i)=>{
            count=item.selected?count+1:count
        })
        setData(service)
        setPrice(cost*count)
        setBranch(temp);
   }
   const checkPayment= async ()=>{

       if(branch.length===0){
           alert('Please select at least one branch')
           return
       }

    //const id=await AsyncStorage.getItem(AppStrings.USER_ID)
    let requestBody=JSON.stringify({
        branch_id:branch,
        subscription_plan_id:planeId,
        subscription_plan_price:price,
    })
    setLoader(true)
    console.log(APIStrings.BuySubscriptionForBranch);
    console.log(requestBody);

    simplePostCall(APIStrings.BuySubscriptionForBranch, requestBody)
        .then((data) => {

            setLoader(false)
            //console.log(data.token);
            if (data.status.code === 200) {
                console.log("success", JSON.stringify(data));
                paymentGetway(data)
                
            }
            else {
                //this.setState({ invalid: true, errorMessage: data.message });
                console.log(data.status.message)

            }

        }).catch((error) => {
            console.log("error ", error);
            setLoader(false)
            alert('api error')
            //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
        });
}
   const paymentGetway = (val) => {
    var options = {
        description: 'This is for Testing',
        image: 'https://i.imgur.com/3g7nmJC.png',
        currency: 'INR',
        key: val.razorpay_public_key, // Your api key
        amount: val.paid_amount,
        order_id: val.order_id,
        name: 'Maharaja Hotel',
        prefill: {
            email: 'void@razorpay.com',
            contact: '9191919191',
            name: 'Razorpay Software'
        },
        theme: { color: AppColor.appColor }
    }
    RazorpayCheckout.open(options).then((data) => {
        // handle success
        //alert(`Success: ${data.razorpay_payment_id}`);
       afterPayment(val.subscription_list_id);
    }).catch((error) => {
        // handle failure
        toggleCancelModal()
        //alert(`Error: ${error.code} | ${error.description}`);
    });
}
const afterPayment = (value) => {

    let requsetBody = JSON.stringify({
        branch_id:branch,
        subscription_id:planeId,
        subscription_list_id:value
    })
    console.log(requsetBody)
    console.log(APIStrings.RazoySubscriptionPaySucess)
    simplePostCall(APIStrings.RazoySubscriptionPaySucess, requsetBody)
        .then((data1) => {

            //setLoader(false)
            //console.log(data.token);
            if (data1.status.code === 200) {
                console.log("Razorpay", JSON.stringify(data1));
                toggleSuccessModal()
                // navigation.navigate('HomeScreen');
               // navigation.navigate('Tab')
                // alert(`Success: ${data.razorpay_payment_id}`);
            }
            else {
                //this.setState({ invalid: true, errorMessage: data.message });
                console.log(data1.status.message)

            }

        })
        .catch((error) => {
            console.log("error ", error);
            // setLoader(false)
            alert('api error')
            //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
        });
}
const toggleCancelModal=()=>{
    setIsCancel(!isCancel);
}
const toggleSuccessModal=()=>{
    setIsSuccess(!isSuccess);
}
const cancelModal = () => {
    return (
        <View>
            <Modal isVisible={isCancel}
                onBackdropPress={() => setIsCancel(false)}
            >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: width - 30, height: '30%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                        <View style={{}}>
                            <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your Transaction is not Completed!"} </Text>
                            {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text> */}
                        </View>
                        <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                            onPress={() => toggleCancelModal()}
                        >
                            <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                        </Pressable>
                        {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                            <Pressable style={{ alignItems: 'center' }}
                            onPress={()=>FetchingData()}
                            >
                                <View style={styles.circle}>
                                    <Image source={AppImage.myselfBooking_icon}
                                        style={styles.modalImg}
                                    />
                                </View>
                                <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                            </Pressable>
                            <View style={{ alignItems: 'center' }}>
                                <Pressable style={styles.circle}
                                    onPress={() => {
                                        navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                    }}
                                >
                                    <Image source={AppImage.otherBooking_icon}
                                        style={styles.modalImg}
                                    />
                                </Pressable>
                                <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                            </View>
                        </View> */}

                    </View>
                </View>
            </Modal>
        </View>
    );
}
const successModal = () => {
    return (
        <View>
            <Modal isVisible={isSuccess}
                onBackdropPress={() => setIsSuccess(false)}
            >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ width: width - 30, height: '50%', backgroundColor: AppColor.appColor, alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                        <View style={{}}>
                            <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Your have purchase subscription\n successfully."} </Text>
                            {/* <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', fontFamily: AppFonts.regular }}>{"Please check in My booking to see\n the details."} </Text> */}
                        </View>
                        <Pressable style={{ width: 100, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderRadius: 20, marginTop: 30 }}
                            onPress={() =>  navigation.navigate('HomeScreen')}
                        >
                            <Text style={{ margin: 5, fontSize: 18, color: AppColor.appColor }}>OK</Text>
                        </Pressable>
                        {/* <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', flex: 1, width: '100%' }}>
                            <Pressable style={{ alignItems: 'center' }}
                            onPress={()=>FetchingData()}
                            >
                                <View style={styles.circle}>
                                    <Image source={AppImage.myselfBooking_icon}
                                        style={styles.modalImg}
                                    />
                                </View>
                                <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Myself</Text>
                            </Pressable>
                            <View style={{ alignItems: 'center' }}>
                                <Pressable style={styles.circle}
                                    onPress={() => {
                                        navigation.navigate('Traveller',{roomId:roomId,branchId:branchId,userId:userId,price:price}), toggleModal()
                                    }}
                                >
                                    <Image source={AppImage.otherBooking_icon}
                                        style={styles.modalImg}
                                    />
                                </Pressable>
                                <Text style={{ color: 'white', fontSize: 18, marginTop: 10 }}>Other</Text>
                            </View>
                        </View> */}

                    </View>
                </View>
            </Modal>
        </View>
    );
}
    const renderList = (item, index) => {
        return (
            <View style={{alignItems:'center',}}>
                <TouchableOpacity style={{width:'95%',height:50,elevation:2,marginTop:5,
                backgroundColor:'white',marginBottom:5,flexDirection:'row',alignItems:'center'}}
                activeOpacity={0.8}
                onPress={()=> onItemPress(item.id)}
                >
                    <View style={{width:18,height:18,borderWidth:1,borderColor:AppColor.appColor,alignItems:'center',justifyContent:'center',
                        borderRadius:9,marginLeft:15}}>
                        <View style={{backgroundColor:item.selected? AppColor.appColor:'white',width:10,height:10,borderRadius:5}}/>
                    </View>
                    <Text style={{fontSize:20,marginLeft:20}}>{item.Branch}</Text>
                </TouchableOpacity>
                
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Payment'
                goBack={() => navigation.goBack()}

            />
            <View>
                <Text style={{marginLeft:10,marginTop:10,fontSize:20}}>Choose Branch for Subscription</Text>
            </View>
            <Content>
            <View style={{ flex: 1, marginBottom: 20,marginTop:10 }}>
              <FlatList 
              data={Data}
              renderItem={({ item, index }) => renderList(item, index)}
                    keyExtractor={item => item.id}
              />
              {/* {selected===0 &&
                  <View style={{backgroundColor:'white',marginTop:20,width:'95%',alignSelf:'center',elevation:3}}>
          <Form style={{marginBottom:10}}>
            <Item floatingLabel >
              <Label>Card Holder Name</Label>
              <Input 
              style={{marginBottom:5}}
              />
            </Item>
            <Item floatingLabel>
              <Label>Card Number</Label>
              <Input 
              style={{marginBottom:5}}
              />
            </Item>
            <View style={{flexDirection:'row',justifyContent:'space-around'}}>
            <Item floatingLabel 
            style={{width:'40%',marginRight:20}}
            >
              <Label>Expiry Date</Label>
              <Input 
              style={{marginBottom:5}}
              />
            </Item>
            <Item floatingLabel  last
            style={{width:'40%'}}
            >
              <Label>CVV</Label>
              <Input 
              style={{marginBottom:5}}
              />
            </Item>
            </View>
          </Form>
          </View>
                } */}
              </View>
              </Content>
              <View style={{
                width: '100%', height: '10%', backgroundColor: AppColor.appColor
                , justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center'
            }}>
                <Text style={{ fontSize: 25, color: 'white' }}>
                    ₹ {price}/yr
                </Text>
                <Pressable style={{ width: '30%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, marginLeft: 30 }}
                    onPress={() => checkPayment()}
                >
                    <Text style={{ margin: 10, fontSize: 18, color: AppColor.appColor, fontFamily: AppFonts.bold }}>Payment</Text>
                </Pressable>
            </View>
            {successModal()}
            {cancelModal()}
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        height: 35,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: AppColor.appColor,
        marginLeft: 10,
        fontSize: 16
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
    },
    innerContainer: {
        //backgroundColor:'red',
        width: '100%',
        marginLeft: 5
    },
    mainTxt: {
        marginLeft: 15,
        fontSize: 18,
        marginBottom: 5,
        color: 'black'
    }

})

export default PaymentScreen;