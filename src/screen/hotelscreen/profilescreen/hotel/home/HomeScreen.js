import React, { useState,useEffect } from 'react';
import {
    View, Text, BackHandler,Alert,
    Dimensions, Image, TouchableOpacity,
     StyleSheet
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import Modal from 'react-native-modal';
import AppImage from '../../../../../utils/AppImage';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import HeaderComponent from '../../../../../component/header/HeaderComponent';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../../utils/AppStrings';

const { width, height } = Dimensions.get('screen')

const HomeScreen = ({ navigation }) => {

    const [isModalVisible, setModalVisible] = useState(false);
    const [hotelName, setHotelName] = useState('Hotel Name');

    useEffect(async() => {

        let hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME)
        setHotelName(hotel)
        // const backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     closeApp
        //   );

        //   return () => backHandler.remove();
       
    },[]);
    
    // const closeApp = () => {

    //     Alert.alert(
    //       'Exit',
    //       'Are you sure you want to exit the App?',
    //       [
    //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //         { text: 'Yes', onPress: () => { BackHandler.exitApp(); } },
    //       ],
    //       { cancelable: false });
    //     return true;
    
    //   }
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };    
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.openDrawer());
    };
    const renderModal=()=> {
        return (
          <View>
            <Modal isVisible={isModalVisible}
            onBackdropPress={()=>setModalVisible(false)}
            >
              <View style={{flex: 1,alignItems:'center',justifyContent:'center'}}>
                  <View style={{width:'95%',height:'50%',backgroundColor:'white',alignItems:'center',borderRadius:20}}>
                <Text style={styles.modalTxt}>Your Verification is pending at Admin level. Please
                        contact admin for furthur Process.
                </Text>
                <TouchableOpacity style={styles.btnContainer}
                onPress={()=>{setModalVisible(false),navigation.navigate('Help')}}>
                    <Text style={styles.btntxt}>Contact Admin</Text>
                </TouchableOpacity>
                <Text style={{color:'red',textDecorationLine:'underline',marginTop:50}}
                onPress={()=>setModalVisible(false)}
                >Close</Text>
                </View>
              </View>
            </Modal>
          </View>
        );
      }

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <HeaderComponent
                title={hotelName}
                profileEnable={true}
                handelDrawer={() => toggleDrawer()}
                profilePress={()=> navigation.navigate('Profile')}
            />
            <View style={{  marginTop: 10,flex:1,paddingBottom:10 }}>
                <View style={styles.Container}>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={()=>navigation.navigate('BranchList')}
                    >
                        <View style={{
                            width: '40%', justifyContent: 'center',
                            height: '75%',
                        }}>
                            <Image source={AppImage.branch}
                                style={styles.img} />
                        </View>
                        <View>
                            <Text style={styles.txt}>Branch</Text>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <Image source={AppImage.arrow_white}
                                style={styles.arrow}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.Container}>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={()=>navigation.navigate('Subscription')}
                    >
                        <View style={{
                            width: '40%', justifyContent: 'center',
                            height: '75%',
                        }}>
                            <Image source={AppImage.subscription}
                                style={styles.img} />
                        </View>
                        <View>
                            <Text style={styles.txt}>Subscription</Text>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <Image source={AppImage.arrow_white}
                                style={styles.arrow}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.Container}>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                    onPress={()=> navigation.navigate('Policy')}
                    >
                        <View style={{
                            width: '40%', justifyContent: 'center',
                            height: '75%',
                        }}>
                            <Image source={AppImage.policy}
                                style={styles.img} />
                        </View>
                        <View>
                            <Text style={styles.txt}>Policy</Text>
                        </View>
                        <View style={{ alignItems: 'center', }}>
                            <Image source={AppImage.arrow_white}
                                style={styles.arrow}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            {renderModal()}
        </View>
    )
}
const styles = StyleSheet.create({
    Container: {
       // width: '85%',
        //height: '28%',
        flex:1,
        margin:15,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: AppColor.appColor
    },
    img: {
        width: '78%',
        height: '78%',
        resizeMode: 'contain',
        marginLeft: 30
    },
    arrow: {
        width: 7,
        height: 18,
        resizeMode: 'contain',
        marginTop: 5
    },
    txt: {
        color: 'white',
        fontSize: 22,
        textAlign: 'center',
        marginRight: 20,
        fontFamily: AppFonts.medium
    },
    modalTxt:{
        fontSize:18,
        marginTop:30,
        marginLeft:20,
        marginRight:20,
        textAlign:'center',
        color:'#ACACAC'
    },
    btnContainer:{
        width:'40%',
        alignItems:'center',
        borderRadius:16,
        marginTop:80,
        backgroundColor:AppColor.appColor
    },
    btntxt:{
        fontSize:14,
        color:'white',
        margin:7
    }
})

export default HomeScreen;