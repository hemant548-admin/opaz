import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import { Container } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../../utils/AppImage';
import FabButton from '../../../../../component/button/FabButton';
import AppColor from '../../../../../utils/AppColor';
import AppFonts from '../../../../../utils/AppFonts';
import AppStrings from '../../../../../utils/AppStrings';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen')

const ProfileScreen = ({ navigation }) => {

    const [address1, setAddress1] = useState('');
    const [address2, setAddress2] = useState('');
    const [pincode, setPincode] = useState('');
    const [landMark, setLandMark] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [web, setWeb] = useState('');
    const [hotelName, setHotelName] = useState('');
    const [phone, setPhone] = useState('');
    const [invalid, setInValid] = useState([false, false]);

    useEffect(async () => {

        const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME)
        const phNumber = await AsyncStorage.getItem(AppStrings.PHONE)
        const addr1 = await AsyncStorage.getItem(AppStrings.ADDRESS)
        const pincode = await AsyncStorage.getItem(AppStrings.PINCODE)
        const city = await AsyncStorage.getItem(AppStrings.CITY)
        const state = await AsyncStorage.getItem(AppStrings.STATE)
        const web = await AsyncStorage.getItem(AppStrings.WEB)
        //const S=await AsyncStorage.getItem(AppStrings.PINCODE)
        setHotelName(hotel)
        setPhone(phNumber)
        setAddress1(addr1)
        setPincode(pincode)
        setCity(city)
        setState(state)
        setWeb(web)
        console.log(addr1)

    }, []);

    return (
        <View style={{ flex: 1, backgroundColor: AppColor.appColor, }}>
            <View style={styles.topHeader}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={AppImage.back_arrow_white}
                            style={{ width: 16, height: 16, marginTop: 4 }}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        textAlign: 'center', marginLeft: 15, color: 'white',
                        fontSize: 16, fontFamily: AppFonts.medium
                    }}>Profile</Text>
                </View>
            </View>

            <View style={styles.topView}>

                <View style={{ marginTop: 25, marginLeft: 25 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.imgContainer}>
                            <Image source={AppImage.hotel_icon}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <Text style={styles.txt}>{hotelName}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                        <View style={styles.imgContainer}>
                            <Image source={AppImage.hotel_contact}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <Text style={styles.txt}>{phone}</Text>
                    </View>
                </View>
            </View>
            <ScrollView style={{ backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopColor: AppColor.appColor, }}>
                <View style={{ flex: 1, backgroundColor: 'white', borderTopLeftRadius: 40, borderTopRightRadius: 40, borderTopColor: AppColor.appColor, }}>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.marker}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={styles.mobileContainer}>
                            <Text style={styles.mobileTxt}> {address1}</Text>
                        </View>
                    </View>
                    {/* <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                    <View style={{ justifyContent: 'center', marginRight: 5 }}>
                        <Image source={null}
                            style={{ width: 30, height: 30 }}
                        />
                    </View>
                    <View style={styles.mobileContainer}>
                        <Text style={styles.mobileTxt}> Adress Line1</Text>
                    </View>
                </View> */}
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={styles.mobileContainer}>
                            <Text style={styles.mobileTxt}> {pincode}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={null}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={styles.mobileContainer1}>
                                <Text style={styles.mobileTxt}> {city}</Text>
                            </View>
                            <View style={styles.mobileContainer1}>
                                <Text style={styles.mobileTxt}> {state}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.web_icon}
                                style={{ width: 25, height: 25 }}
                            />
                        </View>
                        <View style={styles.mobileContainer}>
                            <Text style={styles.mobileTxt}> {web}</Text>
                        </View>
                    </View>
                    {/* <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center', marginRight: 5 }}>
                            <Image source={AppImage.social_media}
                                style={{ width: 30, height: 30 }}
                            />
                        </View>
                        <View style={styles.mobileContainer}>
                            <Text style={styles.mobileTxt}> Social Media</Text>
                        </View>
                    </View> */}
                    <View style={{ width: '100%', height: 80 }} />
                </View>
            </ScrollView>
            <View style={{
                width: '100%', height: '10%', backgroundColor: 'white'
                , justifyContent: 'center', flexDirection: 'row', alignItems: 'center'
            }}>
                <Pressable style={{ alignItems: 'center', }}
                    onPress={() => navigation.navigate('HomeScreen')}
                >
                    <Image source={AppImage.home_icon}
                        style={{ width: 65, height: 65, resizeMode: 'contain' }}
                    />
                </Pressable>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '29%',
        //backgroundColor: 'red'
    },
    topHeader: {
        //marginTop: 10,
        marginLeft: 30,
        justifyContent: 'center',
        width: '100%',
        height: '9%',
        marginHorizontal: 10,
    },
    changePsd: {
        width: 210,
        height: 150,
        resizeMode: 'contain'
        // bottom:0
    },
    imgContainer: {
        width: 45,
        height: 45,
        borderRadius: 25,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txt: {
        fontSize: 20,
        textAlign: 'center',
        color: 'white',
        fontFamily: AppFonts.slab,
        marginLeft: 15
    },
    mobileContainer: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '80%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileContainer1: {
        //backgroundColor:'red',
        //marginTop:50,
        borderRadius: 10,
        borderWidth: 1,
        width: '40%',
        borderColor: AppColor.appColor,
        textAlign: 'center'
    },
    mobileTxt: {
        //textAlign:'center',
        fontSize: 15,
        margin: 10,
        marginTop: 12,
        color: AppColor.appColor,
        fontFamily: AppFonts.light
    },

})

export default ProfileScreen;