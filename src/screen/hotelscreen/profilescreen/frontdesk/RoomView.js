import React, { useEffect, useState } from 'react';
import {
    View, Text, FlatList, BackHandler, Pressable,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import { simplePostCall } from '../../../../webservice/APIServices';
import APIStrings from '../../../../webservice/APIStrings';
import Loader from '../../../../component/loader/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import moment from 'moment';
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';

const { width, height } = Dimensions.get('screen')
const containerHeight = 400;

const arr = [{ id: '1', roomNumber: '201', status: 'Available' },
{ id: '2', roomNumber: '204', status: 'Full' },
{ id: '3', roomNumber: '208', status: 'Available' },
{ id: '4', roomNumber: '203', status: 'Full' },
{ id: '5', roomNumber: '209', status: 'Available' }]

const RoomView = ({ navigation }) => {
    const [loader, setLoader] = useState(false);
    const [roomData, setRoomData] = useState([]);
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [isModalVisible, setisModalVisible] = useState(false);
    const [modalData, setModalData] = useState({});

    useEffect(() => {

        // const unsubscribe = navigation.addListener('focus', () => {
        //     roomFetch();
        //   });
        roomFetch(date);
        // const backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     goBack
        // );


        // return () => {
        //     backHandler.remove()
        // }


    }, [])

    const goBack = () => {
        navigation.goBack()
        return true;
    }

    const roomFetch = async (val) => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: id,
            room_date: moment(val).format('YYYY-MM-DD')
        });
        console.log(requsetBody, APIStrings.RoomView)

        simplePostCall(APIStrings.RoomView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    //const merge3=data.user_booking.concat()
                    const merge3 = [...data.booking_room, ...data.available]
                    console.log('merggg ', merge3);
                    setRoomData(merge3);
                    setModalData(merge3.length? merge3[0]:[]);

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate)
        roomFetch(currentDate);
        //setShow(false)
    };

    const showDatepicker = () => {
        setShow(true);
    };
    const toggleModal = () => {
        setisModalVisible(!isModalVisible);
    }
    const editModal = () => {
        return (

            <View>
                <Modal isVisible={isModalVisible}
                    // animationIn='sl'
                    animationInTiming={500}
                    animationOutTiming={500}
                // onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <View style={{
                            width: width, backgroundColor: 'white', alignSelf: 'center', top: 20, height: 310,
                            borderTopLeftRadius: 20, borderTopRightRadius: 20, paddingBottom: 40
                        }}
                        >
                            <Pressable style={{ alignSelf: 'flex-end', margin: 20, marginBottom: 0 }} onPress={() => toggleModal()}>
                                <Image source={AppImage.cross_icon}
                                    style={{ width: 30, height: 30, }}
                                />
                            </Pressable>
                            <View style={[styles.card, { marginTop: 5 }]}>
                                <View style={{
                                    width: '100%', alignItems: 'center'
                                    , justifyContent: 'center', marginTop: 5, marginBottom: 5
                                }}>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.room_icon}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerCard}>
                                            <Text style={styles.innerTxt}>Room: {modalData.room_no}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.calenderBooking}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerCard}>
                                            <Text style={styles.innerTxt}>Check In -
                                                <Text style={{ color: AppColor.appColor }}> {modalData.room_status !== 'Full' ? '-' : modalData.from_booking_date}</Text></Text>
                                        </View>
                                    </View>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.calenderChecking}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerCard}>
                                            <Text style={styles.innerTxt}>Check Out -
                                                <Text style={{ color: AppColor.appColor }}> {modalData.room_status !== 'Full' ? '-' : modalData.to_booking_date}</Text></Text>
                                        </View>
                                    </View>
                                    <View style={[styles.innerStyles]}>
                                        <Image source={modalData.room_status === 'Available' ? AppImage.roomAvailable :
                                            AppImage.roomFull}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerCard}>
                                            <Text style={styles.innerTxt}>Room status-
                                                <Text style={{ color: modalData.room_status === 'Available' ? AppColor.appColor : 'red' }}> {modalData.room_status}</Text></Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                        </View>

                    </View>
                </Modal>
            </View>

        )

    }
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    width: '100%', alignItems: 'center'
                    , justifyContent: 'center', marginTop: 5, marginBottom: 5
                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Room: {item.room_no}</Text>
                        </View>
                    </View>
                    {/* <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderBooking}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Check In -
                            <Text style={{ color:  AppColor.appColor }}> {item.room_status !== 'Full'?'-': item.from_booking_date}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderChecking}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Check Out -
                            <Text style={{ color:  AppColor.appColor }}> {item.room_status !== 'Full'?'-': item.to_booking_date}</Text></Text>
                        </View>
                    </View> */}
                    <View style={[styles.innerStyles, { marginLeft: 15 }]}>
                        <Image source={item.room_status === 'Available' ? AppImage.roomAvailable :
                            AppImage.roomFull}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Room status-
                                <Text style={{ color: item.room_status === 'Available' ? AppColor.appColor : 'red' }}> {item.room_status}</Text></Text>
                        </View>
                        <Pressable onPress={() => { toggleModal(), setModalData(item) }}>
                            <Image source={AppImage.info_icon}
                                style={{ width: 20, height: 20, resizeMode: 'contain', marginLeft: 10 }}
                            />
                        </Pressable>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Room'
                goBack={() => navigation.goBack()}

            />
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
                <Text style={{ fontSize: 16, marginRight: 5 }}>Filter By date</Text>
                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => showDatepicker()}>
                    <Image source={AppImage.calenderBooking}
                        style={{ width: 30, height: 30, resizeMode: 'contain' }}
                    />
                    <Text style={{ marginLeft: 10, color: AppColor.appColor, textDecorationLine: 'underline', fontSize: 16 }}>{moment(date).format('DD/MM/YYYY')}</Text>
                </Pressable>
            </View>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
            <View style={{ flex: 1, marginBottom: 20 }}>

                {roomData.length > 0 ?
                    <FlatList
                        data={roomData}
                        renderItem={({ item, index }) => renderList(item, index)}
                        keyExtractor={item => item.id}
                    />
                    :
                    <View>
                        <Text style={{
                            color: 'red', textAlign: 'center'
                            , marginTop: '50%', fontSize: 20
                        }}> You don't have any Data</Text>
                    </View>}
            </View>
            <Loader loading={loader} />
            {roomData.length > 0 && editModal()}
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '95%',
        //height: 140,
        marginBottom: 5,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        // justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 10,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: '#72a16f',
        marginLeft: 10,
        fontSize: 16,
        margin: 7
    },
    img: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
        alignItems: 'center'
    }

})

export default RoomView;