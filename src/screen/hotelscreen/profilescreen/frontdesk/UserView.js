import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,
    Dimensions, Image, TouchableOpacity, Pressable,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import APIStrings from '../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import Loader from '../../../../component/loader/Loader';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', status: 'Verified', person: 'Abc',
    check_in: '12 AM', check_out: '8 PM', bookingDate: '20/04/2021'
},
{
    id: '2', roomNumber: '420', status: 'Not verified', person: 'xyz',
    check_in: '10 AM', check_out: '6 PM', bookingDate: '22/04/2021'
},
{
    id: '3', roomNumber: '401', status: 'Verified', person: 'klm',
    check_in: '11 AM', check_out: '9 PM', bookingDate: '21/04/2021'
}]

const UserView = ({ navigation }) => {

    const [loader,setLoader]=useState(false);
    const [roomData, setRoomData]=useState([]);
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);

useEffect(()=>{
    roomFetch(date)
  
},[date])

const goBack=()=>{
    navigation.goBack();
    return true;
}

    const roomFetch= async(val)=>{

        const id=await AsyncStorage.getItem(AppStrings.USER_ID,"")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id:id,
            start_date:moment(val).format('YYYY-MM-DD')
        });
        console.log(requsetBody, APIStrings.UserBookingView)
        
        simplePostCall(APIStrings.UserBookingView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setRoomData(data.user_booking)
                    
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Usermview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate)
        roomFetch(currentDate);
        //setShow(false)
    };

    const showDatepicker = () => {
        setShow(true);
    };
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 12, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Room:
                            <Text style={{ color: AppColor.appColor }}> {item.room_no.room_no}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.branchName_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Name: <Text style={{ color: AppColor.appColor }}>{item.person_name}
                            </Text></Text>
                        </View>
                    </View>
                    {item.check_in &&
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderChecking}
                            style={styles.img}
                        />
                        <View style={styles.innerCard1}>
                            <Text style={styles.innerTxt}>Check-in:
                            <Text style={{ color: AppColor.appColor }}>{moment(item.from_booking_date+" "+item.check_in).format('h:mm a')}</Text></Text>
                        </View>
                        <View style={styles.innerCard1}>
                            <Text style={styles.innerTxt}>Check-out:
                            <Text style={{ color: AppColor.appColor }}>{moment(item.to_booking_date+" "+item.check_out).format('h:mm a')}</Text></Text>
                        </View>
                    </View>
                        }
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderBooking}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                        <Text style={styles.innerTxt}>Booking Date:
                            <Text style={[styles.innerTxt, { color: AppColor.appColor }]}> {moment(item.from_booking_date).format('DD-MM-YYYY')} - {moment(item.to_booking_date).format('DD-MM-YYYY')}</Text>
                            </Text>
                        </View>
                    </View>
                    {/* <View style={styles.innerStyles}>
                        <Image source={AppImage.verified_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={[styles.innerTxt, { color: item.booking_status === 'Unverified' ?  'red': AppColor.appColor }]}>
                                {item.booking_status === 'Unverified' ? 'Not verified':'Verified'}</Text>
                        </View>
                    </View> */}

                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='User'
                goBack={() => navigation.goBack()}

            />
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
                <Text style={{ fontSize: 16, marginRight: 5 }}>Filter By date</Text>
                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => showDatepicker()}>
                    <Image source={AppImage.calenderBooking}
                        style={{ width: 30, height: 30, resizeMode: 'contain' }}
                    />
                    <Text style={{ marginLeft: 10, color: AppColor.appColor, textDecorationLine: 'underline', fontSize: 16 }}>{moment(date).format('DD/MM/YYYY')}</Text>
                </Pressable>
            </View>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
            <Text style={{ marginLeft: 10, marginTop: 10, color: AppColor.appColor, fontSize: 15 }}
            >View details of your customer and their booking details.</Text>
            <View style={{ flex: 1, marginBottom: 20 }}>
               {roomData.length>0 ?
                <FlatList
                    data={roomData}
                    renderItem={({ item, index }) => renderList(item, index)}
                    keyExtractor={item => item.id}
                />
                :
                <View>
                        <Text style={{
                            color: 'red', textAlign: 'center'
                            , marginTop: '50%', fontSize: 20
                        }}> You don't have any Data</Text>
                    </View>}
            </View>
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '40%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: '#72a16f',
        marginLeft: 10,
        fontSize: 16,
        margin:7
    },
    img: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8
    }

})

export default UserView;