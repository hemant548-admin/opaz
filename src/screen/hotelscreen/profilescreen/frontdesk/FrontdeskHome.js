import React, { useEffect, useState } from 'react';
import {
    View, Text, FlatList,BackHandler,Alert,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import { DrawerActions,StackActions,useRoute } from '@react-navigation/native';
//import { StackActions } from '@react-navigation/native';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../utils/AppImage';
import FabButton from '../../../../component/button/FabButton';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import HeaderComponent from '../../../../component/header/HeaderComponent';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';

const { width, height } = Dimensions.get('screen')

const FrontdeskHome = ({ navigation,route }) => {

    const [isModalVisible, setModalVisible] = useState(false);
    const [hotelName, setHotelName] = useState('');
    const [branchName, setBranchName] = useState('');
    const [address, setAddress] = useState('')
    const screen=useRoute();

    useEffect(() => {
        
       
          console.log('nav ',screen.name)
        const data = async () => {

            const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME, "")
            const branch = await AsyncStorage.getItem(AppStrings.BRANCH_NAME, "")
            const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, "")
            setHotelName(hotel);
            setBranchName(branch);
            setAddress(addr);
            console.log(hotel, branch, addr)
        }
        data();
        // const backHandler = BackHandler.addEventListener(
        //     "hardwareBackPress",
        //     closeApp
        //   );
       
        // return () => backHandler.remove()
    }, [])

    // useEffect(()=>{
    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         closeApp
    //       );

    //       return () => backHandler.remove();
    // },[closeApp])

    const closeApp = () => {

        Alert.alert(
          'Exit',
          'Are you sure you want to exit the App?',
          [
            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'Yes', onPress: () => { navigation.dispatch(   
                StackActions.replace('FrontDesk') 
            ); } },
          ],
          { cancelable: false });
          if(screen.name==='FrontDesk'){
            return true;
          }
          else return false;
    
      }

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.openDrawer());
    };


    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <HeaderComponent
                title='Home'
                handelDrawer={() => toggleDrawer()}
                prfileEnable={false}
            />

            <View style={{ flex: 1 }}>
                <View style={styles.card}>
                    <View style={styles.Container}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.frontHotel_icon}
                                style={styles.img}
                            />
                            <Text style={styles.hotelName}>{hotelName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.front_hotel}
                                style={styles.img}
                            />
                            <Text style={{ fontSize: 17, color: AppColor.appColor, fontFamily: AppFonts.light, marginLeft: 10 }}>{branchName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.marker}
                                style={styles.img}
                            />
                            <Text style={{ fontSize: 17, color: AppColor.appColor, fontFamily: AppFonts.thin, marginLeft: 10 }}>{address}</Text>
                        </View>
                    </View>
                </View>
                <View>

                    <Text style={{ fontSize: 16, margin: 10, marginLeft: 30, color: '#003626' }}>Choose from below to see the details !</Text>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('RoomView')}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.roomHome_icon}
                                    style={{ width: 80, height: 80, resizeMode: 'contain', }}
                                />
                                <Text style={{ fontSize: 22, color: 'white', marginTop: 5, marginLeft: 5 }}>ROOM</Text>
                            </View>
                            <Image source={AppImage.arrow_white}
                                style={{ width: 22, height: 22, resizeMode: 'contain', marginLeft: 60 }}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('UserView')}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.frontUser}
                                    style={{ width: 70, height: 70, resizeMode: 'contain', }}
                                />
                                <Text style={{ fontSize: 22, color: 'white', marginTop: 5, marginLeft: 5 }}>USER</Text>
                            </View>
                            <Image source={AppImage.arrow_white}
                                style={{ width: 22, height: 22, resizeMode: 'contain', marginLeft: 60 }}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '90%',
        height: '25%',
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 20,
        borderColor: AppColor.appColor,
        borderWidth: 0.5,
        shadowColor: AppColor.appColor,
        //justifyContent: 'space-evenly',
        backgroundColor: 'white',
        alignItems: 'center',
        elevation: 3,
        alignSelf: 'center',
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    hotelName: {
        fontSize: 20,
        color: AppColor.appColor,
        marginLeft: 10
    },
    greenBox: {
        width: '90%',
        height: '34%',
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: AppColor.appColor,
        alignSelf: 'center',
    }

})

export default FrontdeskHome;