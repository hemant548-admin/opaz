import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import Loader from '../../../../component/loader/Loader';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import APIStrings from '../../../../webservice/APIStrings';
import { multipartPostCall, simplePostCall } from '../../../../webservice/APIServices';
import Toast from 'react-native-simple-toast';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', status: 'Verified', person: 'Abc',
    check_in: '12 AM', check_out: '8 PM', bookingDate: '20/04/2021'
},
{
    id: '2', roomNumber: '420', status: 'Not verified', person: 'xyz',
    check_in: '10 AM', check_out: '6 PM', bookingDate: '22/04/2021'
},
{
    id: '3', roomNumber: '401', status: 'Verified', person: 'klm',
    check_in: '11 AM', check_out: '9 PM', bookingDate: '21/04/2021'
}]

const EmployeeProfile = ({ navigation }) => {
    const [empName, setempName] = useState('');
    const [hotelName, setHotelName] = useState('');
    const [branchName, setBranchName] = useState('');
    const [address, setAddress] = useState('');
    const [mobile, setMobile] = useState('');
    const [gender, setGender] = useState('');
    const [email, setEmail] = useState('');
    const [dob, setDob] = useState('');
    const [age, setAge] = useState('');
    const [anniversary, setAnniversary] = useState('');
    const [city, setCity] = useState('');
    const [stat, setState] = useState('');
    const [pincode, setPincode] = useState('');
    const [empid, setEmpid] = useState('');
    const [img, setimg] = useState(null);
    const [loader, setLoader] = useState(true);
    const [task, setTask] = useState(0);
    const [editable, setEditable] = useState(false);
    const [error, setError] = useState('');
    const [valid, setValid] = useState(false);
    const [isModalVisible, setModalVisible] = useState(false);
    const [filePath, setFilePath] = useState([]);
    const [gid, setGid]=useState('');
    const [gidName, setGidName]=useState('');
    const [country, setCountry]=useState('');
    const [landMark, setLandMark]=useState('');

    const onTaskPress = (id) => {
        setTask(id);
    }

    useEffect(() => {
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            goBack
          );
        const unsubscribe = navigation.addListener('focus', () => {
            profileFetch();
        });
        profileFetch();

        return () => {backHandler.remove()
        unsubscribe;
        }
        // const data = async () => {

        //     const name = await AsyncStorage.getItem(AppStrings.EMPLOYEE_NAME, "")
        //     const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME, "")
        //     const branch = await AsyncStorage.getItem(AppStrings.BRANCH_NAME, "")
        //     const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, "")
        //     const mob = await AsyncStorage.getItem(AppStrings.PHONE, "");
        //     const gend = await AsyncStorage.getItem(AppStrings.Gender, "")
        //     const eml = await AsyncStorage.getItem(AppStrings.EMAIL, "")
        //     const dob = await AsyncStorage.getItem(AppStrings.DOB, "")
        //     const age = await AsyncStorage.getItem(AppStrings.AGE, "")
        //     const anni = await AsyncStorage.getItem(AppStrings.ANNIVERSARY_DATE, "")
        //     const city = await AsyncStorage.getItem(AppStrings.CITY, "")
        //     const state = await AsyncStorage.getItem(AppStrings.STATE, "")
        //     const pincode = await AsyncStorage.getItem(AppStrings.PINCODE, "")
        //     const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        //     const image = await AsyncStorage.getItem(AppStrings.IMAGE, "")
        //     setHotelName(hotel);
        //     setBranchName(branch);
        //     setAddress(addr);
        //     setMobile(mob);
        //     setGender(gend);
        //     setEmail(eml)
        //     setDob(dob)
        //     setAge(age)
        //     setAnniversary(anni)
        //     setCity(city)
        //     setState(state)
        //     setPincode(pincode)
        //     setEmpid(id);
        //     setimg(image);
        //     setempName(name)
        //     //console.log(hotel, branch, gender)
        // }
       

    }, [navigation])
    const goBack=()=>{
        navigation.goBack()
        return true;
    }

    const multipleImage = () => {

        ImagePicker.openPicker({
            includeBase64: false,

        }).then(images => {
            console.log("multiple pick imges", images);

            setFilePath(images)
            setimg(images.path)
            toggleModal()
            //console.log("img ",img)
        });
    }
    const CameraImage = () => {
        ImagePicker.openCamera({

            cropping: true,
        }).then(image => {
            console.log(image);
            //file.push(image)
            setFilePath(image)
            setimg(image.path)
            toggleModal()
            //console.log("im
            //console.log(file)
        });
    }
    const employeeUpdate = () => {
        if (address.trim().length === 0) {
            setValid
            setError('Required')

        }
        setLoader(true);
       const formData = new FormData();
        formData.append('employee_id', empid);
        formData.append('profile_pic', {

            name: 'image.jpg',
            type: filePath.mime,
            uri: filePath.path

        })
        formData.append('address', address)

        console.log(APIStrings.UpdateEmployeeProfile);
        console.log(formData);
        // multipartPostCall(APIStrings.EmployeeProfileView, formData)
        //     .then((data) => {

        //         setLoader(false)
        //         //console.log(data.token);
        //         if (data.status.code === 200) {
        //             console.log("prifle success", JSON.stringify(data));
        //             //setempName(data.Employee_Task.emp_user.first_name);
           
        //         }
        //         else {
        //             //this.setState({ invalid: true, errorMessage: data.message });
        //             console.log(data.status.message)

        //         }

        //     }).catch((error) => {
        //         console.log("Roomview error", error);
        //         setLoader(false)
        //         alert('api error')
        //         //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
        //     });
        axios.post(APIStrings.UpdateEmployeeProfile, formData)
            .then(res => {
                //response
                console.log(res.data)
                alert(res.data.Message)
                profileFetch();
                setEditable(false)
               setLoader(false)
               Toast.show('Profile Updated Successfully !')
            }).catch(err => {
                setLoader(false)
                console.log(err)
                //error
            })

    }
    const profileFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            employee_id: id
        });
        console.log(requsetBody, APIStrings.EmployeeProfileView)

        simplePostCall(APIStrings.EmployeeProfileView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("prifle success", JSON.stringify(data));
                    setempName(data.Employee_Task.emp_user.first_name);
            //setBranchName(branch);
            setAddress(data.Employee_Task.address);
            setMobile(data.Employee_Task.emp_user.phone_number);
            setGender(data.Employee_Task.gender);
            setEmail(data.Employee_Task.email)
            setDob(data.Employee_Task.date_of_birth)
            setAge(data.Employee_Task.age)
            setAnniversary(data.Employee_Task.anniversary_date)
            setCity(data.Employee_Task.city)
            setState(data.Employee_Task.state)
            setPincode(data.Employee_Task.pincode)
            setEmpid(data.Employee_Task.id);
            setimg(data.Employee_Task.emp_user.profile_pic);
            setBranchName(data.Employee_Task.branch.branch_name);
            setGidName(data.Employee_Task.id_proof_name);
            setGid(data.Employee_Task.id_proof_number);
            setCountry(data.Employee_Task.country);
            setHotelName(data.hotel_name);
            setLandMark(data.Employee_Task.landmard)
            //setempName(data.Employee_Task)
                    //setopenTask(data.Employee_Task)
                    //setTaskId(data.Employee_Task[0].id)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };
    const renderModal = () => {
        return (
            <View>
                <Modal isVisible={isModalVisible}
                    onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ width: '95%', height: '22%', backgroundColor: 'white', alignItems: 'center', borderRadius: 20, justifyContent: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
                                <TouchableOpacity onPress={() => CameraImage()}
                                >
                                    <Image source={AppImage.camera_display}
                                        style={{ width: 50, height: 50, marginRight: 50 }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => multipleImage()}>
                                    <Image source={AppImage.gallery_icon}
                                        style={{ width: 50, height: 50, marginLeft: 50 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Profile'
                goBack={() => navigation.goBack()}
                edit={!editable}
                editPress={() => setEditable(true)}

            />
            <View style={styles.topView}>
                <View>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <Pressable
                            onPress={() => toggleModal()}
                            disabled={!editable}
                        >
                            <Image source={{ uri: img }}
                                style={{
                                    width: 100, height: 100, borderRadius: 10,
                                    backgroundColor: 'silver',
                                }}
                            />
                           {editable && <Image source={AppImage.edit_green}
                                style={{ width: 30, height: 30, position: 'absolute', bottom: 0, right: 0, resizeMode: 'contain' }}
                            />
                            }
                        </Pressable>
                        <View style={{ marginLeft: 10 }}>
                            <Text style={{ color: 'white', fontSize: 17, fontFamily: AppFonts.medium }}>{empName}</Text>
                            <Text style={{ color: 'white' }}>{mobile}</Text>
                            <Text style={{ color: 'white' }}>{gender}</Text>
                            <Text style={{ color: 'white' }}>{email}</Text>
                        </View>
                    </View>
                    <View style={styles.circularGreen}>
                        <View style={{ marginTop: 5, marginBottom: 5 }}>
                            <Text style={styles.txt}>Birth date</Text>
                            <Text style={styles.darktxt}>{dob}</Text>
                        </View>
                        <View style={styles.line} />
                        <View style={{ marginTop: 5, marginBottom: 5 }}>
                            <Text style={styles.txt}>Age</Text>
                            <Text style={styles.darktxt}>{age}</Text>
                        </View>
                        <View style={styles.line} />
                        <View style={{ marginTop: 5, marginBottom: 5 }}>
                            <Text style={styles.txt}>Anniversary</Text>
                            <Text style={styles.darktxt}>{anniversary}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 20, color: AppColor.appColor, margin: 10, marginLeft: 15 }}>Personal</Text>
                    <Text style={styles.titleName}>Address :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Address1'
                            value={address}
                            onChangeText={(val)=>setAddress(val)}
                            editable={editable}
                        />
                    </View>
                    {/* <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Address2'
                            value={address2}
                            editable={editable}
                        />
                    </View> */}
                    <Text style={{fontSize:18,color:AppColor.appColor,marginLeft:20,marginTop:10}}>For Office use only</Text>
                    <Text style={styles.titleName}>Landmark :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Landmark'
                            value={landMark}
                            editable={false}
                            
                        />
                    </View>
                    <Text style={styles.titleName}>Pincode :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Pincode'
                            value={pincode}
                            editable={false}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <View style={styles.inputContainer1}>
                            <TextInput
                                style={styles.inputbox}
                                placeholder='City'
                                value={city}
                                editable={false}
                            />
                        </View>
                        <View style={styles.inputContainer1}>
                            <TextInput
                                style={styles.inputbox}
                                placeholder='State'
                                value={stat}
                                editable={false}
                            />
                        </View>
                        <View style={styles.inputContainer1}>
                            <TextInput
                                style={styles.inputbox}
                                placeholder='Country'
                                value={country}
                                editable={false}
                            />
                        </View>
                    </View>
                    <Text style={styles.titleName}>ID proof name :</Text>
                    <View style={styles.inputContainer}> 
                        <TextInput
                            style={styles.inputbox}
                            placeholder='ID proof name'
                            value={gidName}
                            editable={false}
                        />
                    </View>
                    <Text style={styles.titleName}>ID proof number :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='ID proof number'
                            value={gid}
                            editable={false}
                        />
                    </View>
                    <TouchableOpacity activeOpacity={0.8}
                        style={[styles.inputContainer, {
                            backgroundColor: AppColor.appColor, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-between'
                        }]} onPress={()=>navigation.navigate('EmpChangePassword1')}>
                        <Text style={{ margin: 10, textAlign: 'left', marginLeft: 20, color: 'white' }}>Change Password</Text>
                        <Image source={AppImage.arrow_right}
                            style={{ width: 15, height: 15, resizeMode: 'contain', marginRight: 20 }}
                        />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 20, color: AppColor.appColor, margin: 10, marginLeft: 20 }}>Hotel</Text>
                    <Text style={styles.titleName}>Hotel name :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Hotel ID'
                            value={hotelName}
                            editable={false}
                        />
                    </View>
                    <Text style={styles.titleName}>Branch name :</Text>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Branch ID'
                            value={branchName}
                            editable={false}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputbox}
                            placeholder='Employee ID'
                            value={"Employee ID"+" "+empid}
                            editable={false}

                        />
                    </View>
                    {editable &&
                        <TouchableOpacity style={{
                            width: '40%', backgroundColor: AppColor.appColor
                            , alignItems: 'center', alignSelf: 'center', borderRadius: 10, marginBottom: 10
                        }}
                        onPress={()=>employeeUpdate()}
                        >
                            <Text style={{ margin: 10, color: 'white' }}>Save</Text>
                        </TouchableOpacity>
                    }
                </View>
            </ScrollView>
            {renderModal()}
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({
    topView: {
        width: '100%',
        height: '30%',
        backgroundColor: AppColor.appColor
    },
    circularGreen: {
        flexDirection: 'row',
        width: '95%',
        // backgroundColor:'red',
        alignSelf: 'center',
        borderRadius: 25,
        justifyContent: 'space-around',
        elevation: 4,
        marginTop: 12
    },
    txt: {
        color: 'white',
        textAlign: 'center',
    },
    darktxt: {
        color: 'white',
        textAlign: 'center',
        fontFamily: AppFonts.medium,
        marginTop: 5
    },
    line: {
        width: 1,
        height: '70%',
        backgroundColor: 'white',
        alignSelf: 'center'
    },
    inputContainer: {
        alignItems: 'center',
        width: '90%',
        alignSelf: 'center',
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 5,
        marginBottom: 10,

    },
    inputContainer1: {
        alignItems: 'center',
        width: '27%',
        alignSelf: 'center',
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 5,
        marginBottom: 10,

    },
    inputbox: {
        width: '90%',
        height: 40,
        color: AppColor.appColor
    },
    titleName:{
        marginLeft:20,
        marginTop:5,
        fontFamily:AppFonts.regular
    }

})

export default EmployeeProfile;