import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,
    Dimensions, Image, TouchableOpacity, TextInput,Linking,
    ScrollView, ImageBackground, StyleSheet, Alert,Share
} from 'react-native';
import { Container } from 'native-base';
import { DrawerActions } from '@react-navigation/native';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../utils/AppImage';
import FabButton from '../../../../component/button/FabButton';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import {useDispatch} from 'react-redux';
import HeaderComponent from '../../../../component/header/HeaderComponent';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import { checkScreen } from '../../../../redux/reducer';
import { AuthContext } from '../../../../Routes/AuthContext';

const { width, height } = Dimensions.get('screen')

const EmployeeHome = ({ navigation }) => {
    const [empName, setempName]=useState('');
    const [hotelName, setHotelName] = useState('');
    const [branchName, setBranchName] = useState('');
    const [address, setAddress] = useState('');
    const dispatch=useDispatch()
    const { signOutHotel } = React.useContext(AuthContext);

    useEffect(() => {
        const data = async () => {

            const name=await AsyncStorage.getItem(AppStrings.EMPLOYEE_NAME, "")
            const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME, "")
            const branch = await AsyncStorage.getItem(AppStrings.BRANCH_NAME, "")
            const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, "")
            setempName(name);
            setHotelName(hotel);
            setBranchName(branch);
            setAddress(addr);
            console.log(hotel, branch, addr)
        }
        data();


    }, [])
    // useEffect(()=>{
    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         closeApp
    //       );

    //       return () => backHandler.remove();
    // },[])

    const closeApp = () => {

        Alert.alert(
          'Exit',
          'Are you sure you want to exit the App?',
          [
            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'Yes', onPress: () => { BackHandler.exitApp(); } },
          ],
          { cancelable: false });
        return true;
    
      }
    const signOut = async () => {
        try {
            const token=await AsyncStorage.getItem(AppStrings.DEVICE_TOKEN,'')
            dispatch(checkScreen('Main',token,true))
          AsyncStorage.clear();
          navigation.navigate('Main')
          signOutHotel();
        } catch (error) {
          console.error(error);
        }
      };
    
      const logOutConfirmation = () => {
    
        Alert.alert(
          'Logout',
          'Are you sure you want to logout?',
          [
            { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            { text: 'Yes', onPress: () => { signOut(); } },
          ],
          { cancelable: false });
        return true;
    
      }
      const onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'Download Opaz App For Exciting offers',
          });
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };
      const dialCall = () => {

        let phoneNumber = '';
    
        if (Platform.OS === 'android') {
          phoneNumber = 'tel:${+1234567890}';
        }
        else {
          phoneNumber = 'telprompt:${+1234567890}';
        }
    
        Linking.openURL(phoneNumber);
      };
    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <View style={{
                flex: 0.45, backgroundColor: AppColor.appColor,
                borderBottomEndRadius: 100,
                borderBottomStartRadius: 100,
                //transform: [{ scaleX: 1 }],
            }}>
                <Text style={{
                    color: 'white', fontSize: 22, marginTop: 30, marginLeft: 20,
                }}>Home</Text>
                {/* <Image source={AppImage.emp_home}
                style={{width:'100%',height:'40%',resizeMode:'stretch',flex:1}}
               /> */}
            </View>
            <View style={{ flex: 1,}}>
                <View style={styles.whiteCard}>
                    <Text style={{ fontSize: 16, marginTop: 25, marginLeft: 10,color:'#06AE7D',fontSize:18 }}>Welcome</Text>
                    <Text style={{ fontSize: 25, margin: 10,color:AppColor.appColor,
                        fontFamily:AppFonts.slab,marginLeft:15,fontWeight:'600' }}>{empName}</Text>
                    <Text style={{ margin: 10 ,color:'#004732'}}>{hotelName}, {branchName}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
                    <TouchableOpacity style={styles.greenContainer}
                    onPress={()=>navigation.navigate('EmployeeProfile')} activeOpacity={0.8}
                    >
                        <View style={{ justifyContent: 'space-around', flexDirection: 'column', alignItems: 'center', flex: 1 }}>
                            <Image source={AppImage.emp_profile}
                                style={styles.img}
                            />
                            <Text style={styles.txt}>Profile</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.greenContainer, { bottom: 120 }]}
                    onPress={()=>navigation.navigate('Department')} activeOpacity={0.8}
                    >
                        <View style={{ justifyContent: 'space-around', flexDirection: 'column', alignItems: 'center', flex: 1 }}>
                            <Image source={AppImage.department_white}
                                style={styles.img}
                            />
                            <Text style={styles.txt}>Department</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.greenContainer}
                    onPress={()=>navigation.navigate('Task')} activeOpacity={0.8}
                    >
                        <View style={{ justifyContent: 'space-around', flexDirection: 'column', alignItems: 'center', flex: 1 }}>
                            <Image source={AppImage.task_img}
                                style={styles.img}
                            />
                            <Text style={styles.txt}>Task</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width:'100%',
                position:'absolute',bottom:5,flexDirection:'row',justifyContent:'space-around'}}>
                <View style={{}}>
                    <TouchableOpacity style={styles.btnContainer} onPress={()=>onShare()}>
                        <Image source={AppImage.share_green}
                        style={styles.icon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.iconTxt}>Share</Text>
                </View>
                <View style={{}}>
                    <TouchableOpacity style={styles.btnContainer} onPress={()=>dialCall()}>
                        <Image source={AppImage.call_green}
                        style={styles.icon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.iconTxt}>Help</Text>
                </View>
                <View style={{}}>
                    <TouchableOpacity style={styles.btnContainer} onPress={()=>
                        logOutConfirmation()}>
                        <Image source={AppImage.signout_green}
                        style={styles.icon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.iconTxt}>Signout</Text>
                </View>
                </View>
            </View>
            {/* <View style={{backgroundColor:'yellow',flex:1}}>

           </View> */}
        </View>
    )
}
const styles = StyleSheet.create({
    whiteCard:{
        height: '36%', 
        width: '90%', 
        bottom: 130,
        backgroundColor: 'white', 
        alignSelf: 'center',
        borderRadius:15,
        elevation:3
    },
    greenContainer: {
        backgroundColor: AppColor.appColor,
        width: '30%',
        height: '72%',
        borderRadius: 10,
        //flex:1
    },
    img: {
        width: 60,
        height: 60,
        resizeMode: 'contain'
    },
    txt: {
        color: 'white',
        fontSize: 18,
        fontFamily: AppFonts.medium
    },
    btnContainer:{
        backgroundColor:'#D8FDF2',
        borderRadius:5,
        alignItems:'center'
    },
    icon:{
        width:25,
        height:25,
        margin:5,
        resizeMode:'contain'
    },
    iconTxt:{
        textAlign:'center',
        fontSize:16
    }
})

export default EmployeeHome;