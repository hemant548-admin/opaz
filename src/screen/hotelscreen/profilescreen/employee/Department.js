import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,Alert,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import {
    Content,
    Picker, Form,
} from "native-base";
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AppStrings from '../../../../utils/AppStrings';
import APIStrings from '../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', status: 'Verified', person: 'Abc',
    check_in: '12 AM', check_out: '8 PM', bookingDate: '20/04/2021'
},
{
    id: '2', roomNumber: '420', status: 'Not verified', person: 'xyz',
    check_in: '10 AM', check_out: '6 PM', bookingDate: '22/04/2021'
},
{
    id: '3', roomNumber: '401', status: 'Verified', person: 'klm',
    check_in: '11 AM', check_out: '9 PM', bookingDate: '21/04/2021'
}]
const branches = [
    { address_line: 'Department Laundary', id: 1 },
    { address_line: 'Cooking', id: 2 },
    { address_line: 'Cleanig', id: 3 },
    { address_line: 'Make up', id: 3 }
]

const Department = ({ navigation }) => {
    const [department, setDepartment] = useState("");
    const [task, setTask] = useState(0);
    const [loader, setLoader] = useState(false)
    const [list, setList] = useState([]);
    const [departmentList, setDepartmentList] = useState([]);

    useEffect(() => {
        taskFetch();
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            goBack
          );

          return () => backHandler.remove();
    }, [])
   const goBack=()=>{
        navigation.goBack()
        return true;
    }
    const onTaskPress = (id) => {
        setTask(id);
    }
    const taskFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.BRANCH_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: id
        });
        console.log(requsetBody, APIStrings.EndUserServiceDepartmentList, id)

        simplePostCall(APIStrings.EndUserServiceDepartmentList, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setDepartmentList(data.list_of_department)
                    departmentTask(data.list_of_department[0].id);
                    //setTaskId(data.Employee_Task[0].id)
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const departmentTask = async (id) => {

        //const id = await AsyncStorage.getItem(AppStrings.BRANCH_ID, "")
        setDepartment(id)
        setLoader(true)
        let requsetBody = JSON.stringify({
            department_id: department === "" ? id : department
        });
        console.log(requsetBody, APIStrings.DepartmentWiseTaskList)

        simplePostCall(APIStrings.DepartmentWiseTaskList, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setList(data.Employee_Task)
                    //setTaskId(data.Employee_Task[0].id)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const acceptTask = async (val) => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            employee_id: id,
            task_id:val
        });
        console.log(requsetBody, APIStrings.AcceptDepartmentTask)

        simplePostCall(APIStrings.AcceptDepartmentTask, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    //setList(data.Employee_Task)
                    departmentTask(department);
                    Toast.show(data.Message)
                    //setTaskId(data.Employee_Task[0].id)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 12, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Room:
                            <Text style={{ color: AppColor.appColor }}> {item.room.room_no}</Text></Text>
                        </View>
                    </View>
                    {item.sub_department &&
                        <View style={styles.innerStyles}>
                        <Image source={AppImage.subdepartment_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Sub-department:
                            <Text style={{ color: AppColor.appColor }}> {item.sub_department.name}</Text></Text>
                        </View>
                        {/* <View style={styles.innerCard1}>
                            <Text style={styles.innerTxt}>Check-out:
                            <Text style={{ color: AppColor.appColor }}> {item.check_out}</Text></Text>
                        </View> */}
                    </View>}
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.description_icon}
                            style={styles.img}
                        />
                        <View style={[styles.innerCard, { height: 80, justifyContent: 'flex-start' }]}>
                            <Text style={[styles.innerTxt, { color: AppColor.appColor, marginTop: 5 }]}>Description: {item.description}</Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.branchName_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>
                                State of Task: {item.state_of_task}</Text>
                        </View>
                    </View>
                    {/*
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.timer_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>
                                Date/Time of Task Assignment</Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.edit_green}
                            style={[styles.img, { width: 20, height: 20, marginLeft: 7 }]}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>
                                Comment:</Text>
                        </View>
                    </View> */}
                    <TouchableOpacity style={{
                        backgroundColor: AppColor.appColor, width: '40%',
                        alignItems: 'center', alignSelf: 'center', borderRadius: 10, marginTop: 10
                    }} onPress={()=>acceptTask(item.id)}>
                        <Text style={{ color: 'white', margin: 10, fontSize: 15, fontFamily: AppFonts.medium }}>{item.task_status === 'Open' ? "Accept" : "Done"}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Department'
                goBack={() => navigation.goBack()}

            />
            <Text style={{ fontSize: 18, margin: 10 }}>Choose your department to see the task</Text>
            <View style={{
                flex: 0.18, flexDirection: 'row', backgroundColor: 'white',
                alignItems: 'center', justifyContent: 'center'
            }}>
                <Image source={AppImage.department_icon}
                    style={{ width: 25, height: 25, marginLeft: 20, marginTop: 10 }}
                />
                <Content>
                    <Form style={{
                        borderWidth: 1, borderColor: AppColor.appColor, width: '80%', alignSelf: 'center', marginTop: 25,
                        borderRadius: 10
                    }}>
                        <Picker
                            style={{ width: '95%', height: 40, color: '#add9ab' }}
                            iosHeader="Branch"
                            Header="Branch"
                            mode="dropdown"
                            textStyle={{ color: 'grey' }}
                            placeholder='Select branch'
                            headerBackButtonText='Geri'
                            selectedValue={department}
                            onValueChange={(value) => departmentTask(value)}
                        >
                            {departmentList.map((branches, i) => {
                                return (
                                    <Picker.Item label={branches.department} value={branches.id} key={i} />
                                );
                            }
                            )}
                        </Picker>
                    </Form>
                </Content>
            </View>
            {/* <View style={{
                width: '75%', flexDirection: 'row', marginTop: 10, borderRadius: 20,
                alignItems: 'center', justifyContent: 'space-around', alignSelf: 'center', elevation: 3
            }}>
                <TouchableOpacity style={{
                    width: '50%', alignItems: 'center', backgroundColor:task===0? AppColor.appColor:'white', borderRadius: 25,
                    borderTopRightRadius: 0, borderBottomRightRadius: 0, elevation: 3
                }}
                onPress={()=>onTaskPress(0)}
                >
                    <Text style={{ margin: 10, fontSize: 18,color:task===0? 'white':AppColor.appColor }}>Open</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    width: '50%', alignItems: 'center', backgroundColor: task===1? AppColor.appColor :'white',
                    borderTopLeftRadius: 0, borderBottomLeftRadius: 0, borderRadius: 25, elevation: 3
                }}
                onPress={()=>onTaskPress(1)}
                >
                    <Text style={{ margin: 10, fontSize: 18,color:task===1? 'white':AppColor.appColor }}>Assigned</Text>
                </TouchableOpacity>
            </View> */}
            <View style={{ flex: 1, marginBottom: 20, marginTop: 10 }}>
                {list.length > 0 ?
                    <FlatList
                        data={list}
                        renderItem={({ item, index }) => renderList(item, index)}
                        keyExtractor={item => item.id}
                    />
                    :
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: 'red', fontSize: 16 }}>No data Available</Text>
                    </View>
                }
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: '#72a16f',
        marginLeft: 10,
        fontSize: 16,
        margin: 7
    },
    img: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8
    }

})

export default Department;