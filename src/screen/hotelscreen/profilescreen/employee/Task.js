import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,BackHandler,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import APIStrings from '../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import Loader from '../../../../component/loader/Loader';
import moment from 'moment';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', status: 'Verified', person: 'Laundary',
    check_in: 'Iron', check_out: '8 PM', bookingDate: '20/04/2021', description: 'I want Iron for my cloths'
},
{
    id: '2', roomNumber: '420', status: 'Not verified', person: 'Cleaning',
    check_in: 'Cleaning tool', check_out: '6 PM', bookingDate: '22/04/2021',
    description: 'I want to clean my room'
},
{
    id: '3', roomNumber: '401', status: 'Verified', person: 'Cooking',
    check_in: 'Cooking', check_out: '9 PM', bookingDate: '21/04/2021', description: 'I want my dinner'
}]

const Task = ({ navigation }) => {

    const [loader, setLoader] = useState(true);
    const [assignTask, setAssignTask] = useState([]);
    const [openTask, setopenTask] = useState([]);
    const [taskid, setTaskId] = useState('');
    const [task, setTask] = useState(0);
    const [note, setNote]=useState([]);

    useEffect(() => {

        taskFetch();
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            goBack
          );

          
        setTimeout(() => {
            setLoader(false);
        }, 1000)

        return () => backHandler.remove();
    }, [])
    const goBack=()=>{
        navigation.goBack()
        return true;
    }
    const onTaskPress = (id) => {
        setTask(id);
        if (id === 1) {
            AssignTask();
        }
        else {
            taskFetch();
        }
    }

    const taskFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            employee_id: id
        });
        console.log(requsetBody, APIStrings.EmployeeAssignTask)

        simplePostCall(APIStrings.EmployeeAssignTask, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setopenTask(data.Employee_Task)
                    //setTaskId(data.Employee_Task[0].id)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const AssignTask = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            employee_id: id
        });
        console.log(requsetBody, APIStrings.EmployeeOpenTask)

        simplePostCall(APIStrings.EmployeeOpenTask, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setAssignTask(data.Employee_Task)
                    const temp=Array(data.Employee_Task).fill('');
                    setNote(temp);
                    //setTaskId(data.Employee_Task[0].id)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onButtonClick = (id,i) => {
        if (task === 1) {
            taskDone(id,i);
        }
        else {
            taskAccept(id);
        }

    }
    const taskAccept = async (id) => {


        //const id=await AsyncStorage.getItem(AppStrings.USER_ID,"")
        setLoader(true)
        let requsetBody = JSON.stringify({
            task_id: id
        });
        console.log(requsetBody, APIStrings.EmployeeAssignTaskAccept)

        simplePostCall(APIStrings.EmployeeAssignTaskAccept, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    //setAssignTask(data.Employee_Task)
                    taskFetch();

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const taskDone = async (id,i) => {
        if(note[i].trim().length===0){
            alert('Please write payment note')
            return ;
        }
        //const id=await AsyncStorage.getItem(AppStrings.USER_ID,"")
        setLoader(true)
        let requsetBody = JSON.stringify({
            task_id: id,
            payment_note:note
        });
        console.log(requsetBody, APIStrings.EmployeeOpenTaskDone)

        simplePostCall(APIStrings.EmployeeOpenTaskDone, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    //setAssignTask(data.Employee_Task)
                    AssignTask();

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 15, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Room:
                            <Text style={{ color: AppColor.appColor }}> {item.room.room_no}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.department_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Department: <Text style={{ color: AppColor.appColor }}>{item.department.department}
                            </Text></Text>
                        </View>
                    </View>
                    {item.sub_department &&
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.subdepartment_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Sub-department:
                            <Text style={{ color: AppColor.appColor }}> {item.sub_department.name}</Text></Text>
                        </View>
                        {/* <View style={styles.innerCard1}>
                            <Text style={styles.innerTxt}>Check-out:
                            <Text style={{ color: AppColor.appColor }}> {item.check_out}</Text></Text>
                        </View> */}
                    </View>
                    }
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.description_icon}
                            style={styles.img}
                        />
                        <View style={[styles.innerCard, { justifyContent: 'flex-start', }]}>
                            <Text style={[styles.innerTxt, { marginTop: 5, marginRight: 10 }]}>Description:
                            <Text style={{ textAlign: 'center', color: AppColor.appColor }}> {item.description}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.branchName_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>State of Task :
                            <Text style={{ color: AppColor.appColor }}> {item.state_of_task}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.timer_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Date :
                            <Text style={{ color: AppColor.appColor }}> {moment(item.task_assign_date+" "+item.task_assign_time).format('DD-MM-YYYY H:mm')}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.edit_green}
                            style={[styles.img, { width: 20, height: 20, marginLeft: 7 }]}
                        />
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}>Comment:
                            <Text style={{ color: AppColor.appColor }}> {item.comment}</Text></Text>
                        </View>
                    </View>
                    {item.task_status !== 'Open' &&
                    <React.Fragment>
                    <Text style={{marginLeft:30,marginTop:10,color:AppColor.appColor}}>Payment Note:</Text>
                    <View style={styles.innerStyles}>
                        <Image source={null}
                            style={[styles.img, { width: 20, height: 20, marginLeft: 7 }]}
                        />
                        
                        <View style={styles.innerCard}>
                            <TextInput 
                            style={{paddingLeft:10}}
                            placeholder='Write payment note'
                            value={note[index]}
                            onChangeText={(val)=>{
                                let noteCopy=note.concat();
                                noteCopy[index]=val;
                                setNote(noteCopy)}}
                            />
                        </View>
                    </View>
                    </React.Fragment>
                    }
                    <TouchableOpacity style={{
                        backgroundColor: AppColor.appColor, width: '40%',
                        alignItems: 'center', alignSelf: 'center', borderRadius: 10, marginTop: 10
                    }} onPress={() => onButtonClick(item.id,index)}>
                        <Text style={{ color: 'white', margin: 10, fontSize: 15, fontFamily: AppFonts.medium }}>{item.task_status === 'Open' ? "Accept" : "Done"}</Text>
                    </TouchableOpacity>
                </View>
                <Loader loading={loader} />
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Task'
                goBack={() => navigation.goBack()}

            />
            <View style={{
                width: '75%', flexDirection: 'row', marginTop: 10, borderRadius: 20,
                alignItems: 'center', justifyContent: 'space-around', alignSelf: 'center', elevation: 3
            }}>
                <TouchableOpacity style={{
                    width: '50%', alignItems: 'center', backgroundColor: task === 0 ? AppColor.appColor : 'white', borderRadius: 25,
                    borderTopRightRadius: 0, borderBottomRightRadius: 0, elevation: 3
                }} activeOpacity={0.8}
                    onPress={() => onTaskPress(0)}
                >
                    <Text style={{ margin: 10, fontSize: 18, color: task === 0 ? 'white' : AppColor.appColor }}>Open</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    width: '50%', alignItems: 'center', backgroundColor: task === 1 ? AppColor.appColor : 'white',
                    borderTopLeftRadius: 0, borderBottomLeftRadius: 0, borderRadius: 25, elevation: 3
                }} activeOpacity={0.8}
                    onPress={() => onTaskPress(1)}
                >
                    <Text style={{ margin: 10, fontSize: 18, color: task === 1 ? 'white' : AppColor.appColor }}>Assigned</Text>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, marginBottom: 20, marginTop: 10 }}>
                {task === 0 ?
                    openTask.length > 0 ?
                        <FlatList
                            data={openTask}
                            renderItem={({ item, index }) => renderList(item, index)}
                            keyExtractor={item => item.id}
                        />

                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'red', fontSize: 16 }}>No data available</Text>
                        </View>

                    :
                    assignTask.length > 0 ?
                        <FlatList
                            data={assignTask}
                            renderItem={({ item, index }) => renderList(item, index)}
                            keyExtractor={item => item.id}
                        />
                        :
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'red', fontSize: 16 }}>No data available</Text>
                        </View>

                }
            </View>
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: '#72a16f',
        marginLeft: 10,
        fontSize: 16,
        margin: 7,
    },
    img: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8
    }

})

export default Task;