import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet,Platform,PermissionsAndroid
} from 'react-native';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import RNFetchBlob from 'rn-fetch-blob';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import APIStrings, { BASE_URL } from '../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import Loader from '../../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')
//const BASE_URL = 'http://103.145.51.114:8085/';
const arr = [{
    id: '1', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '2', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '3', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '4', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '5', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '6', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '7', name: 'Dummy Name', checkIn: '10 AM', checkOut: '9 PM'
}]

const btn = [{ id: '1', name: 'Employee' }, { id: '2', name: 'User' },
]
const ReportScreen = ({ navigation }) => {
    const[selected, setSelected]=useState(0)
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [loader,setLoader]=useState(false);
    const [empData, setempData]=useState([]);
    const [userData, setuserData]=useState([]);
    const [user,setUser]=useState('employee');
    const [fileUrl, setFileUrl]=useState('');

useEffect(()=>{


    const unsubscribe = navigation.addListener('focus', () => {
           empDataFetch(date);
           setSelected(0);
          });
      
          return unsubscribe;
},[navigation])

const checkPermission = async () => {
    
    // Function to check the platform
    // If Platform is Android then check for permissions.
 
    if (Platform.OS === 'ios') {
      downloadFile();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Start downloading
          downloadFile();
          console.log('Storage Permission Granted.');
        } else {
          // If permission denied then show alert
          Alert.alert('Error','Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log("++++"+err);
      }
    }
  };
 
  const downloadFile = () => {
   
    // Get today's date to add the time suffix in filename
    let date = new Date();
    // File URL which we want to download
    let FILE_URL = BASE_URL+fileUrl;    
    // Function to get extention of the file url
    let file_ext = getFileExtention(FILE_URL);
   
    file_ext = '.' + file_ext[0];
   
    // config: To get response by passing the downloading related options
    // fs: Root directory path to download
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.PictureDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path:
          RootDir+
          '/file_' + 
          Math.floor(date.getTime() + date.getSeconds() / 2) +
          file_ext,
        description: 'downloading file...',
        notification: true,
        // useDownloadManager works with Android only
        useDownloadManager: true,   
      },
    };
    config(options)
      .fetch('GET', FILE_URL)
      .then(res => {
        // Alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        alert('File Downloaded Successfully.');
      });
  };
 
  const getFileExtention = fileUrl => {
    // To get the file extension
    return /[.]/.exec(fileUrl) ?
             /[^.]+$/.exec(fileUrl) : undefined;
  };
    const empDataFetch= async(val)=>{
            
        const id=await AsyncStorage.getItem(AppStrings.USER_ID,"")
        setUser('employee')
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id:id,
            start_date:moment(val).format('YYYY-MM-DD'), 
        });
        console.log(requsetBody, APIStrings.EmployeeReportView)
        
        simplePostCall(APIStrings.EmployeeReportView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setempData(data.emp_attendance)
                    setFileUrl(data.download_report);
                    
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const userDataFetch= async(val)=>{
            
        const id=await AsyncStorage.getItem(AppStrings.USER_ID,"")
        setUser('user')
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id:id,
            start_date:moment(val).format('YYYY-MM-DD'), 
        });
        console.log(requsetBody, APIStrings.UserReportView)
        
        simplePostCall(APIStrings.UserReportView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setuserData(data.booking_user)
                    setFileUrl(data.download_report);
                    
                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        if(user==='employee'){
            empDataFetch(currentDate)
        }
        else{
            userDataFetch(currentDate)
        }
        //setShow(false)
      };
    
    const showDatepicker = () => {
        setShow(true);
      };
    const onItemPress=(id)=>{
            setSelected(id);
            switch(id){
                case 0:
                    empDataFetch(date);
                break;
                case 1:
                    userDataFetch(date);
                    break;
                // case 2:
                //     bookingDataFetch();
                //     break;        


            }
            
    }
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={styles.innerCard1}>
                    <Text style={styles.name}>{user==='employee'? item.employee.emp_user.first_name:
                    item.person_name}</Text>
                </View>
                <View style={styles.innerCard2}>
                    <Text style={styles.name}>{user==='employee'?item.check_in_time:item.from_booking_date}</Text>
                </View>
                <View style={styles.innerCard2}>
                    <Text style={styles.name}>{user==='employee'?item.check_out_time:item.to_booking_date}</Text>
                </View>
            </View>
        )
    }

    const renderButton=(item,index)=>{
        return(
            <View>
                <TouchableOpacity style={[styles.btnContainer,{backgroundColor:selected===index? AppColor.appColor:'white'}]}
                onPress={()=>onItemPress(index)}
                >
                    <Text style={{color:selected===index? 'white':AppColor.appColor,
                fontSize:16}}>{item.name}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <Header
                title='Report'
                goBack={() => navigation.goBack()}

            />
            <View>
                
            {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={'date'}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
      
            <FlatList
                    data={btn}
                    horizontal
                    // backgroundColor={'red'}
                    renderItem={({ item, index }) => renderButton(item, index)}
                    keyExtractor={item => item.id}
                />
            </View>
            <View style={{alignItems:'center',marginBottom:10,justifyContent:'space-around',flexDirection:'row'}}>
                <TouchableOpacity style={{alignItems:'center'}} activeOpacity={0.8}
                onPress={()=>showDatepicker()}
                >
                <Image source={AppImage.calenderBooking}
                    style={{width:30,height:30}}                
                />
                <Text>{moment(date).format('DD-MM-YYYY')}</Text>
                    <Text>Select Date</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems:'center'}} activeOpacity={0.8} onPress={()=>downloadFile()}>
                    <Image source={AppImage.download}
                    style={{width:25,height:25}}                
                />
                <Text style={{textAlign:'center'}}>Download Report</Text>
                </TouchableOpacity>
                </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around',left:10 ,marginTop:10}}>
                <View style={styles.innerView}>
                    <Image source={AppImage.branchName_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{selected===0?'Employee Name':
                    'User Name'}</Text>
                </View>
                <View style={[styles.innerView, { left: 10 }]}>
                    <Image source={AppImage.timer_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{selected===0?'LoginTime':
                    'Check-in'}</Text>
                </View>
                <View style={styles.innerView}>
                    <Image source={AppImage.timer_icon}
                        style={styles.img}
                    />
                    <Text style={styles.txt}>{selected===0?'LogoutTime':
                    'Check-out'}</Text>
                </View>
            </View>
            <View style={{ flex: 1, marginBottom: 20, marginTop: 10, }}>
                <FlatList
                    data={user==='employee'?empData:userData}
                    // backgroundColor={'red'}
                    renderItem={({ item, index }) => renderList(item, index)}
                    keyExtractor={item => item.id}
                />
            </View>
         <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '100%',
        // height: 260,
        // shadowColor: 'black',
        // shadowOpacity: 0.26,
        // shadowOffset: { width: 0, height: 2 },
        // shadowRadius: 10,
        // elevation: 3,
        // backgroundColor: 'white',
        // marginTop: 15,
        // borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'space-around',
        alignSelf: 'center',
        flexDirection: 'row',
        marginBottom: 10,
       // backgroundColor: 'red'

    },
    innerCard1: {
        width: '45%',
        height: 37,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        //marginLeft: 15,
        marginTop: 10,
        justifyContent: 'center',
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard2: {
        //width: '21%',
        height: 37,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        //marginLeft: 8,
        marginTop: 10,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: AppColor.appColor,
        marginLeft: 10,
        fontSize: 16
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerView: {
        alignItems: 'center'
    },
    txt: {
        textAlign: 'center'
    },
    btnContainer:{
        width:150,
        height:40,
       // backgroundColor:'red',
        borderRadius:10,
        elevation:2,
        margin:10,
        justifyContent:'center',
        alignItems:'center',
        //borderWidth:0.1,
        borderColor:AppColor.appColor
    },
    name:{
        textAlign:'center',
        color:AppColor.appColor,
        fontSize:16,
        margin:5
        // marginTop:15,
        // marginBottom:15
    }

})

export default ReportScreen;