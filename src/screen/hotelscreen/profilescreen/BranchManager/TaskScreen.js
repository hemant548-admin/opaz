import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList, Pressable,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import APIStrings from '../../../../webservice/APIStrings';
import AppStrings from '../../../../utils/AppStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import Loader from '../../../../component/loader/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import Modal from 'react-native-modal';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', department: 'Laundry', subDepartment: 'Iron',
    Description: 'I want roll press for my cloths', assignedTo: 'Abc verma'
},
{
    id: '2', roomNumber: '201', department: 'Laundry', subDepartment: 'Iron',
    Description: 'I want roll press for my cloths', assignedTo: 'Abc verma'
},
{
    id: '3', roomNumber: '201', department: 'Laundry', subDepartment: 'Iron',
    Description: 'I want roll press for my cloths', assignedTo: 'Abc verma'
}]

const TaskScreen = ({ navigation }) => {
    const [loader, setLoader] = useState(false);
    const [roomData, setRoomData] = useState([]);
    const [isModalVisible, setisModalVisible] = useState(false);
    const [modalData, setModalData] = useState({});

    useEffect(() => {

        task();
    }, [])


    const task = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: id
        });
        console.log(requsetBody, APIStrings.ManagerTaskView)

        simplePostCall(APIStrings.ManagerTaskView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setRoomData(data.room_list);
                    setModalData(data.room_list.length>0? data.room_list[0]:[])

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                //alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const toggleModal = () => {

        setisModalVisible(!isModalVisible);
    }

    const editModal = () => {
        return (

            <View>
                <Modal isVisible={isModalVisible}
                    // animationIn='sl'
                    animationInTiming={500}
                    animationOutTiming={500}
                // onBackdropPress={() => setModalVisible(false)}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                        <ScrollView style={{
                            width: width, backgroundColor: 'white', alignSelf: 'center', top: 50,
                            borderTopLeftRadius: 20, borderTopRightRadius: 20,
                        }} contentContainerStyle={{ paddingBottom: 40, }}
                        >
                            <Pressable style={{ alignSelf: 'flex-end', margin: 20, marginBottom: 0 }} onPress={() => toggleModal()}>
                                <Image source={AppImage.cross_icon}
                                    style={{ width: 30, height: 30, }}
                                />
                            </Pressable>
                            <View style={styles.card}>
                                <View style={{
                                    marginLeft: 12, marginTop: 10, marginBottom: 10

                                }}>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.room_icon}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerContainer}>
                                            <Text style={styles.mainTxt}>Room:</Text>
                                            <View style={styles.innerCard}>
                                                <Text style={styles.innerTxt}> {modalData.room}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.department_icon}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerContainer}>
                                            <Text style={styles.mainTxt}>Department:</Text>
                                            <View style={styles.innerCard}>
                                                <Text style={styles.innerTxt}> {modalData.department?.department}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    {modalData.sub_department &&
                                        <View style={styles.innerStyles}>
                                            <Image source={AppImage.subdepartment_icon}
                                                style={styles.img}
                                            />
                                            <View style={styles.innerContainer}>
                                                <Text style={styles.mainTxt}>Sub-department:</Text>
                                                <View style={styles.innerCard}>
                                                    <Text style={styles.innerTxt}> {modalData.sub_department.name}</Text>
                                                </View>
                                            </View>
                                        </View>}
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.description_icon}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerContainer}>
                                            <Text style={styles.mainTxt}>Description:</Text>
                                            <View style={styles.innerCard}>
                                                <Text style={styles.innerTxt}> {modalData.description}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.branchName_icon}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerContainer}>
                                            <Text style={styles.mainTxt}>Assigned To:</Text>
                                            <View style={styles.innerCard}>
                                                <Text style={styles.innerTxt}> {modalData.employeetas_veiw_name}</Text>
                                            </View>
                                        </View>
                                    </View>

                                    <View style={styles.innerStyles}>
                                        <Image source={AppImage.calenderBooking}
                                            style={styles.img}
                                        />
                                        <View style={styles.innerContainer}>
                                            <Text style={styles.mainTxt}>Task Date/Time:</Text>
                                            <View style={styles.innerCard}>
                                                <Text style={styles.innerTxt}> {moment(modalData.task_assign_date + " " + modalData.task_assign_time).format('DD-MM-YYYY h:mm a')}</Text>
                                            </View>
                                        </View>
                                    </View>

                                </View>

                            </View>

                        </ScrollView>

                    </View>
                </Modal>
            </View>

        )

    }
    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 12, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Room:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.room}</Text>
                            </View>
                        </View>
                    </View>
                    {/* <View style={styles.innerStyles}>
                        <Image source={AppImage.department_icon}
                            style={styles.img}
                        />
                         <View style={styles.innerContainer}>
                        <Text style={styles.mainTxt}>Department:</Text>
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}> {item.department.department}</Text>
                        </View>
                        </View>
                    </View>
                    {item.sub_department &&
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.subdepartment_icon}
                            style={styles.img}
                        />
                         <View style={styles.innerContainer}>
                        <Text style={styles.mainTxt}>Sub-department:</Text>
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}> {item.sub_department.name}</Text>
                        </View>
                        </View>
                    </View>}
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.description_icon}
                            style={styles.img}
                        />
                         <View style={styles.innerContainer}>
                        <Text style={styles.mainTxt}>Description:</Text>
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}> {item.description}</Text>
                        </View>
                        </View>
                    </View> */}
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.branchName_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Assigned To:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.employeetas_veiw_name}</Text>
                            </View>
                        </View>
                    </View>
                    <Pressable style={{
                        alignSelf: 'flex-end', width: 25,
                        height: 25, alignItems: 'center', justifyContent: 'center', marginRight: 10
                    }} accessible onPress={() => { toggleModal(), setModalData(item) }}>
                        <Image source={AppImage.info_icon}
                            style={{ width: 20, height: 20, resizeMode: 'contain' }}
                        />
                    </Pressable>
                    {/* <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderBooking}
                            style={styles.img}
                        />
                         <View style={styles.innerContainer}>
                        <Text style={styles.mainTxt}>Task Date/Time:</Text>
                        <View style={styles.innerCard}>
                            <Text style={styles.innerTxt}> {moment(item.task_assign_date+" "+item.task_assign_time).format('DD-MM-YYYY h:mm a')}</Text>
                        </View>
                        </View>
                    </View> */}

                </View>

            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Task'
                goBack={() => navigation.goBack()}

            />
            {/* <Text style={{ marginLeft: 10, marginTop: 10, color: AppColor.appColor, fontSize: 15 }}
            >View details of your customer and their booking details.</Text> */}
            <View style={{ flex: 1, marginBottom: 20 }}>
                {roomData.length > 0 ?
                    <FlatList
                        data={roomData}
                        renderItem={({ item, index }) => renderList(item, index)}
                        keyExtractor={item => item.id}
                    />
                    :
                    <View>
                        <Text style={{
                            color: 'red', textAlign: 'center'
                            , marginTop: '50%', fontSize: 20
                        }}> You don't have any task to show</Text>
                    </View>}
            </View>
            {roomData.length>0 && editModal()}
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 35,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: AppColor.appColor,
        marginLeft: 10,
        fontSize: 16,
        margin: 7
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
    },
    innerContainer: {
        //backgroundColor:'red',
        width: '100%',
        marginLeft: 5
    },
    mainTxt: {
        marginLeft: 15,
        fontSize: 18,
        marginBottom: 5,
        color: 'black'
    }

})

export default TaskScreen;