import React, { useState,useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';
import APIStrings from '../../../../webservice/APIStrings';
import { simplePostCall } from '../../../../webservice/APIServices';
import Loader from '../../../../component/loader/Loader';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', employeeName: 'Abc Verma', time: '10 AM - 8 PM'
},
{
    id: '2', employeeName: 'Abc Verma', time: '10 AM - 8 PM'
},
{
    id: '3', employeeName: 'Abc Verma', time: '10 AM - 8 PM'
}]

const EmployeeScreen = ({ navigation }) => {

    const [loader, setLoader] = useState(false);
    const [Data, setData] = useState([]);

    useEffect(() => {

        empDataFetch();
    }, [])


    const empDataFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: id
        });
        console.log(requsetBody, APIStrings.EmpAttendanceView)

        simplePostCall(APIStrings.EmpAttendanceView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    setData(data.emp_attendance)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Roomview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }

    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 12, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.branchName_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Employee Name:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.employee.emp_user.first_name}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.timer_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Login/out Time:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.check_in_time}/{item.check_out_time}</Text>
                            </View>
                        </View>
                    </View>

                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Employee'
                goBack={() => navigation.goBack()}

            />

            <View style={{ flex: 1, marginBottom: 20 }}>
               { Data.length>0?
               <FlatList
                    data={Data}
                    renderItem={({ item, index }) => renderList(item, index)}
                    keyExtractor={item => item.id}
                />
                :
                <View>
                    <Text style={{color:'red',textAlign:'center'
                    ,marginTop:'50%',fontSize:20}}> You don't have any task to show</Text>
                    </View>
                }
            </View>
            <Loader loading={loader} />
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        //height: 35,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: AppColor.appColor,
        marginLeft: 10,
        fontSize: 16,
        margin:7
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
    },
    innerContainer: {
        //backgroundColor:'red',
        width: '100%',
        marginLeft: 5
    },
    mainTxt: {
        marginLeft: 15,
        fontSize: 18,
        marginBottom: 5,
        color: 'black'
    }

})

export default EmployeeScreen;