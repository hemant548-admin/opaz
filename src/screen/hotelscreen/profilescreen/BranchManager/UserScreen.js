import React, { useState, useEffect } from 'react';
import {
    View, Text, FlatList,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet, Pressable
} from 'react-native';
import moment from 'moment';
import AppImage from '../../../../utils/AppImage';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import Header from '../../../../component/header/Header';
import AsyncStorage from '@react-native-community/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import AppStrings from '../../../../utils/AppStrings';
import APIStrings from '../../../../webservice/APIStrings';
import Loader from '../../../../component/loader/Loader';
import { simplePostCall } from '../../../../webservice/APIServices';

const { width, height } = Dimensions.get('screen')

const arr = [{
    id: '1', roomNumber: '201', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '2', roomNumber: '201', checkIn: '10 AM', checkOut: '9 PM'
},
{
    id: '3', roomNumber: '201', checkIn: '10 AM', checkOut: '9 PM'
}]

const UserScreen = ({ navigation }) => {

    const [loader, setLoader] = useState(false);
    const [roomData, setRoomData] = useState([]);
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);

    useEffect(() => {

        taskFetch();
    }, [date])


    const taskFetch = async () => {

        const id = await AsyncStorage.getItem(AppStrings.USER_ID, "")
        setLoader(true)
        let requsetBody = JSON.stringify({
            branch_id: id,
            room_date: moment(date).format('YYYY-MM-DD')
        });
        console.log(requsetBody, APIStrings.ManagerUserBookingView)

        simplePostCall(APIStrings.ManagerUserBookingView, requsetBody)
            .then((data) => {

                setLoader(false)
                //console.log(data.token);
                if (data.status.code === 200) {
                    console.log("api success", JSON.stringify(data));
                    //const merge3 = [].concat(data.output, data.user_booking); //The depth level specifying how deep a nested array structure should be flattened. Defaults to 1.
                    const merge3 = [...data.user_booking, ...data.output]
                    console.log('merggg ',merge3);
                    setRoomData(merge3)

                }
                else {
                    //this.setState({ invalid: true, errorMessage: data.message });
                    console.log(data.status.message)

                }

            }).catch((error) => {
                console.log("Userview error", error);
                setLoader(false)
                alert('api error')
                //this.setState({ isProcessing: false, invalid: true, errorMessage: error.message });
            });


    }
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
       // () => taskFetch()
        //setShow(false)
    };

    const showDatepicker = () => {
        setShow(true);
    };

    const renderList = (item, index) => {
        return (
            <View style={styles.card}>
                <View style={{
                    marginLeft: 12, marginTop: 10, marginBottom: 10

                }}>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.room_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Room:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.room_no}</Text>
                            </View>
                        </View>
                    </View>
                    {item.room_status==='Full' &&
                        <React.Fragment>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderBooking}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Check-in:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.room_status !== 'Full'?'-': item.booking_to}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.calenderChecking}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Check-out:</Text>
                            <View style={styles.innerCard}>
                                <Text style={styles.innerTxt}> {item.room_status !== 'Full'?'-': item.booking_from}</Text>
                            </View>
                        </View>
                    </View>
                    </React.Fragment>
                        }
                    <View style={styles.innerStyles}>
                        <Image source={AppImage.roomHome_icon}
                            style={styles.img}
                        />
                        <View style={styles.innerContainer}>
                            <Text style={styles.mainTxt}>Room Status:</Text>
                            <View style={styles.innerCard}>
                                <Text style={[styles.innerTxt, { color: item.room_status === 'Full' ? 'red' : AppColor.appColor }]}> {item.room_status}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#E5E5E5', }}>
            <Header
                title='Room'
                goBack={() => navigation.goBack()}

            />
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
                <Text style={{ fontSize: 16, marginRight: 5 }}>Filter By date</Text>
                <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => showDatepicker()}>
                    <Image source={AppImage.calenderBooking}
                        style={{ width: 30, height: 30, resizeMode: 'contain' }}
                    />
                    <Text style={{ marginLeft: 10, color: AppColor.appColor, textDecorationLine: 'underline', fontSize: 16 }}>{moment(date).format('DD/MM/YYYY')}</Text>
                </Pressable>
            </View>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={'date'}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
            <View style={{ flex: 1, paddingBottom: 20 }}>
                {roomData.length > 0 ?
                    <FlatList
                        data={roomData}
                        renderItem={({ item, index }) => renderList(item, index)}
                        keyExtractor={item => item.id}
                    />
                    :
                    <View>
                        <Text style={{
                            color: 'red', textAlign: 'center'
                            , marginTop: '50%', fontSize: 20
                        }}> You don't have any task to show</Text>
                    </View>}
            </View>
            <Loader loading={loader}/>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '98%',
        // height: 260,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        backgroundColor: 'white',
        marginTop: 15,
        borderRadius: 10,
        //alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    innerCard: {
        width: '70%',
        height: 35,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerCard1: {
        width: '35%',
        height: 36,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        elevation: 3,
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 15,
        justifyContent: 'center'
        //alignSelf:'center',
        //marginTop:10

    },
    innerTxt: {
        color: AppColor.appColor,
        marginLeft: 10,
        fontSize: 16
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    innerStyles: {
        flexDirection: 'row',
        marginTop: 8,
        marginBottom: 8,
    },
    innerContainer: {
        //backgroundColor:'red',
        width: '100%',
        marginLeft: 5
    },
    mainTxt: {
        marginLeft: 15,
        fontSize: 18,
        marginBottom: 5,
        color: 'black'
    }

})

export default UserScreen;