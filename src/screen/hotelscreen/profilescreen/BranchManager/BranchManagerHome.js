import React, { useState ,useEffect} from 'react';
import {
    View, Text, FlatList,BackHandler,Alert,
    Dimensions, Image, TouchableOpacity, TextInput,
    ScrollView, ImageBackground, StyleSheet
} from 'react-native';
import { Container } from 'native-base';
import { DrawerActions } from '@react-navigation/native';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import AppImage from '../../../../utils/AppImage';
import FabButton from '../../../../component/button/FabButton';
import AppColor from '../../../../utils/AppColor';
import AppFonts from '../../../../utils/AppFonts';
import HeaderComponent from '../../../../component/header/HeaderComponent';
import AsyncStorage from '@react-native-community/async-storage';
import AppStrings from '../../../../utils/AppStrings';

const { width, height } = Dimensions.get('screen')

const BranchManagerHome = ({ navigation }) => {

    const [hotelName, setHotelName] = useState('');
    const [branchName, setBranchName] = useState('');
    const [address, setAddress] = useState('');

    useEffect(() => {
        const data = async () => {

            const hotel = await AsyncStorage.getItem(AppStrings.HOTEL_NAME, "")
            const branch = await AsyncStorage.getItem(AppStrings.BRANCH_NAME, "")
            const addr = await AsyncStorage.getItem(AppStrings.ADDRESS, "")
            setHotelName(hotel);
            setBranchName(branch);
            setAddress(addr);
            console.log(hotel, branch, addr)
        }
        data();


    }, [])
    // useEffect(()=>{
    //     const backHandler = BackHandler.addEventListener(
    //         "hardwareBackPress",
    //         closeApp
    //       );

    //       return () => backHandler.remove();
    // },[])

    // const closeApp = () => {

    //     Alert.alert(
    //       'Exit',
    //       'Are you sure you want to exit the App?',
    //       [
    //         { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //         { text: 'Yes', onPress: () => { BackHandler.exitApp(); } },
    //       ],
    //       { cancelable: false });
    //     return true;
    
    //   }
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.openDrawer());
    };
    

    return (
        <View style={{ flex: 1, backgroundColor: 'white', }}>
            <HeaderComponent
                title={hotelName}
                handelDrawer={() => toggleDrawer()}
            />
            <View style={{ flex: 1 }}>
                <View style={styles.card}>
                    <View style={styles.Container}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.frontHotel_icon}
                                style={styles.img}
                            />
                            <Text style={styles.hotelName}>{hotelName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.front_hotel}
                                style={styles.img}
                            />
                            <Text style={{ fontSize: 17, color: AppColor.appColor, fontFamily: AppFonts.light, marginLeft: 10 }}>{branchName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={AppImage.marker}
                                style={styles.img}
                            />
                            <Text style={{ fontSize: 17, color: AppColor.appColor, fontFamily: AppFonts.thin, marginLeft: 10,marginRight:10 }}>{address}</Text>
                        </View>
                    </View>
                </View>


                <Text style={{ fontSize: 16, margin: 10, bottom: 10, marginLeft: 30, color: '#003626' }}>Choose from below to see the details !</Text>
            </View>
            <View style={{ flex: 2 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1, marginBottom: 5 }}>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('TaskScreen')} activeOpacity={0.8}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.task_img}
                                    style={{ width: 75, height: 75, resizeMode: 'contain', }}
                                />
                                <Text style={styles.txt}>TASK</Text>
                            </View>
                            {/* <Image source={AppImage.arrow_white}
                                style={{ width: 22, height: 22, resizeMode: 'contain', marginLeft: 60 }}
                            /> */}
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('EmployeeScreen')} activeOpacity={0.8}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.employee_img}
                                    style={{ width: 70, height: 70, resizeMode: 'contain', alignSelf: 'center' }}
                                />
                                <Text style={styles.txt}>EMPLOYEE</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1, marginBottom: 10 }}>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('UserScreen')} activeOpacity={0.8}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.frontUser}
                                    style={{ width: 70, height: 70, resizeMode: 'contain', }}
                                />
                                <Text style={styles.txt}>ROOM</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.greenBox} onPress={() => navigation.navigate('ReportScreen')} activeOpacity={0.8}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{}}>
                                <Image source={AppImage.report_img}
                                    style={{ width: 70, height: 70, resizeMode: 'contain',alignSelf:'center' }}
                                />
                                <Text style={styles.txt}>REPORT</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        width: '90%',
        height: '24%',
        marginTop: 10,
        marginBottom: 15,
        borderRadius: 20,
        borderColor: AppColor.appColor,
        borderWidth: 0.5,
        shadowColor: AppColor.appColor,
        //justifyContent: 'space-evenly',
        backgroundColor: 'white',
        alignItems: 'center',
        elevation: 3,
        alignSelf: 'center',
        flex: 1
    },
    img: {
        width: 25,
        height: 25,
        resizeMode: 'contain'
    },
    Container: {
        //margin:25,
        width: '90%',
        height: '100%',
        justifyContent: 'space-evenly',
        // backgroundColor:'red'
    },
    hotelName: {
        fontSize: 20,
        color: AppColor.appColor,
        marginLeft: 10
    },
    greenBox: {
        //width: '45%',
        //height: '90%',
        flex: 1,
        //marginTop: 10,
        //marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 20,
        justifyContent: 'center',
        backgroundColor: AppColor.appColor,
        //alignSelf: 'center',
    },
    txt: {
        fontSize: 22,
        color: 'white',
        marginTop: 40,
        marginLeft: 5,
        textAlign: 'center',
        fontFamily: AppFonts.medium
    }

})


export default BranchManagerHome;