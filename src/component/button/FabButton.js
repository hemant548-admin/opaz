import React, { useState } from 'react';
import {
    Text, TouchableOpacity,
} from 'react-native';
import AppColor from '../../utils/AppColor';
import AppFonts from '../../utils/AppFonts';

const FabButton = (props) => {

    return (
        <TouchableOpacity style={{ width: '50%', backgroundColor: '#CD9933', alignSelf: 'center', marginTop: 30, elevation: 3, borderRadius: 5 }}
            onPress={props.onItem}
        >
            <Text style={{ textAlign: 'center', margin: 10, fontSize: 15, color: '#003223', fontFamily: AppFonts.bold ,}}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default FabButton;