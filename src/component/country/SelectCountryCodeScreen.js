import React, { useEffect, useState } from 'react';
import {
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    FlatList,
    TouchableWithoutFeedback
} from 'react-native';

//import AppColors from '../../constants/AppColors';
//import AppImages from '../../constants/AppImages';

import countries from '../../assets/countries/countries.json';
import AppImage from '../../utils/AppImage';

const SelectCountryCodeScreen = ({ navigation, route }) => {

    const [country, setCountries] = useState([]);
    const [searchInput, setSearchInput] = useState('');



    useEffect(() => {
        // this.props.navigation.setParams({
        //     title: 'Select Country'
        // });
        console.log("nav", route)
        getCountries();

    }, [])

    const getCountries = () => {

        //this.setState({ countries: countries.sort((a, b) => a.name > b.name) });
        setCountries(countries.sort((a, b) => a.name > b.name))
    }

    const searchCountry = (input) => {
        //this.setState({ searchInput: input });
        setSearchInput(input)
        let filteredData = countries.filter(item => item.name.toLowerCase().includes(input.trim().toLowerCase()));
        //this.setState({ countries: filteredData });
        setCountries(filteredData)
    }

    const onItemPress = (country) => {
        // const { navigation } = this.props;

        route.params.onCountrySelect(country);
        navigation.goBack();
    }

    const renderItem = (item, index) => {
        return (
            <TouchableWithoutFeedback
                onPress={() => onItemPress(item)}>
                <View style={styles.item}>
                    <Text style={{ margin: 5 }}>{item.flag}</Text>
                    <Text style={{ margin: 5 }}>+{item.countryCode}</Text>
                    <Text style={{ margin: 5, flex: 1 }}>{item.name}</Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    // Render any loading content that you like here


    return (
        <View style={styles.container}>

            <View style={styles.search}>
                <Image
                    style={styles.searchIcon}
                    source={AppImage.search_icon} />
                <TextInput
                    // flex={1}
                    placeholder='Search'
                    //placeholderTextColor={AppColors.colorLightText}
                    // backgroundColor='red'
                    padding={10}
                    keyboardType='default'
                    autoCapitalize='none'
                    autoCorrect={false}
                    maxLength={20}
                    value={searchInput}
                    onChangeText={(value) => searchCountry(value)}
                    returnKeyType='done'
                    // onSubmitEditing={() => { }}
                    //ref={(ref) => { this.searchRef = ref }}
                    underlineColorAndroid="transparent"
                />

            </View>

            <FlatList
                nestedScrollEnabled={true}
                data={country}
                horizontal={false}
                renderItem={({ item, index }) => renderItem(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7'
    },
    search: {
        borderRadius: 50,
        elevation: 2,
        zIndex: 1,
        backgroundColor: 'white',
        margin: 20,
        alignItems: 'center',
        flexDirection: 'row'
    },
    searchIcon: {
        width: 20,
        height: 20,
        right: 10,
        resizeMode: 'contain',
        marginLeft: 25
        //flex:1
    },
    item: {
        flex: 1,
        backgroundColor: 'white',
        minHeight: 60,
        padding: 15,
        alignItems: 'center',
        flexDirection: 'row',
        margin: 1.5
    }
});

export default SelectCountryCodeScreen;