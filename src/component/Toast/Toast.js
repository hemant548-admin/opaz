import React from 'react';

import Toast, { BaseToast } from 'react-native-toast-message';
import AppColor from '../../utils/AppColor';
import AppImage from '../../utils/AppImage';

const toastConfig = {
  success: ({ text1, ...rest }) => (
    <BaseToast
      {...rest}
      style={{ borderLeftColor: AppColor.appColor,borderLeftWidth:25 }}
      contentContainerStyle={{ paddingHorizontal: 15 }}
      text1Style={{
        fontSize: 15,
        //fontWeight: 'semibold'
      }}
      leadingIcon={AppImage.verified_icon}
      leadingIconStyle={{width:40,height:40,resizeMode:'contain'}}
      text1={text1}
      text2={null}
    />
  )
};

export default toastConfig;