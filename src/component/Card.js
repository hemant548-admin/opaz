import React, { useEffect, useRef, useState } from 'react';

import {
    View, Text, FlatList,
    Animated, Dimensions, ActivityIndicator, Image, TouchableOpacity, TextInput, StyleSheet
} from 'react-native';
import AppImage from '../utils/AppImage';

const Card = ({ item }) => {

    //console.log('item', item.item?item.item.item:'')
    const arr = item.item ? item.item.item : []
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', }}>
            <View style={{ width: '98%', alignItems: 'center' }}>
                <View style={Style.card}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={AppImage.profile2}
                            style={{ width: 70, height: 70, marginLeft: 5 }} />
                        <View style={{ marginTop: 20 }}>
                            <Text style={{}}>{arr.name}</Text>
                            <Text style={{ fontSize: 10, color: 'grey' }}>{arr.date}</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{ marginLeft: 10, marginRight: 10, marginBottom: 5, color: 'grey' }}>
                            {arr.description}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={AppImage.like}
                                style={{ width: 20, height: 20 }} />
                            <Text style={{ color: '#0bbdb1' }}>  382k</Text>
                        </View>
                        <Text style={{ color: '#0bbdb1' }}>452k<Text> Comments</Text></Text>
                    </View>
                    <View style={{
                        borderWidth: 1, borderColor: '#e4ede6',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 4.84,

                        //elevation: .1,
                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={AppImage.thumb_up}
                                    style={{ width: 20, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>Like</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={AppImage.Comment}
                                    style={{ width: 20, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>Comment</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={AppImage.share}
                                    style={{ width: 20, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>Share</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View>
                <View style={{ flexDirection: 'row', marginLeft: 5 }}>
                    <Image source={AppImage.profile_women}
                        style={{ width: 30, height: 30 }} />
                    <View style={{ width: '90%', backgroundColor: "#e1e6e2", justifyContent: 'center', borderRadius: 10, marginLeft: 5 }}>
                        <Text style={{ fontSize: 14, marginLeft: 10, marginTop: 5 }}>Jhonson</Text>
                        <Text style={{ fontSize: 10, marginLeft: 10, marginRight: 20, marginBottom: 7 }}> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                    </View>
                </View>
                <Text style={{ marginLeft: 45, fontSize: 11, color: '#0bbdb1' }}>Like</Text>
                <View style={{ flexDirection: 'row', marginTop: 10, }}>

                    {/* <Text onPress={()=>toggleDrawer()}>Main screen</Text>   */}
                    <Image source={AppImage.profile1}
                        style={{ width: 30, height: 30, marginLeft: 5 }} />
                    <TextInput
                        style={{
                            width: '89%', height: 35, marginLeft: 5, borderRadius: 25,
                            paddingHorizontal: 20, fontSize: 10, textAlignVertical: 'center', backgroundColor: "#e1e6e2"
                        }}
                        placeholder='Write something here ...'
                    />
                </View>
            </View>
        </View>
    )
}
const Style = StyleSheet.create({
    card: {
        width: '97%',
        backgroundColor: 'white',
        marginBottom: 5,
        borderWidth: 1,
        borderColor: '#e4ede6',
        marginTop: 5,
        borderRadius: 10,
        shadowColor: '#470000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
    }
})
export default Card;