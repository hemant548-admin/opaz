import React, { Fragment } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import AppColor from '../../utils/AppColor';
import AppImage from '../../utils/AppImage';
//import HeaderStyle from './HeaderStyle';

//import LinearGradient from 'react-native-linear-gradient';

const Header = props => {

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View>
            <TouchableOpacity onPress={props.goBack}>
              <Image source={AppImage.back_arrow_white}
                style={styles.img}
              />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.titleTxt}>{props.title}</Text>

          </View>
        </View>
        {props.edit &&
          <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}
            onPress={props.editPress}
          >
            <Image source={AppImage.edit_white}
              style={styles.profile}
            />
          </TouchableOpacity>
        }
        {props.isView &&
          <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20, width: 30, height: 30, borderRadius: 15, backgroundColor: 'white',alignSelf:'center' }}
          onPress={props.onDeliverypress}
          >
            <Image source={AppImage.viewdeleivery_icon}
              style={{width:18,height:18,resizeMode:'contain'}}
            />
          </TouchableOpacity>
        }
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#d5dbdb',
    backgroundColor: AppColor.appColor,
    elevation: 2,
    width: '100%',
    height: '8%',
    marginHorizontal: 10,
    alignSelf: 'center'


  },
  img: {
    width: 20,
    height: 19,
    margin: 10,
    resizeMode: 'contain',
    marginLeft: 15
  },
  titleTxt: {
    fontSize: 20,
    color: 'white',
    marginLeft: 10
  },
  profile: {
    width: 30,
    height: 35,
    resizeMode: 'contain'
  }

})