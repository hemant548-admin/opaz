import React, { Fragment } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import AppColor from '../../utils/AppColor';
import AppImage from '../../utils/AppImage';
//import HeaderStyle from './HeaderStyle';

//import LinearGradient from 'react-native-linear-gradient';

const HeaderComponent = props => {

  return (
    <View style={styles.container}>
      <View style={{flexDirection:'row',justifyContent:'space-between',flex:1}}>
      <View style={{flexDirection:'row',alignItems:'center'}}>
      <View>
        <TouchableOpacity onPress={props.handelDrawer}>
          <Image source={AppImage.menu}
            style={styles.img}
          />
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.titleTxt}>{props.title}</Text>

      </View>
      </View>
      {props.profileEnable &&
      <TouchableOpacity style={{alignItems:'center',justifyContent:'center',marginRight:20}}
      onPress={props.profilePress}
      >
          <Image source={AppImage.profile}
            style={styles.profile}          
          />
      </TouchableOpacity>
      }
      </View>
    </View>
  );
};

export default HeaderComponent;

const styles = StyleSheet.create({
  container: {
   // justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#d5dbdb',
    backgroundColor: 'white',
    elevation: 2,
    width: '100%',
    height: '8%',
    marginHorizontal: 10,
    alignSelf:'center'


  },
  img: {
    width: 30,
    height: 30,
    margin: 10
  },
  titleTxt:{
      fontSize:20,
      color:AppColor.appColor
  },
  profile:{
      width:35,
      height:35
  }

})