import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

class MapInput extends React.Component {

    render() {
        return (

            <GooglePlacesAutocomplete
                placeholder='Search'
                minLength={2} // minimum length of text to search
                autoFocus={true}
                returnKeyType={'search'} // Can be left out for default return key 
                listViewDisplayed={'auto'}    // true/false/undefined
                fetchDetails={true}
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    this.props.notifyChange(details.geometry.location);
                    console.log("autocpmlte ",data, details);
                }
                }

                query={{
                    key: 'AIzaSyCyqFylvGrVpqp2UakctuOFu1V6o_SmsW0',
                    language: 'en'
                }}

                nearbyPlacesAPI='GooglePlacesSearch'
                debounce={300}
                onFail={error => console.error(error)}
                styles={{width:'100%',height:100}}
            />
        );
    }
}
export default MapInput;