import React from 'react';
import {
  StyleSheet, View, ActivityIndicator,
} from 'react-native';
import AppColor from '../../utils/AppColor';
import Modal from 'react-native-modal';


const Loader = props => {

  const { loading
  } = props;

  return (
    <View style={{  }}>
      {loading ?
      <Modal isVisible={loading}>
        <View style={styles.modalBackground}>
          <ActivityIndicator
            size={60}
            // color={AppColor.headerBg}
            color={AppColor.appColor}
            animating={loading} />
        </View>
        </Modal>
        
        : null}
    </View>

  )
}
const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  }
});
export default Loader;
