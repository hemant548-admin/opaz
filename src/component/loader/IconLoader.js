import React from 'react';
import {
  StyleSheet, View, ActivityIndicator,
} from 'react-native';
import AppColor from '../../utils/AppColor';
import Modal from 'react-native-modal';
import LottieView from 'lottie-react-native';



const IconLoader = props => {

  const { loading
  } = props;

  return (
    <View style={{  }}>
      {loading ?
      <Modal isVisible={loading}>
        <View style={styles.modalBackground}>
        <LottieView source={require('../../assets/man_travel.json')} autoPlay loop 
        style={{width:'100%',height:200}}
        />
        </View>
        </Modal>
        
        : null}
    </View>

  )
}
const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  }
});
export default IconLoader;
