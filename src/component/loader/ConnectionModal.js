import React from 'react';
import {
    StyleSheet, View, ActivityIndicator, Text, Dimensions, Pressable
} from 'react-native';
import AppColor from '../../utils/AppColor';
import Modal from 'react-native-modal';
import LottieView from 'lottie-react-native';

const { width, height } = Dimensions.get('window');
const ConnectionModal = props => {

    const { connection
    } = props;

    return (
        <View style={{}}>
            <Modal isVisible={connection}
                onBackdropPress={props.backdrop}
            //deviceWidth={width}
            //deviceHeight={height}
            //style={{ marginLeft: 20,marginRight:20,marginTop:'30%',marginBottom:'30%' }}
            >
                <View style={styles.modalBackground}>
                    <View style={{ width: width * 0.8, height: height * 0.6, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderRadius: 7, flexDirection: 'column' }}>
                        <LottieView source={require('../../assets/no_internet_connection.json')} autoPlay loop
                            style={{ width: '100%', height: 200 }}
                        />
                        <Text style={{ margin: 10, textAlign: 'center', fontSize: 17 }}>Oops! Looks like your device is not connected to the Internet.</Text>
                        <Pressable style={{
                            alignItems: 'center', justifyContent: 'center', width: 80, height: 35,
                            backgroundColor: AppColor.appColor, marginTop: 20, borderRadius: 5
                        }} onPress={props.backdrop}>
                            <Text style={{ color: 'white' }}>Try Again</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>

    )
}
const styles = StyleSheet.create({
    modalBackground: {
        // backgroundColor:'yellow',
        alignItems: 'center',
        flexDirection: 'column',
        width: width * 0.8,
        alignSelf: 'center',
        borderRadius: 7
    }
});
export default ConnectionModal;
