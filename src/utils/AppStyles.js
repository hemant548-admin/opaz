import { StyleSheet } from 'react-native';


const AppStyles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: AppColors.backgroundColor,
  },
  imageIcon: {
    width: 25,
    height: 25,
    marginLeft: 5
  },
  containerWhite: {
    flex: 1,
    //backgroundColor: AppColors.colorWhite,
  },
  boldText: {
    //color: AppColors.colorBlack,
    //fontFamily: AppFonts.BoldFont,
    padding: 3,
    fontSize: 18,
  },
  greyText: {
    //color: AppColors.colorLightText,
    paddingTop: 3,
    //fontFamily: AppFonts.RegularFont,
  },
  normalText: {
    //color: AppColors.colorBlack,

    //fontFamily: AppFonts.RegularFont,
  },
  apiErrorStyle: {
    fontSize: 14,
    color: 'red',
    //fontFamily: AppFonts.RegularFont,
    //alignSelf: 'center',
    flex: 1,
    textAlign: 'right',
    padding: 10,
  },
  inputStyle: {
    borderRadius: 50,
    //backgroundColor: AppColors.colorWhite,
    elevation: 1,
    padding: 10,
    //fontFamily: AppFonts.RegularFont,
  },
  edocText: {
    textAlign: 'center',
    fontSize: 40,
    //color: AppColors.colorWhite,
    //fontFamily: AppFonts.BoldFont,
  },
  edocView: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  leftArrowWhite: {
    position: 'absolute',
    margin: 15,
    zIndex: 1,
  },
  topDots: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: 10,
  },
  arrowBottom: {
    width: 30,
    height: 20,
    left: 20,
    resizeMode: 'contain',
  },
  noDataText: {
    textAlign: 'center',
    color: 'red',
    marginTop: 50
  },
});

export default AppStyles;
