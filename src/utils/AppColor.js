
const AppColor = {
    appColor:'#007654',
    inputColor:'#039D70',
    inputlight_txtColor:'#76dbbf',
    input_placeholderColor:'#00B580'
  };
  
  export default AppColor;
  