const AppStrings = {
    TOKEN: 'token',
    HOTEL_NAME: 'hotel_name',
    FIRST_NAME:'first_name',
    LAST_Name:'last_name',
    WEB: 'web',
    USER_ID: 'user_id',
    ROLE: 'role',
    NAME: 'name',
    EMAIL: 'email',
    PHONE: 'phone',
    IMAGE: 'image',
    DEVICE_TOKEN: 'deviceToken',
    ADDRESS: 'address',
    PINCODE: 'pincode',
    CITY: 'city',
    STATE: 'state',
    SOCIAL_MEDIA: 'social_media',
    VERIFIED_STATUS: 'verified_status',
    BRANCH_NAME: 'branch_name',
    Gender: 'gender',
    DOB: 'date_of_birth',
    ANNIVERSARY_DATE: 'anniversary_date',
    AGE: 'age',
    BRANCH_ID: 'branch_id',
    EMPLOYEE_NAME:'employee_name',
    SERVICE_USERID:'service_userid',
    ROOM_ID:'room_id',

    something_went_wrong: "Something went wrong. Please try again later!"
};

export default AppStrings;
