const AppFonts = {
    bold: 'Roboto-Bold',
    light: 'Roboto-Light',
    medium: 'Roboto-Medium',
    regular: 'Roboto-Regular',
    thin: 'Roboto-Thin',
    algeria:'Algerian_Regular',
    slab:'RobotoSlab-Regular',
  };
  
  export default AppFonts;
  