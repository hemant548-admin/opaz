/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useEffect } from 'react'
import { View, Text, LogBox, SafeAreaView } from 'react-native'
import SplashScreen from 'react-native-splash-screen';
import MainRoutes from './src/Routes/MainRoutes';
import { Provider as StateProvider } from 'react-redux'
import store from './src/redux/store';
import AppStrings from './src/utils/AppStrings';
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-community/async-storage';

export default function App({ navigation }) {

  useEffect(() => {
    SplashScreen.hide();
    
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: async function (token) {
        console.log("TOKEN:", token);
        await AsyncStorage.setItem(AppStrings.DEVICE_TOKEN, token.token + "")
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);

        // process the notification here

        // required on iOS only 
        if (Platform.OS === 'ios') {
          notification.finish(PushNotificationIOS.FetchResult.NoData);
          //this.updateCount(notification);
        }
        handleNotification(notification);
      },
      // Android only
      senderID: "303273891964",
      // iOS only
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
      popInitialNotification: true,
      requestPermissions: true,
      contentAvailable: true
    });


  }, []);
  const handleNotification = async (notification) => {



    if (!notification.foreground) {

      //alert('success if');

    } else {
      //alert('success else')
    }
  }


  return (


    <StateProvider store={store}>
      <SafeAreaProvider>
        <SafeAreaView style={{ flex: 1 }}>
          <MainRoutes />
        </SafeAreaView>
      </SafeAreaProvider>
    </StateProvider>

  )
}

LogBox.ignoreAllLogs()