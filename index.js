/**
 * @format
 */
 import React from 'react';
 import { AppRegistry, Platform } from 'react-native';
 import App from './App';
 import { name as appName } from './app.json';
 import { useDispatch,useSelector } from 'react-redux'
 import AsyncStorage from '@react-native-community/async-storage';
 import messaging from '@react-native-firebase/messaging';
 import { EventRegister } from 'react-native-event-listeners'
 //import PushNotification from "react-native-push-notification";
//import PushNotificationIOS from "@react-native-community/push-notification-ios";

 
 //import AppStrings from './src/constants/AppStrings';
  
 const onMessageReceived = async (notification, value) => {
 
     console.log('onMessageReceived', JSON.stringify(notification));
    //  PushNotification.localNotification({
    //     /* Android Only Properties */
    //     ticker: 'My Notification Ticker', // (optional)
    //     autoCancel: true, // (optional) default: true
    //     //largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
    //     //smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
    //     bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
    //     subText: 'This is a subText', // (optional) default: none
    //     ongoing: false, // (optional) set whether this is an "ongoing" notification
    //     ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear)
    //     alertAction: 'view', // (optional) default: view
    //     /* iOS and Android properties */
    //     title: 'My Notification Title', // (optional)
    //     message: notification.body, // (required)
    //     soundName: 'cuccu', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
    //     number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    //     actions: '["Yes", "No"]', // (Android only) See the doc for notification actions to know more
    //     foreground: true,
    //   });
    
    //  const data = await AsyncStorage.getItem(AppStrings.NOTI_COUNT, null);
 
    //  const type = notification.data ? notification.data.type : '';
 
    //  if (data) {
    //      let newData = JSON.parse(data);
    //      if (type === 'booking_canceled' || type === 'request_finish_ride'
    //          || type === 'ride_extended' || type === 'new_booking')
    //          newData.booking = parseInt(newData.booking) + 1;
    //      else if (type === 'charging_request' || type === 'charging_cancelled')
    //          newData.charging = parseInt(newData.charging) + 1;
    //      await AsyncStorage.setItem(AppStrings.NOTI_COUNT, JSON.stringify(newData));
 
    //  } else {
    //      let newData = { booking: 0, charging: 0 };
    //      if (type === 'booking_canceled' || type === 'request_finish_ride'
    //          || type === 'ride_extended' || type === 'new_booking')
    //          newData.booking = 1;
    //      else if (type === 'charging_request' || type === 'charging_cancelled')
    //          newData.charging = 1;
    //      await AsyncStorage.setItem(AppStrings.NOTI_COUNT, JSON.stringify(newData));
    //  }
 
     if (value)
         EventRegister.emit('NotificationReceived');
 
 }
 
 
 try {
     if (Platform.OS === 'android') {
         messaging().setBackgroundMessageHandler(remoteMessage => onMessageReceived(remoteMessage, false));
         messaging().onMessage(remoteMessage => onMessageReceived(remoteMessage, true));
     }
 } catch (e) {
     console.log('error setting message listener', e);
 }
 
 const HeadlessCheck = ({ isHeadless }) => {
     if (isHeadless) {
         // App has been launched in the background by iOS, ignore
         return null;
     }
     return <App />;
 }
 
 AppRegistry.registerComponent(appName, () => HeadlessCheck);